<?php
namespace PHPMaker2019\inventaris_assets;

// Session
if (session_status() !== PHP_SESSION_ACTIVE)
	session_start(); // Init session data

// Output buffering
ob_start(); 

// Autoload
include_once "autoload.php";
?>
<?php

// Write header
WriteHeader(FALSE);

// Create page object
$karyawan_view = new karyawan_view();

// Run the page
$karyawan_view->run();

// Setup login status
SetupLoginStatus();
SetClientVar("login", LoginStatus());

// Global Page Rendering event (in userfn*.php)
Page_Rendering();

// Page Rendering event
$karyawan_view->Page_Render();
?>
<?php include_once "header.php" ?>
<?php if (!$karyawan->isExport()) { ?>
<script>

// Form object
currentPageID = ew.PAGE_ID = "view";
var fkaryawanview = currentForm = new ew.Form("fkaryawanview", "view");

// Form_CustomValidate event
fkaryawanview.Form_CustomValidate = function(fobj) { // DO NOT CHANGE THIS LINE!

	// Your custom validation code here, return false if invalid.
	return true;
}

// Use JavaScript validation or not
fkaryawanview.validateRequired = <?php echo json_encode(CLIENT_VALIDATE) ?>;

// Dynamic selection lists
fkaryawanview.lists["x_created_by"] = <?php echo $karyawan_view->created_by->Lookup->toClientList() ?>;
fkaryawanview.lists["x_created_by"].options = <?php echo JsonEncode($karyawan_view->created_by->lookupOptions()) ?>;

// Form object for search
</script>
<script>

// Write your client script here, no need to add script tags.
</script>
<?php } ?>
<?php if (!$karyawan->isExport()) { ?>
<div class="btn-toolbar ew-toolbar">
<?php $karyawan_view->ExportOptions->render("body") ?>
<?php $karyawan_view->OtherOptions->render("body") ?>
<div class="clearfix"></div>
</div>
<?php } ?>
<?php $karyawan_view->showPageHeader(); ?>
<?php
$karyawan_view->showMessage();
?>
<form name="fkaryawanview" id="fkaryawanview" class="form-inline ew-form ew-view-form" action="<?php echo CurrentPageName() ?>" method="post">
<?php if ($karyawan_view->CheckToken) { ?>
<input type="hidden" name="<?php echo TOKEN_NAME ?>" value="<?php echo $karyawan_view->Token ?>">
<?php } ?>
<input type="hidden" name="t" value="karyawan">
<input type="hidden" name="modal" value="<?php echo (int)$karyawan_view->IsModal ?>">
<table class="table table-striped table-sm ew-view-table">
<?php if ($karyawan->nama_karyawan->Visible) { // nama_karyawan ?>
	<tr id="r_nama_karyawan">
		<td class="<?php echo $karyawan_view->TableLeftColumnClass ?>"><span id="elh_karyawan_nama_karyawan"><?php echo $karyawan->nama_karyawan->caption() ?></span></td>
		<td data-name="nama_karyawan"<?php echo $karyawan->nama_karyawan->cellAttributes() ?>>
<span id="el_karyawan_nama_karyawan">
<span<?php echo $karyawan->nama_karyawan->viewAttributes() ?>>
<?php echo $karyawan->nama_karyawan->getViewValue() ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($karyawan->nik->Visible) { // nik ?>
	<tr id="r_nik">
		<td class="<?php echo $karyawan_view->TableLeftColumnClass ?>"><span id="elh_karyawan_nik"><?php echo $karyawan->nik->caption() ?></span></td>
		<td data-name="nik"<?php echo $karyawan->nik->cellAttributes() ?>>
<span id="el_karyawan_nik">
<span<?php echo $karyawan->nik->viewAttributes() ?>>
<?php echo $karyawan->nik->getViewValue() ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($karyawan->divisi->Visible) { // divisi ?>
	<tr id="r_divisi">
		<td class="<?php echo $karyawan_view->TableLeftColumnClass ?>"><span id="elh_karyawan_divisi"><?php echo $karyawan->divisi->caption() ?></span></td>
		<td data-name="divisi"<?php echo $karyawan->divisi->cellAttributes() ?>>
<span id="el_karyawan_divisi">
<span<?php echo $karyawan->divisi->viewAttributes() ?>>
<?php echo $karyawan->divisi->getViewValue() ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($karyawan->jabatan->Visible) { // jabatan ?>
	<tr id="r_jabatan">
		<td class="<?php echo $karyawan_view->TableLeftColumnClass ?>"><span id="elh_karyawan_jabatan"><?php echo $karyawan->jabatan->caption() ?></span></td>
		<td data-name="jabatan"<?php echo $karyawan->jabatan->cellAttributes() ?>>
<span id="el_karyawan_jabatan">
<span<?php echo $karyawan->jabatan->viewAttributes() ?>>
<?php echo $karyawan->jabatan->getViewValue() ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($karyawan->atasan_langsung->Visible) { // atasan_langsung ?>
	<tr id="r_atasan_langsung">
		<td class="<?php echo $karyawan_view->TableLeftColumnClass ?>"><span id="elh_karyawan_atasan_langsung"><?php echo $karyawan->atasan_langsung->caption() ?></span></td>
		<td data-name="atasan_langsung"<?php echo $karyawan->atasan_langsung->cellAttributes() ?>>
<span id="el_karyawan_atasan_langsung">
<span<?php echo $karyawan->atasan_langsung->viewAttributes() ?>>
<?php echo $karyawan->atasan_langsung->getViewValue() ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($karyawan->nomor_telepon->Visible) { // nomor_telepon ?>
	<tr id="r_nomor_telepon">
		<td class="<?php echo $karyawan_view->TableLeftColumnClass ?>"><span id="elh_karyawan_nomor_telepon"><?php echo $karyawan->nomor_telepon->caption() ?></span></td>
		<td data-name="nomor_telepon"<?php echo $karyawan->nomor_telepon->cellAttributes() ?>>
<span id="el_karyawan_nomor_telepon">
<span<?php echo $karyawan->nomor_telepon->viewAttributes() ?>>
<?php echo $karyawan->nomor_telepon->getViewValue() ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($karyawan->_email->Visible) { // email ?>
	<tr id="r__email">
		<td class="<?php echo $karyawan_view->TableLeftColumnClass ?>"><span id="elh_karyawan__email"><?php echo $karyawan->_email->caption() ?></span></td>
		<td data-name="_email"<?php echo $karyawan->_email->cellAttributes() ?>>
<span id="el_karyawan__email">
<span<?php echo $karyawan->_email->viewAttributes() ?>>
<?php echo $karyawan->_email->getViewValue() ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($karyawan->created_by->Visible) { // created_by ?>
	<tr id="r_created_by">
		<td class="<?php echo $karyawan_view->TableLeftColumnClass ?>"><span id="elh_karyawan_created_by"><?php echo $karyawan->created_by->caption() ?></span></td>
		<td data-name="created_by"<?php echo $karyawan->created_by->cellAttributes() ?>>
<span id="el_karyawan_created_by">
<span<?php echo $karyawan->created_by->viewAttributes() ?>>
<?php echo $karyawan->created_by->getViewValue() ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($karyawan->created_date->Visible) { // created_date ?>
	<tr id="r_created_date">
		<td class="<?php echo $karyawan_view->TableLeftColumnClass ?>"><span id="elh_karyawan_created_date"><?php echo $karyawan->created_date->caption() ?></span></td>
		<td data-name="created_date"<?php echo $karyawan->created_date->cellAttributes() ?>>
<span id="el_karyawan_created_date">
<span<?php echo $karyawan->created_date->viewAttributes() ?>>
<?php echo $karyawan->created_date->getViewValue() ?></span>
</span>
</td>
	</tr>
<?php } ?>
</table>
</form>
<?php
$karyawan_view->showPageFooter();
if (DEBUG_ENABLED)
	echo GetDebugMessage();
?>
<?php if (!$karyawan->isExport()) { ?>
<script>

// Write your table-specific startup script here
// document.write("page loaded");

</script>
<?php } ?>
<?php include_once "footer.php" ?>
<?php
$karyawan_view->terminate();
?>