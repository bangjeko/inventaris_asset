<?php
namespace PHPMaker2019\inventaris_assets;

// Session
if (session_status() !== PHP_SESSION_ACTIVE)
	session_start(); // Init session data

// Output buffering
ob_start(); 

// Autoload
include_once "autoload.php";
?>
<?php

// Write header
WriteHeader(FALSE);

// Create page object
$transaksi_peminjaman_delete = new transaksi_peminjaman_delete();

// Run the page
$transaksi_peminjaman_delete->run();

// Setup login status
SetupLoginStatus();
SetClientVar("login", LoginStatus());

// Global Page Rendering event (in userfn*.php)
Page_Rendering();

// Page Rendering event
$transaksi_peminjaman_delete->Page_Render();
?>
<?php include_once "header.php" ?>
<script>

// Form object
currentPageID = ew.PAGE_ID = "delete";
var ftransaksi_peminjamandelete = currentForm = new ew.Form("ftransaksi_peminjamandelete", "delete");

// Form_CustomValidate event
ftransaksi_peminjamandelete.Form_CustomValidate = function(fobj) { // DO NOT CHANGE THIS LINE!

	// Your custom validation code here, return false if invalid.
	return true;
}

// Use JavaScript validation or not
ftransaksi_peminjamandelete.validateRequired = <?php echo json_encode(CLIENT_VALIDATE) ?>;

// Dynamic selection lists
ftransaksi_peminjamandelete.lists["x_id_karyawan"] = <?php echo $transaksi_peminjaman_delete->id_karyawan->Lookup->toClientList() ?>;
ftransaksi_peminjamandelete.lists["x_id_karyawan"].options = <?php echo JsonEncode($transaksi_peminjaman_delete->id_karyawan->lookupOptions()) ?>;
ftransaksi_peminjamandelete.lists["x_id_asset"] = <?php echo $transaksi_peminjaman_delete->id_asset->Lookup->toClientList() ?>;
ftransaksi_peminjamandelete.lists["x_id_asset"].options = <?php echo JsonEncode($transaksi_peminjaman_delete->id_asset->lookupOptions()) ?>;
ftransaksi_peminjamandelete.lists["x_created_by"] = <?php echo $transaksi_peminjaman_delete->created_by->Lookup->toClientList() ?>;
ftransaksi_peminjamandelete.lists["x_created_by"].options = <?php echo JsonEncode($transaksi_peminjaman_delete->created_by->lookupOptions()) ?>;

// Form object for search
</script>
<script>

// Write your client script here, no need to add script tags.
</script>
<?php $transaksi_peminjaman_delete->showPageHeader(); ?>
<?php
$transaksi_peminjaman_delete->showMessage();
?>
<form name="ftransaksi_peminjamandelete" id="ftransaksi_peminjamandelete" class="form-inline ew-form ew-delete-form" action="<?php echo CurrentPageName() ?>" method="post">
<?php if ($transaksi_peminjaman_delete->CheckToken) { ?>
<input type="hidden" name="<?php echo TOKEN_NAME ?>" value="<?php echo $transaksi_peminjaman_delete->Token ?>">
<?php } ?>
<input type="hidden" name="t" value="transaksi_peminjaman">
<input type="hidden" name="action" id="action" value="delete">
<?php foreach ($transaksi_peminjaman_delete->RecKeys as $key) { ?>
<?php $keyvalue = is_array($key) ? implode($COMPOSITE_KEY_SEPARATOR, $key) : $key; ?>
<input type="hidden" name="key_m[]" value="<?php echo HtmlEncode($keyvalue) ?>">
<?php } ?>
<div class="card ew-card ew-grid">
<div class="<?php if (IsResponsiveLayout()) { ?>table-responsive <?php } ?>card-body ew-grid-middle-panel">
<table class="table ew-table">
	<thead>
	<tr class="ew-table-header">
<?php if ($transaksi_peminjaman->id_karyawan->Visible) { // id_karyawan ?>
		<th class="<?php echo $transaksi_peminjaman->id_karyawan->headerCellClass() ?>"><span id="elh_transaksi_peminjaman_id_karyawan" class="transaksi_peminjaman_id_karyawan"><?php echo $transaksi_peminjaman->id_karyawan->caption() ?></span></th>
<?php } ?>
<?php if ($transaksi_peminjaman->id_asset->Visible) { // id_asset ?>
		<th class="<?php echo $transaksi_peminjaman->id_asset->headerCellClass() ?>"><span id="elh_transaksi_peminjaman_id_asset" class="transaksi_peminjaman_id_asset"><?php echo $transaksi_peminjaman->id_asset->caption() ?></span></th>
<?php } ?>
<?php if ($transaksi_peminjaman->tanggal_pinjam->Visible) { // tanggal_pinjam ?>
		<th class="<?php echo $transaksi_peminjaman->tanggal_pinjam->headerCellClass() ?>"><span id="elh_transaksi_peminjaman_tanggal_pinjam" class="transaksi_peminjaman_tanggal_pinjam"><?php echo $transaksi_peminjaman->tanggal_pinjam->caption() ?></span></th>
<?php } ?>
<?php if ($transaksi_peminjaman->tanggal_kembali->Visible) { // tanggal_kembali ?>
		<th class="<?php echo $transaksi_peminjaman->tanggal_kembali->headerCellClass() ?>"><span id="elh_transaksi_peminjaman_tanggal_kembali" class="transaksi_peminjaman_tanggal_kembali"><?php echo $transaksi_peminjaman->tanggal_kembali->caption() ?></span></th>
<?php } ?>
<?php if ($transaksi_peminjaman->keterangan->Visible) { // keterangan ?>
		<th class="<?php echo $transaksi_peminjaman->keterangan->headerCellClass() ?>"><span id="elh_transaksi_peminjaman_keterangan" class="transaksi_peminjaman_keterangan"><?php echo $transaksi_peminjaman->keterangan->caption() ?></span></th>
<?php } ?>
<?php if ($transaksi_peminjaman->created_by->Visible) { // created_by ?>
		<th class="<?php echo $transaksi_peminjaman->created_by->headerCellClass() ?>"><span id="elh_transaksi_peminjaman_created_by" class="transaksi_peminjaman_created_by"><?php echo $transaksi_peminjaman->created_by->caption() ?></span></th>
<?php } ?>
<?php if ($transaksi_peminjaman->created_date->Visible) { // created_date ?>
		<th class="<?php echo $transaksi_peminjaman->created_date->headerCellClass() ?>"><span id="elh_transaksi_peminjaman_created_date" class="transaksi_peminjaman_created_date"><?php echo $transaksi_peminjaman->created_date->caption() ?></span></th>
<?php } ?>
<?php if ($transaksi_peminjaman->cetak->Visible) { // cetak ?>
		<th class="<?php echo $transaksi_peminjaman->cetak->headerCellClass() ?>"><span id="elh_transaksi_peminjaman_cetak" class="transaksi_peminjaman_cetak"><?php echo $transaksi_peminjaman->cetak->caption() ?></span></th>
<?php } ?>
	</tr>
	</thead>
	<tbody>
<?php
$transaksi_peminjaman_delete->RecCnt = 0;
$i = 0;
while (!$transaksi_peminjaman_delete->Recordset->EOF) {
	$transaksi_peminjaman_delete->RecCnt++;
	$transaksi_peminjaman_delete->RowCnt++;

	// Set row properties
	$transaksi_peminjaman->resetAttributes();
	$transaksi_peminjaman->RowType = ROWTYPE_VIEW; // View

	// Get the field contents
	$transaksi_peminjaman_delete->loadRowValues($transaksi_peminjaman_delete->Recordset);

	// Render row
	$transaksi_peminjaman_delete->renderRow();
?>
	<tr<?php echo $transaksi_peminjaman->rowAttributes() ?>>
<?php if ($transaksi_peminjaman->id_karyawan->Visible) { // id_karyawan ?>
		<td<?php echo $transaksi_peminjaman->id_karyawan->cellAttributes() ?>>
<span id="el<?php echo $transaksi_peminjaman_delete->RowCnt ?>_transaksi_peminjaman_id_karyawan" class="transaksi_peminjaman_id_karyawan">
<span<?php echo $transaksi_peminjaman->id_karyawan->viewAttributes() ?>>
<?php echo $transaksi_peminjaman->id_karyawan->getViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($transaksi_peminjaman->id_asset->Visible) { // id_asset ?>
		<td<?php echo $transaksi_peminjaman->id_asset->cellAttributes() ?>>
<span id="el<?php echo $transaksi_peminjaman_delete->RowCnt ?>_transaksi_peminjaman_id_asset" class="transaksi_peminjaman_id_asset">
<span<?php echo $transaksi_peminjaman->id_asset->viewAttributes() ?>>
<?php echo $transaksi_peminjaman->id_asset->getViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($transaksi_peminjaman->tanggal_pinjam->Visible) { // tanggal_pinjam ?>
		<td<?php echo $transaksi_peminjaman->tanggal_pinjam->cellAttributes() ?>>
<span id="el<?php echo $transaksi_peminjaman_delete->RowCnt ?>_transaksi_peminjaman_tanggal_pinjam" class="transaksi_peminjaman_tanggal_pinjam">
<span<?php echo $transaksi_peminjaman->tanggal_pinjam->viewAttributes() ?>>
<?php echo $transaksi_peminjaman->tanggal_pinjam->getViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($transaksi_peminjaman->tanggal_kembali->Visible) { // tanggal_kembali ?>
		<td<?php echo $transaksi_peminjaman->tanggal_kembali->cellAttributes() ?>>
<span id="el<?php echo $transaksi_peminjaman_delete->RowCnt ?>_transaksi_peminjaman_tanggal_kembali" class="transaksi_peminjaman_tanggal_kembali">
<span<?php echo $transaksi_peminjaman->tanggal_kembali->viewAttributes() ?>>
<?php echo $transaksi_peminjaman->tanggal_kembali->getViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($transaksi_peminjaman->keterangan->Visible) { // keterangan ?>
		<td<?php echo $transaksi_peminjaman->keterangan->cellAttributes() ?>>
<span id="el<?php echo $transaksi_peminjaman_delete->RowCnt ?>_transaksi_peminjaman_keterangan" class="transaksi_peminjaman_keterangan">
<span<?php echo $transaksi_peminjaman->keterangan->viewAttributes() ?>>
<?php echo $transaksi_peminjaman->keterangan->getViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($transaksi_peminjaman->created_by->Visible) { // created_by ?>
		<td<?php echo $transaksi_peminjaman->created_by->cellAttributes() ?>>
<span id="el<?php echo $transaksi_peminjaman_delete->RowCnt ?>_transaksi_peminjaman_created_by" class="transaksi_peminjaman_created_by">
<span<?php echo $transaksi_peminjaman->created_by->viewAttributes() ?>>
<?php echo $transaksi_peminjaman->created_by->getViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($transaksi_peminjaman->created_date->Visible) { // created_date ?>
		<td<?php echo $transaksi_peminjaman->created_date->cellAttributes() ?>>
<span id="el<?php echo $transaksi_peminjaman_delete->RowCnt ?>_transaksi_peminjaman_created_date" class="transaksi_peminjaman_created_date">
<span<?php echo $transaksi_peminjaman->created_date->viewAttributes() ?>>
<?php echo $transaksi_peminjaman->created_date->getViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($transaksi_peminjaman->cetak->Visible) { // cetak ?>
		<td<?php echo $transaksi_peminjaman->cetak->cellAttributes() ?>>
<span id="el<?php echo $transaksi_peminjaman_delete->RowCnt ?>_transaksi_peminjaman_cetak" class="transaksi_peminjaman_cetak">
<span<?php echo $transaksi_peminjaman->cetak->viewAttributes() ?>>
<?php echo $transaksi_peminjaman->cetak->getViewValue() ?></span>
</span>
</td>
<?php } ?>
	</tr>
<?php
	$transaksi_peminjaman_delete->Recordset->moveNext();
}
$transaksi_peminjaman_delete->Recordset->close();
?>
</tbody>
</table>
</div>
</div>
<div>
<button class="btn btn-primary ew-btn" name="btn-action" id="btn-action" type="submit"><?php echo $Language->phrase("DeleteBtn") ?></button>
<button class="btn btn-default ew-btn" name="btn-cancel" id="btn-cancel" type="button" data-href="<?php echo $transaksi_peminjaman_delete->getReturnUrl() ?>"><?php echo $Language->phrase("CancelBtn") ?></button>
</div>
</form>
<?php
$transaksi_peminjaman_delete->showPageFooter();
if (DEBUG_ENABLED)
	echo GetDebugMessage();
?>
<script>

// Write your table-specific startup script here
// document.write("page loaded");

</script>
<?php include_once "footer.php" ?>
<?php
$transaksi_peminjaman_delete->terminate();
?>