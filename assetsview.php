<?php
namespace PHPMaker2019\inventaris_assets;

// Session
if (session_status() !== PHP_SESSION_ACTIVE)
	session_start(); // Init session data

// Output buffering
ob_start(); 

// Autoload
include_once "autoload.php";
?>
<?php

// Write header
WriteHeader(FALSE);

// Create page object
$assets_view = new assets_view();

// Run the page
$assets_view->run();

// Setup login status
SetupLoginStatus();
SetClientVar("login", LoginStatus());

// Global Page Rendering event (in userfn*.php)
Page_Rendering();

// Page Rendering event
$assets_view->Page_Render();
?>
<?php include_once "header.php" ?>
<?php if (!$assets->isExport()) { ?>
<script>

// Form object
currentPageID = ew.PAGE_ID = "view";
var fassetsview = currentForm = new ew.Form("fassetsview", "view");

// Form_CustomValidate event
fassetsview.Form_CustomValidate = function(fobj) { // DO NOT CHANGE THIS LINE!

	// Your custom validation code here, return false if invalid.
	return true;
}

// Use JavaScript validation or not
fassetsview.validateRequired = <?php echo json_encode(CLIENT_VALIDATE) ?>;

// Multi-Page
fassetsview.multiPage = new ew.MultiPage("fassetsview");

// Dynamic selection lists
fassetsview.lists["x_jenis_asset"] = <?php echo $assets_view->jenis_asset->Lookup->toClientList() ?>;
fassetsview.lists["x_jenis_asset"].options = <?php echo JsonEncode($assets_view->jenis_asset->lookupOptions()) ?>;
fassetsview.lists["x_status"] = <?php echo $assets_view->status->Lookup->toClientList() ?>;
fassetsview.lists["x_status"].options = <?php echo JsonEncode($assets_view->status->options(FALSE, TRUE)) ?>;
fassetsview.lists["x_created_by"] = <?php echo $assets_view->created_by->Lookup->toClientList() ?>;
fassetsview.lists["x_created_by"].options = <?php echo JsonEncode($assets_view->created_by->lookupOptions()) ?>;
fassetsview.lists["x_current_asign"] = <?php echo $assets_view->current_asign->Lookup->toClientList() ?>;
fassetsview.lists["x_current_asign"].options = <?php echo JsonEncode($assets_view->current_asign->lookupOptions()) ?>;

// Form object for search
</script>
<script>

// Write your client script here, no need to add script tags.
</script>
<?php } ?>
<?php if (!$assets->isExport()) { ?>
<div class="btn-toolbar ew-toolbar">
<?php $assets_view->ExportOptions->render("body") ?>
<?php $assets_view->OtherOptions->render("body") ?>
<div class="clearfix"></div>
</div>
<?php } ?>
<?php $assets_view->showPageHeader(); ?>
<?php
$assets_view->showMessage();
?>
<form name="fassetsview" id="fassetsview" class="form-inline ew-form ew-view-form" action="<?php echo CurrentPageName() ?>" method="post">
<?php if ($assets_view->CheckToken) { ?>
<input type="hidden" name="<?php echo TOKEN_NAME ?>" value="<?php echo $assets_view->Token ?>">
<?php } ?>
<input type="hidden" name="t" value="assets">
<input type="hidden" name="modal" value="<?php echo (int)$assets_view->IsModal ?>">
<?php if (!$assets->isExport()) { ?>
<div class="ew-multi-page">
<div class="ew-nav-tabs" id="assets_view"><!-- multi-page tabs -->
	<ul class="<?php echo $assets_view->MultiPages->navStyle() ?>">
		<li class="nav-item"><a class="nav-link<?php echo $assets_view->MultiPages->pageStyle("1") ?>" href="#tab_assets1" data-toggle="tab"><?php echo $assets->pageCaption(1) ?></a></li>
		<li class="nav-item"><a class="nav-link<?php echo $assets_view->MultiPages->pageStyle("2") ?>" href="#tab_assets2" data-toggle="tab"><?php echo $assets->pageCaption(2) ?></a></li>
	</ul>
	<div class="tab-content">
<?php } ?>
<?php if (!$assets->isExport()) { ?>
		<div class="tab-pane<?php echo $assets_view->MultiPages->pageStyle("1") ?>" id="tab_assets1"><!-- multi-page .tab-pane -->
<?php } ?>
<table class="table table-striped table-sm ew-view-table">
<?php if ($assets->kode_asset->Visible) { // kode_asset ?>
	<tr id="r_kode_asset">
		<td class="<?php echo $assets_view->TableLeftColumnClass ?>"><span id="elh_assets_kode_asset"><?php echo $assets->kode_asset->caption() ?></span></td>
		<td data-name="kode_asset"<?php echo $assets->kode_asset->cellAttributes() ?>>
<span id="el_assets_kode_asset" data-page="1">
<span<?php echo $assets->kode_asset->viewAttributes() ?>>
<?php echo $assets->kode_asset->getViewValue() ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($assets->jenis_asset->Visible) { // jenis_asset ?>
	<tr id="r_jenis_asset">
		<td class="<?php echo $assets_view->TableLeftColumnClass ?>"><span id="elh_assets_jenis_asset"><?php echo $assets->jenis_asset->caption() ?></span></td>
		<td data-name="jenis_asset"<?php echo $assets->jenis_asset->cellAttributes() ?>>
<span id="el_assets_jenis_asset" data-page="1">
<span<?php echo $assets->jenis_asset->viewAttributes() ?>>
<?php echo $assets->jenis_asset->getViewValue() ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($assets->merk->Visible) { // merk ?>
	<tr id="r_merk">
		<td class="<?php echo $assets_view->TableLeftColumnClass ?>"><span id="elh_assets_merk"><?php echo $assets->merk->caption() ?></span></td>
		<td data-name="merk"<?php echo $assets->merk->cellAttributes() ?>>
<span id="el_assets_merk" data-page="1">
<span<?php echo $assets->merk->viewAttributes() ?>>
<?php echo $assets->merk->getViewValue() ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($assets->spesifikasi->Visible) { // spesifikasi ?>
	<tr id="r_spesifikasi">
		<td class="<?php echo $assets_view->TableLeftColumnClass ?>"><span id="elh_assets_spesifikasi"><?php echo $assets->spesifikasi->caption() ?></span></td>
		<td data-name="spesifikasi"<?php echo $assets->spesifikasi->cellAttributes() ?>>
<span id="el_assets_spesifikasi" data-page="1">
<span<?php echo $assets->spesifikasi->viewAttributes() ?>>
<?php echo $assets->spesifikasi->getViewValue() ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($assets->foto->Visible) { // foto ?>
	<tr id="r_foto">
		<td class="<?php echo $assets_view->TableLeftColumnClass ?>"><span id="elh_assets_foto"><?php echo $assets->foto->caption() ?></span></td>
		<td data-name="foto"<?php echo $assets->foto->cellAttributes() ?>>
<span id="el_assets_foto" data-page="1">
<span>
<?php echo GetFileViewTag($assets->foto, $assets->foto->getViewValue()) ?>
</span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($assets->kelengkapan_awal->Visible) { // kelengkapan_awal ?>
	<tr id="r_kelengkapan_awal">
		<td class="<?php echo $assets_view->TableLeftColumnClass ?>"><span id="elh_assets_kelengkapan_awal"><?php echo $assets->kelengkapan_awal->caption() ?></span></td>
		<td data-name="kelengkapan_awal"<?php echo $assets->kelengkapan_awal->cellAttributes() ?>>
<span id="el_assets_kelengkapan_awal" data-page="1">
<span<?php echo $assets->kelengkapan_awal->viewAttributes() ?>>
<?php echo $assets->kelengkapan_awal->getViewValue() ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($assets->keterangan_terbaru->Visible) { // keterangan_terbaru ?>
	<tr id="r_keterangan_terbaru">
		<td class="<?php echo $assets_view->TableLeftColumnClass ?>"><span id="elh_assets_keterangan_terbaru"><?php echo $assets->keterangan_terbaru->caption() ?></span></td>
		<td data-name="keterangan_terbaru"<?php echo $assets->keterangan_terbaru->cellAttributes() ?>>
<span id="el_assets_keterangan_terbaru" data-page="1">
<span<?php echo $assets->keterangan_terbaru->viewAttributes() ?>>
<?php echo $assets->keterangan_terbaru->getViewValue() ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($assets->status->Visible) { // status ?>
	<tr id="r_status">
		<td class="<?php echo $assets_view->TableLeftColumnClass ?>"><span id="elh_assets_status"><?php echo $assets->status->caption() ?></span></td>
		<td data-name="status"<?php echo $assets->status->cellAttributes() ?>>
<span id="el_assets_status" data-page="1">
<span<?php echo $assets->status->viewAttributes() ?>>
<?php echo $assets->status->getViewValue() ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($assets->created_by->Visible) { // created_by ?>
	<tr id="r_created_by">
		<td class="<?php echo $assets_view->TableLeftColumnClass ?>"><span id="elh_assets_created_by"><?php echo $assets->created_by->caption() ?></span></td>
		<td data-name="created_by"<?php echo $assets->created_by->cellAttributes() ?>>
<span id="el_assets_created_by" data-page="1">
<span<?php echo $assets->created_by->viewAttributes() ?>>
<?php echo $assets->created_by->getViewValue() ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($assets->created_date->Visible) { // created_date ?>
	<tr id="r_created_date">
		<td class="<?php echo $assets_view->TableLeftColumnClass ?>"><span id="elh_assets_created_date"><?php echo $assets->created_date->caption() ?></span></td>
		<td data-name="created_date"<?php echo $assets->created_date->cellAttributes() ?>>
<span id="el_assets_created_date" data-page="1">
<span<?php echo $assets->created_date->viewAttributes() ?>>
<?php echo $assets->created_date->getViewValue() ?></span>
</span>
</td>
	</tr>
<?php } ?>
</table>
<?php if (!$assets->isExport()) { ?>
		</div>
<?php } ?>
<?php if (!$assets->isExport()) { ?>
		<div class="tab-pane<?php echo $assets_view->MultiPages->pageStyle("2") ?>" id="tab_assets2"><!-- multi-page .tab-pane -->
<?php } ?>
<table class="table table-striped table-sm ew-view-table">
<?php if ($assets->current_asign->Visible) { // current_asign ?>
	<tr id="r_current_asign">
		<td class="<?php echo $assets_view->TableLeftColumnClass ?>"><span id="elh_assets_current_asign"><?php echo $assets->current_asign->caption() ?></span></td>
		<td data-name="current_asign"<?php echo $assets->current_asign->cellAttributes() ?>>
<span id="el_assets_current_asign" data-page="2">
<span<?php echo $assets->current_asign->viewAttributes() ?>>
<?php echo $assets->current_asign->getViewValue() ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($assets->assign_date->Visible) { // assign_date ?>
	<tr id="r_assign_date">
		<td class="<?php echo $assets_view->TableLeftColumnClass ?>"><span id="elh_assets_assign_date"><?php echo $assets->assign_date->caption() ?></span></td>
		<td data-name="assign_date"<?php echo $assets->assign_date->cellAttributes() ?>>
<span id="el_assets_assign_date" data-page="2">
<span<?php echo $assets->assign_date->viewAttributes() ?>>
<?php echo $assets->assign_date->getViewValue() ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($assets->back_date->Visible) { // back_date ?>
	<tr id="r_back_date">
		<td class="<?php echo $assets_view->TableLeftColumnClass ?>"><span id="elh_assets_back_date"><?php echo $assets->back_date->caption() ?></span></td>
		<td data-name="back_date"<?php echo $assets->back_date->cellAttributes() ?>>
<span id="el_assets_back_date" data-page="2">
<span<?php echo $assets->back_date->viewAttributes() ?>>
<?php echo $assets->back_date->getViewValue() ?></span>
</span>
</td>
	</tr>
<?php } ?>
</table>
<?php if (!$assets->isExport()) { ?>
		</div>
<?php } ?>
<?php if (!$assets->isExport()) { ?>
	</div>
</div>
</div>
<?php } ?>
</form>
<?php
$assets_view->showPageFooter();
if (DEBUG_ENABLED)
	echo GetDebugMessage();
?>
<?php if (!$assets->isExport()) { ?>
<script>

// Write your table-specific startup script here
// document.write("page loaded");

</script>
<?php } ?>
<?php include_once "footer.php" ?>
<?php
$assets_view->terminate();
?>