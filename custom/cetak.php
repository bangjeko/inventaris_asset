<?php
namespace PHPMaker2019\inventaris_assets;

$table_name = $_REQUEST["gos"];
$id = $_REQUEST["id"];

$get_data = get1row("SELECT a.*, b.kode_asset,b.jenis_asset, b.merk, b.spesifikasi, c.nama_lengkap, d.nama_karyawan, d.nik, d.divisi, d.jabatan, d.atasan_langsung, e.nama_jenis FROM ".$table_name." a 
	LEFT OUTER JOIN assets b ON a.id_asset = b.id 
	LEFT OUTER JOIN users c ON a.created_by = c.id
	LEFT OUTER JOIN karyawan d ON a.id_karyawan = d.id
	LEFT OUTER JOIN jenis_asset e ON b.jenis_asset = e.id
	WHERE a.id = '".$id."'");
$tanggal_pinjam = tgl_indo($get_data["tanggal_pinjam"]); 
$tahun_pinjam = date("Y",strtotime($get_data["tanggal_pinjam"]));
$kpinjam = str_replace("-", "", $get_data["tanggal_pinjam"]);
$id_pinjam = "P".$id.$kpinjam;
$tanggal_kembali = tgl_indo($get_data["tanggal_kembali"]); 
$tahun_kembali = date("Y",strtotime($get_data["tanggal_kembali"]));
$kkembali= str_replace("-", "", $get_data["tanggal_kembali"]);
$id_kembali = "K".$id.$kkembali;

?>
<style type="text/css">
.index1{
	padding-left: 50px;
	margin-right: 50px;
	text-align: justify;
}
.index2{
	padding-left: 60px;
}
.outer{
	border: 1px solid black;
}
</style>
<?php
include("./custom/".$table_name.".php");
?>