<div class="outer">
	<table cellspacing="0" border="0" width="100%">
		
		<tbody><tr>
			<td style="border-bottom: 2px solid #000000; " rowspan="5" height="122" align="center" valign="middle"><font color="#000000">PT Adyawinsa Telecomunication &amp; Electrical</font><br><img class="center" width="150" height="150" alt="" src="./custom/assets/logo_perusahaan.jpg" /></td>
			<td style="border-bottom: 2px solid #000000; border-left: 2px solid #000000; " colspan="3" rowspan="3" align="center" valign="middle"><font color="#000000">BERITA ACARA <br>SERAH TERIMA INVENTARIS ICT</font></td>
		</tr>
		<tr>
		</tr>
		<tr>
		</tr>
		<tr>
			<td style="border-right: 2px solid #000000" align="center" valign="middle"><font size="1" color="#000000">Tanggal Bulan</font></td>
			<td style="border-right: 2px solid #000000" align="center" valign="middle"><font size="1" color="#000000">No. Urut</font></td>
			<td align="center" valign="middle"><font size="1" color="#000000">Tahun</font></td>
		</tr>
		<tr>
			<td style="border-bottom: 2px solid #000000; border-right: 2px solid #000000" align="center" valign="middle"><font size="1" color="#000000"><?php echo $tanggal_pinjam;?></font></td>
			<td style="border-bottom: 2px solid #000000; border-right: 2px solid #000000" align="center" valign="middle" sdval="201910" sdnum="1033;"><font size="1" color="#000000"><?php echo $id_pinjam;?></font></td>
			<td style="border-bottom: 2px solid #000000; " align="center" valign="middle" sdval="2020" sdnum="1033;"><font size="1" color="#000000"><?php echo $tahun_pinjam;?></font></td>
		</tr>
	</tbody></table>
	<p>&nbsp;</p>
	<center><h3>PEMINJAMAN ASSET</h3></center>

	<div class="index1">
		<p>Dalam rangka menunjang kelancaran pelaksanaan tugas dan tanggung jawab karyawan PT ADYAWINSA TELECOMUNICATION &amp; ELECTRICAL maka pada hari ini telah diserah terimakan inventaris kantor dengan perangkat ICT milik</p>	
		<div class="index2">
			<table style="height: 79px; width: 302px;padding-left: 100px">
				<tbody>
					<tr style="height: 13px;">
						<td style="width: 500px; height: 13px;">Jenis Perangkat</td>
						<td style="width: 10px; height: 13px;">:</td>
						<td style="width: 304px; height: 13px;"><?php echo $get_data["nama_jenis"];?></td>
					</tr>
					<tr style="height: 13px;">
						<td style="width: 500px; height: 13px;">Merk / Type</td>
						<td style="width: 10px; height: 13px;">:</td>
						<td style="width: 304px; height: 13px;"><?php echo $get_data["merk"];?></td>
					</tr>
					<tr style="height: 13px;">
						<td style="width: 500px; height: 13px;">Asset Code&nbsp;</td>
						<td style="width: 10px; height: 13px;">:</td>
						<td style="width: 304px; height: 13px;"><?php echo $get_data["kode_asset"];?></td>
					</tr>
					<tr style="height: 13px;">
						<td style="width: 500px; height: 13px;vertical-align: top">Spesifikasi</td>
						<td style="width: 10px; height: 13px;vertical-align: top">:</td>
						<td style="width: 304px; height: 13px;vertical-align: top"><?php echo $get_data["spesifikasi"];?></td>
					</tr>
					<tr style="height: 13px;">
						<td style="width: 500px; height: 13px;vertical-align: top">Kelengkapan Asset</td>
						<td style="width: 10px; height: 13px;vertical-align: top">:</td>
						<td style="width: 304px; height: 13px;vertical-align: top"><?php echo $get_data["kelengkapan_asset"];?></td>
					</tr>
					<tr style="height: 13px;">
						<td style="width: 500px; height: 13px;vertical-align: top">Kondisi</td>
						<td style="width: 10px; height: 13px;vertical-align: top">:</td>
						<td style="width: 304px; height: 13px;vertical-align: top"><?php echo $get_data["keterangan"];?></td>
					</tr>
				</tbody>
			</table>
		</div>
		<hr>
		<div class="index2">
			<table style="height: 79px; width: 302px;padding-left: 100px">
				<tbody>
					<tr style="height: 13px;">
						<td style="width: 500px; height: 13px;">Nama Lengkap</td>
						<td style="width: 10px; height: 13px;">:</td>
						<td style="width: 304px; height: 13px;"><?php echo $get_data["nama_karyawan"];?></td>
					</tr>
					<tr style="height: 13px;">
						<td style="width: 500px; height: 13px;">NIK</td>
						<td style="width: 10px; height: 13px;">:</td>
						<td style="width: 304px; height: 13px;"><?php echo $get_data["nik"];?></td>
					</tr>
					<tr style="height: 13px;">
						<td style="width: 500px; height: 13px;">Divisi</td>
						<td style="width: 10px; height: 13px;">:</td>
						<td style="width: 304px; height: 13px;"><?php echo $get_data["divisi"];?></td>
					</tr>
					<tr style="height: 13px;">
						<td style="width: 500px; height: 13px;">Jabatan</td>
						<td style="width: 10px; height: 13px;">:</td>
						<td style="width: 304px; height: 13px;"><?php echo $get_data["jabatan"];?></td>
					</tr>
					<tr style="height: 13px;">
						<td style="width: 500px; height: 13px;">Atasan langsung</td>
						<td style="width: 10px; height: 13px;">:</td>
						<td style="width: 304px; height: 13px;"><?php echo $get_data["atasan_langsung"];?></td>
					</tr>
				</tbody>
			</table>
		</div>
		<br>
		<p>Setelah pengecekan dilakukan, perangkat tersebut dalam kondisi utuh dan dapat berfungsi serta dipergunakan sebagaimana mestinya, sehingga karyawan bersangkutan menyetujui dan mengesahkan pernyataan berikut :</p>
		<center><p><strong>SURAT PERNYATAAN</strong></p></center>
		<p>Saya yang bertanda tangan dibawah ini , menyatakan dengan sebenarnya bahwa saya telah menerima perangkat inventaris sesuai dengan data dalam surat ini maupun lampirannya. Selanjutnya terhadap perangkat inventaris yang telah saya terima tersebut, saya berjanji dan bersedia untuk :</p>
		<ol>
			<li>Merawat perangkat inventaris tersebut sebaik mungkin.</li>
			<li>Menanggung biaya perbaikan dan atau penggantian sparepart apabila terjadi kerusakan atau kehilangan perangkat tersebut.</li>
			<li>Mempergunakan perangkat tersebut sesuai dengan cara yang benar hanya untuk kepentingan tugas dan tanggung jawab kerja saya.</li>
			<li>Mematuhi aturan penggunaan software sesuai standart operasional.</li>
		</ol>
		<p>&nbsp;</p>
		<p>Demikian pernyataan ini saya tanda tangani sebagai bukti persetujuan dan janji saya bagi pihak &ndash; pihak yang berwenang serta yang berkepentingan, khusunya berkaitan dengan perangkat inventaris yang telah saya terima dan saya pinjam dari ADYAWINSA GROUP</p>
		<p>&nbsp;</p>
	</div>

	<table cellspacing="0" border="0" width="100%" border="0">

		<tbody><tr>
			<td height="41" align="center" valign="middle"><font color="#000000">Diserahkan Oleh</font></td>
			<td align="center" valign="middle"><font color="#000000">Diterima Oleh</font></td>
			<td align="center" valign="middle"><font color="#000000">Disetujui Oleh &amp; Disahkan Oleh</font></td>
			<td align="center" valign="middle"><font color="#000000">Mengetahui</font></td>
		</tr>
		<tr>
			<td rowspan="6" height="121" align="justify" valign="middle"><font color="#000000"><br></font></td>
			<td rowspan="6" align="justify" valign="middle"><font color="#000000"><br></font></td>
			<td rowspan="6" align="justify" valign="middle"><font color="#000000"><br></font></td>
			<td rowspan="6" align="justify" valign="middle"><font color="#000000"><br></font></td>
		</tr>
		<tr>
		</tr>
		<tr>
		</tr>
		<tr>
		</tr>
		<tr>
		</tr>
		<tr>
		</tr>
		<tr>
			<td height="41" align="center" valign="middle"><font color="#000000"><?php echo $get_data["nama_lengkap"];?></font></td>
			<td align="center" valign="middle"><font color="#000000"><?php echo $get_data["nama_karyawan"];?></font></td>
			<td align="center" valign="middle"><font color="#000000">Department / DI Head Ybs</font></td>
			<td align="center" valign="middle"><font color="#000000">ICT Department / Div Head</font></td>
		</tr>
	</tbody></table>
</div>
