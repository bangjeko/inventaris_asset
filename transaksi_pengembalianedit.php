<?php
namespace PHPMaker2019\inventaris_assets;

// Session
if (session_status() !== PHP_SESSION_ACTIVE)
	session_start(); // Init session data

// Output buffering
ob_start(); 

// Autoload
include_once "autoload.php";
?>
<?php

// Write header
WriteHeader(FALSE);

// Create page object
$transaksi_pengembalian_edit = new transaksi_pengembalian_edit();

// Run the page
$transaksi_pengembalian_edit->run();

// Setup login status
SetupLoginStatus();
SetClientVar("login", LoginStatus());

// Global Page Rendering event (in userfn*.php)
Page_Rendering();

// Page Rendering event
$transaksi_pengembalian_edit->Page_Render();
?>
<?php include_once "header.php" ?>
<script>

// Form object
currentPageID = ew.PAGE_ID = "edit";
var ftransaksi_pengembalianedit = currentForm = new ew.Form("ftransaksi_pengembalianedit", "edit");

// Validate form
ftransaksi_pengembalianedit.validate = function() {
	if (!this.validateRequired)
		return true; // Ignore validation
	var $ = jQuery, fobj = this.getForm(), $fobj = $(fobj);
	if ($fobj.find("#confirm").val() == "F")
		return true;
	var elm, felm, uelm, addcnt = 0;
	var $k = $fobj.find("#" + this.formKeyCountName); // Get key_count
	var rowcnt = ($k[0]) ? parseInt($k.val(), 10) : 1;
	var startcnt = (rowcnt == 0) ? 0 : 1; // Check rowcnt == 0 => Inline-Add
	var gridinsert = ["insert", "gridinsert"].includes($fobj.find("#action").val()) && $k[0];
	for (var i = startcnt; i <= rowcnt; i++) {
		var infix = ($k[0]) ? String(i) : "";
		$fobj.data("rowindex", infix);
		<?php if ($transaksi_pengembalian_edit->id->Required) { ?>
			elm = this.getElements("x" + infix + "_id");
			if (elm && !ew.isHidden(elm) && !ew.hasValue(elm))
				return this.onError(elm, "<?php echo JsEncode(str_replace("%s", $transaksi_pengembalian->id->caption(), $transaksi_pengembalian->id->RequiredErrorMessage)) ?>");
		<?php } ?>
		<?php if ($transaksi_pengembalian_edit->id_karyawan->Required) { ?>
			elm = this.getElements("x" + infix + "_id_karyawan");
			if (elm && !ew.isHidden(elm) && !ew.hasValue(elm))
				return this.onError(elm, "<?php echo JsEncode(str_replace("%s", $transaksi_pengembalian->id_karyawan->caption(), $transaksi_pengembalian->id_karyawan->RequiredErrorMessage)) ?>");
		<?php } ?>
		<?php if ($transaksi_pengembalian_edit->id_asset->Required) { ?>
			elm = this.getElements("x" + infix + "_id_asset");
			if (elm && !ew.isHidden(elm) && !ew.hasValue(elm))
				return this.onError(elm, "<?php echo JsEncode(str_replace("%s", $transaksi_pengembalian->id_asset->caption(), $transaksi_pengembalian->id_asset->RequiredErrorMessage)) ?>");
		<?php } ?>
		<?php if ($transaksi_pengembalian_edit->tanggal_kembali->Required) { ?>
			elm = this.getElements("x" + infix + "_tanggal_kembali");
			if (elm && !ew.isHidden(elm) && !ew.hasValue(elm))
				return this.onError(elm, "<?php echo JsEncode(str_replace("%s", $transaksi_pengembalian->tanggal_kembali->caption(), $transaksi_pengembalian->tanggal_kembali->RequiredErrorMessage)) ?>");
		<?php } ?>
			elm = this.getElements("x" + infix + "_tanggal_kembali");
			if (elm && !ew.checkDateDef(elm.value))
				return this.onError(elm, "<?php echo JsEncode($transaksi_pengembalian->tanggal_kembali->errorMessage()) ?>");
		<?php if ($transaksi_pengembalian_edit->keterangan->Required) { ?>
			elm = this.getElements("x" + infix + "_keterangan");
			if (elm && !ew.isHidden(elm) && !ew.hasValue(elm))
				return this.onError(elm, "<?php echo JsEncode(str_replace("%s", $transaksi_pengembalian->keterangan->caption(), $transaksi_pengembalian->keterangan->RequiredErrorMessage)) ?>");
		<?php } ?>
		<?php if ($transaksi_pengembalian_edit->created_by->Required) { ?>
			elm = this.getElements("x" + infix + "_created_by");
			if (elm && !ew.isHidden(elm) && !ew.hasValue(elm))
				return this.onError(elm, "<?php echo JsEncode(str_replace("%s", $transaksi_pengembalian->created_by->caption(), $transaksi_pengembalian->created_by->RequiredErrorMessage)) ?>");
		<?php } ?>
		<?php if ($transaksi_pengembalian_edit->created_date->Required) { ?>
			elm = this.getElements("x" + infix + "_created_date");
			if (elm && !ew.isHidden(elm) && !ew.hasValue(elm))
				return this.onError(elm, "<?php echo JsEncode(str_replace("%s", $transaksi_pengembalian->created_date->caption(), $transaksi_pengembalian->created_date->RequiredErrorMessage)) ?>");
		<?php } ?>

			// Fire Form_CustomValidate event
			if (!this.Form_CustomValidate(fobj))
				return false;
	}

	// Process detail forms
	var dfs = $fobj.find("input[name='detailpage']").get();
	for (var i = 0; i < dfs.length; i++) {
		var df = dfs[i], val = df.value;
		if (val && ew.forms[val])
			if (!ew.forms[val].validate())
				return false;
	}
	return true;
}

// Form_CustomValidate event
ftransaksi_pengembalianedit.Form_CustomValidate = function(fobj) { // DO NOT CHANGE THIS LINE!

	// Your custom validation code here, return false if invalid.
	return true;
}

// Use JavaScript validation or not
ftransaksi_pengembalianedit.validateRequired = <?php echo json_encode(CLIENT_VALIDATE) ?>;

// Dynamic selection lists
ftransaksi_pengembalianedit.lists["x_id_karyawan"] = <?php echo $transaksi_pengembalian_edit->id_karyawan->Lookup->toClientList() ?>;
ftransaksi_pengembalianedit.lists["x_id_karyawan"].options = <?php echo JsonEncode($transaksi_pengembalian_edit->id_karyawan->lookupOptions()) ?>;
ftransaksi_pengembalianedit.lists["x_id_asset"] = <?php echo $transaksi_pengembalian_edit->id_asset->Lookup->toClientList() ?>;
ftransaksi_pengembalianedit.lists["x_id_asset"].options = <?php echo JsonEncode($transaksi_pengembalian_edit->id_asset->lookupOptions()) ?>;
ftransaksi_pengembalianedit.lists["x_created_by"] = <?php echo $transaksi_pengembalian_edit->created_by->Lookup->toClientList() ?>;
ftransaksi_pengembalianedit.lists["x_created_by"].options = <?php echo JsonEncode($transaksi_pengembalian_edit->created_by->lookupOptions()) ?>;

// Form object for search
</script>
<script>

// Write your client script here, no need to add script tags.
</script>
<?php $transaksi_pengembalian_edit->showPageHeader(); ?>
<?php
$transaksi_pengembalian_edit->showMessage();
?>
<form name="ftransaksi_pengembalianedit" id="ftransaksi_pengembalianedit" class="<?php echo $transaksi_pengembalian_edit->FormClassName ?>" action="<?php echo CurrentPageName() ?>" method="post">
<?php if ($transaksi_pengembalian_edit->CheckToken) { ?>
<input type="hidden" name="<?php echo TOKEN_NAME ?>" value="<?php echo $transaksi_pengembalian_edit->Token ?>">
<?php } ?>
<input type="hidden" name="t" value="transaksi_pengembalian">
<input type="hidden" name="action" id="action" value="update">
<input type="hidden" name="modal" value="<?php echo (int)$transaksi_pengembalian_edit->IsModal ?>">
<div class="ew-edit-div"><!-- page* -->
<?php if ($transaksi_pengembalian->id->Visible) { // id ?>
	<div id="r_id" class="form-group row">
		<label id="elh_transaksi_pengembalian_id" class="<?php echo $transaksi_pengembalian_edit->LeftColumnClass ?>"><?php echo $transaksi_pengembalian->id->caption() ?><?php echo ($transaksi_pengembalian->id->Required) ? $Language->phrase("FieldRequiredIndicator") : "" ?></label>
		<div class="<?php echo $transaksi_pengembalian_edit->RightColumnClass ?>"><div<?php echo $transaksi_pengembalian->id->cellAttributes() ?>>
<span id="el_transaksi_pengembalian_id">
<span<?php echo $transaksi_pengembalian->id->viewAttributes() ?>>
<input type="text" readonly class="form-control-plaintext" value="<?php echo RemoveHtml($transaksi_pengembalian->id->EditValue) ?>"></span>
</span>
<input type="hidden" data-table="transaksi_pengembalian" data-field="x_id" name="x_id" id="x_id" value="<?php echo HtmlEncode($transaksi_pengembalian->id->CurrentValue) ?>">
<?php echo $transaksi_pengembalian->id->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($transaksi_pengembalian->id_karyawan->Visible) { // id_karyawan ?>
	<div id="r_id_karyawan" class="form-group row">
		<label id="elh_transaksi_pengembalian_id_karyawan" for="x_id_karyawan" class="<?php echo $transaksi_pengembalian_edit->LeftColumnClass ?>"><?php echo $transaksi_pengembalian->id_karyawan->caption() ?><?php echo ($transaksi_pengembalian->id_karyawan->Required) ? $Language->phrase("FieldRequiredIndicator") : "" ?></label>
		<div class="<?php echo $transaksi_pengembalian_edit->RightColumnClass ?>"><div<?php echo $transaksi_pengembalian->id_karyawan->cellAttributes() ?>>
<span id="el_transaksi_pengembalian_id_karyawan">
<div class="input-group ew-lookup-list">
	<div class="form-control ew-lookup-text" tabindex="-1" id="lu_x_id_karyawan"><?php echo strval($transaksi_pengembalian->id_karyawan->ViewValue) == "" ? $Language->phrase("PleaseSelect") : (REMOVE_XSS ? HtmlDecode($transaksi_pengembalian->id_karyawan->ViewValue) : $transaksi_pengembalian->id_karyawan->ViewValue) ?></div>
	<div class="input-group-append">
		<button type="button" title="<?php echo HtmlEncode(str_replace("%s", RemoveHtml($transaksi_pengembalian->id_karyawan->caption()), $Language->phrase("LookupLink", TRUE))) ?>" class="ew-lookup-btn btn btn-default"<?php echo (($transaksi_pengembalian->id_karyawan->ReadOnly || $transaksi_pengembalian->id_karyawan->Disabled) ? " disabled" : "")?> onclick="ew.modalLookupShow({lnk:this,el:'x_id_karyawan',m:0,n:10});"><i class="fa fa-search ew-icon"></i></button>
	</div>
</div>
<?php echo $transaksi_pengembalian->id_karyawan->Lookup->getParamTag("p_x_id_karyawan") ?>
<input type="hidden" data-table="transaksi_pengembalian" data-field="x_id_karyawan" data-multiple="0" data-lookup="1" data-value-separator="<?php echo $transaksi_pengembalian->id_karyawan->displayValueSeparatorAttribute() ?>" name="x_id_karyawan" id="x_id_karyawan" value="<?php echo $transaksi_pengembalian->id_karyawan->CurrentValue ?>"<?php echo $transaksi_pengembalian->id_karyawan->editAttributes() ?>>
</span>
<?php echo $transaksi_pengembalian->id_karyawan->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($transaksi_pengembalian->id_asset->Visible) { // id_asset ?>
	<div id="r_id_asset" class="form-group row">
		<label id="elh_transaksi_pengembalian_id_asset" for="x_id_asset" class="<?php echo $transaksi_pengembalian_edit->LeftColumnClass ?>"><?php echo $transaksi_pengembalian->id_asset->caption() ?><?php echo ($transaksi_pengembalian->id_asset->Required) ? $Language->phrase("FieldRequiredIndicator") : "" ?></label>
		<div class="<?php echo $transaksi_pengembalian_edit->RightColumnClass ?>"><div<?php echo $transaksi_pengembalian->id_asset->cellAttributes() ?>>
<span id="el_transaksi_pengembalian_id_asset">
<div class="input-group ew-lookup-list">
	<div class="form-control ew-lookup-text" tabindex="-1" id="lu_x_id_asset"><?php echo strval($transaksi_pengembalian->id_asset->ViewValue) == "" ? $Language->phrase("PleaseSelect") : (REMOVE_XSS ? HtmlDecode($transaksi_pengembalian->id_asset->ViewValue) : $transaksi_pengembalian->id_asset->ViewValue) ?></div>
	<div class="input-group-append">
		<button type="button" title="<?php echo HtmlEncode(str_replace("%s", RemoveHtml($transaksi_pengembalian->id_asset->caption()), $Language->phrase("LookupLink", TRUE))) ?>" class="ew-lookup-btn btn btn-default"<?php echo (($transaksi_pengembalian->id_asset->ReadOnly || $transaksi_pengembalian->id_asset->Disabled) ? " disabled" : "")?> onclick="ew.modalLookupShow({lnk:this,el:'x_id_asset',m:0,n:10});"><i class="fa fa-search ew-icon"></i></button>
	</div>
</div>
<?php echo $transaksi_pengembalian->id_asset->Lookup->getParamTag("p_x_id_asset") ?>
<input type="hidden" data-table="transaksi_pengembalian" data-field="x_id_asset" data-multiple="0" data-lookup="1" data-value-separator="<?php echo $transaksi_pengembalian->id_asset->displayValueSeparatorAttribute() ?>" name="x_id_asset" id="x_id_asset" value="<?php echo $transaksi_pengembalian->id_asset->CurrentValue ?>"<?php echo $transaksi_pengembalian->id_asset->editAttributes() ?>>
</span>
<?php echo $transaksi_pengembalian->id_asset->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($transaksi_pengembalian->tanggal_kembali->Visible) { // tanggal_kembali ?>
	<div id="r_tanggal_kembali" class="form-group row">
		<label id="elh_transaksi_pengembalian_tanggal_kembali" for="x_tanggal_kembali" class="<?php echo $transaksi_pengembalian_edit->LeftColumnClass ?>"><?php echo $transaksi_pengembalian->tanggal_kembali->caption() ?><?php echo ($transaksi_pengembalian->tanggal_kembali->Required) ? $Language->phrase("FieldRequiredIndicator") : "" ?></label>
		<div class="<?php echo $transaksi_pengembalian_edit->RightColumnClass ?>"><div<?php echo $transaksi_pengembalian->tanggal_kembali->cellAttributes() ?>>
<span id="el_transaksi_pengembalian_tanggal_kembali">
<input type="text" data-table="transaksi_pengembalian" data-field="x_tanggal_kembali" name="x_tanggal_kembali" id="x_tanggal_kembali" placeholder="<?php echo HtmlEncode($transaksi_pengembalian->tanggal_kembali->getPlaceHolder()) ?>" value="<?php echo $transaksi_pengembalian->tanggal_kembali->EditValue ?>"<?php echo $transaksi_pengembalian->tanggal_kembali->editAttributes() ?>>
<?php if (!$transaksi_pengembalian->tanggal_kembali->ReadOnly && !$transaksi_pengembalian->tanggal_kembali->Disabled && !isset($transaksi_pengembalian->tanggal_kembali->EditAttrs["readonly"]) && !isset($transaksi_pengembalian->tanggal_kembali->EditAttrs["disabled"])) { ?>
<script>
ew.createDateTimePicker("ftransaksi_pengembalianedit", "x_tanggal_kembali", {"ignoreReadonly":true,"useCurrent":false,"format":0});
</script>
<?php } ?>
</span>
<?php echo $transaksi_pengembalian->tanggal_kembali->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($transaksi_pengembalian->keterangan->Visible) { // keterangan ?>
	<div id="r_keterangan" class="form-group row">
		<label id="elh_transaksi_pengembalian_keterangan" for="x_keterangan" class="<?php echo $transaksi_pengembalian_edit->LeftColumnClass ?>"><?php echo $transaksi_pengembalian->keterangan->caption() ?><?php echo ($transaksi_pengembalian->keterangan->Required) ? $Language->phrase("FieldRequiredIndicator") : "" ?></label>
		<div class="<?php echo $transaksi_pengembalian_edit->RightColumnClass ?>"><div<?php echo $transaksi_pengembalian->keterangan->cellAttributes() ?>>
<span id="el_transaksi_pengembalian_keterangan">
<textarea data-table="transaksi_pengembalian" data-field="x_keterangan" name="x_keterangan" id="x_keterangan" cols="35" rows="4" placeholder="<?php echo HtmlEncode($transaksi_pengembalian->keterangan->getPlaceHolder()) ?>"<?php echo $transaksi_pengembalian->keterangan->editAttributes() ?>><?php echo $transaksi_pengembalian->keterangan->EditValue ?></textarea>
</span>
<?php echo $transaksi_pengembalian->keterangan->CustomMsg ?></div></div>
	</div>
<?php } ?>
</div><!-- /page* -->
<?php if (!$transaksi_pengembalian_edit->IsModal) { ?>
<div class="form-group row"><!-- buttons .form-group -->
	<div class="<?php echo $transaksi_pengembalian_edit->OffsetColumnClass ?>"><!-- buttons offset -->
<button class="btn btn-primary ew-btn" name="btn-action" id="btn-action" type="submit"><?php echo $Language->phrase("SaveBtn") ?></button>
<button class="btn btn-default ew-btn" name="btn-cancel" id="btn-cancel" type="button" data-href="<?php echo $transaksi_pengembalian_edit->getReturnUrl() ?>"><?php echo $Language->phrase("CancelBtn") ?></button>
	</div><!-- /buttons offset -->
</div><!-- /buttons .form-group -->
<?php } ?>
</form>
<?php
$transaksi_pengembalian_edit->showPageFooter();
if (DEBUG_ENABLED)
	echo GetDebugMessage();
?>
<script>

// Write your table-specific startup script here
// document.write("page loaded");

</script>
<?php include_once "footer.php" ?>
<?php
$transaksi_pengembalian_edit->terminate();
?>