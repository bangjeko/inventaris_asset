<?php
namespace PHPMaker2019\inventaris_assets;

// Session
if (session_status() !== PHP_SESSION_ACTIVE)
	session_start(); // Init session data

// Output buffering
ob_start(); 

// Autoload
include_once "autoload.php";
?>
<?php

// Write header
WriteHeader(FALSE);

// Create page object
$assets_add = new assets_add();

// Run the page
$assets_add->run();

// Setup login status
SetupLoginStatus();
SetClientVar("login", LoginStatus());

// Global Page Rendering event (in userfn*.php)
Page_Rendering();

// Page Rendering event
$assets_add->Page_Render();
?>
<?php include_once "header.php" ?>
<script>

// Form object
currentPageID = ew.PAGE_ID = "add";
var fassetsadd = currentForm = new ew.Form("fassetsadd", "add");

// Validate form
fassetsadd.validate = function() {
	if (!this.validateRequired)
		return true; // Ignore validation
	var $ = jQuery, fobj = this.getForm(), $fobj = $(fobj);
	if ($fobj.find("#confirm").val() == "F")
		return true;
	var elm, felm, uelm, addcnt = 0;
	var $k = $fobj.find("#" + this.formKeyCountName); // Get key_count
	var rowcnt = ($k[0]) ? parseInt($k.val(), 10) : 1;
	var startcnt = (rowcnt == 0) ? 0 : 1; // Check rowcnt == 0 => Inline-Add
	var gridinsert = ["insert", "gridinsert"].includes($fobj.find("#action").val()) && $k[0];
	for (var i = startcnt; i <= rowcnt; i++) {
		var infix = ($k[0]) ? String(i) : "";
		$fobj.data("rowindex", infix);
		<?php if ($assets_add->kode_asset->Required) { ?>
			elm = this.getElements("x" + infix + "_kode_asset");
			if (elm && !ew.isHidden(elm) && !ew.hasValue(elm))
				return this.onError(elm, "<?php echo JsEncode(str_replace("%s", $assets->kode_asset->caption(), $assets->kode_asset->RequiredErrorMessage)) ?>");
		<?php } ?>
		<?php if ($assets_add->jenis_asset->Required) { ?>
			elm = this.getElements("x" + infix + "_jenis_asset");
			if (elm && !ew.isHidden(elm) && !ew.hasValue(elm))
				return this.onError(elm, "<?php echo JsEncode(str_replace("%s", $assets->jenis_asset->caption(), $assets->jenis_asset->RequiredErrorMessage)) ?>");
		<?php } ?>
		<?php if ($assets_add->merk->Required) { ?>
			elm = this.getElements("x" + infix + "_merk");
			if (elm && !ew.isHidden(elm) && !ew.hasValue(elm))
				return this.onError(elm, "<?php echo JsEncode(str_replace("%s", $assets->merk->caption(), $assets->merk->RequiredErrorMessage)) ?>");
		<?php } ?>
		<?php if ($assets_add->spesifikasi->Required) { ?>
			elm = this.getElements("x" + infix + "_spesifikasi");
			if (elm && !ew.isHidden(elm) && !ew.hasValue(elm))
				return this.onError(elm, "<?php echo JsEncode(str_replace("%s", $assets->spesifikasi->caption(), $assets->spesifikasi->RequiredErrorMessage)) ?>");
		<?php } ?>
		<?php if ($assets_add->foto->Required) { ?>
			felm = this.getElements("x" + infix + "_foto");
			elm = this.getElements("fn_x" + infix + "_foto");
			if (felm && elm && !ew.hasValue(elm))
				return this.onError(felm, "<?php echo JsEncode(str_replace("%s", $assets->foto->caption(), $assets->foto->RequiredErrorMessage)) ?>");
		<?php } ?>
		<?php if ($assets_add->kelengkapan_awal->Required) { ?>
			elm = this.getElements("x" + infix + "_kelengkapan_awal");
			if (elm && !ew.isHidden(elm) && !ew.hasValue(elm))
				return this.onError(elm, "<?php echo JsEncode(str_replace("%s", $assets->kelengkapan_awal->caption(), $assets->kelengkapan_awal->RequiredErrorMessage)) ?>");
		<?php } ?>
		<?php if ($assets_add->keterangan_terbaru->Required) { ?>
			elm = this.getElements("x" + infix + "_keterangan_terbaru");
			if (elm && !ew.isHidden(elm) && !ew.hasValue(elm))
				return this.onError(elm, "<?php echo JsEncode(str_replace("%s", $assets->keterangan_terbaru->caption(), $assets->keterangan_terbaru->RequiredErrorMessage)) ?>");
		<?php } ?>
		<?php if ($assets_add->status->Required) { ?>
			elm = this.getElements("x" + infix + "_status");
			if (elm && !ew.isHidden(elm) && !ew.hasValue(elm))
				return this.onError(elm, "<?php echo JsEncode(str_replace("%s", $assets->status->caption(), $assets->status->RequiredErrorMessage)) ?>");
		<?php } ?>
		<?php if ($assets_add->created_by->Required) { ?>
			elm = this.getElements("x" + infix + "_created_by");
			if (elm && !ew.isHidden(elm) && !ew.hasValue(elm))
				return this.onError(elm, "<?php echo JsEncode(str_replace("%s", $assets->created_by->caption(), $assets->created_by->RequiredErrorMessage)) ?>");
		<?php } ?>
		<?php if ($assets_add->created_date->Required) { ?>
			elm = this.getElements("x" + infix + "_created_date");
			if (elm && !ew.isHidden(elm) && !ew.hasValue(elm))
				return this.onError(elm, "<?php echo JsEncode(str_replace("%s", $assets->created_date->caption(), $assets->created_date->RequiredErrorMessage)) ?>");
		<?php } ?>

			// Fire Form_CustomValidate event
			if (!this.Form_CustomValidate(fobj))
				return false;
	}

	// Process detail forms
	var dfs = $fobj.find("input[name='detailpage']").get();
	for (var i = 0; i < dfs.length; i++) {
		var df = dfs[i], val = df.value;
		if (val && ew.forms[val])
			if (!ew.forms[val].validate())
				return false;
	}
	return true;
}

// Form_CustomValidate event
fassetsadd.Form_CustomValidate = function(fobj) { // DO NOT CHANGE THIS LINE!

	// Your custom validation code here, return false if invalid.
	return true;
}

// Use JavaScript validation or not
fassetsadd.validateRequired = <?php echo json_encode(CLIENT_VALIDATE) ?>;

// Dynamic selection lists
fassetsadd.lists["x_jenis_asset"] = <?php echo $assets_add->jenis_asset->Lookup->toClientList() ?>;
fassetsadd.lists["x_jenis_asset"].options = <?php echo JsonEncode($assets_add->jenis_asset->lookupOptions()) ?>;
fassetsadd.lists["x_status"] = <?php echo $assets_add->status->Lookup->toClientList() ?>;
fassetsadd.lists["x_status"].options = <?php echo JsonEncode($assets_add->status->options(FALSE, TRUE)) ?>;
fassetsadd.lists["x_created_by"] = <?php echo $assets_add->created_by->Lookup->toClientList() ?>;
fassetsadd.lists["x_created_by"].options = <?php echo JsonEncode($assets_add->created_by->lookupOptions()) ?>;

// Form object for search
</script>
<script>

// Write your client script here, no need to add script tags.
</script>
<?php $assets_add->showPageHeader(); ?>
<?php
$assets_add->showMessage();
?>
<form name="fassetsadd" id="fassetsadd" class="<?php echo $assets_add->FormClassName ?>" action="<?php echo CurrentPageName() ?>" method="post">
<?php if ($assets_add->CheckToken) { ?>
<input type="hidden" name="<?php echo TOKEN_NAME ?>" value="<?php echo $assets_add->Token ?>">
<?php } ?>
<input type="hidden" name="t" value="assets">
<input type="hidden" name="action" id="action" value="insert">
<input type="hidden" name="modal" value="<?php echo (int)$assets_add->IsModal ?>">
<div class="ew-add-div"><!-- page* -->
<?php if ($assets->kode_asset->Visible) { // kode_asset ?>
	<div id="r_kode_asset" class="form-group row">
		<label id="elh_assets_kode_asset" for="x_kode_asset" class="<?php echo $assets_add->LeftColumnClass ?>"><?php echo $assets->kode_asset->caption() ?><?php echo ($assets->kode_asset->Required) ? $Language->phrase("FieldRequiredIndicator") : "" ?></label>
		<div class="<?php echo $assets_add->RightColumnClass ?>"><div<?php echo $assets->kode_asset->cellAttributes() ?>>
<span id="el_assets_kode_asset">
<input type="text" data-table="assets" data-field="x_kode_asset" name="x_kode_asset" id="x_kode_asset" size="30" maxlength="255" placeholder="<?php echo HtmlEncode($assets->kode_asset->getPlaceHolder()) ?>" value="<?php echo $assets->kode_asset->EditValue ?>"<?php echo $assets->kode_asset->editAttributes() ?>>
</span>
<?php echo $assets->kode_asset->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($assets->jenis_asset->Visible) { // jenis_asset ?>
	<div id="r_jenis_asset" class="form-group row">
		<label id="elh_assets_jenis_asset" for="x_jenis_asset" class="<?php echo $assets_add->LeftColumnClass ?>"><?php echo $assets->jenis_asset->caption() ?><?php echo ($assets->jenis_asset->Required) ? $Language->phrase("FieldRequiredIndicator") : "" ?></label>
		<div class="<?php echo $assets_add->RightColumnClass ?>"><div<?php echo $assets->jenis_asset->cellAttributes() ?>>
<span id="el_assets_jenis_asset">
<div class="input-group">
	<select class="custom-select ew-custom-select" data-table="assets" data-field="x_jenis_asset" data-value-separator="<?php echo $assets->jenis_asset->displayValueSeparatorAttribute() ?>" id="x_jenis_asset" name="x_jenis_asset"<?php echo $assets->jenis_asset->editAttributes() ?>>
		<?php echo $assets->jenis_asset->selectOptionListHtml("x_jenis_asset") ?>
	</select>
</div>
<?php echo $assets->jenis_asset->Lookup->getParamTag("p_x_jenis_asset") ?>
</span>
<?php echo $assets->jenis_asset->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($assets->merk->Visible) { // merk ?>
	<div id="r_merk" class="form-group row">
		<label id="elh_assets_merk" for="x_merk" class="<?php echo $assets_add->LeftColumnClass ?>"><?php echo $assets->merk->caption() ?><?php echo ($assets->merk->Required) ? $Language->phrase("FieldRequiredIndicator") : "" ?></label>
		<div class="<?php echo $assets_add->RightColumnClass ?>"><div<?php echo $assets->merk->cellAttributes() ?>>
<span id="el_assets_merk">
<input type="text" data-table="assets" data-field="x_merk" name="x_merk" id="x_merk" size="30" maxlength="255" placeholder="<?php echo HtmlEncode($assets->merk->getPlaceHolder()) ?>" value="<?php echo $assets->merk->EditValue ?>"<?php echo $assets->merk->editAttributes() ?>>
</span>
<?php echo $assets->merk->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($assets->spesifikasi->Visible) { // spesifikasi ?>
	<div id="r_spesifikasi" class="form-group row">
		<label id="elh_assets_spesifikasi" for="x_spesifikasi" class="<?php echo $assets_add->LeftColumnClass ?>"><?php echo $assets->spesifikasi->caption() ?><?php echo ($assets->spesifikasi->Required) ? $Language->phrase("FieldRequiredIndicator") : "" ?></label>
		<div class="<?php echo $assets_add->RightColumnClass ?>"><div<?php echo $assets->spesifikasi->cellAttributes() ?>>
<span id="el_assets_spesifikasi">
<textarea data-table="assets" data-field="x_spesifikasi" name="x_spesifikasi" id="x_spesifikasi" cols="35" rows="4" placeholder="<?php echo HtmlEncode($assets->spesifikasi->getPlaceHolder()) ?>"<?php echo $assets->spesifikasi->editAttributes() ?>><?php echo $assets->spesifikasi->EditValue ?></textarea>
</span>
<?php echo $assets->spesifikasi->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($assets->foto->Visible) { // foto ?>
	<div id="r_foto" class="form-group row">
		<label id="elh_assets_foto" class="<?php echo $assets_add->LeftColumnClass ?>"><?php echo $assets->foto->caption() ?><?php echo ($assets->foto->Required) ? $Language->phrase("FieldRequiredIndicator") : "" ?></label>
		<div class="<?php echo $assets_add->RightColumnClass ?>"><div<?php echo $assets->foto->cellAttributes() ?>>
<span id="el_assets_foto">
<div id="fd_x_foto">
<span title="<?php echo $assets->foto->title() ? $assets->foto->title() : $Language->phrase("ChooseFile") ?>" class="btn btn-default btn-sm fileinput-button ew-tooltip<?php if ($assets->foto->ReadOnly || $assets->foto->Disabled) echo " d-none"; ?>">
	<span><?php echo $Language->phrase("ChooseFileBtn") ?></span>
	<input type="file" title=" " data-table="assets" data-field="x_foto" name="x_foto" id="x_foto"<?php echo $assets->foto->editAttributes() ?>>
</span>
<input type="hidden" name="fn_x_foto" id= "fn_x_foto" value="<?php echo $assets->foto->Upload->FileName ?>">
<input type="hidden" name="fa_x_foto" id= "fa_x_foto" value="0">
<input type="hidden" name="fs_x_foto" id= "fs_x_foto" value="65535">
<input type="hidden" name="fx_x_foto" id= "fx_x_foto" value="<?php echo $assets->foto->UploadAllowedFileExt ?>">
<input type="hidden" name="fm_x_foto" id= "fm_x_foto" value="<?php echo $assets->foto->UploadMaxFileSize ?>">
</div>
<table id="ft_x_foto" class="table table-sm float-left ew-upload-table"><tbody class="files"></tbody></table>
</span>
<?php echo $assets->foto->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($assets->kelengkapan_awal->Visible) { // kelengkapan_awal ?>
	<div id="r_kelengkapan_awal" class="form-group row">
		<label id="elh_assets_kelengkapan_awal" for="x_kelengkapan_awal" class="<?php echo $assets_add->LeftColumnClass ?>"><?php echo $assets->kelengkapan_awal->caption() ?><?php echo ($assets->kelengkapan_awal->Required) ? $Language->phrase("FieldRequiredIndicator") : "" ?></label>
		<div class="<?php echo $assets_add->RightColumnClass ?>"><div<?php echo $assets->kelengkapan_awal->cellAttributes() ?>>
<span id="el_assets_kelengkapan_awal">
<textarea data-table="assets" data-field="x_kelengkapan_awal" name="x_kelengkapan_awal" id="x_kelengkapan_awal" cols="35" rows="4" placeholder="<?php echo HtmlEncode($assets->kelengkapan_awal->getPlaceHolder()) ?>"<?php echo $assets->kelengkapan_awal->editAttributes() ?>><?php echo $assets->kelengkapan_awal->EditValue ?></textarea>
</span>
<?php echo $assets->kelengkapan_awal->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($assets->keterangan_terbaru->Visible) { // keterangan_terbaru ?>
	<div id="r_keterangan_terbaru" class="form-group row">
		<label id="elh_assets_keterangan_terbaru" for="x_keterangan_terbaru" class="<?php echo $assets_add->LeftColumnClass ?>"><?php echo $assets->keterangan_terbaru->caption() ?><?php echo ($assets->keterangan_terbaru->Required) ? $Language->phrase("FieldRequiredIndicator") : "" ?></label>
		<div class="<?php echo $assets_add->RightColumnClass ?>"><div<?php echo $assets->keterangan_terbaru->cellAttributes() ?>>
<span id="el_assets_keterangan_terbaru">
<textarea data-table="assets" data-field="x_keterangan_terbaru" name="x_keterangan_terbaru" id="x_keterangan_terbaru" cols="35" rows="4" placeholder="<?php echo HtmlEncode($assets->keterangan_terbaru->getPlaceHolder()) ?>"<?php echo $assets->keterangan_terbaru->editAttributes() ?>><?php echo $assets->keterangan_terbaru->EditValue ?></textarea>
</span>
<?php echo $assets->keterangan_terbaru->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($assets->status->Visible) { // status ?>
	<div id="r_status" class="form-group row">
		<label id="elh_assets_status" for="x_status" class="<?php echo $assets_add->LeftColumnClass ?>"><?php echo $assets->status->caption() ?><?php echo ($assets->status->Required) ? $Language->phrase("FieldRequiredIndicator") : "" ?></label>
		<div class="<?php echo $assets_add->RightColumnClass ?>"><div<?php echo $assets->status->cellAttributes() ?>>
<span id="el_assets_status">
<div class="input-group">
	<select class="custom-select ew-custom-select" data-table="assets" data-field="x_status" data-value-separator="<?php echo $assets->status->displayValueSeparatorAttribute() ?>" id="x_status" name="x_status"<?php echo $assets->status->editAttributes() ?>>
		<?php echo $assets->status->selectOptionListHtml("x_status") ?>
	</select>
</div>
</span>
<?php echo $assets->status->CustomMsg ?></div></div>
	</div>
<?php } ?>
</div><!-- /page* -->
<?php if (!$assets_add->IsModal) { ?>
<div class="form-group row"><!-- buttons .form-group -->
	<div class="<?php echo $assets_add->OffsetColumnClass ?>"><!-- buttons offset -->
<button class="btn btn-primary ew-btn" name="btn-action" id="btn-action" type="submit"><?php echo $Language->phrase("AddBtn") ?></button>
<button class="btn btn-default ew-btn" name="btn-cancel" id="btn-cancel" type="button" data-href="<?php echo $assets_add->getReturnUrl() ?>"><?php echo $Language->phrase("CancelBtn") ?></button>
	</div><!-- /buttons offset -->
</div><!-- /buttons .form-group -->
<?php } ?>
</form>
<?php
$assets_add->showPageFooter();
if (DEBUG_ENABLED)
	echo GetDebugMessage();
?>
<script>

// Write your table-specific startup script here
// document.write("page loaded");

</script>
<?php include_once "footer.php" ?>
<?php
$assets_add->terminate();
?>