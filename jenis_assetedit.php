<?php
namespace PHPMaker2019\inventaris_assets;

// Session
if (session_status() !== PHP_SESSION_ACTIVE)
	session_start(); // Init session data

// Output buffering
ob_start(); 

// Autoload
include_once "autoload.php";
?>
<?php

// Write header
WriteHeader(FALSE);

// Create page object
$jenis_asset_edit = new jenis_asset_edit();

// Run the page
$jenis_asset_edit->run();

// Setup login status
SetupLoginStatus();
SetClientVar("login", LoginStatus());

// Global Page Rendering event (in userfn*.php)
Page_Rendering();

// Page Rendering event
$jenis_asset_edit->Page_Render();
?>
<?php include_once "header.php" ?>
<script>

// Form object
currentPageID = ew.PAGE_ID = "edit";
var fjenis_assetedit = currentForm = new ew.Form("fjenis_assetedit", "edit");

// Validate form
fjenis_assetedit.validate = function() {
	if (!this.validateRequired)
		return true; // Ignore validation
	var $ = jQuery, fobj = this.getForm(), $fobj = $(fobj);
	if ($fobj.find("#confirm").val() == "F")
		return true;
	var elm, felm, uelm, addcnt = 0;
	var $k = $fobj.find("#" + this.formKeyCountName); // Get key_count
	var rowcnt = ($k[0]) ? parseInt($k.val(), 10) : 1;
	var startcnt = (rowcnt == 0) ? 0 : 1; // Check rowcnt == 0 => Inline-Add
	var gridinsert = ["insert", "gridinsert"].includes($fobj.find("#action").val()) && $k[0];
	for (var i = startcnt; i <= rowcnt; i++) {
		var infix = ($k[0]) ? String(i) : "";
		$fobj.data("rowindex", infix);
		<?php if ($jenis_asset_edit->nama_jenis->Required) { ?>
			elm = this.getElements("x" + infix + "_nama_jenis");
			if (elm && !ew.isHidden(elm) && !ew.hasValue(elm))
				return this.onError(elm, "<?php echo JsEncode(str_replace("%s", $jenis_asset->nama_jenis->caption(), $jenis_asset->nama_jenis->RequiredErrorMessage)) ?>");
		<?php } ?>

			// Fire Form_CustomValidate event
			if (!this.Form_CustomValidate(fobj))
				return false;
	}

	// Process detail forms
	var dfs = $fobj.find("input[name='detailpage']").get();
	for (var i = 0; i < dfs.length; i++) {
		var df = dfs[i], val = df.value;
		if (val && ew.forms[val])
			if (!ew.forms[val].validate())
				return false;
	}
	return true;
}

// Form_CustomValidate event
fjenis_assetedit.Form_CustomValidate = function(fobj) { // DO NOT CHANGE THIS LINE!

	// Your custom validation code here, return false if invalid.
	return true;
}

// Use JavaScript validation or not
fjenis_assetedit.validateRequired = <?php echo json_encode(CLIENT_VALIDATE) ?>;

// Dynamic selection lists
// Form object for search

</script>
<script>

// Write your client script here, no need to add script tags.
</script>
<?php $jenis_asset_edit->showPageHeader(); ?>
<?php
$jenis_asset_edit->showMessage();
?>
<form name="fjenis_assetedit" id="fjenis_assetedit" class="<?php echo $jenis_asset_edit->FormClassName ?>" action="<?php echo CurrentPageName() ?>" method="post">
<?php if ($jenis_asset_edit->CheckToken) { ?>
<input type="hidden" name="<?php echo TOKEN_NAME ?>" value="<?php echo $jenis_asset_edit->Token ?>">
<?php } ?>
<input type="hidden" name="t" value="jenis_asset">
<input type="hidden" name="action" id="action" value="update">
<input type="hidden" name="modal" value="<?php echo (int)$jenis_asset_edit->IsModal ?>">
<div class="ew-edit-div"><!-- page* -->
<?php if ($jenis_asset->nama_jenis->Visible) { // nama_jenis ?>
	<div id="r_nama_jenis" class="form-group row">
		<label id="elh_jenis_asset_nama_jenis" for="x_nama_jenis" class="<?php echo $jenis_asset_edit->LeftColumnClass ?>"><?php echo $jenis_asset->nama_jenis->caption() ?><?php echo ($jenis_asset->nama_jenis->Required) ? $Language->phrase("FieldRequiredIndicator") : "" ?></label>
		<div class="<?php echo $jenis_asset_edit->RightColumnClass ?>"><div<?php echo $jenis_asset->nama_jenis->cellAttributes() ?>>
<span id="el_jenis_asset_nama_jenis">
<input type="text" data-table="jenis_asset" data-field="x_nama_jenis" name="x_nama_jenis" id="x_nama_jenis" size="30" maxlength="255" placeholder="<?php echo HtmlEncode($jenis_asset->nama_jenis->getPlaceHolder()) ?>" value="<?php echo $jenis_asset->nama_jenis->EditValue ?>"<?php echo $jenis_asset->nama_jenis->editAttributes() ?>>
</span>
<?php echo $jenis_asset->nama_jenis->CustomMsg ?></div></div>
	</div>
<?php } ?>
</div><!-- /page* -->
	<input type="hidden" data-table="jenis_asset" data-field="x_id" name="x_id" id="x_id" value="<?php echo HtmlEncode($jenis_asset->id->CurrentValue) ?>">
<?php if (!$jenis_asset_edit->IsModal) { ?>
<div class="form-group row"><!-- buttons .form-group -->
	<div class="<?php echo $jenis_asset_edit->OffsetColumnClass ?>"><!-- buttons offset -->
<button class="btn btn-primary ew-btn" name="btn-action" id="btn-action" type="submit"><?php echo $Language->phrase("SaveBtn") ?></button>
<button class="btn btn-default ew-btn" name="btn-cancel" id="btn-cancel" type="button" data-href="<?php echo $jenis_asset_edit->getReturnUrl() ?>"><?php echo $Language->phrase("CancelBtn") ?></button>
	</div><!-- /buttons offset -->
</div><!-- /buttons .form-group -->
<?php } ?>
</form>
<?php
$jenis_asset_edit->showPageFooter();
if (DEBUG_ENABLED)
	echo GetDebugMessage();
?>
<script>

// Write your table-specific startup script here
// document.write("page loaded");

</script>
<?php include_once "footer.php" ?>
<?php
$jenis_asset_edit->terminate();
?>