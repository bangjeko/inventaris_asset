<?php
namespace PHPMaker2019\inventaris_assets;

// Session
if (session_status() !== PHP_SESSION_ACTIVE)
	session_start(); // Init session data

// Output buffering
ob_start(); 

// Autoload
include_once "autoload.php";
?>
<?php

// Write header
WriteHeader(FALSE);

// Create page object
$users_edit = new users_edit();

// Run the page
$users_edit->run();

// Setup login status
SetupLoginStatus();
SetClientVar("login", LoginStatus());

// Global Page Rendering event (in userfn*.php)
Page_Rendering();

// Page Rendering event
$users_edit->Page_Render();
?>
<?php include_once "header.php" ?>
<script>

// Form object
currentPageID = ew.PAGE_ID = "edit";
var fusersedit = currentForm = new ew.Form("fusersedit", "edit");

// Validate form
fusersedit.validate = function() {
	if (!this.validateRequired)
		return true; // Ignore validation
	var $ = jQuery, fobj = this.getForm(), $fobj = $(fobj);
	if ($fobj.find("#confirm").val() == "F")
		return true;
	var elm, felm, uelm, addcnt = 0;
	var $k = $fobj.find("#" + this.formKeyCountName); // Get key_count
	var rowcnt = ($k[0]) ? parseInt($k.val(), 10) : 1;
	var startcnt = (rowcnt == 0) ? 0 : 1; // Check rowcnt == 0 => Inline-Add
	var gridinsert = ["insert", "gridinsert"].includes($fobj.find("#action").val()) && $k[0];
	for (var i = startcnt; i <= rowcnt; i++) {
		var infix = ($k[0]) ? String(i) : "";
		$fobj.data("rowindex", infix);
		<?php if ($users_edit->nama_lengkap->Required) { ?>
			elm = this.getElements("x" + infix + "_nama_lengkap");
			if (elm && !ew.isHidden(elm) && !ew.hasValue(elm))
				return this.onError(elm, "<?php echo JsEncode(str_replace("%s", $users->nama_lengkap->caption(), $users->nama_lengkap->RequiredErrorMessage)) ?>");
		<?php } ?>
		<?php if ($users_edit->username->Required) { ?>
			elm = this.getElements("x" + infix + "_username");
			if (elm && !ew.isHidden(elm) && !ew.hasValue(elm))
				return this.onError(elm, "<?php echo JsEncode(str_replace("%s", $users->username->caption(), $users->username->RequiredErrorMessage)) ?>");
		<?php } ?>
		<?php if ($users_edit->password->Required) { ?>
			elm = this.getElements("x" + infix + "_password");
			if (elm && !ew.isHidden(elm) && !ew.hasValue(elm))
				return this.onError(elm, "<?php echo JsEncode(str_replace("%s", $users->password->caption(), $users->password->RequiredErrorMessage)) ?>");
		<?php } ?>
		<?php if ($users_edit->userlevel_id->Required) { ?>
			elm = this.getElements("x" + infix + "_userlevel_id");
			if (elm && !ew.isHidden(elm) && !ew.hasValue(elm))
				return this.onError(elm, "<?php echo JsEncode(str_replace("%s", $users->userlevel_id->caption(), $users->userlevel_id->RequiredErrorMessage)) ?>");
		<?php } ?>

			// Fire Form_CustomValidate event
			if (!this.Form_CustomValidate(fobj))
				return false;
	}

	// Process detail forms
	var dfs = $fobj.find("input[name='detailpage']").get();
	for (var i = 0; i < dfs.length; i++) {
		var df = dfs[i], val = df.value;
		if (val && ew.forms[val])
			if (!ew.forms[val].validate())
				return false;
	}
	return true;
}

// Form_CustomValidate event
fusersedit.Form_CustomValidate = function(fobj) { // DO NOT CHANGE THIS LINE!

	// Your custom validation code here, return false if invalid.
	return true;
}

// Use JavaScript validation or not
fusersedit.validateRequired = <?php echo json_encode(CLIENT_VALIDATE) ?>;

// Dynamic selection lists
fusersedit.lists["x_userlevel_id"] = <?php echo $users_edit->userlevel_id->Lookup->toClientList() ?>;
fusersedit.lists["x_userlevel_id"].options = <?php echo JsonEncode($users_edit->userlevel_id->lookupOptions()) ?>;

// Form object for search
</script>
<script>

// Write your client script here, no need to add script tags.
</script>
<?php $users_edit->showPageHeader(); ?>
<?php
$users_edit->showMessage();
?>
<form name="fusersedit" id="fusersedit" class="<?php echo $users_edit->FormClassName ?>" action="<?php echo CurrentPageName() ?>" method="post">
<?php if ($users_edit->CheckToken) { ?>
<input type="hidden" name="<?php echo TOKEN_NAME ?>" value="<?php echo $users_edit->Token ?>">
<?php } ?>
<input type="hidden" name="t" value="users">
<input type="hidden" name="action" id="action" value="update">
<input type="hidden" name="modal" value="<?php echo (int)$users_edit->IsModal ?>">
<!-- Fields to prevent google autofill -->
<input class="d-none" type="text" name="<?php echo Encrypt(Random()) ?>">
<input class="d-none" type="password" name="<?php echo Encrypt(Random()) ?>">
<div class="ew-edit-div"><!-- page* -->
<?php if ($users->nama_lengkap->Visible) { // nama_lengkap ?>
	<div id="r_nama_lengkap" class="form-group row">
		<label id="elh_users_nama_lengkap" for="x_nama_lengkap" class="<?php echo $users_edit->LeftColumnClass ?>"><?php echo $users->nama_lengkap->caption() ?><?php echo ($users->nama_lengkap->Required) ? $Language->phrase("FieldRequiredIndicator") : "" ?></label>
		<div class="<?php echo $users_edit->RightColumnClass ?>"><div<?php echo $users->nama_lengkap->cellAttributes() ?>>
<span id="el_users_nama_lengkap">
<input type="text" data-table="users" data-field="x_nama_lengkap" name="x_nama_lengkap" id="x_nama_lengkap" size="30" maxlength="255" placeholder="<?php echo HtmlEncode($users->nama_lengkap->getPlaceHolder()) ?>" value="<?php echo $users->nama_lengkap->EditValue ?>"<?php echo $users->nama_lengkap->editAttributes() ?>>
</span>
<?php echo $users->nama_lengkap->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($users->username->Visible) { // username ?>
	<div id="r_username" class="form-group row">
		<label id="elh_users_username" for="x_username" class="<?php echo $users_edit->LeftColumnClass ?>"><?php echo $users->username->caption() ?><?php echo ($users->username->Required) ? $Language->phrase("FieldRequiredIndicator") : "" ?></label>
		<div class="<?php echo $users_edit->RightColumnClass ?>"><div<?php echo $users->username->cellAttributes() ?>>
<span id="el_users_username">
<input type="text" data-table="users" data-field="x_username" name="x_username" id="x_username" size="30" maxlength="50" placeholder="<?php echo HtmlEncode($users->username->getPlaceHolder()) ?>" value="<?php echo $users->username->EditValue ?>"<?php echo $users->username->editAttributes() ?>>
</span>
<?php echo $users->username->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($users->password->Visible) { // password ?>
	<div id="r_password" class="form-group row">
		<label id="elh_users_password" for="x_password" class="<?php echo $users_edit->LeftColumnClass ?>"><?php echo $users->password->caption() ?><?php echo ($users->password->Required) ? $Language->phrase("FieldRequiredIndicator") : "" ?></label>
		<div class="<?php echo $users_edit->RightColumnClass ?>"><div<?php echo $users->password->cellAttributes() ?>>
<span id="el_users_password">
<input type="password" data-field="x_password" name="x_password" id="x_password" value="<?php echo $users->password->EditValue ?>" placeholder="<?php echo HtmlEncode($users->password->getPlaceHolder()) ?>"<?php echo $users->password->editAttributes() ?>>
</span>
<?php echo $users->password->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($users->userlevel_id->Visible) { // userlevel_id ?>
	<div id="r_userlevel_id" class="form-group row">
		<label id="elh_users_userlevel_id" for="x_userlevel_id" class="<?php echo $users_edit->LeftColumnClass ?>"><?php echo $users->userlevel_id->caption() ?><?php echo ($users->userlevel_id->Required) ? $Language->phrase("FieldRequiredIndicator") : "" ?></label>
		<div class="<?php echo $users_edit->RightColumnClass ?>"><div<?php echo $users->userlevel_id->cellAttributes() ?>>
<?php if (!$Security->isAdmin() && $Security->isLoggedIn()) { // Non system admin ?>
<span id="el_users_userlevel_id">
<input type="text" readonly class="form-control-plaintext" value="<?php echo RemoveHtml($users->userlevel_id->EditValue) ?>">
</span>
<?php } else { ?>
<span id="el_users_userlevel_id">
<div class="input-group">
	<select class="custom-select ew-custom-select" data-table="users" data-field="x_userlevel_id" data-value-separator="<?php echo $users->userlevel_id->displayValueSeparatorAttribute() ?>" id="x_userlevel_id" name="x_userlevel_id"<?php echo $users->userlevel_id->editAttributes() ?>>
		<?php echo $users->userlevel_id->selectOptionListHtml("x_userlevel_id") ?>
	</select>
</div>
<?php echo $users->userlevel_id->Lookup->getParamTag("p_x_userlevel_id") ?>
</span>
<?php } ?>
<?php echo $users->userlevel_id->CustomMsg ?></div></div>
	</div>
<?php } ?>
</div><!-- /page* -->
	<input type="hidden" data-table="users" data-field="x_id" name="x_id" id="x_id" value="<?php echo HtmlEncode($users->id->CurrentValue) ?>">
<?php if (!$users_edit->IsModal) { ?>
<div class="form-group row"><!-- buttons .form-group -->
	<div class="<?php echo $users_edit->OffsetColumnClass ?>"><!-- buttons offset -->
<button class="btn btn-primary ew-btn" name="btn-action" id="btn-action" type="submit"><?php echo $Language->phrase("SaveBtn") ?></button>
<button class="btn btn-default ew-btn" name="btn-cancel" id="btn-cancel" type="button" data-href="<?php echo $users_edit->getReturnUrl() ?>"><?php echo $Language->phrase("CancelBtn") ?></button>
	</div><!-- /buttons offset -->
</div><!-- /buttons .form-group -->
<?php } ?>
</form>
<?php
$users_edit->showPageFooter();
if (DEBUG_ENABLED)
	echo GetDebugMessage();
?>
<script>

// Write your table-specific startup script here
// document.write("page loaded");

</script>
<?php include_once "footer.php" ?>
<?php
$users_edit->terminate();
?>