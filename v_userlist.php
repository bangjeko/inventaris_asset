<?php
namespace PHPMaker2019\inventaris_assets;

// Session
if (session_status() !== PHP_SESSION_ACTIVE)
	session_start(); // Init session data

// Output buffering
ob_start(); 

// Autoload
include_once "autoload.php";
?>
<?php

// Write header
WriteHeader(FALSE);

// Create page object
$v_user_list = new v_user_list();

// Run the page
$v_user_list->run();

// Setup login status
SetupLoginStatus();
SetClientVar("login", LoginStatus());

// Global Page Rendering event (in userfn*.php)
Page_Rendering();

// Page Rendering event
$v_user_list->Page_Render();
?>
<?php include_once "header.php" ?>
<?php if (!$v_user->isExport()) { ?>
<script>

// Form object
currentPageID = ew.PAGE_ID = "list";
var fv_userlist = currentForm = new ew.Form("fv_userlist", "list");
fv_userlist.formKeyCountName = '<?php echo $v_user_list->FormKeyCountName ?>';

// Form_CustomValidate event
fv_userlist.Form_CustomValidate = function(fobj) { // DO NOT CHANGE THIS LINE!

	// Your custom validation code here, return false if invalid.
	return true;
}

// Use JavaScript validation or not
fv_userlist.validateRequired = <?php echo json_encode(CLIENT_VALIDATE) ?>;

// Dynamic selection lists
// Form object for search

var fv_userlistsrch = currentSearchForm = new ew.Form("fv_userlistsrch");

// Filters
fv_userlistsrch.filterList = <?php echo $v_user_list->getFilterList() ?>;

// Init search panel as collapsed
fv_userlistsrch.initSearchPanel = true;
</script>
<script>

// Write your client script here, no need to add script tags.
</script>
<?php } ?>
<?php if (!$v_user->isExport()) { ?>
<div class="btn-toolbar ew-toolbar">
<?php if ($v_user_list->TotalRecs > 0 && $v_user_list->ExportOptions->visible()) { ?>
<?php $v_user_list->ExportOptions->render("body") ?>
<?php } ?>
<?php if ($v_user_list->ImportOptions->visible()) { ?>
<?php $v_user_list->ImportOptions->render("body") ?>
<?php } ?>
<?php if ($v_user_list->SearchOptions->visible()) { ?>
<?php $v_user_list->SearchOptions->render("body") ?>
<?php } ?>
<?php if ($v_user_list->FilterOptions->visible()) { ?>
<?php $v_user_list->FilterOptions->render("body") ?>
<?php } ?>
<div class="clearfix"></div>
</div>
<?php } ?>
<?php
$v_user_list->renderOtherOptions();
?>
<?php if ($Security->CanSearch()) { ?>
<?php if (!$v_user->isExport() && !$v_user->CurrentAction) { ?>
<form name="fv_userlistsrch" id="fv_userlistsrch" class="form-inline ew-form ew-ext-search-form" action="<?php echo CurrentPageName() ?>">
<?php $searchPanelClass = ($v_user_list->SearchWhere <> "") ? " show" : ""; ?>
<div id="fv_userlistsrch-search-panel" class="ew-search-panel collapse<?php echo $searchPanelClass ?>">
<input type="hidden" name="cmd" value="search">
<input type="hidden" name="t" value="v_user">
	<div class="ew-basic-search">
<div id="xsr_1" class="ew-row d-sm-flex">
	<div class="ew-quick-search input-group">
		<input type="text" name="<?php echo TABLE_BASIC_SEARCH ?>" id="<?php echo TABLE_BASIC_SEARCH ?>" class="form-control" value="<?php echo HtmlEncode($v_user_list->BasicSearch->getKeyword()) ?>" placeholder="<?php echo HtmlEncode($Language->phrase("Search")) ?>">
		<input type="hidden" name="<?php echo TABLE_BASIC_SEARCH_TYPE ?>" id="<?php echo TABLE_BASIC_SEARCH_TYPE ?>" value="<?php echo HtmlEncode($v_user_list->BasicSearch->getType()) ?>">
		<div class="input-group-append">
			<button class="btn btn-primary" name="btn-submit" id="btn-submit" type="submit"><?php echo $Language->phrase("SearchBtn") ?></button>
			<button type="button" data-toggle="dropdown" class="btn btn-primary dropdown-toggle dropdown-toggle-split" aria-haspopup="true" aria-expanded="false"><span id="searchtype"><?php echo $v_user_list->BasicSearch->getTypeNameShort() ?></span></button>
			<div class="dropdown-menu dropdown-menu-right">
				<a class="dropdown-item<?php if ($v_user_list->BasicSearch->getType() == "") echo " active"; ?>" href="javascript:void(0);" onclick="ew.setSearchType(this)"><?php echo $Language->phrase("QuickSearchAuto") ?></a>
				<a class="dropdown-item<?php if ($v_user_list->BasicSearch->getType() == "=") echo " active"; ?>" href="javascript:void(0);" onclick="ew.setSearchType(this,'=')"><?php echo $Language->phrase("QuickSearchExact") ?></a>
				<a class="dropdown-item<?php if ($v_user_list->BasicSearch->getType() == "AND") echo " active"; ?>" href="javascript:void(0);" onclick="ew.setSearchType(this,'AND')"><?php echo $Language->phrase("QuickSearchAll") ?></a>
				<a class="dropdown-item<?php if ($v_user_list->BasicSearch->getType() == "OR") echo " active"; ?>" href="javascript:void(0);" onclick="ew.setSearchType(this,'OR')"><?php echo $Language->phrase("QuickSearchAny") ?></a>
			</div>
		</div>
	</div>
</div>
	</div>
</div>
</form>
<?php } ?>
<?php } ?>
<?php $v_user_list->showPageHeader(); ?>
<?php
$v_user_list->showMessage();
?>
<?php if ($v_user_list->TotalRecs > 0 || $v_user->CurrentAction) { ?>
<div class="card ew-card ew-grid<?php if ($v_user_list->isAddOrEdit()) { ?> ew-grid-add-edit<?php } ?> v_user">
<?php if (!$v_user->isExport()) { ?>
<div class="card-header ew-grid-upper-panel">
<?php if (!$v_user->isGridAdd()) { ?>
<form name="ew-pager-form" class="form-inline ew-form ew-pager-form" action="<?php echo CurrentPageName() ?>">
<?php if (!isset($v_user_list->Pager)) $v_user_list->Pager = new PrevNextPager($v_user_list->StartRec, $v_user_list->DisplayRecs, $v_user_list->TotalRecs, $v_user_list->AutoHidePager) ?>
<?php if ($v_user_list->Pager->RecordCount > 0 && $v_user_list->Pager->Visible) { ?>
<div class="ew-pager">
<span><?php echo $Language->Phrase("Page") ?>&nbsp;</span>
<div class="ew-prev-next"><div class="input-group input-group-sm">
<div class="input-group-prepend">
<!-- first page button -->
	<?php if ($v_user_list->Pager->FirstButton->Enabled) { ?>
	<a class="btn btn-default" title="<?php echo $Language->phrase("PagerFirst") ?>" href="<?php echo $v_user_list->pageUrl() ?>start=<?php echo $v_user_list->Pager->FirstButton->Start ?>"><i class="icon-first ew-icon"></i></a>
	<?php } else { ?>
	<a class="btn btn-default disabled" title="<?php echo $Language->phrase("PagerFirst") ?>"><i class="icon-first ew-icon"></i></a>
	<?php } ?>
<!-- previous page button -->
	<?php if ($v_user_list->Pager->PrevButton->Enabled) { ?>
	<a class="btn btn-default" title="<?php echo $Language->phrase("PagerPrevious") ?>" href="<?php echo $v_user_list->pageUrl() ?>start=<?php echo $v_user_list->Pager->PrevButton->Start ?>"><i class="icon-prev ew-icon"></i></a>
	<?php } else { ?>
	<a class="btn btn-default disabled" title="<?php echo $Language->phrase("PagerPrevious") ?>"><i class="icon-prev ew-icon"></i></a>
	<?php } ?>
</div>
<!-- current page number -->
	<input class="form-control" type="text" name="<?php echo TABLE_PAGE_NO ?>" value="<?php echo $v_user_list->Pager->CurrentPage ?>">
<div class="input-group-append">
<!-- next page button -->
	<?php if ($v_user_list->Pager->NextButton->Enabled) { ?>
	<a class="btn btn-default" title="<?php echo $Language->phrase("PagerNext") ?>" href="<?php echo $v_user_list->pageUrl() ?>start=<?php echo $v_user_list->Pager->NextButton->Start ?>"><i class="icon-next ew-icon"></i></a>
	<?php } else { ?>
	<a class="btn btn-default disabled" title="<?php echo $Language->phrase("PagerNext") ?>"><i class="icon-next ew-icon"></i></a>
	<?php } ?>
<!-- last page button -->
	<?php if ($v_user_list->Pager->LastButton->Enabled) { ?>
	<a class="btn btn-default" title="<?php echo $Language->phrase("PagerLast") ?>" href="<?php echo $v_user_list->pageUrl() ?>start=<?php echo $v_user_list->Pager->LastButton->Start ?>"><i class="icon-last ew-icon"></i></a>
	<?php } else { ?>
	<a class="btn btn-default disabled" title="<?php echo $Language->phrase("PagerLast") ?>"><i class="icon-last ew-icon"></i></a>
	<?php } ?>
</div>
</div>
</div>
<span>&nbsp;<?php echo $Language->Phrase("of") ?>&nbsp;<?php echo $v_user_list->Pager->PageCount ?></span>
<div class="clearfix"></div>
</div>
<?php } ?>
<?php if ($v_user_list->Pager->RecordCount > 0) { ?>
<div class="ew-pager ew-rec">
	<span><?php echo $Language->Phrase("Record") ?>&nbsp;<?php echo $v_user_list->Pager->FromIndex ?>&nbsp;<?php echo $Language->Phrase("To") ?>&nbsp;<?php echo $v_user_list->Pager->ToIndex ?>&nbsp;<?php echo $Language->Phrase("Of") ?>&nbsp;<?php echo $v_user_list->Pager->RecordCount ?></span>
</div>
<?php } ?>
<?php if ($v_user_list->TotalRecs > 0 && (!$v_user_list->AutoHidePageSizeSelector || $v_user_list->Pager->Visible)) { ?>
<div class="ew-pager">
<input type="hidden" name="t" value="v_user">
<select name="<?php echo TABLE_REC_PER_PAGE ?>" class="form-control form-control-sm ew-tooltip" title="<?php echo $Language->phrase("RecordsPerPage") ?>" onchange="this.form.submit();">
<option value="5"<?php if ($v_user_list->DisplayRecs == 5) { ?> selected<?php } ?>>5</option>
<option value="10"<?php if ($v_user_list->DisplayRecs == 10) { ?> selected<?php } ?>>10</option>
<option value="20"<?php if ($v_user_list->DisplayRecs == 20) { ?> selected<?php } ?>>20</option>
<option value="50"<?php if ($v_user_list->DisplayRecs == 50) { ?> selected<?php } ?>>50</option>
<option value="100"<?php if ($v_user_list->DisplayRecs == 100) { ?> selected<?php } ?>>100</option>
</select>
</div>
<?php } ?>
</form>
<?php } ?>
<div class="ew-list-other-options">
<?php $v_user_list->OtherOptions->render("body") ?>
</div>
<div class="clearfix"></div>
</div>
<?php } ?>
<form name="fv_userlist" id="fv_userlist" class="form-inline ew-form ew-list-form" action="<?php echo CurrentPageName() ?>" method="post">
<?php if ($v_user_list->CheckToken) { ?>
<input type="hidden" name="<?php echo TOKEN_NAME ?>" value="<?php echo $v_user_list->Token ?>">
<?php } ?>
<input type="hidden" name="t" value="v_user">
<div id="gmp_v_user" class="<?php if (IsResponsiveLayout()) { ?>table-responsive <?php } ?>card-body ew-grid-middle-panel">
<?php if ($v_user_list->TotalRecs > 0 || $v_user->isGridEdit()) { ?>
<table id="tbl_v_userlist" class="table ew-table"><!-- .ew-table ##-->
<thead>
	<tr class="ew-table-header">
<?php

// Header row
$v_user_list->RowType = ROWTYPE_HEADER;

// Render list options
$v_user_list->renderListOptions();

// Render list options (header, left)
$v_user_list->ListOptions->render("header", "left");
?>
<?php if ($v_user->id->Visible) { // id ?>
	<?php if ($v_user->sortUrl($v_user->id) == "") { ?>
		<th data-name="id" class="<?php echo $v_user->id->headerCellClass() ?>"><div id="elh_v_user_id" class="v_user_id"><div class="ew-table-header-caption"><?php echo $v_user->id->caption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="id" class="<?php echo $v_user->id->headerCellClass() ?>"><div class="ew-pointer" onclick="ew.sort(event,'<?php echo $v_user->SortUrl($v_user->id) ?>',1);"><div id="elh_v_user_id" class="v_user_id">
			<div class="ew-table-header-btn"><span class="ew-table-header-caption"><?php echo $v_user->id->caption() ?></span><span class="ew-table-header-sort"><?php if ($v_user->id->getSort() == "ASC") { ?><i class="fa fa-sort-up"></i><?php } elseif ($v_user->id->getSort() == "DESC") { ?><i class="fa fa-sort-down"></i><?php } ?></span></div>
		</div></div></th>
	<?php } ?>
<?php } ?>
<?php if ($v_user->username->Visible) { // username ?>
	<?php if ($v_user->sortUrl($v_user->username) == "") { ?>
		<th data-name="username" class="<?php echo $v_user->username->headerCellClass() ?>"><div id="elh_v_user_username" class="v_user_username"><div class="ew-table-header-caption"><?php echo $v_user->username->caption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="username" class="<?php echo $v_user->username->headerCellClass() ?>"><div class="ew-pointer" onclick="ew.sort(event,'<?php echo $v_user->SortUrl($v_user->username) ?>',1);"><div id="elh_v_user_username" class="v_user_username">
			<div class="ew-table-header-btn"><span class="ew-table-header-caption"><?php echo $v_user->username->caption() ?><?php echo $Language->phrase("SrchLegend") ?></span><span class="ew-table-header-sort"><?php if ($v_user->username->getSort() == "ASC") { ?><i class="fa fa-sort-up"></i><?php } elseif ($v_user->username->getSort() == "DESC") { ?><i class="fa fa-sort-down"></i><?php } ?></span></div>
		</div></div></th>
	<?php } ?>
<?php } ?>
<?php if ($v_user->userlevel_id->Visible) { // userlevel_id ?>
	<?php if ($v_user->sortUrl($v_user->userlevel_id) == "") { ?>
		<th data-name="userlevel_id" class="<?php echo $v_user->userlevel_id->headerCellClass() ?>"><div id="elh_v_user_userlevel_id" class="v_user_userlevel_id"><div class="ew-table-header-caption"><?php echo $v_user->userlevel_id->caption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="userlevel_id" class="<?php echo $v_user->userlevel_id->headerCellClass() ?>"><div class="ew-pointer" onclick="ew.sort(event,'<?php echo $v_user->SortUrl($v_user->userlevel_id) ?>',1);"><div id="elh_v_user_userlevel_id" class="v_user_userlevel_id">
			<div class="ew-table-header-btn"><span class="ew-table-header-caption"><?php echo $v_user->userlevel_id->caption() ?></span><span class="ew-table-header-sort"><?php if ($v_user->userlevel_id->getSort() == "ASC") { ?><i class="fa fa-sort-up"></i><?php } elseif ($v_user->userlevel_id->getSort() == "DESC") { ?><i class="fa fa-sort-down"></i><?php } ?></span></div>
		</div></div></th>
	<?php } ?>
<?php } ?>
<?php

// Render list options (header, right)
$v_user_list->ListOptions->render("header", "right");
?>
	</tr>
</thead>
<tbody>
<?php
if ($v_user->ExportAll && $v_user->isExport()) {
	$v_user_list->StopRec = $v_user_list->TotalRecs;
} else {

	// Set the last record to display
	if ($v_user_list->TotalRecs > $v_user_list->StartRec + $v_user_list->DisplayRecs - 1)
		$v_user_list->StopRec = $v_user_list->StartRec + $v_user_list->DisplayRecs - 1;
	else
		$v_user_list->StopRec = $v_user_list->TotalRecs;
}
$v_user_list->RecCnt = $v_user_list->StartRec - 1;
if ($v_user_list->Recordset && !$v_user_list->Recordset->EOF) {
	$v_user_list->Recordset->moveFirst();
	$selectLimit = $v_user_list->UseSelectLimit;
	if (!$selectLimit && $v_user_list->StartRec > 1)
		$v_user_list->Recordset->move($v_user_list->StartRec - 1);
} elseif (!$v_user->AllowAddDeleteRow && $v_user_list->StopRec == 0) {
	$v_user_list->StopRec = $v_user->GridAddRowCount;
}

// Initialize aggregate
$v_user->RowType = ROWTYPE_AGGREGATEINIT;
$v_user->resetAttributes();
$v_user_list->renderRow();
while ($v_user_list->RecCnt < $v_user_list->StopRec) {
	$v_user_list->RecCnt++;
	if ($v_user_list->RecCnt >= $v_user_list->StartRec) {
		$v_user_list->RowCnt++;

		// Set up key count
		$v_user_list->KeyCount = $v_user_list->RowIndex;

		// Init row class and style
		$v_user->resetAttributes();
		$v_user->CssClass = "";
		if ($v_user->isGridAdd()) {
		} else {
			$v_user_list->loadRowValues($v_user_list->Recordset); // Load row values
		}
		$v_user->RowType = ROWTYPE_VIEW; // Render view

		// Set up row id / data-rowindex
		$v_user->RowAttrs = array_merge($v_user->RowAttrs, array('data-rowindex'=>$v_user_list->RowCnt, 'id'=>'r' . $v_user_list->RowCnt . '_v_user', 'data-rowtype'=>$v_user->RowType));

		// Render row
		$v_user_list->renderRow();

		// Render list options
		$v_user_list->renderListOptions();
?>
	<tr<?php echo $v_user->rowAttributes() ?>>
<?php

// Render list options (body, left)
$v_user_list->ListOptions->render("body", "left", $v_user_list->RowCnt);
?>
	<?php if ($v_user->id->Visible) { // id ?>
		<td data-name="id"<?php echo $v_user->id->cellAttributes() ?>>
<span id="el<?php echo $v_user_list->RowCnt ?>_v_user_id" class="v_user_id">
<span<?php echo $v_user->id->viewAttributes() ?>>
<?php echo $v_user->id->getViewValue() ?></span>
</span>
</td>
	<?php } ?>
	<?php if ($v_user->username->Visible) { // username ?>
		<td data-name="username"<?php echo $v_user->username->cellAttributes() ?>>
<span id="el<?php echo $v_user_list->RowCnt ?>_v_user_username" class="v_user_username">
<span<?php echo $v_user->username->viewAttributes() ?>>
<?php echo $v_user->username->getViewValue() ?></span>
</span>
</td>
	<?php } ?>
	<?php if ($v_user->userlevel_id->Visible) { // userlevel_id ?>
		<td data-name="userlevel_id"<?php echo $v_user->userlevel_id->cellAttributes() ?>>
<span id="el<?php echo $v_user_list->RowCnt ?>_v_user_userlevel_id" class="v_user_userlevel_id">
<span<?php echo $v_user->userlevel_id->viewAttributes() ?>>
<?php echo $v_user->userlevel_id->getViewValue() ?></span>
</span>
</td>
	<?php } ?>
<?php

// Render list options (body, right)
$v_user_list->ListOptions->render("body", "right", $v_user_list->RowCnt);
?>
	</tr>
<?php
	}
	if (!$v_user->isGridAdd())
		$v_user_list->Recordset->moveNext();
}
?>
</tbody>
</table><!-- /.ew-table -->
<?php } ?>
<?php if (!$v_user->CurrentAction) { ?>
<input type="hidden" name="action" id="action" value="">
<?php } ?>
</div><!-- /.ew-grid-middle-panel -->
</form><!-- /.ew-list-form -->
<?php

// Close recordset
if ($v_user_list->Recordset)
	$v_user_list->Recordset->Close();
?>
</div><!-- /.ew-grid -->
<?php } ?>
<?php if ($v_user_list->TotalRecs == 0 && !$v_user->CurrentAction) { // Show other options ?>
<div class="ew-list-other-options">
<?php $v_user_list->OtherOptions->render("body") ?>
</div>
<div class="clearfix"></div>
<?php } ?>
<?php
$v_user_list->showPageFooter();
if (DEBUG_ENABLED)
	echo GetDebugMessage();
?>
<?php if (!$v_user->isExport()) { ?>
<script>

// Write your table-specific startup script here
// document.write("page loaded");

</script>
<?php } ?>
<?php include_once "footer.php" ?>
<?php
$v_user_list->terminate();
?>