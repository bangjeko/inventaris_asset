<?php
namespace PHPMaker2019\inventaris_assets;

// Session
if (session_status() !== PHP_SESSION_ACTIVE)
	session_start(); // Init session data

// Output buffering
ob_start(); 

// Autoload
include_once "autoload.php";
?>
<?php

// Write header
WriteHeader(FALSE);

// Create page object
$karyawan_delete = new karyawan_delete();

// Run the page
$karyawan_delete->run();

// Setup login status
SetupLoginStatus();
SetClientVar("login", LoginStatus());

// Global Page Rendering event (in userfn*.php)
Page_Rendering();

// Page Rendering event
$karyawan_delete->Page_Render();
?>
<?php include_once "header.php" ?>
<script>

// Form object
currentPageID = ew.PAGE_ID = "delete";
var fkaryawandelete = currentForm = new ew.Form("fkaryawandelete", "delete");

// Form_CustomValidate event
fkaryawandelete.Form_CustomValidate = function(fobj) { // DO NOT CHANGE THIS LINE!

	// Your custom validation code here, return false if invalid.
	return true;
}

// Use JavaScript validation or not
fkaryawandelete.validateRequired = <?php echo json_encode(CLIENT_VALIDATE) ?>;

// Dynamic selection lists
// Form object for search

</script>
<script>

// Write your client script here, no need to add script tags.
</script>
<?php $karyawan_delete->showPageHeader(); ?>
<?php
$karyawan_delete->showMessage();
?>
<form name="fkaryawandelete" id="fkaryawandelete" class="form-inline ew-form ew-delete-form" action="<?php echo CurrentPageName() ?>" method="post">
<?php if ($karyawan_delete->CheckToken) { ?>
<input type="hidden" name="<?php echo TOKEN_NAME ?>" value="<?php echo $karyawan_delete->Token ?>">
<?php } ?>
<input type="hidden" name="t" value="karyawan">
<input type="hidden" name="action" id="action" value="delete">
<?php foreach ($karyawan_delete->RecKeys as $key) { ?>
<?php $keyvalue = is_array($key) ? implode($COMPOSITE_KEY_SEPARATOR, $key) : $key; ?>
<input type="hidden" name="key_m[]" value="<?php echo HtmlEncode($keyvalue) ?>">
<?php } ?>
<div class="card ew-card ew-grid">
<div class="<?php if (IsResponsiveLayout()) { ?>table-responsive <?php } ?>card-body ew-grid-middle-panel">
<table class="table ew-table">
	<thead>
	<tr class="ew-table-header">
<?php if ($karyawan->nama_karyawan->Visible) { // nama_karyawan ?>
		<th class="<?php echo $karyawan->nama_karyawan->headerCellClass() ?>"><span id="elh_karyawan_nama_karyawan" class="karyawan_nama_karyawan"><?php echo $karyawan->nama_karyawan->caption() ?></span></th>
<?php } ?>
<?php if ($karyawan->nik->Visible) { // nik ?>
		<th class="<?php echo $karyawan->nik->headerCellClass() ?>"><span id="elh_karyawan_nik" class="karyawan_nik"><?php echo $karyawan->nik->caption() ?></span></th>
<?php } ?>
<?php if ($karyawan->divisi->Visible) { // divisi ?>
		<th class="<?php echo $karyawan->divisi->headerCellClass() ?>"><span id="elh_karyawan_divisi" class="karyawan_divisi"><?php echo $karyawan->divisi->caption() ?></span></th>
<?php } ?>
<?php if ($karyawan->jabatan->Visible) { // jabatan ?>
		<th class="<?php echo $karyawan->jabatan->headerCellClass() ?>"><span id="elh_karyawan_jabatan" class="karyawan_jabatan"><?php echo $karyawan->jabatan->caption() ?></span></th>
<?php } ?>
<?php if ($karyawan->atasan_langsung->Visible) { // atasan_langsung ?>
		<th class="<?php echo $karyawan->atasan_langsung->headerCellClass() ?>"><span id="elh_karyawan_atasan_langsung" class="karyawan_atasan_langsung"><?php echo $karyawan->atasan_langsung->caption() ?></span></th>
<?php } ?>
<?php if ($karyawan->nomor_telepon->Visible) { // nomor_telepon ?>
		<th class="<?php echo $karyawan->nomor_telepon->headerCellClass() ?>"><span id="elh_karyawan_nomor_telepon" class="karyawan_nomor_telepon"><?php echo $karyawan->nomor_telepon->caption() ?></span></th>
<?php } ?>
<?php if ($karyawan->_email->Visible) { // email ?>
		<th class="<?php echo $karyawan->_email->headerCellClass() ?>"><span id="elh_karyawan__email" class="karyawan__email"><?php echo $karyawan->_email->caption() ?></span></th>
<?php } ?>
	</tr>
	</thead>
	<tbody>
<?php
$karyawan_delete->RecCnt = 0;
$i = 0;
while (!$karyawan_delete->Recordset->EOF) {
	$karyawan_delete->RecCnt++;
	$karyawan_delete->RowCnt++;

	// Set row properties
	$karyawan->resetAttributes();
	$karyawan->RowType = ROWTYPE_VIEW; // View

	// Get the field contents
	$karyawan_delete->loadRowValues($karyawan_delete->Recordset);

	// Render row
	$karyawan_delete->renderRow();
?>
	<tr<?php echo $karyawan->rowAttributes() ?>>
<?php if ($karyawan->nama_karyawan->Visible) { // nama_karyawan ?>
		<td<?php echo $karyawan->nama_karyawan->cellAttributes() ?>>
<span id="el<?php echo $karyawan_delete->RowCnt ?>_karyawan_nama_karyawan" class="karyawan_nama_karyawan">
<span<?php echo $karyawan->nama_karyawan->viewAttributes() ?>>
<?php echo $karyawan->nama_karyawan->getViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($karyawan->nik->Visible) { // nik ?>
		<td<?php echo $karyawan->nik->cellAttributes() ?>>
<span id="el<?php echo $karyawan_delete->RowCnt ?>_karyawan_nik" class="karyawan_nik">
<span<?php echo $karyawan->nik->viewAttributes() ?>>
<?php echo $karyawan->nik->getViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($karyawan->divisi->Visible) { // divisi ?>
		<td<?php echo $karyawan->divisi->cellAttributes() ?>>
<span id="el<?php echo $karyawan_delete->RowCnt ?>_karyawan_divisi" class="karyawan_divisi">
<span<?php echo $karyawan->divisi->viewAttributes() ?>>
<?php echo $karyawan->divisi->getViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($karyawan->jabatan->Visible) { // jabatan ?>
		<td<?php echo $karyawan->jabatan->cellAttributes() ?>>
<span id="el<?php echo $karyawan_delete->RowCnt ?>_karyawan_jabatan" class="karyawan_jabatan">
<span<?php echo $karyawan->jabatan->viewAttributes() ?>>
<?php echo $karyawan->jabatan->getViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($karyawan->atasan_langsung->Visible) { // atasan_langsung ?>
		<td<?php echo $karyawan->atasan_langsung->cellAttributes() ?>>
<span id="el<?php echo $karyawan_delete->RowCnt ?>_karyawan_atasan_langsung" class="karyawan_atasan_langsung">
<span<?php echo $karyawan->atasan_langsung->viewAttributes() ?>>
<?php echo $karyawan->atasan_langsung->getViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($karyawan->nomor_telepon->Visible) { // nomor_telepon ?>
		<td<?php echo $karyawan->nomor_telepon->cellAttributes() ?>>
<span id="el<?php echo $karyawan_delete->RowCnt ?>_karyawan_nomor_telepon" class="karyawan_nomor_telepon">
<span<?php echo $karyawan->nomor_telepon->viewAttributes() ?>>
<?php echo $karyawan->nomor_telepon->getViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($karyawan->_email->Visible) { // email ?>
		<td<?php echo $karyawan->_email->cellAttributes() ?>>
<span id="el<?php echo $karyawan_delete->RowCnt ?>_karyawan__email" class="karyawan__email">
<span<?php echo $karyawan->_email->viewAttributes() ?>>
<?php echo $karyawan->_email->getViewValue() ?></span>
</span>
</td>
<?php } ?>
	</tr>
<?php
	$karyawan_delete->Recordset->moveNext();
}
$karyawan_delete->Recordset->close();
?>
</tbody>
</table>
</div>
</div>
<div>
<button class="btn btn-primary ew-btn" name="btn-action" id="btn-action" type="submit"><?php echo $Language->phrase("DeleteBtn") ?></button>
<button class="btn btn-default ew-btn" name="btn-cancel" id="btn-cancel" type="button" data-href="<?php echo $karyawan_delete->getReturnUrl() ?>"><?php echo $Language->phrase("CancelBtn") ?></button>
</div>
</form>
<?php
$karyawan_delete->showPageFooter();
if (DEBUG_ENABLED)
	echo GetDebugMessage();
?>
<script>

// Write your table-specific startup script here
// document.write("page loaded");

</script>
<?php include_once "footer.php" ?>
<?php
$karyawan_delete->terminate();
?>