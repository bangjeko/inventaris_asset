<?php
namespace PHPMaker2019\inventaris_assets;

// Session
if (session_status() !== PHP_SESSION_ACTIVE)
	session_start(); // Init session data

// Output buffering
ob_start(); 

// Autoload
include_once "autoload.php";
?>
<?php

// Write header
WriteHeader(FALSE);

// Create page object
$karyawan_edit = new karyawan_edit();

// Run the page
$karyawan_edit->run();

// Setup login status
SetupLoginStatus();
SetClientVar("login", LoginStatus());

// Global Page Rendering event (in userfn*.php)
Page_Rendering();

// Page Rendering event
$karyawan_edit->Page_Render();
?>
<?php include_once "header.php" ?>
<script>

// Form object
currentPageID = ew.PAGE_ID = "edit";
var fkaryawanedit = currentForm = new ew.Form("fkaryawanedit", "edit");

// Validate form
fkaryawanedit.validate = function() {
	if (!this.validateRequired)
		return true; // Ignore validation
	var $ = jQuery, fobj = this.getForm(), $fobj = $(fobj);
	if ($fobj.find("#confirm").val() == "F")
		return true;
	var elm, felm, uelm, addcnt = 0;
	var $k = $fobj.find("#" + this.formKeyCountName); // Get key_count
	var rowcnt = ($k[0]) ? parseInt($k.val(), 10) : 1;
	var startcnt = (rowcnt == 0) ? 0 : 1; // Check rowcnt == 0 => Inline-Add
	var gridinsert = ["insert", "gridinsert"].includes($fobj.find("#action").val()) && $k[0];
	for (var i = startcnt; i <= rowcnt; i++) {
		var infix = ($k[0]) ? String(i) : "";
		$fobj.data("rowindex", infix);
		<?php if ($karyawan_edit->nama_karyawan->Required) { ?>
			elm = this.getElements("x" + infix + "_nama_karyawan");
			if (elm && !ew.isHidden(elm) && !ew.hasValue(elm))
				return this.onError(elm, "<?php echo JsEncode(str_replace("%s", $karyawan->nama_karyawan->caption(), $karyawan->nama_karyawan->RequiredErrorMessage)) ?>");
		<?php } ?>
		<?php if ($karyawan_edit->nik->Required) { ?>
			elm = this.getElements("x" + infix + "_nik");
			if (elm && !ew.isHidden(elm) && !ew.hasValue(elm))
				return this.onError(elm, "<?php echo JsEncode(str_replace("%s", $karyawan->nik->caption(), $karyawan->nik->RequiredErrorMessage)) ?>");
		<?php } ?>
		<?php if ($karyawan_edit->divisi->Required) { ?>
			elm = this.getElements("x" + infix + "_divisi");
			if (elm && !ew.isHidden(elm) && !ew.hasValue(elm))
				return this.onError(elm, "<?php echo JsEncode(str_replace("%s", $karyawan->divisi->caption(), $karyawan->divisi->RequiredErrorMessage)) ?>");
		<?php } ?>
		<?php if ($karyawan_edit->jabatan->Required) { ?>
			elm = this.getElements("x" + infix + "_jabatan");
			if (elm && !ew.isHidden(elm) && !ew.hasValue(elm))
				return this.onError(elm, "<?php echo JsEncode(str_replace("%s", $karyawan->jabatan->caption(), $karyawan->jabatan->RequiredErrorMessage)) ?>");
		<?php } ?>
		<?php if ($karyawan_edit->atasan_langsung->Required) { ?>
			elm = this.getElements("x" + infix + "_atasan_langsung");
			if (elm && !ew.isHidden(elm) && !ew.hasValue(elm))
				return this.onError(elm, "<?php echo JsEncode(str_replace("%s", $karyawan->atasan_langsung->caption(), $karyawan->atasan_langsung->RequiredErrorMessage)) ?>");
		<?php } ?>
		<?php if ($karyawan_edit->nomor_telepon->Required) { ?>
			elm = this.getElements("x" + infix + "_nomor_telepon");
			if (elm && !ew.isHidden(elm) && !ew.hasValue(elm))
				return this.onError(elm, "<?php echo JsEncode(str_replace("%s", $karyawan->nomor_telepon->caption(), $karyawan->nomor_telepon->RequiredErrorMessage)) ?>");
		<?php } ?>
		<?php if ($karyawan_edit->_email->Required) { ?>
			elm = this.getElements("x" + infix + "__email");
			if (elm && !ew.isHidden(elm) && !ew.hasValue(elm))
				return this.onError(elm, "<?php echo JsEncode(str_replace("%s", $karyawan->_email->caption(), $karyawan->_email->RequiredErrorMessage)) ?>");
		<?php } ?>
		<?php if ($karyawan_edit->created_by->Required) { ?>
			elm = this.getElements("x" + infix + "_created_by");
			if (elm && !ew.isHidden(elm) && !ew.hasValue(elm))
				return this.onError(elm, "<?php echo JsEncode(str_replace("%s", $karyawan->created_by->caption(), $karyawan->created_by->RequiredErrorMessage)) ?>");
		<?php } ?>
		<?php if ($karyawan_edit->created_date->Required) { ?>
			elm = this.getElements("x" + infix + "_created_date");
			if (elm && !ew.isHidden(elm) && !ew.hasValue(elm))
				return this.onError(elm, "<?php echo JsEncode(str_replace("%s", $karyawan->created_date->caption(), $karyawan->created_date->RequiredErrorMessage)) ?>");
		<?php } ?>

			// Fire Form_CustomValidate event
			if (!this.Form_CustomValidate(fobj))
				return false;
	}

	// Process detail forms
	var dfs = $fobj.find("input[name='detailpage']").get();
	for (var i = 0; i < dfs.length; i++) {
		var df = dfs[i], val = df.value;
		if (val && ew.forms[val])
			if (!ew.forms[val].validate())
				return false;
	}
	return true;
}

// Form_CustomValidate event
fkaryawanedit.Form_CustomValidate = function(fobj) { // DO NOT CHANGE THIS LINE!

	// Your custom validation code here, return false if invalid.
	return true;
}

// Use JavaScript validation or not
fkaryawanedit.validateRequired = <?php echo json_encode(CLIENT_VALIDATE) ?>;

// Dynamic selection lists
fkaryawanedit.lists["x_created_by"] = <?php echo $karyawan_edit->created_by->Lookup->toClientList() ?>;
fkaryawanedit.lists["x_created_by"].options = <?php echo JsonEncode($karyawan_edit->created_by->lookupOptions()) ?>;

// Form object for search
</script>
<script>

// Write your client script here, no need to add script tags.
</script>
<?php $karyawan_edit->showPageHeader(); ?>
<?php
$karyawan_edit->showMessage();
?>
<form name="fkaryawanedit" id="fkaryawanedit" class="<?php echo $karyawan_edit->FormClassName ?>" action="<?php echo CurrentPageName() ?>" method="post">
<?php if ($karyawan_edit->CheckToken) { ?>
<input type="hidden" name="<?php echo TOKEN_NAME ?>" value="<?php echo $karyawan_edit->Token ?>">
<?php } ?>
<input type="hidden" name="t" value="karyawan">
<input type="hidden" name="action" id="action" value="update">
<input type="hidden" name="modal" value="<?php echo (int)$karyawan_edit->IsModal ?>">
<div class="ew-edit-div"><!-- page* -->
<?php if ($karyawan->nama_karyawan->Visible) { // nama_karyawan ?>
	<div id="r_nama_karyawan" class="form-group row">
		<label id="elh_karyawan_nama_karyawan" for="x_nama_karyawan" class="<?php echo $karyawan_edit->LeftColumnClass ?>"><?php echo $karyawan->nama_karyawan->caption() ?><?php echo ($karyawan->nama_karyawan->Required) ? $Language->phrase("FieldRequiredIndicator") : "" ?></label>
		<div class="<?php echo $karyawan_edit->RightColumnClass ?>"><div<?php echo $karyawan->nama_karyawan->cellAttributes() ?>>
<span id="el_karyawan_nama_karyawan">
<input type="text" data-table="karyawan" data-field="x_nama_karyawan" name="x_nama_karyawan" id="x_nama_karyawan" size="30" maxlength="255" placeholder="<?php echo HtmlEncode($karyawan->nama_karyawan->getPlaceHolder()) ?>" value="<?php echo $karyawan->nama_karyawan->EditValue ?>"<?php echo $karyawan->nama_karyawan->editAttributes() ?>>
</span>
<?php echo $karyawan->nama_karyawan->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($karyawan->nik->Visible) { // nik ?>
	<div id="r_nik" class="form-group row">
		<label id="elh_karyawan_nik" for="x_nik" class="<?php echo $karyawan_edit->LeftColumnClass ?>"><?php echo $karyawan->nik->caption() ?><?php echo ($karyawan->nik->Required) ? $Language->phrase("FieldRequiredIndicator") : "" ?></label>
		<div class="<?php echo $karyawan_edit->RightColumnClass ?>"><div<?php echo $karyawan->nik->cellAttributes() ?>>
<span id="el_karyawan_nik">
<input type="text" data-table="karyawan" data-field="x_nik" name="x_nik" id="x_nik" size="30" maxlength="20" placeholder="<?php echo HtmlEncode($karyawan->nik->getPlaceHolder()) ?>" value="<?php echo $karyawan->nik->EditValue ?>"<?php echo $karyawan->nik->editAttributes() ?>>
</span>
<?php echo $karyawan->nik->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($karyawan->divisi->Visible) { // divisi ?>
	<div id="r_divisi" class="form-group row">
		<label id="elh_karyawan_divisi" for="x_divisi" class="<?php echo $karyawan_edit->LeftColumnClass ?>"><?php echo $karyawan->divisi->caption() ?><?php echo ($karyawan->divisi->Required) ? $Language->phrase("FieldRequiredIndicator") : "" ?></label>
		<div class="<?php echo $karyawan_edit->RightColumnClass ?>"><div<?php echo $karyawan->divisi->cellAttributes() ?>>
<span id="el_karyawan_divisi">
<input type="text" data-table="karyawan" data-field="x_divisi" name="x_divisi" id="x_divisi" size="30" maxlength="255" placeholder="<?php echo HtmlEncode($karyawan->divisi->getPlaceHolder()) ?>" value="<?php echo $karyawan->divisi->EditValue ?>"<?php echo $karyawan->divisi->editAttributes() ?>>
</span>
<?php echo $karyawan->divisi->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($karyawan->jabatan->Visible) { // jabatan ?>
	<div id="r_jabatan" class="form-group row">
		<label id="elh_karyawan_jabatan" for="x_jabatan" class="<?php echo $karyawan_edit->LeftColumnClass ?>"><?php echo $karyawan->jabatan->caption() ?><?php echo ($karyawan->jabatan->Required) ? $Language->phrase("FieldRequiredIndicator") : "" ?></label>
		<div class="<?php echo $karyawan_edit->RightColumnClass ?>"><div<?php echo $karyawan->jabatan->cellAttributes() ?>>
<span id="el_karyawan_jabatan">
<input type="text" data-table="karyawan" data-field="x_jabatan" name="x_jabatan" id="x_jabatan" size="30" maxlength="255" placeholder="<?php echo HtmlEncode($karyawan->jabatan->getPlaceHolder()) ?>" value="<?php echo $karyawan->jabatan->EditValue ?>"<?php echo $karyawan->jabatan->editAttributes() ?>>
</span>
<?php echo $karyawan->jabatan->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($karyawan->atasan_langsung->Visible) { // atasan_langsung ?>
	<div id="r_atasan_langsung" class="form-group row">
		<label id="elh_karyawan_atasan_langsung" for="x_atasan_langsung" class="<?php echo $karyawan_edit->LeftColumnClass ?>"><?php echo $karyawan->atasan_langsung->caption() ?><?php echo ($karyawan->atasan_langsung->Required) ? $Language->phrase("FieldRequiredIndicator") : "" ?></label>
		<div class="<?php echo $karyawan_edit->RightColumnClass ?>"><div<?php echo $karyawan->atasan_langsung->cellAttributes() ?>>
<span id="el_karyawan_atasan_langsung">
<input type="text" data-table="karyawan" data-field="x_atasan_langsung" name="x_atasan_langsung" id="x_atasan_langsung" size="30" maxlength="255" placeholder="<?php echo HtmlEncode($karyawan->atasan_langsung->getPlaceHolder()) ?>" value="<?php echo $karyawan->atasan_langsung->EditValue ?>"<?php echo $karyawan->atasan_langsung->editAttributes() ?>>
</span>
<?php echo $karyawan->atasan_langsung->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($karyawan->nomor_telepon->Visible) { // nomor_telepon ?>
	<div id="r_nomor_telepon" class="form-group row">
		<label id="elh_karyawan_nomor_telepon" for="x_nomor_telepon" class="<?php echo $karyawan_edit->LeftColumnClass ?>"><?php echo $karyawan->nomor_telepon->caption() ?><?php echo ($karyawan->nomor_telepon->Required) ? $Language->phrase("FieldRequiredIndicator") : "" ?></label>
		<div class="<?php echo $karyawan_edit->RightColumnClass ?>"><div<?php echo $karyawan->nomor_telepon->cellAttributes() ?>>
<span id="el_karyawan_nomor_telepon">
<input type="text" data-table="karyawan" data-field="x_nomor_telepon" name="x_nomor_telepon" id="x_nomor_telepon" size="30" maxlength="20" placeholder="<?php echo HtmlEncode($karyawan->nomor_telepon->getPlaceHolder()) ?>" value="<?php echo $karyawan->nomor_telepon->EditValue ?>"<?php echo $karyawan->nomor_telepon->editAttributes() ?>>
</span>
<?php echo $karyawan->nomor_telepon->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($karyawan->_email->Visible) { // email ?>
	<div id="r__email" class="form-group row">
		<label id="elh_karyawan__email" for="x__email" class="<?php echo $karyawan_edit->LeftColumnClass ?>"><?php echo $karyawan->_email->caption() ?><?php echo ($karyawan->_email->Required) ? $Language->phrase("FieldRequiredIndicator") : "" ?></label>
		<div class="<?php echo $karyawan_edit->RightColumnClass ?>"><div<?php echo $karyawan->_email->cellAttributes() ?>>
<span id="el_karyawan__email">
<input type="text" data-table="karyawan" data-field="x__email" name="x__email" id="x__email" size="30" maxlength="50" placeholder="<?php echo HtmlEncode($karyawan->_email->getPlaceHolder()) ?>" value="<?php echo $karyawan->_email->EditValue ?>"<?php echo $karyawan->_email->editAttributes() ?>>
</span>
<?php echo $karyawan->_email->CustomMsg ?></div></div>
	</div>
<?php } ?>
</div><!-- /page* -->
	<input type="hidden" data-table="karyawan" data-field="x_id" name="x_id" id="x_id" value="<?php echo HtmlEncode($karyawan->id->CurrentValue) ?>">
<?php if (!$karyawan_edit->IsModal) { ?>
<div class="form-group row"><!-- buttons .form-group -->
	<div class="<?php echo $karyawan_edit->OffsetColumnClass ?>"><!-- buttons offset -->
<button class="btn btn-primary ew-btn" name="btn-action" id="btn-action" type="submit"><?php echo $Language->phrase("SaveBtn") ?></button>
<button class="btn btn-default ew-btn" name="btn-cancel" id="btn-cancel" type="button" data-href="<?php echo $karyawan_edit->getReturnUrl() ?>"><?php echo $Language->phrase("CancelBtn") ?></button>
	</div><!-- /buttons offset -->
</div><!-- /buttons .form-group -->
<?php } ?>
</form>
<?php
$karyawan_edit->showPageFooter();
if (DEBUG_ENABLED)
	echo GetDebugMessage();
?>
<script>

// Write your table-specific startup script here
// document.write("page loaded");

</script>
<?php include_once "footer.php" ?>
<?php
$karyawan_edit->terminate();
?>