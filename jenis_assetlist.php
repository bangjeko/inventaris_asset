<?php
namespace PHPMaker2019\inventaris_assets;

// Session
if (session_status() !== PHP_SESSION_ACTIVE)
	session_start(); // Init session data

// Output buffering
ob_start(); 

// Autoload
include_once "autoload.php";
?>
<?php

// Write header
WriteHeader(FALSE);

// Create page object
$jenis_asset_list = new jenis_asset_list();

// Run the page
$jenis_asset_list->run();

// Setup login status
SetupLoginStatus();
SetClientVar("login", LoginStatus());

// Global Page Rendering event (in userfn*.php)
Page_Rendering();

// Page Rendering event
$jenis_asset_list->Page_Render();
?>
<?php include_once "header.php" ?>
<?php if (!$jenis_asset->isExport()) { ?>
<script>

// Form object
currentPageID = ew.PAGE_ID = "list";
var fjenis_assetlist = currentForm = new ew.Form("fjenis_assetlist", "list");
fjenis_assetlist.formKeyCountName = '<?php echo $jenis_asset_list->FormKeyCountName ?>';

// Form_CustomValidate event
fjenis_assetlist.Form_CustomValidate = function(fobj) { // DO NOT CHANGE THIS LINE!

	// Your custom validation code here, return false if invalid.
	return true;
}

// Use JavaScript validation or not
fjenis_assetlist.validateRequired = <?php echo json_encode(CLIENT_VALIDATE) ?>;

// Dynamic selection lists
// Form object for search

var fjenis_assetlistsrch = currentSearchForm = new ew.Form("fjenis_assetlistsrch");

// Filters
fjenis_assetlistsrch.filterList = <?php echo $jenis_asset_list->getFilterList() ?>;

// Init search panel as collapsed
fjenis_assetlistsrch.initSearchPanel = true;
</script>
<script>

// Write your client script here, no need to add script tags.
</script>
<?php } ?>
<?php if (!$jenis_asset->isExport()) { ?>
<div class="btn-toolbar ew-toolbar">
<?php if ($jenis_asset_list->TotalRecs > 0 && $jenis_asset_list->ExportOptions->visible()) { ?>
<?php $jenis_asset_list->ExportOptions->render("body") ?>
<?php } ?>
<?php if ($jenis_asset_list->ImportOptions->visible()) { ?>
<?php $jenis_asset_list->ImportOptions->render("body") ?>
<?php } ?>
<?php if ($jenis_asset_list->SearchOptions->visible()) { ?>
<?php $jenis_asset_list->SearchOptions->render("body") ?>
<?php } ?>
<?php if ($jenis_asset_list->FilterOptions->visible()) { ?>
<?php $jenis_asset_list->FilterOptions->render("body") ?>
<?php } ?>
<div class="clearfix"></div>
</div>
<?php } ?>
<?php
$jenis_asset_list->renderOtherOptions();
?>
<?php if ($Security->CanSearch()) { ?>
<?php if (!$jenis_asset->isExport() && !$jenis_asset->CurrentAction) { ?>
<form name="fjenis_assetlistsrch" id="fjenis_assetlistsrch" class="form-inline ew-form ew-ext-search-form" action="<?php echo CurrentPageName() ?>">
<?php $searchPanelClass = ($jenis_asset_list->SearchWhere <> "") ? " show" : ""; ?>
<div id="fjenis_assetlistsrch-search-panel" class="ew-search-panel collapse<?php echo $searchPanelClass ?>">
<input type="hidden" name="cmd" value="search">
<input type="hidden" name="t" value="jenis_asset">
	<div class="ew-basic-search">
<div id="xsr_1" class="ew-row d-sm-flex">
	<div class="ew-quick-search input-group">
		<input type="text" name="<?php echo TABLE_BASIC_SEARCH ?>" id="<?php echo TABLE_BASIC_SEARCH ?>" class="form-control" value="<?php echo HtmlEncode($jenis_asset_list->BasicSearch->getKeyword()) ?>" placeholder="<?php echo HtmlEncode($Language->phrase("Search")) ?>">
		<input type="hidden" name="<?php echo TABLE_BASIC_SEARCH_TYPE ?>" id="<?php echo TABLE_BASIC_SEARCH_TYPE ?>" value="<?php echo HtmlEncode($jenis_asset_list->BasicSearch->getType()) ?>">
		<div class="input-group-append">
			<button class="btn btn-primary" name="btn-submit" id="btn-submit" type="submit"><?php echo $Language->phrase("SearchBtn") ?></button>
			<button type="button" data-toggle="dropdown" class="btn btn-primary dropdown-toggle dropdown-toggle-split" aria-haspopup="true" aria-expanded="false"><span id="searchtype"><?php echo $jenis_asset_list->BasicSearch->getTypeNameShort() ?></span></button>
			<div class="dropdown-menu dropdown-menu-right">
				<a class="dropdown-item<?php if ($jenis_asset_list->BasicSearch->getType() == "") echo " active"; ?>" href="javascript:void(0);" onclick="ew.setSearchType(this)"><?php echo $Language->phrase("QuickSearchAuto") ?></a>
				<a class="dropdown-item<?php if ($jenis_asset_list->BasicSearch->getType() == "=") echo " active"; ?>" href="javascript:void(0);" onclick="ew.setSearchType(this,'=')"><?php echo $Language->phrase("QuickSearchExact") ?></a>
				<a class="dropdown-item<?php if ($jenis_asset_list->BasicSearch->getType() == "AND") echo " active"; ?>" href="javascript:void(0);" onclick="ew.setSearchType(this,'AND')"><?php echo $Language->phrase("QuickSearchAll") ?></a>
				<a class="dropdown-item<?php if ($jenis_asset_list->BasicSearch->getType() == "OR") echo " active"; ?>" href="javascript:void(0);" onclick="ew.setSearchType(this,'OR')"><?php echo $Language->phrase("QuickSearchAny") ?></a>
			</div>
		</div>
	</div>
</div>
	</div>
</div>
</form>
<?php } ?>
<?php } ?>
<?php $jenis_asset_list->showPageHeader(); ?>
<?php
$jenis_asset_list->showMessage();
?>
<?php if ($jenis_asset_list->TotalRecs > 0 || $jenis_asset->CurrentAction) { ?>
<div class="card ew-card ew-grid<?php if ($jenis_asset_list->isAddOrEdit()) { ?> ew-grid-add-edit<?php } ?> jenis_asset">
<?php if (!$jenis_asset->isExport()) { ?>
<div class="card-header ew-grid-upper-panel">
<?php if (!$jenis_asset->isGridAdd()) { ?>
<form name="ew-pager-form" class="form-inline ew-form ew-pager-form" action="<?php echo CurrentPageName() ?>">
<?php if (!isset($jenis_asset_list->Pager)) $jenis_asset_list->Pager = new PrevNextPager($jenis_asset_list->StartRec, $jenis_asset_list->DisplayRecs, $jenis_asset_list->TotalRecs, $jenis_asset_list->AutoHidePager) ?>
<?php if ($jenis_asset_list->Pager->RecordCount > 0 && $jenis_asset_list->Pager->Visible) { ?>
<div class="ew-pager">
<span><?php echo $Language->Phrase("Page") ?>&nbsp;</span>
<div class="ew-prev-next"><div class="input-group input-group-sm">
<div class="input-group-prepend">
<!-- first page button -->
	<?php if ($jenis_asset_list->Pager->FirstButton->Enabled) { ?>
	<a class="btn btn-default" title="<?php echo $Language->phrase("PagerFirst") ?>" href="<?php echo $jenis_asset_list->pageUrl() ?>start=<?php echo $jenis_asset_list->Pager->FirstButton->Start ?>"><i class="icon-first ew-icon"></i></a>
	<?php } else { ?>
	<a class="btn btn-default disabled" title="<?php echo $Language->phrase("PagerFirst") ?>"><i class="icon-first ew-icon"></i></a>
	<?php } ?>
<!-- previous page button -->
	<?php if ($jenis_asset_list->Pager->PrevButton->Enabled) { ?>
	<a class="btn btn-default" title="<?php echo $Language->phrase("PagerPrevious") ?>" href="<?php echo $jenis_asset_list->pageUrl() ?>start=<?php echo $jenis_asset_list->Pager->PrevButton->Start ?>"><i class="icon-prev ew-icon"></i></a>
	<?php } else { ?>
	<a class="btn btn-default disabled" title="<?php echo $Language->phrase("PagerPrevious") ?>"><i class="icon-prev ew-icon"></i></a>
	<?php } ?>
</div>
<!-- current page number -->
	<input class="form-control" type="text" name="<?php echo TABLE_PAGE_NO ?>" value="<?php echo $jenis_asset_list->Pager->CurrentPage ?>">
<div class="input-group-append">
<!-- next page button -->
	<?php if ($jenis_asset_list->Pager->NextButton->Enabled) { ?>
	<a class="btn btn-default" title="<?php echo $Language->phrase("PagerNext") ?>" href="<?php echo $jenis_asset_list->pageUrl() ?>start=<?php echo $jenis_asset_list->Pager->NextButton->Start ?>"><i class="icon-next ew-icon"></i></a>
	<?php } else { ?>
	<a class="btn btn-default disabled" title="<?php echo $Language->phrase("PagerNext") ?>"><i class="icon-next ew-icon"></i></a>
	<?php } ?>
<!-- last page button -->
	<?php if ($jenis_asset_list->Pager->LastButton->Enabled) { ?>
	<a class="btn btn-default" title="<?php echo $Language->phrase("PagerLast") ?>" href="<?php echo $jenis_asset_list->pageUrl() ?>start=<?php echo $jenis_asset_list->Pager->LastButton->Start ?>"><i class="icon-last ew-icon"></i></a>
	<?php } else { ?>
	<a class="btn btn-default disabled" title="<?php echo $Language->phrase("PagerLast") ?>"><i class="icon-last ew-icon"></i></a>
	<?php } ?>
</div>
</div>
</div>
<span>&nbsp;<?php echo $Language->Phrase("of") ?>&nbsp;<?php echo $jenis_asset_list->Pager->PageCount ?></span>
<div class="clearfix"></div>
</div>
<?php } ?>
<?php if ($jenis_asset_list->Pager->RecordCount > 0) { ?>
<div class="ew-pager ew-rec">
	<span><?php echo $Language->Phrase("Record") ?>&nbsp;<?php echo $jenis_asset_list->Pager->FromIndex ?>&nbsp;<?php echo $Language->Phrase("To") ?>&nbsp;<?php echo $jenis_asset_list->Pager->ToIndex ?>&nbsp;<?php echo $Language->Phrase("Of") ?>&nbsp;<?php echo $jenis_asset_list->Pager->RecordCount ?></span>
</div>
<?php } ?>
<?php if ($jenis_asset_list->TotalRecs > 0 && (!$jenis_asset_list->AutoHidePageSizeSelector || $jenis_asset_list->Pager->Visible)) { ?>
<div class="ew-pager">
<input type="hidden" name="t" value="jenis_asset">
<select name="<?php echo TABLE_REC_PER_PAGE ?>" class="form-control form-control-sm ew-tooltip" title="<?php echo $Language->phrase("RecordsPerPage") ?>" onchange="this.form.submit();">
<option value="5"<?php if ($jenis_asset_list->DisplayRecs == 5) { ?> selected<?php } ?>>5</option>
<option value="10"<?php if ($jenis_asset_list->DisplayRecs == 10) { ?> selected<?php } ?>>10</option>
<option value="20"<?php if ($jenis_asset_list->DisplayRecs == 20) { ?> selected<?php } ?>>20</option>
<option value="50"<?php if ($jenis_asset_list->DisplayRecs == 50) { ?> selected<?php } ?>>50</option>
<option value="100"<?php if ($jenis_asset_list->DisplayRecs == 100) { ?> selected<?php } ?>>100</option>
</select>
</div>
<?php } ?>
</form>
<?php } ?>
<div class="ew-list-other-options">
<?php $jenis_asset_list->OtherOptions->render("body") ?>
</div>
<div class="clearfix"></div>
</div>
<?php } ?>
<form name="fjenis_assetlist" id="fjenis_assetlist" class="form-inline ew-form ew-list-form" action="<?php echo CurrentPageName() ?>" method="post">
<?php if ($jenis_asset_list->CheckToken) { ?>
<input type="hidden" name="<?php echo TOKEN_NAME ?>" value="<?php echo $jenis_asset_list->Token ?>">
<?php } ?>
<input type="hidden" name="t" value="jenis_asset">
<div id="gmp_jenis_asset" class="<?php if (IsResponsiveLayout()) { ?>table-responsive <?php } ?>card-body ew-grid-middle-panel">
<?php if ($jenis_asset_list->TotalRecs > 0 || $jenis_asset->isGridEdit()) { ?>
<table id="tbl_jenis_assetlist" class="table ew-table"><!-- .ew-table ##-->
<thead>
	<tr class="ew-table-header">
<?php

// Header row
$jenis_asset_list->RowType = ROWTYPE_HEADER;

// Render list options
$jenis_asset_list->renderListOptions();

// Render list options (header, left)
$jenis_asset_list->ListOptions->render("header", "left");
?>
<?php if ($jenis_asset->nama_jenis->Visible) { // nama_jenis ?>
	<?php if ($jenis_asset->sortUrl($jenis_asset->nama_jenis) == "") { ?>
		<th data-name="nama_jenis" class="<?php echo $jenis_asset->nama_jenis->headerCellClass() ?>"><div id="elh_jenis_asset_nama_jenis" class="jenis_asset_nama_jenis"><div class="ew-table-header-caption"><?php echo $jenis_asset->nama_jenis->caption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="nama_jenis" class="<?php echo $jenis_asset->nama_jenis->headerCellClass() ?>"><div class="ew-pointer" onclick="ew.sort(event,'<?php echo $jenis_asset->SortUrl($jenis_asset->nama_jenis) ?>',1);"><div id="elh_jenis_asset_nama_jenis" class="jenis_asset_nama_jenis">
			<div class="ew-table-header-btn"><span class="ew-table-header-caption"><?php echo $jenis_asset->nama_jenis->caption() ?><?php echo $Language->phrase("SrchLegend") ?></span><span class="ew-table-header-sort"><?php if ($jenis_asset->nama_jenis->getSort() == "ASC") { ?><i class="fa fa-sort-up"></i><?php } elseif ($jenis_asset->nama_jenis->getSort() == "DESC") { ?><i class="fa fa-sort-down"></i><?php } ?></span></div>
		</div></div></th>
	<?php } ?>
<?php } ?>
<?php

// Render list options (header, right)
$jenis_asset_list->ListOptions->render("header", "right");
?>
	</tr>
</thead>
<tbody>
<?php
if ($jenis_asset->ExportAll && $jenis_asset->isExport()) {
	$jenis_asset_list->StopRec = $jenis_asset_list->TotalRecs;
} else {

	// Set the last record to display
	if ($jenis_asset_list->TotalRecs > $jenis_asset_list->StartRec + $jenis_asset_list->DisplayRecs - 1)
		$jenis_asset_list->StopRec = $jenis_asset_list->StartRec + $jenis_asset_list->DisplayRecs - 1;
	else
		$jenis_asset_list->StopRec = $jenis_asset_list->TotalRecs;
}
$jenis_asset_list->RecCnt = $jenis_asset_list->StartRec - 1;
if ($jenis_asset_list->Recordset && !$jenis_asset_list->Recordset->EOF) {
	$jenis_asset_list->Recordset->moveFirst();
	$selectLimit = $jenis_asset_list->UseSelectLimit;
	if (!$selectLimit && $jenis_asset_list->StartRec > 1)
		$jenis_asset_list->Recordset->move($jenis_asset_list->StartRec - 1);
} elseif (!$jenis_asset->AllowAddDeleteRow && $jenis_asset_list->StopRec == 0) {
	$jenis_asset_list->StopRec = $jenis_asset->GridAddRowCount;
}

// Initialize aggregate
$jenis_asset->RowType = ROWTYPE_AGGREGATEINIT;
$jenis_asset->resetAttributes();
$jenis_asset_list->renderRow();
while ($jenis_asset_list->RecCnt < $jenis_asset_list->StopRec) {
	$jenis_asset_list->RecCnt++;
	if ($jenis_asset_list->RecCnt >= $jenis_asset_list->StartRec) {
		$jenis_asset_list->RowCnt++;

		// Set up key count
		$jenis_asset_list->KeyCount = $jenis_asset_list->RowIndex;

		// Init row class and style
		$jenis_asset->resetAttributes();
		$jenis_asset->CssClass = "";
		if ($jenis_asset->isGridAdd()) {
		} else {
			$jenis_asset_list->loadRowValues($jenis_asset_list->Recordset); // Load row values
		}
		$jenis_asset->RowType = ROWTYPE_VIEW; // Render view

		// Set up row id / data-rowindex
		$jenis_asset->RowAttrs = array_merge($jenis_asset->RowAttrs, array('data-rowindex'=>$jenis_asset_list->RowCnt, 'id'=>'r' . $jenis_asset_list->RowCnt . '_jenis_asset', 'data-rowtype'=>$jenis_asset->RowType));

		// Render row
		$jenis_asset_list->renderRow();

		// Render list options
		$jenis_asset_list->renderListOptions();
?>
	<tr<?php echo $jenis_asset->rowAttributes() ?>>
<?php

// Render list options (body, left)
$jenis_asset_list->ListOptions->render("body", "left", $jenis_asset_list->RowCnt);
?>
	<?php if ($jenis_asset->nama_jenis->Visible) { // nama_jenis ?>
		<td data-name="nama_jenis"<?php echo $jenis_asset->nama_jenis->cellAttributes() ?>>
<span id="el<?php echo $jenis_asset_list->RowCnt ?>_jenis_asset_nama_jenis" class="jenis_asset_nama_jenis">
<span<?php echo $jenis_asset->nama_jenis->viewAttributes() ?>>
<?php echo $jenis_asset->nama_jenis->getViewValue() ?></span>
</span>
</td>
	<?php } ?>
<?php

// Render list options (body, right)
$jenis_asset_list->ListOptions->render("body", "right", $jenis_asset_list->RowCnt);
?>
	</tr>
<?php
	}
	if (!$jenis_asset->isGridAdd())
		$jenis_asset_list->Recordset->moveNext();
}
?>
</tbody>
</table><!-- /.ew-table -->
<?php } ?>
<?php if (!$jenis_asset->CurrentAction) { ?>
<input type="hidden" name="action" id="action" value="">
<?php } ?>
</div><!-- /.ew-grid-middle-panel -->
</form><!-- /.ew-list-form -->
<?php

// Close recordset
if ($jenis_asset_list->Recordset)
	$jenis_asset_list->Recordset->Close();
?>
</div><!-- /.ew-grid -->
<?php } ?>
<?php if ($jenis_asset_list->TotalRecs == 0 && !$jenis_asset->CurrentAction) { // Show other options ?>
<div class="ew-list-other-options">
<?php $jenis_asset_list->OtherOptions->render("body") ?>
</div>
<div class="clearfix"></div>
<?php } ?>
<?php
$jenis_asset_list->showPageFooter();
if (DEBUG_ENABLED)
	echo GetDebugMessage();
?>
<?php if (!$jenis_asset->isExport()) { ?>
<script>

// Write your table-specific startup script here
// document.write("page loaded");

</script>
<?php } ?>
<?php include_once "footer.php" ?>
<?php
$jenis_asset_list->terminate();
?>