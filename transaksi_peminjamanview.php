<?php
namespace PHPMaker2019\inventaris_assets;

// Session
if (session_status() !== PHP_SESSION_ACTIVE)
	session_start(); // Init session data

// Output buffering
ob_start(); 

// Autoload
include_once "autoload.php";
?>
<?php

// Write header
WriteHeader(FALSE);

// Create page object
$transaksi_peminjaman_view = new transaksi_peminjaman_view();

// Run the page
$transaksi_peminjaman_view->run();

// Setup login status
SetupLoginStatus();
SetClientVar("login", LoginStatus());

// Global Page Rendering event (in userfn*.php)
Page_Rendering();

// Page Rendering event
$transaksi_peminjaman_view->Page_Render();
?>
<?php include_once "header.php" ?>
<?php if (!$transaksi_peminjaman->isExport()) { ?>
<script>

// Form object
currentPageID = ew.PAGE_ID = "view";
var ftransaksi_peminjamanview = currentForm = new ew.Form("ftransaksi_peminjamanview", "view");

// Form_CustomValidate event
ftransaksi_peminjamanview.Form_CustomValidate = function(fobj) { // DO NOT CHANGE THIS LINE!

	// Your custom validation code here, return false if invalid.
	return true;
}

// Use JavaScript validation or not
ftransaksi_peminjamanview.validateRequired = <?php echo json_encode(CLIENT_VALIDATE) ?>;

// Dynamic selection lists
ftransaksi_peminjamanview.lists["x_id_karyawan"] = <?php echo $transaksi_peminjaman_view->id_karyawan->Lookup->toClientList() ?>;
ftransaksi_peminjamanview.lists["x_id_karyawan"].options = <?php echo JsonEncode($transaksi_peminjaman_view->id_karyawan->lookupOptions()) ?>;
ftransaksi_peminjamanview.lists["x_id_asset"] = <?php echo $transaksi_peminjaman_view->id_asset->Lookup->toClientList() ?>;
ftransaksi_peminjamanview.lists["x_id_asset"].options = <?php echo JsonEncode($transaksi_peminjaman_view->id_asset->lookupOptions()) ?>;
ftransaksi_peminjamanview.lists["x_created_by"] = <?php echo $transaksi_peminjaman_view->created_by->Lookup->toClientList() ?>;
ftransaksi_peminjamanview.lists["x_created_by"].options = <?php echo JsonEncode($transaksi_peminjaman_view->created_by->lookupOptions()) ?>;

// Form object for search
</script>
<script>

// Write your client script here, no need to add script tags.
</script>
<?php } ?>
<?php if (!$transaksi_peminjaman->isExport()) { ?>
<div class="btn-toolbar ew-toolbar">
<?php $transaksi_peminjaman_view->ExportOptions->render("body") ?>
<?php $transaksi_peminjaman_view->OtherOptions->render("body") ?>
<div class="clearfix"></div>
</div>
<?php } ?>
<?php $transaksi_peminjaman_view->showPageHeader(); ?>
<?php
$transaksi_peminjaman_view->showMessage();
?>
<form name="ftransaksi_peminjamanview" id="ftransaksi_peminjamanview" class="form-inline ew-form ew-view-form" action="<?php echo CurrentPageName() ?>" method="post">
<?php if ($transaksi_peminjaman_view->CheckToken) { ?>
<input type="hidden" name="<?php echo TOKEN_NAME ?>" value="<?php echo $transaksi_peminjaman_view->Token ?>">
<?php } ?>
<input type="hidden" name="t" value="transaksi_peminjaman">
<input type="hidden" name="modal" value="<?php echo (int)$transaksi_peminjaman_view->IsModal ?>">
<table class="table table-striped table-sm ew-view-table">
<?php if ($transaksi_peminjaman->id_karyawan->Visible) { // id_karyawan ?>
	<tr id="r_id_karyawan">
		<td class="<?php echo $transaksi_peminjaman_view->TableLeftColumnClass ?>"><span id="elh_transaksi_peminjaman_id_karyawan"><?php echo $transaksi_peminjaman->id_karyawan->caption() ?></span></td>
		<td data-name="id_karyawan"<?php echo $transaksi_peminjaman->id_karyawan->cellAttributes() ?>>
<span id="el_transaksi_peminjaman_id_karyawan">
<span<?php echo $transaksi_peminjaman->id_karyawan->viewAttributes() ?>>
<?php echo $transaksi_peminjaman->id_karyawan->getViewValue() ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($transaksi_peminjaman->id_asset->Visible) { // id_asset ?>
	<tr id="r_id_asset">
		<td class="<?php echo $transaksi_peminjaman_view->TableLeftColumnClass ?>"><span id="elh_transaksi_peminjaman_id_asset"><?php echo $transaksi_peminjaman->id_asset->caption() ?></span></td>
		<td data-name="id_asset"<?php echo $transaksi_peminjaman->id_asset->cellAttributes() ?>>
<span id="el_transaksi_peminjaman_id_asset">
<span<?php echo $transaksi_peminjaman->id_asset->viewAttributes() ?>>
<?php echo $transaksi_peminjaman->id_asset->getViewValue() ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($transaksi_peminjaman->tanggal_pinjam->Visible) { // tanggal_pinjam ?>
	<tr id="r_tanggal_pinjam">
		<td class="<?php echo $transaksi_peminjaman_view->TableLeftColumnClass ?>"><span id="elh_transaksi_peminjaman_tanggal_pinjam"><?php echo $transaksi_peminjaman->tanggal_pinjam->caption() ?></span></td>
		<td data-name="tanggal_pinjam"<?php echo $transaksi_peminjaman->tanggal_pinjam->cellAttributes() ?>>
<span id="el_transaksi_peminjaman_tanggal_pinjam">
<span<?php echo $transaksi_peminjaman->tanggal_pinjam->viewAttributes() ?>>
<?php echo $transaksi_peminjaman->tanggal_pinjam->getViewValue() ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($transaksi_peminjaman->tanggal_kembali->Visible) { // tanggal_kembali ?>
	<tr id="r_tanggal_kembali">
		<td class="<?php echo $transaksi_peminjaman_view->TableLeftColumnClass ?>"><span id="elh_transaksi_peminjaman_tanggal_kembali"><?php echo $transaksi_peminjaman->tanggal_kembali->caption() ?></span></td>
		<td data-name="tanggal_kembali"<?php echo $transaksi_peminjaman->tanggal_kembali->cellAttributes() ?>>
<span id="el_transaksi_peminjaman_tanggal_kembali">
<span<?php echo $transaksi_peminjaman->tanggal_kembali->viewAttributes() ?>>
<?php echo $transaksi_peminjaman->tanggal_kembali->getViewValue() ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($transaksi_peminjaman->kelengkapan_asset->Visible) { // kelengkapan_asset ?>
	<tr id="r_kelengkapan_asset">
		<td class="<?php echo $transaksi_peminjaman_view->TableLeftColumnClass ?>"><span id="elh_transaksi_peminjaman_kelengkapan_asset"><?php echo $transaksi_peminjaman->kelengkapan_asset->caption() ?></span></td>
		<td data-name="kelengkapan_asset"<?php echo $transaksi_peminjaman->kelengkapan_asset->cellAttributes() ?>>
<span id="el_transaksi_peminjaman_kelengkapan_asset">
<span<?php echo $transaksi_peminjaman->kelengkapan_asset->viewAttributes() ?>>
<?php echo $transaksi_peminjaman->kelengkapan_asset->getViewValue() ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($transaksi_peminjaman->keterangan->Visible) { // keterangan ?>
	<tr id="r_keterangan">
		<td class="<?php echo $transaksi_peminjaman_view->TableLeftColumnClass ?>"><span id="elh_transaksi_peminjaman_keterangan"><?php echo $transaksi_peminjaman->keterangan->caption() ?></span></td>
		<td data-name="keterangan"<?php echo $transaksi_peminjaman->keterangan->cellAttributes() ?>>
<span id="el_transaksi_peminjaman_keterangan">
<span<?php echo $transaksi_peminjaman->keterangan->viewAttributes() ?>>
<?php echo $transaksi_peminjaman->keterangan->getViewValue() ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($transaksi_peminjaman->created_by->Visible) { // created_by ?>
	<tr id="r_created_by">
		<td class="<?php echo $transaksi_peminjaman_view->TableLeftColumnClass ?>"><span id="elh_transaksi_peminjaman_created_by"><?php echo $transaksi_peminjaman->created_by->caption() ?></span></td>
		<td data-name="created_by"<?php echo $transaksi_peminjaman->created_by->cellAttributes() ?>>
<span id="el_transaksi_peminjaman_created_by">
<span<?php echo $transaksi_peminjaman->created_by->viewAttributes() ?>>
<?php echo $transaksi_peminjaman->created_by->getViewValue() ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($transaksi_peminjaman->created_date->Visible) { // created_date ?>
	<tr id="r_created_date">
		<td class="<?php echo $transaksi_peminjaman_view->TableLeftColumnClass ?>"><span id="elh_transaksi_peminjaman_created_date"><?php echo $transaksi_peminjaman->created_date->caption() ?></span></td>
		<td data-name="created_date"<?php echo $transaksi_peminjaman->created_date->cellAttributes() ?>>
<span id="el_transaksi_peminjaman_created_date">
<span<?php echo $transaksi_peminjaman->created_date->viewAttributes() ?>>
<?php echo $transaksi_peminjaman->created_date->getViewValue() ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($transaksi_peminjaman->cetak->Visible) { // cetak ?>
	<tr id="r_cetak">
		<td class="<?php echo $transaksi_peminjaman_view->TableLeftColumnClass ?>"><span id="elh_transaksi_peminjaman_cetak"><?php echo $transaksi_peminjaman->cetak->caption() ?></span></td>
		<td data-name="cetak"<?php echo $transaksi_peminjaman->cetak->cellAttributes() ?>>
<span id="el_transaksi_peminjaman_cetak">
<span<?php echo $transaksi_peminjaman->cetak->viewAttributes() ?>>
<?php echo $transaksi_peminjaman->cetak->getViewValue() ?></span>
</span>
</td>
	</tr>
<?php } ?>
</table>
</form>
<?php
$transaksi_peminjaman_view->showPageFooter();
if (DEBUG_ENABLED)
	echo GetDebugMessage();
?>
<?php if (!$transaksi_peminjaman->isExport()) { ?>
<script>

// Write your table-specific startup script here
// document.write("page loaded");

</script>
<?php } ?>
<?php include_once "footer.php" ?>
<?php
$transaksi_peminjaman_view->terminate();
?>