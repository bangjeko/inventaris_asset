<?php
namespace PHPMaker2019\inventaris_assets;

// Session
if (session_status() !== PHP_SESSION_ACTIVE)
	session_start(); // Init session data

// Output buffering
ob_start(); 

// Autoload
include_once "autoload.php";
?>
<?php

// Write header
WriteHeader(FALSE);

// Create page object
$transaksi_pengembalian_list = new transaksi_pengembalian_list();

// Run the page
$transaksi_pengembalian_list->run();

// Setup login status
SetupLoginStatus();
SetClientVar("login", LoginStatus());

// Global Page Rendering event (in userfn*.php)
Page_Rendering();

// Page Rendering event
$transaksi_pengembalian_list->Page_Render();
?>
<?php include_once "header.php" ?>
<?php if (!$transaksi_pengembalian->isExport()) { ?>
<script>

// Form object
currentPageID = ew.PAGE_ID = "list";
var ftransaksi_pengembalianlist = currentForm = new ew.Form("ftransaksi_pengembalianlist", "list");
ftransaksi_pengembalianlist.formKeyCountName = '<?php echo $transaksi_pengembalian_list->FormKeyCountName ?>';

// Form_CustomValidate event
ftransaksi_pengembalianlist.Form_CustomValidate = function(fobj) { // DO NOT CHANGE THIS LINE!

	// Your custom validation code here, return false if invalid.
	return true;
}

// Use JavaScript validation or not
ftransaksi_pengembalianlist.validateRequired = <?php echo json_encode(CLIENT_VALIDATE) ?>;

// Dynamic selection lists
ftransaksi_pengembalianlist.lists["x_id_karyawan"] = <?php echo $transaksi_pengembalian_list->id_karyawan->Lookup->toClientList() ?>;
ftransaksi_pengembalianlist.lists["x_id_karyawan"].options = <?php echo JsonEncode($transaksi_pengembalian_list->id_karyawan->lookupOptions()) ?>;
ftransaksi_pengembalianlist.lists["x_id_asset"] = <?php echo $transaksi_pengembalian_list->id_asset->Lookup->toClientList() ?>;
ftransaksi_pengembalianlist.lists["x_id_asset"].options = <?php echo JsonEncode($transaksi_pengembalian_list->id_asset->lookupOptions()) ?>;
ftransaksi_pengembalianlist.lists["x_created_by"] = <?php echo $transaksi_pengembalian_list->created_by->Lookup->toClientList() ?>;
ftransaksi_pengembalianlist.lists["x_created_by"].options = <?php echo JsonEncode($transaksi_pengembalian_list->created_by->lookupOptions()) ?>;

// Form object for search
var ftransaksi_pengembalianlistsrch = currentSearchForm = new ew.Form("ftransaksi_pengembalianlistsrch");

// Validate function for search
ftransaksi_pengembalianlistsrch.validate = function(fobj) {
	if (!this.validateRequired)
		return true; // Ignore validation
	fobj = fobj || this._form;
	var infix = "";
	elm = this.getElements("x" + infix + "_tanggal_kembali");
	if (elm && !ew.checkDateDef(elm.value))
		return this.onError(elm, "<?php echo JsEncode($transaksi_pengembalian->tanggal_kembali->errorMessage()) ?>");

	// Fire Form_CustomValidate event
	if (!this.Form_CustomValidate(fobj))
		return false;
	return true;
}

// Form_CustomValidate event
ftransaksi_pengembalianlistsrch.Form_CustomValidate = function(fobj) { // DO NOT CHANGE THIS LINE!

	// Your custom validation code here, return false if invalid.
	return true;
}

// Use JavaScript validation or not
ftransaksi_pengembalianlistsrch.validateRequired = <?php echo json_encode(CLIENT_VALIDATE) ?>;

// Dynamic selection lists
ftransaksi_pengembalianlistsrch.lists["x_id_karyawan"] = <?php echo $transaksi_pengembalian_list->id_karyawan->Lookup->toClientList() ?>;
ftransaksi_pengembalianlistsrch.lists["x_id_karyawan"].options = <?php echo JsonEncode($transaksi_pengembalian_list->id_karyawan->lookupOptions()) ?>;
ftransaksi_pengembalianlistsrch.lists["x_id_asset"] = <?php echo $transaksi_pengembalian_list->id_asset->Lookup->toClientList() ?>;
ftransaksi_pengembalianlistsrch.lists["x_id_asset"].options = <?php echo JsonEncode($transaksi_pengembalian_list->id_asset->lookupOptions()) ?>;

// Filters
ftransaksi_pengembalianlistsrch.filterList = <?php echo $transaksi_pengembalian_list->getFilterList() ?>;

// Init search panel as collapsed
ftransaksi_pengembalianlistsrch.initSearchPanel = true;
</script>
<script>

// Write your client script here, no need to add script tags.
</script>
<?php } ?>
<?php if (!$transaksi_pengembalian->isExport()) { ?>
<div class="btn-toolbar ew-toolbar">
<?php if ($transaksi_pengembalian_list->TotalRecs > 0 && $transaksi_pengembalian_list->ExportOptions->visible()) { ?>
<?php $transaksi_pengembalian_list->ExportOptions->render("body") ?>
<?php } ?>
<?php if ($transaksi_pengembalian_list->ImportOptions->visible()) { ?>
<?php $transaksi_pengembalian_list->ImportOptions->render("body") ?>
<?php } ?>
<?php if ($transaksi_pengembalian_list->SearchOptions->visible()) { ?>
<?php $transaksi_pengembalian_list->SearchOptions->render("body") ?>
<?php } ?>
<?php if ($transaksi_pengembalian_list->FilterOptions->visible()) { ?>
<?php $transaksi_pengembalian_list->FilterOptions->render("body") ?>
<?php } ?>
<div class="clearfix"></div>
</div>
<?php } ?>
<?php
$transaksi_pengembalian_list->renderOtherOptions();
?>
<?php if ($Security->CanSearch()) { ?>
<?php if (!$transaksi_pengembalian->isExport() && !$transaksi_pengembalian->CurrentAction) { ?>
<form name="ftransaksi_pengembalianlistsrch" id="ftransaksi_pengembalianlistsrch" class="form-inline ew-form ew-ext-search-form" action="<?php echo CurrentPageName() ?>">
<?php $searchPanelClass = ($transaksi_pengembalian_list->SearchWhere <> "") ? " show" : ""; ?>
<div id="ftransaksi_pengembalianlistsrch-search-panel" class="ew-search-panel collapse<?php echo $searchPanelClass ?>">
<input type="hidden" name="cmd" value="search">
<input type="hidden" name="t" value="transaksi_pengembalian">
	<div class="ew-basic-search">
<?php
if ($SearchError == "")
	$transaksi_pengembalian_list->LoadAdvancedSearch(); // Load advanced search

// Render for search
$transaksi_pengembalian->RowType = ROWTYPE_SEARCH;

// Render row
$transaksi_pengembalian->resetAttributes();
$transaksi_pengembalian_list->renderRow();
?>
<div id="xsr_1" class="ew-row d-sm-flex">
<?php if ($transaksi_pengembalian->id_karyawan->Visible) { // id_karyawan ?>
	<div id="xsc_id_karyawan" class="ew-cell form-group">
		<label for="x_id_karyawan" class="ew-search-caption ew-label"><?php echo $transaksi_pengembalian->id_karyawan->caption() ?></label>
		<span class="ew-search-operator"><?php echo $Language->phrase("=") ?><input type="hidden" name="z_id_karyawan" id="z_id_karyawan" value="="></span>
		<span class="ew-search-field">
<div class="input-group ew-lookup-list">
	<div class="form-control ew-lookup-text" tabindex="-1" id="lu_x_id_karyawan"><?php echo strval($transaksi_pengembalian->id_karyawan->AdvancedSearch->ViewValue) == "" ? $Language->phrase("PleaseSelect") : (REMOVE_XSS ? HtmlDecode($transaksi_pengembalian->id_karyawan->AdvancedSearch->ViewValue) : $transaksi_pengembalian->id_karyawan->AdvancedSearch->ViewValue) ?></div>
	<div class="input-group-append">
		<button type="button" title="<?php echo HtmlEncode(str_replace("%s", RemoveHtml($transaksi_pengembalian->id_karyawan->caption()), $Language->phrase("LookupLink", TRUE))) ?>" class="ew-lookup-btn btn btn-default"<?php echo (($transaksi_pengembalian->id_karyawan->ReadOnly || $transaksi_pengembalian->id_karyawan->Disabled) ? " disabled" : "")?> onclick="ew.modalLookupShow({lnk:this,el:'x_id_karyawan',m:0,n:10});"><i class="fa fa-search ew-icon"></i></button>
	</div>
</div>
<?php echo $transaksi_pengembalian->id_karyawan->Lookup->getParamTag("p_x_id_karyawan") ?>
<input type="hidden" data-table="transaksi_pengembalian" data-field="x_id_karyawan" data-multiple="0" data-lookup="1" data-value-separator="<?php echo $transaksi_pengembalian->id_karyawan->displayValueSeparatorAttribute() ?>" name="x_id_karyawan" id="x_id_karyawan" value="<?php echo $transaksi_pengembalian->id_karyawan->AdvancedSearch->SearchValue ?>"<?php echo $transaksi_pengembalian->id_karyawan->editAttributes() ?>>
</span>
	</div>
<?php } ?>
</div>
<div id="xsr_2" class="ew-row d-sm-flex">
<?php if ($transaksi_pengembalian->id_asset->Visible) { // id_asset ?>
	<div id="xsc_id_asset" class="ew-cell form-group">
		<label for="x_id_asset" class="ew-search-caption ew-label"><?php echo $transaksi_pengembalian->id_asset->caption() ?></label>
		<span class="ew-search-operator"><?php echo $Language->phrase("=") ?><input type="hidden" name="z_id_asset" id="z_id_asset" value="="></span>
		<span class="ew-search-field">
<div class="input-group ew-lookup-list">
	<div class="form-control ew-lookup-text" tabindex="-1" id="lu_x_id_asset"><?php echo strval($transaksi_pengembalian->id_asset->AdvancedSearch->ViewValue) == "" ? $Language->phrase("PleaseSelect") : (REMOVE_XSS ? HtmlDecode($transaksi_pengembalian->id_asset->AdvancedSearch->ViewValue) : $transaksi_pengembalian->id_asset->AdvancedSearch->ViewValue) ?></div>
	<div class="input-group-append">
		<button type="button" title="<?php echo HtmlEncode(str_replace("%s", RemoveHtml($transaksi_pengembalian->id_asset->caption()), $Language->phrase("LookupLink", TRUE))) ?>" class="ew-lookup-btn btn btn-default"<?php echo (($transaksi_pengembalian->id_asset->ReadOnly || $transaksi_pengembalian->id_asset->Disabled) ? " disabled" : "")?> onclick="ew.modalLookupShow({lnk:this,el:'x_id_asset',m:0,n:10});"><i class="fa fa-search ew-icon"></i></button>
	</div>
</div>
<?php echo $transaksi_pengembalian->id_asset->Lookup->getParamTag("p_x_id_asset") ?>
<input type="hidden" data-table="transaksi_pengembalian" data-field="x_id_asset" data-multiple="0" data-lookup="1" data-value-separator="<?php echo $transaksi_pengembalian->id_asset->displayValueSeparatorAttribute() ?>" name="x_id_asset" id="x_id_asset" value="<?php echo $transaksi_pengembalian->id_asset->AdvancedSearch->SearchValue ?>"<?php echo $transaksi_pengembalian->id_asset->editAttributes() ?>>
</span>
	</div>
<?php } ?>
</div>
<div id="xsr_3" class="ew-row d-sm-flex">
<?php if ($transaksi_pengembalian->tanggal_kembali->Visible) { // tanggal_kembali ?>
	<div id="xsc_tanggal_kembali" class="ew-cell form-group">
		<label for="x_tanggal_kembali" class="ew-search-caption ew-label"><?php echo $transaksi_pengembalian->tanggal_kembali->caption() ?></label>
		<span class="ew-search-operator"><?php echo $Language->phrase("BETWEEN") ?><input type="hidden" name="z_tanggal_kembali" id="z_tanggal_kembali" value="BETWEEN"></span>
		<span class="ew-search-field">
<input type="text" data-table="transaksi_pengembalian" data-field="x_tanggal_kembali" name="x_tanggal_kembali" id="x_tanggal_kembali" placeholder="<?php echo HtmlEncode($transaksi_pengembalian->tanggal_kembali->getPlaceHolder()) ?>" value="<?php echo $transaksi_pengembalian->tanggal_kembali->EditValue ?>"<?php echo $transaksi_pengembalian->tanggal_kembali->editAttributes() ?>>
<?php if (!$transaksi_pengembalian->tanggal_kembali->ReadOnly && !$transaksi_pengembalian->tanggal_kembali->Disabled && !isset($transaksi_pengembalian->tanggal_kembali->EditAttrs["readonly"]) && !isset($transaksi_pengembalian->tanggal_kembali->EditAttrs["disabled"])) { ?>
<script>
ew.createDateTimePicker("ftransaksi_pengembalianlistsrch", "x_tanggal_kembali", {"ignoreReadonly":true,"useCurrent":false,"format":0});
</script>
<?php } ?>
</span>
		<span class="ew-search-cond btw1_tanggal_kembali"><label><?php echo $Language->Phrase("AND") ?></label></span>
		<span class="ew-search-field btw1_tanggal_kembali">
<input type="text" data-table="transaksi_pengembalian" data-field="x_tanggal_kembali" name="y_tanggal_kembali" id="y_tanggal_kembali" placeholder="<?php echo HtmlEncode($transaksi_pengembalian->tanggal_kembali->getPlaceHolder()) ?>" value="<?php echo $transaksi_pengembalian->tanggal_kembali->EditValue2 ?>"<?php echo $transaksi_pengembalian->tanggal_kembali->editAttributes() ?>>
<?php if (!$transaksi_pengembalian->tanggal_kembali->ReadOnly && !$transaksi_pengembalian->tanggal_kembali->Disabled && !isset($transaksi_pengembalian->tanggal_kembali->EditAttrs["readonly"]) && !isset($transaksi_pengembalian->tanggal_kembali->EditAttrs["disabled"])) { ?>
<script>
ew.createDateTimePicker("ftransaksi_pengembalianlistsrch", "y_tanggal_kembali", {"ignoreReadonly":true,"useCurrent":false,"format":0});
</script>
<?php } ?>
</span>
	</div>
<?php } ?>
</div>
<div id="xsr_4" class="ew-row d-sm-flex">
	<div class="ew-quick-search input-group">
		<input type="text" name="<?php echo TABLE_BASIC_SEARCH ?>" id="<?php echo TABLE_BASIC_SEARCH ?>" class="form-control" value="<?php echo HtmlEncode($transaksi_pengembalian_list->BasicSearch->getKeyword()) ?>" placeholder="<?php echo HtmlEncode($Language->phrase("Search")) ?>">
		<input type="hidden" name="<?php echo TABLE_BASIC_SEARCH_TYPE ?>" id="<?php echo TABLE_BASIC_SEARCH_TYPE ?>" value="<?php echo HtmlEncode($transaksi_pengembalian_list->BasicSearch->getType()) ?>">
		<div class="input-group-append">
			<button class="btn btn-primary" name="btn-submit" id="btn-submit" type="submit"><?php echo $Language->phrase("SearchBtn") ?></button>
			<button type="button" data-toggle="dropdown" class="btn btn-primary dropdown-toggle dropdown-toggle-split" aria-haspopup="true" aria-expanded="false"><span id="searchtype"><?php echo $transaksi_pengembalian_list->BasicSearch->getTypeNameShort() ?></span></button>
			<div class="dropdown-menu dropdown-menu-right">
				<a class="dropdown-item<?php if ($transaksi_pengembalian_list->BasicSearch->getType() == "") echo " active"; ?>" href="javascript:void(0);" onclick="ew.setSearchType(this)"><?php echo $Language->phrase("QuickSearchAuto") ?></a>
				<a class="dropdown-item<?php if ($transaksi_pengembalian_list->BasicSearch->getType() == "=") echo " active"; ?>" href="javascript:void(0);" onclick="ew.setSearchType(this,'=')"><?php echo $Language->phrase("QuickSearchExact") ?></a>
				<a class="dropdown-item<?php if ($transaksi_pengembalian_list->BasicSearch->getType() == "AND") echo " active"; ?>" href="javascript:void(0);" onclick="ew.setSearchType(this,'AND')"><?php echo $Language->phrase("QuickSearchAll") ?></a>
				<a class="dropdown-item<?php if ($transaksi_pengembalian_list->BasicSearch->getType() == "OR") echo " active"; ?>" href="javascript:void(0);" onclick="ew.setSearchType(this,'OR')"><?php echo $Language->phrase("QuickSearchAny") ?></a>
			</div>
		</div>
	</div>
</div>
	</div>
</div>
</form>
<?php } ?>
<?php } ?>
<?php $transaksi_pengembalian_list->showPageHeader(); ?>
<?php
$transaksi_pengembalian_list->showMessage();
?>
<?php if ($transaksi_pengembalian_list->TotalRecs > 0 || $transaksi_pengembalian->CurrentAction) { ?>
<div class="card ew-card ew-grid<?php if ($transaksi_pengembalian_list->isAddOrEdit()) { ?> ew-grid-add-edit<?php } ?> transaksi_pengembalian">
<?php if (!$transaksi_pengembalian->isExport()) { ?>
<div class="card-header ew-grid-upper-panel">
<?php if (!$transaksi_pengembalian->isGridAdd()) { ?>
<form name="ew-pager-form" class="form-inline ew-form ew-pager-form" action="<?php echo CurrentPageName() ?>">
<?php if (!isset($transaksi_pengembalian_list->Pager)) $transaksi_pengembalian_list->Pager = new PrevNextPager($transaksi_pengembalian_list->StartRec, $transaksi_pengembalian_list->DisplayRecs, $transaksi_pengembalian_list->TotalRecs, $transaksi_pengembalian_list->AutoHidePager) ?>
<?php if ($transaksi_pengembalian_list->Pager->RecordCount > 0 && $transaksi_pengembalian_list->Pager->Visible) { ?>
<div class="ew-pager">
<span><?php echo $Language->Phrase("Page") ?>&nbsp;</span>
<div class="ew-prev-next"><div class="input-group input-group-sm">
<div class="input-group-prepend">
<!-- first page button -->
	<?php if ($transaksi_pengembalian_list->Pager->FirstButton->Enabled) { ?>
	<a class="btn btn-default" title="<?php echo $Language->phrase("PagerFirst") ?>" href="<?php echo $transaksi_pengembalian_list->pageUrl() ?>start=<?php echo $transaksi_pengembalian_list->Pager->FirstButton->Start ?>"><i class="icon-first ew-icon"></i></a>
	<?php } else { ?>
	<a class="btn btn-default disabled" title="<?php echo $Language->phrase("PagerFirst") ?>"><i class="icon-first ew-icon"></i></a>
	<?php } ?>
<!-- previous page button -->
	<?php if ($transaksi_pengembalian_list->Pager->PrevButton->Enabled) { ?>
	<a class="btn btn-default" title="<?php echo $Language->phrase("PagerPrevious") ?>" href="<?php echo $transaksi_pengembalian_list->pageUrl() ?>start=<?php echo $transaksi_pengembalian_list->Pager->PrevButton->Start ?>"><i class="icon-prev ew-icon"></i></a>
	<?php } else { ?>
	<a class="btn btn-default disabled" title="<?php echo $Language->phrase("PagerPrevious") ?>"><i class="icon-prev ew-icon"></i></a>
	<?php } ?>
</div>
<!-- current page number -->
	<input class="form-control" type="text" name="<?php echo TABLE_PAGE_NO ?>" value="<?php echo $transaksi_pengembalian_list->Pager->CurrentPage ?>">
<div class="input-group-append">
<!-- next page button -->
	<?php if ($transaksi_pengembalian_list->Pager->NextButton->Enabled) { ?>
	<a class="btn btn-default" title="<?php echo $Language->phrase("PagerNext") ?>" href="<?php echo $transaksi_pengembalian_list->pageUrl() ?>start=<?php echo $transaksi_pengembalian_list->Pager->NextButton->Start ?>"><i class="icon-next ew-icon"></i></a>
	<?php } else { ?>
	<a class="btn btn-default disabled" title="<?php echo $Language->phrase("PagerNext") ?>"><i class="icon-next ew-icon"></i></a>
	<?php } ?>
<!-- last page button -->
	<?php if ($transaksi_pengembalian_list->Pager->LastButton->Enabled) { ?>
	<a class="btn btn-default" title="<?php echo $Language->phrase("PagerLast") ?>" href="<?php echo $transaksi_pengembalian_list->pageUrl() ?>start=<?php echo $transaksi_pengembalian_list->Pager->LastButton->Start ?>"><i class="icon-last ew-icon"></i></a>
	<?php } else { ?>
	<a class="btn btn-default disabled" title="<?php echo $Language->phrase("PagerLast") ?>"><i class="icon-last ew-icon"></i></a>
	<?php } ?>
</div>
</div>
</div>
<span>&nbsp;<?php echo $Language->Phrase("of") ?>&nbsp;<?php echo $transaksi_pengembalian_list->Pager->PageCount ?></span>
<div class="clearfix"></div>
</div>
<?php } ?>
<?php if ($transaksi_pengembalian_list->Pager->RecordCount > 0) { ?>
<div class="ew-pager ew-rec">
	<span><?php echo $Language->Phrase("Record") ?>&nbsp;<?php echo $transaksi_pengembalian_list->Pager->FromIndex ?>&nbsp;<?php echo $Language->Phrase("To") ?>&nbsp;<?php echo $transaksi_pengembalian_list->Pager->ToIndex ?>&nbsp;<?php echo $Language->Phrase("Of") ?>&nbsp;<?php echo $transaksi_pengembalian_list->Pager->RecordCount ?></span>
</div>
<?php } ?>
<?php if ($transaksi_pengembalian_list->TotalRecs > 0 && (!$transaksi_pengembalian_list->AutoHidePageSizeSelector || $transaksi_pengembalian_list->Pager->Visible)) { ?>
<div class="ew-pager">
<input type="hidden" name="t" value="transaksi_pengembalian">
<select name="<?php echo TABLE_REC_PER_PAGE ?>" class="form-control form-control-sm ew-tooltip" title="<?php echo $Language->phrase("RecordsPerPage") ?>" onchange="this.form.submit();">
<option value="5"<?php if ($transaksi_pengembalian_list->DisplayRecs == 5) { ?> selected<?php } ?>>5</option>
<option value="10"<?php if ($transaksi_pengembalian_list->DisplayRecs == 10) { ?> selected<?php } ?>>10</option>
<option value="20"<?php if ($transaksi_pengembalian_list->DisplayRecs == 20) { ?> selected<?php } ?>>20</option>
<option value="50"<?php if ($transaksi_pengembalian_list->DisplayRecs == 50) { ?> selected<?php } ?>>50</option>
<option value="100"<?php if ($transaksi_pengembalian_list->DisplayRecs == 100) { ?> selected<?php } ?>>100</option>
</select>
</div>
<?php } ?>
</form>
<?php } ?>
<div class="ew-list-other-options">
<?php $transaksi_pengembalian_list->OtherOptions->render("body") ?>
</div>
<div class="clearfix"></div>
</div>
<?php } ?>
<form name="ftransaksi_pengembalianlist" id="ftransaksi_pengembalianlist" class="form-inline ew-form ew-list-form" action="<?php echo CurrentPageName() ?>" method="post">
<?php if ($transaksi_pengembalian_list->CheckToken) { ?>
<input type="hidden" name="<?php echo TOKEN_NAME ?>" value="<?php echo $transaksi_pengembalian_list->Token ?>">
<?php } ?>
<input type="hidden" name="t" value="transaksi_pengembalian">
<div id="gmp_transaksi_pengembalian" class="<?php if (IsResponsiveLayout()) { ?>table-responsive <?php } ?>card-body ew-grid-middle-panel">
<?php if ($transaksi_pengembalian_list->TotalRecs > 0 || $transaksi_pengembalian->isGridEdit()) { ?>
<table id="tbl_transaksi_pengembalianlist" class="table ew-table"><!-- .ew-table ##-->
<thead>
	<tr class="ew-table-header">
<?php

// Header row
$transaksi_pengembalian_list->RowType = ROWTYPE_HEADER;

// Render list options
$transaksi_pengembalian_list->renderListOptions();

// Render list options (header, left)
$transaksi_pengembalian_list->ListOptions->render("header", "left");
?>
<?php if ($transaksi_pengembalian->id_karyawan->Visible) { // id_karyawan ?>
	<?php if ($transaksi_pengembalian->sortUrl($transaksi_pengembalian->id_karyawan) == "") { ?>
		<th data-name="id_karyawan" class="<?php echo $transaksi_pengembalian->id_karyawan->headerCellClass() ?>"><div id="elh_transaksi_pengembalian_id_karyawan" class="transaksi_pengembalian_id_karyawan"><div class="ew-table-header-caption"><?php echo $transaksi_pengembalian->id_karyawan->caption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="id_karyawan" class="<?php echo $transaksi_pengembalian->id_karyawan->headerCellClass() ?>"><div class="ew-pointer" onclick="ew.sort(event,'<?php echo $transaksi_pengembalian->SortUrl($transaksi_pengembalian->id_karyawan) ?>',1);"><div id="elh_transaksi_pengembalian_id_karyawan" class="transaksi_pengembalian_id_karyawan">
			<div class="ew-table-header-btn"><span class="ew-table-header-caption"><?php echo $transaksi_pengembalian->id_karyawan->caption() ?></span><span class="ew-table-header-sort"><?php if ($transaksi_pengembalian->id_karyawan->getSort() == "ASC") { ?><i class="fa fa-sort-up"></i><?php } elseif ($transaksi_pengembalian->id_karyawan->getSort() == "DESC") { ?><i class="fa fa-sort-down"></i><?php } ?></span></div>
		</div></div></th>
	<?php } ?>
<?php } ?>
<?php if ($transaksi_pengembalian->id_asset->Visible) { // id_asset ?>
	<?php if ($transaksi_pengembalian->sortUrl($transaksi_pengembalian->id_asset) == "") { ?>
		<th data-name="id_asset" class="<?php echo $transaksi_pengembalian->id_asset->headerCellClass() ?>"><div id="elh_transaksi_pengembalian_id_asset" class="transaksi_pengembalian_id_asset"><div class="ew-table-header-caption"><?php echo $transaksi_pengembalian->id_asset->caption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="id_asset" class="<?php echo $transaksi_pengembalian->id_asset->headerCellClass() ?>"><div class="ew-pointer" onclick="ew.sort(event,'<?php echo $transaksi_pengembalian->SortUrl($transaksi_pengembalian->id_asset) ?>',1);"><div id="elh_transaksi_pengembalian_id_asset" class="transaksi_pengembalian_id_asset">
			<div class="ew-table-header-btn"><span class="ew-table-header-caption"><?php echo $transaksi_pengembalian->id_asset->caption() ?></span><span class="ew-table-header-sort"><?php if ($transaksi_pengembalian->id_asset->getSort() == "ASC") { ?><i class="fa fa-sort-up"></i><?php } elseif ($transaksi_pengembalian->id_asset->getSort() == "DESC") { ?><i class="fa fa-sort-down"></i><?php } ?></span></div>
		</div></div></th>
	<?php } ?>
<?php } ?>
<?php if ($transaksi_pengembalian->tanggal_kembali->Visible) { // tanggal_kembali ?>
	<?php if ($transaksi_pengembalian->sortUrl($transaksi_pengembalian->tanggal_kembali) == "") { ?>
		<th data-name="tanggal_kembali" class="<?php echo $transaksi_pengembalian->tanggal_kembali->headerCellClass() ?>"><div id="elh_transaksi_pengembalian_tanggal_kembali" class="transaksi_pengembalian_tanggal_kembali"><div class="ew-table-header-caption"><?php echo $transaksi_pengembalian->tanggal_kembali->caption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="tanggal_kembali" class="<?php echo $transaksi_pengembalian->tanggal_kembali->headerCellClass() ?>"><div class="ew-pointer" onclick="ew.sort(event,'<?php echo $transaksi_pengembalian->SortUrl($transaksi_pengembalian->tanggal_kembali) ?>',1);"><div id="elh_transaksi_pengembalian_tanggal_kembali" class="transaksi_pengembalian_tanggal_kembali">
			<div class="ew-table-header-btn"><span class="ew-table-header-caption"><?php echo $transaksi_pengembalian->tanggal_kembali->caption() ?></span><span class="ew-table-header-sort"><?php if ($transaksi_pengembalian->tanggal_kembali->getSort() == "ASC") { ?><i class="fa fa-sort-up"></i><?php } elseif ($transaksi_pengembalian->tanggal_kembali->getSort() == "DESC") { ?><i class="fa fa-sort-down"></i><?php } ?></span></div>
		</div></div></th>
	<?php } ?>
<?php } ?>
<?php if ($transaksi_pengembalian->created_by->Visible) { // created_by ?>
	<?php if ($transaksi_pengembalian->sortUrl($transaksi_pengembalian->created_by) == "") { ?>
		<th data-name="created_by" class="<?php echo $transaksi_pengembalian->created_by->headerCellClass() ?>"><div id="elh_transaksi_pengembalian_created_by" class="transaksi_pengembalian_created_by"><div class="ew-table-header-caption"><?php echo $transaksi_pengembalian->created_by->caption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="created_by" class="<?php echo $transaksi_pengembalian->created_by->headerCellClass() ?>"><div class="ew-pointer" onclick="ew.sort(event,'<?php echo $transaksi_pengembalian->SortUrl($transaksi_pengembalian->created_by) ?>',1);"><div id="elh_transaksi_pengembalian_created_by" class="transaksi_pengembalian_created_by">
			<div class="ew-table-header-btn"><span class="ew-table-header-caption"><?php echo $transaksi_pengembalian->created_by->caption() ?></span><span class="ew-table-header-sort"><?php if ($transaksi_pengembalian->created_by->getSort() == "ASC") { ?><i class="fa fa-sort-up"></i><?php } elseif ($transaksi_pengembalian->created_by->getSort() == "DESC") { ?><i class="fa fa-sort-down"></i><?php } ?></span></div>
		</div></div></th>
	<?php } ?>
<?php } ?>
<?php if ($transaksi_pengembalian->created_date->Visible) { // created_date ?>
	<?php if ($transaksi_pengembalian->sortUrl($transaksi_pengembalian->created_date) == "") { ?>
		<th data-name="created_date" class="<?php echo $transaksi_pengembalian->created_date->headerCellClass() ?>"><div id="elh_transaksi_pengembalian_created_date" class="transaksi_pengembalian_created_date"><div class="ew-table-header-caption"><?php echo $transaksi_pengembalian->created_date->caption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="created_date" class="<?php echo $transaksi_pengembalian->created_date->headerCellClass() ?>"><div class="ew-pointer" onclick="ew.sort(event,'<?php echo $transaksi_pengembalian->SortUrl($transaksi_pengembalian->created_date) ?>',1);"><div id="elh_transaksi_pengembalian_created_date" class="transaksi_pengembalian_created_date">
			<div class="ew-table-header-btn"><span class="ew-table-header-caption"><?php echo $transaksi_pengembalian->created_date->caption() ?></span><span class="ew-table-header-sort"><?php if ($transaksi_pengembalian->created_date->getSort() == "ASC") { ?><i class="fa fa-sort-up"></i><?php } elseif ($transaksi_pengembalian->created_date->getSort() == "DESC") { ?><i class="fa fa-sort-down"></i><?php } ?></span></div>
		</div></div></th>
	<?php } ?>
<?php } ?>
<?php if ($transaksi_pengembalian->cetak->Visible) { // cetak ?>
	<?php if ($transaksi_pengembalian->sortUrl($transaksi_pengembalian->cetak) == "") { ?>
		<th data-name="cetak" class="<?php echo $transaksi_pengembalian->cetak->headerCellClass() ?>"><div id="elh_transaksi_pengembalian_cetak" class="transaksi_pengembalian_cetak"><div class="ew-table-header-caption"><?php echo $transaksi_pengembalian->cetak->caption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="cetak" class="<?php echo $transaksi_pengembalian->cetak->headerCellClass() ?>"><div class="ew-pointer" onclick="ew.sort(event,'<?php echo $transaksi_pengembalian->SortUrl($transaksi_pengembalian->cetak) ?>',1);"><div id="elh_transaksi_pengembalian_cetak" class="transaksi_pengembalian_cetak">
			<div class="ew-table-header-btn"><span class="ew-table-header-caption"><?php echo $transaksi_pengembalian->cetak->caption() ?><?php echo $Language->phrase("SrchLegend") ?></span><span class="ew-table-header-sort"><?php if ($transaksi_pengembalian->cetak->getSort() == "ASC") { ?><i class="fa fa-sort-up"></i><?php } elseif ($transaksi_pengembalian->cetak->getSort() == "DESC") { ?><i class="fa fa-sort-down"></i><?php } ?></span></div>
		</div></div></th>
	<?php } ?>
<?php } ?>
<?php

// Render list options (header, right)
$transaksi_pengembalian_list->ListOptions->render("header", "right");
?>
	</tr>
</thead>
<tbody>
<?php
if ($transaksi_pengembalian->ExportAll && $transaksi_pengembalian->isExport()) {
	$transaksi_pengembalian_list->StopRec = $transaksi_pengembalian_list->TotalRecs;
} else {

	// Set the last record to display
	if ($transaksi_pengembalian_list->TotalRecs > $transaksi_pengembalian_list->StartRec + $transaksi_pengembalian_list->DisplayRecs - 1)
		$transaksi_pengembalian_list->StopRec = $transaksi_pengembalian_list->StartRec + $transaksi_pengembalian_list->DisplayRecs - 1;
	else
		$transaksi_pengembalian_list->StopRec = $transaksi_pengembalian_list->TotalRecs;
}
$transaksi_pengembalian_list->RecCnt = $transaksi_pengembalian_list->StartRec - 1;
if ($transaksi_pengembalian_list->Recordset && !$transaksi_pengembalian_list->Recordset->EOF) {
	$transaksi_pengembalian_list->Recordset->moveFirst();
	$selectLimit = $transaksi_pengembalian_list->UseSelectLimit;
	if (!$selectLimit && $transaksi_pengembalian_list->StartRec > 1)
		$transaksi_pengembalian_list->Recordset->move($transaksi_pengembalian_list->StartRec - 1);
} elseif (!$transaksi_pengembalian->AllowAddDeleteRow && $transaksi_pengembalian_list->StopRec == 0) {
	$transaksi_pengembalian_list->StopRec = $transaksi_pengembalian->GridAddRowCount;
}

// Initialize aggregate
$transaksi_pengembalian->RowType = ROWTYPE_AGGREGATEINIT;
$transaksi_pengembalian->resetAttributes();
$transaksi_pengembalian_list->renderRow();
while ($transaksi_pengembalian_list->RecCnt < $transaksi_pengembalian_list->StopRec) {
	$transaksi_pengembalian_list->RecCnt++;
	if ($transaksi_pengembalian_list->RecCnt >= $transaksi_pengembalian_list->StartRec) {
		$transaksi_pengembalian_list->RowCnt++;

		// Set up key count
		$transaksi_pengembalian_list->KeyCount = $transaksi_pengembalian_list->RowIndex;

		// Init row class and style
		$transaksi_pengembalian->resetAttributes();
		$transaksi_pengembalian->CssClass = "";
		if ($transaksi_pengembalian->isGridAdd()) {
		} else {
			$transaksi_pengembalian_list->loadRowValues($transaksi_pengembalian_list->Recordset); // Load row values
		}
		$transaksi_pengembalian->RowType = ROWTYPE_VIEW; // Render view

		// Set up row id / data-rowindex
		$transaksi_pengembalian->RowAttrs = array_merge($transaksi_pengembalian->RowAttrs, array('data-rowindex'=>$transaksi_pengembalian_list->RowCnt, 'id'=>'r' . $transaksi_pengembalian_list->RowCnt . '_transaksi_pengembalian', 'data-rowtype'=>$transaksi_pengembalian->RowType));

		// Render row
		$transaksi_pengembalian_list->renderRow();

		// Render list options
		$transaksi_pengembalian_list->renderListOptions();
?>
	<tr<?php echo $transaksi_pengembalian->rowAttributes() ?>>
<?php

// Render list options (body, left)
$transaksi_pengembalian_list->ListOptions->render("body", "left", $transaksi_pengembalian_list->RowCnt);
?>
	<?php if ($transaksi_pengembalian->id_karyawan->Visible) { // id_karyawan ?>
		<td data-name="id_karyawan"<?php echo $transaksi_pengembalian->id_karyawan->cellAttributes() ?>>
<span id="el<?php echo $transaksi_pengembalian_list->RowCnt ?>_transaksi_pengembalian_id_karyawan" class="transaksi_pengembalian_id_karyawan">
<span<?php echo $transaksi_pengembalian->id_karyawan->viewAttributes() ?>>
<?php echo $transaksi_pengembalian->id_karyawan->getViewValue() ?></span>
</span>
</td>
	<?php } ?>
	<?php if ($transaksi_pengembalian->id_asset->Visible) { // id_asset ?>
		<td data-name="id_asset"<?php echo $transaksi_pengembalian->id_asset->cellAttributes() ?>>
<span id="el<?php echo $transaksi_pengembalian_list->RowCnt ?>_transaksi_pengembalian_id_asset" class="transaksi_pengembalian_id_asset">
<span<?php echo $transaksi_pengembalian->id_asset->viewAttributes() ?>>
<?php echo $transaksi_pengembalian->id_asset->getViewValue() ?></span>
</span>
</td>
	<?php } ?>
	<?php if ($transaksi_pengembalian->tanggal_kembali->Visible) { // tanggal_kembali ?>
		<td data-name="tanggal_kembali"<?php echo $transaksi_pengembalian->tanggal_kembali->cellAttributes() ?>>
<span id="el<?php echo $transaksi_pengembalian_list->RowCnt ?>_transaksi_pengembalian_tanggal_kembali" class="transaksi_pengembalian_tanggal_kembali">
<span<?php echo $transaksi_pengembalian->tanggal_kembali->viewAttributes() ?>>
<?php echo $transaksi_pengembalian->tanggal_kembali->getViewValue() ?></span>
</span>
</td>
	<?php } ?>
	<?php if ($transaksi_pengembalian->created_by->Visible) { // created_by ?>
		<td data-name="created_by"<?php echo $transaksi_pengembalian->created_by->cellAttributes() ?>>
<span id="el<?php echo $transaksi_pengembalian_list->RowCnt ?>_transaksi_pengembalian_created_by" class="transaksi_pengembalian_created_by">
<span<?php echo $transaksi_pengembalian->created_by->viewAttributes() ?>>
<?php echo $transaksi_pengembalian->created_by->getViewValue() ?></span>
</span>
</td>
	<?php } ?>
	<?php if ($transaksi_pengembalian->created_date->Visible) { // created_date ?>
		<td data-name="created_date"<?php echo $transaksi_pengembalian->created_date->cellAttributes() ?>>
<span id="el<?php echo $transaksi_pengembalian_list->RowCnt ?>_transaksi_pengembalian_created_date" class="transaksi_pengembalian_created_date">
<span<?php echo $transaksi_pengembalian->created_date->viewAttributes() ?>>
<?php echo $transaksi_pengembalian->created_date->getViewValue() ?></span>
</span>
</td>
	<?php } ?>
	<?php if ($transaksi_pengembalian->cetak->Visible) { // cetak ?>
		<td data-name="cetak"<?php echo $transaksi_pengembalian->cetak->cellAttributes() ?>>
<span id="el<?php echo $transaksi_pengembalian_list->RowCnt ?>_transaksi_pengembalian_cetak" class="transaksi_pengembalian_cetak">
<span<?php echo $transaksi_pengembalian->cetak->viewAttributes() ?>>
<?php echo $transaksi_pengembalian->cetak->getViewValue() ?></span>
</span>
</td>
	<?php } ?>
<?php

// Render list options (body, right)
$transaksi_pengembalian_list->ListOptions->render("body", "right", $transaksi_pengembalian_list->RowCnt);
?>
	</tr>
<?php
	}
	if (!$transaksi_pengembalian->isGridAdd())
		$transaksi_pengembalian_list->Recordset->moveNext();
}
?>
</tbody>
</table><!-- /.ew-table -->
<?php } ?>
<?php if (!$transaksi_pengembalian->CurrentAction) { ?>
<input type="hidden" name="action" id="action" value="">
<?php } ?>
</div><!-- /.ew-grid-middle-panel -->
</form><!-- /.ew-list-form -->
<?php

// Close recordset
if ($transaksi_pengembalian_list->Recordset)
	$transaksi_pengembalian_list->Recordset->Close();
?>
</div><!-- /.ew-grid -->
<?php } ?>
<?php if ($transaksi_pengembalian_list->TotalRecs == 0 && !$transaksi_pengembalian->CurrentAction) { // Show other options ?>
<div class="ew-list-other-options">
<?php $transaksi_pengembalian_list->OtherOptions->render("body") ?>
</div>
<div class="clearfix"></div>
<?php } ?>
<?php
$transaksi_pengembalian_list->showPageFooter();
if (DEBUG_ENABLED)
	echo GetDebugMessage();
?>
<?php if (!$transaksi_pengembalian->isExport()) { ?>
<script>

// Write your table-specific startup script here
// document.write("page loaded");

</script>
<?php } ?>
<?php include_once "footer.php" ?>
<?php
$transaksi_pengembalian_list->terminate();
?>