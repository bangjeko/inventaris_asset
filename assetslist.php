<?php
namespace PHPMaker2019\inventaris_assets;

// Session
if (session_status() !== PHP_SESSION_ACTIVE)
	session_start(); // Init session data

// Output buffering
ob_start(); 

// Autoload
include_once "autoload.php";
?>
<?php

// Write header
WriteHeader(FALSE);

// Create page object
$assets_list = new assets_list();

// Run the page
$assets_list->run();

// Setup login status
SetupLoginStatus();
SetClientVar("login", LoginStatus());

// Global Page Rendering event (in userfn*.php)
Page_Rendering();

// Page Rendering event
$assets_list->Page_Render();
?>
<?php include_once "header.php" ?>
<?php if (!$assets->isExport()) { ?>
<script>

// Form object
currentPageID = ew.PAGE_ID = "list";
var fassetslist = currentForm = new ew.Form("fassetslist", "list");
fassetslist.formKeyCountName = '<?php echo $assets_list->FormKeyCountName ?>';

// Form_CustomValidate event
fassetslist.Form_CustomValidate = function(fobj) { // DO NOT CHANGE THIS LINE!

	// Your custom validation code here, return false if invalid.
	return true;
}

// Use JavaScript validation or not
fassetslist.validateRequired = <?php echo json_encode(CLIENT_VALIDATE) ?>;

// Dynamic selection lists
fassetslist.lists["x_jenis_asset"] = <?php echo $assets_list->jenis_asset->Lookup->toClientList() ?>;
fassetslist.lists["x_jenis_asset"].options = <?php echo JsonEncode($assets_list->jenis_asset->lookupOptions()) ?>;
fassetslist.lists["x_status"] = <?php echo $assets_list->status->Lookup->toClientList() ?>;
fassetslist.lists["x_status"].options = <?php echo JsonEncode($assets_list->status->options(FALSE, TRUE)) ?>;

// Form object for search
var fassetslistsrch = currentSearchForm = new ew.Form("fassetslistsrch");

// Validate function for search
fassetslistsrch.validate = function(fobj) {
	if (!this.validateRequired)
		return true; // Ignore validation
	fobj = fobj || this._form;
	var infix = "";

	// Fire Form_CustomValidate event
	if (!this.Form_CustomValidate(fobj))
		return false;
	return true;
}

// Form_CustomValidate event
fassetslistsrch.Form_CustomValidate = function(fobj) { // DO NOT CHANGE THIS LINE!

	// Your custom validation code here, return false if invalid.
	return true;
}

// Use JavaScript validation or not
fassetslistsrch.validateRequired = <?php echo json_encode(CLIENT_VALIDATE) ?>;

// Dynamic selection lists
fassetslistsrch.lists["x_status"] = <?php echo $assets_list->status->Lookup->toClientList() ?>;
fassetslistsrch.lists["x_status"].options = <?php echo JsonEncode($assets_list->status->options(FALSE, TRUE)) ?>;

// Filters
fassetslistsrch.filterList = <?php echo $assets_list->getFilterList() ?>;

// Init search panel as collapsed
fassetslistsrch.initSearchPanel = true;
</script>
<script>

// Write your client script here, no need to add script tags.
</script>
<?php } ?>
<?php if (!$assets->isExport()) { ?>
<div class="btn-toolbar ew-toolbar">
<?php if ($assets_list->TotalRecs > 0 && $assets_list->ExportOptions->visible()) { ?>
<?php $assets_list->ExportOptions->render("body") ?>
<?php } ?>
<?php if ($assets_list->ImportOptions->visible()) { ?>
<?php $assets_list->ImportOptions->render("body") ?>
<?php } ?>
<?php if ($assets_list->SearchOptions->visible()) { ?>
<?php $assets_list->SearchOptions->render("body") ?>
<?php } ?>
<?php if ($assets_list->FilterOptions->visible()) { ?>
<?php $assets_list->FilterOptions->render("body") ?>
<?php } ?>
<div class="clearfix"></div>
</div>
<?php } ?>
<?php
$assets_list->renderOtherOptions();
?>
<?php if ($Security->CanSearch()) { ?>
<?php if (!$assets->isExport() && !$assets->CurrentAction) { ?>
<form name="fassetslistsrch" id="fassetslistsrch" class="form-inline ew-form ew-ext-search-form" action="<?php echo CurrentPageName() ?>">
<?php $searchPanelClass = ($assets_list->SearchWhere <> "") ? " show" : ""; ?>
<div id="fassetslistsrch-search-panel" class="ew-search-panel collapse<?php echo $searchPanelClass ?>">
<input type="hidden" name="cmd" value="search">
<input type="hidden" name="t" value="assets">
	<div class="ew-basic-search">
<?php
if ($SearchError == "")
	$assets_list->LoadAdvancedSearch(); // Load advanced search

// Render for search
$assets->RowType = ROWTYPE_SEARCH;

// Render row
$assets->resetAttributes();
$assets_list->renderRow();
?>
<div id="xsr_1" class="ew-row d-sm-flex">
<?php if ($assets->kode_asset->Visible) { // kode_asset ?>
	<div id="xsc_kode_asset" class="ew-cell form-group">
		<label for="x_kode_asset" class="ew-search-caption ew-label"><?php echo $assets->kode_asset->caption() ?></label>
		<span class="ew-search-operator"><?php echo $Language->phrase("LIKE") ?><input type="hidden" name="z_kode_asset" id="z_kode_asset" value="LIKE"></span>
		<span class="ew-search-field">
<input type="text" data-table="assets" data-field="x_kode_asset" name="x_kode_asset" id="x_kode_asset" size="30" maxlength="255" placeholder="<?php echo HtmlEncode($assets->kode_asset->getPlaceHolder()) ?>" value="<?php echo $assets->kode_asset->EditValue ?>"<?php echo $assets->kode_asset->editAttributes() ?>>
</span>
	</div>
<?php } ?>
</div>
<div id="xsr_2" class="ew-row d-sm-flex">
<?php if ($assets->status->Visible) { // status ?>
	<div id="xsc_status" class="ew-cell form-group">
		<label for="x_status" class="ew-search-caption ew-label"><?php echo $assets->status->caption() ?></label>
		<span class="ew-search-operator"><?php echo $Language->phrase("=") ?><input type="hidden" name="z_status" id="z_status" value="="></span>
		<span class="ew-search-field">
<div class="input-group">
	<select class="custom-select ew-custom-select" data-table="assets" data-field="x_status" data-value-separator="<?php echo $assets->status->displayValueSeparatorAttribute() ?>" id="x_status" name="x_status"<?php echo $assets->status->editAttributes() ?>>
		<?php echo $assets->status->selectOptionListHtml("x_status") ?>
	</select>
</div>
</span>
	</div>
<?php } ?>
</div>
<div id="xsr_3" class="ew-row d-sm-flex">
	<div class="ew-quick-search input-group">
		<input type="text" name="<?php echo TABLE_BASIC_SEARCH ?>" id="<?php echo TABLE_BASIC_SEARCH ?>" class="form-control" value="<?php echo HtmlEncode($assets_list->BasicSearch->getKeyword()) ?>" placeholder="<?php echo HtmlEncode($Language->phrase("Search")) ?>">
		<input type="hidden" name="<?php echo TABLE_BASIC_SEARCH_TYPE ?>" id="<?php echo TABLE_BASIC_SEARCH_TYPE ?>" value="<?php echo HtmlEncode($assets_list->BasicSearch->getType()) ?>">
		<div class="input-group-append">
			<button class="btn btn-primary" name="btn-submit" id="btn-submit" type="submit"><?php echo $Language->phrase("SearchBtn") ?></button>
			<button type="button" data-toggle="dropdown" class="btn btn-primary dropdown-toggle dropdown-toggle-split" aria-haspopup="true" aria-expanded="false"><span id="searchtype"><?php echo $assets_list->BasicSearch->getTypeNameShort() ?></span></button>
			<div class="dropdown-menu dropdown-menu-right">
				<a class="dropdown-item<?php if ($assets_list->BasicSearch->getType() == "") echo " active"; ?>" href="javascript:void(0);" onclick="ew.setSearchType(this)"><?php echo $Language->phrase("QuickSearchAuto") ?></a>
				<a class="dropdown-item<?php if ($assets_list->BasicSearch->getType() == "=") echo " active"; ?>" href="javascript:void(0);" onclick="ew.setSearchType(this,'=')"><?php echo $Language->phrase("QuickSearchExact") ?></a>
				<a class="dropdown-item<?php if ($assets_list->BasicSearch->getType() == "AND") echo " active"; ?>" href="javascript:void(0);" onclick="ew.setSearchType(this,'AND')"><?php echo $Language->phrase("QuickSearchAll") ?></a>
				<a class="dropdown-item<?php if ($assets_list->BasicSearch->getType() == "OR") echo " active"; ?>" href="javascript:void(0);" onclick="ew.setSearchType(this,'OR')"><?php echo $Language->phrase("QuickSearchAny") ?></a>
			</div>
		</div>
	</div>
</div>
	</div>
</div>
</form>
<?php } ?>
<?php } ?>
<?php $assets_list->showPageHeader(); ?>
<?php
$assets_list->showMessage();
?>
<?php if ($assets_list->TotalRecs > 0 || $assets->CurrentAction) { ?>
<div class="card ew-card ew-grid<?php if ($assets_list->isAddOrEdit()) { ?> ew-grid-add-edit<?php } ?> assets">
<?php if (!$assets->isExport()) { ?>
<div class="card-header ew-grid-upper-panel">
<?php if (!$assets->isGridAdd()) { ?>
<form name="ew-pager-form" class="form-inline ew-form ew-pager-form" action="<?php echo CurrentPageName() ?>">
<?php if (!isset($assets_list->Pager)) $assets_list->Pager = new PrevNextPager($assets_list->StartRec, $assets_list->DisplayRecs, $assets_list->TotalRecs, $assets_list->AutoHidePager) ?>
<?php if ($assets_list->Pager->RecordCount > 0 && $assets_list->Pager->Visible) { ?>
<div class="ew-pager">
<span><?php echo $Language->Phrase("Page") ?>&nbsp;</span>
<div class="ew-prev-next"><div class="input-group input-group-sm">
<div class="input-group-prepend">
<!-- first page button -->
	<?php if ($assets_list->Pager->FirstButton->Enabled) { ?>
	<a class="btn btn-default" title="<?php echo $Language->phrase("PagerFirst") ?>" href="<?php echo $assets_list->pageUrl() ?>start=<?php echo $assets_list->Pager->FirstButton->Start ?>"><i class="icon-first ew-icon"></i></a>
	<?php } else { ?>
	<a class="btn btn-default disabled" title="<?php echo $Language->phrase("PagerFirst") ?>"><i class="icon-first ew-icon"></i></a>
	<?php } ?>
<!-- previous page button -->
	<?php if ($assets_list->Pager->PrevButton->Enabled) { ?>
	<a class="btn btn-default" title="<?php echo $Language->phrase("PagerPrevious") ?>" href="<?php echo $assets_list->pageUrl() ?>start=<?php echo $assets_list->Pager->PrevButton->Start ?>"><i class="icon-prev ew-icon"></i></a>
	<?php } else { ?>
	<a class="btn btn-default disabled" title="<?php echo $Language->phrase("PagerPrevious") ?>"><i class="icon-prev ew-icon"></i></a>
	<?php } ?>
</div>
<!-- current page number -->
	<input class="form-control" type="text" name="<?php echo TABLE_PAGE_NO ?>" value="<?php echo $assets_list->Pager->CurrentPage ?>">
<div class="input-group-append">
<!-- next page button -->
	<?php if ($assets_list->Pager->NextButton->Enabled) { ?>
	<a class="btn btn-default" title="<?php echo $Language->phrase("PagerNext") ?>" href="<?php echo $assets_list->pageUrl() ?>start=<?php echo $assets_list->Pager->NextButton->Start ?>"><i class="icon-next ew-icon"></i></a>
	<?php } else { ?>
	<a class="btn btn-default disabled" title="<?php echo $Language->phrase("PagerNext") ?>"><i class="icon-next ew-icon"></i></a>
	<?php } ?>
<!-- last page button -->
	<?php if ($assets_list->Pager->LastButton->Enabled) { ?>
	<a class="btn btn-default" title="<?php echo $Language->phrase("PagerLast") ?>" href="<?php echo $assets_list->pageUrl() ?>start=<?php echo $assets_list->Pager->LastButton->Start ?>"><i class="icon-last ew-icon"></i></a>
	<?php } else { ?>
	<a class="btn btn-default disabled" title="<?php echo $Language->phrase("PagerLast") ?>"><i class="icon-last ew-icon"></i></a>
	<?php } ?>
</div>
</div>
</div>
<span>&nbsp;<?php echo $Language->Phrase("of") ?>&nbsp;<?php echo $assets_list->Pager->PageCount ?></span>
<div class="clearfix"></div>
</div>
<?php } ?>
<?php if ($assets_list->Pager->RecordCount > 0) { ?>
<div class="ew-pager ew-rec">
	<span><?php echo $Language->Phrase("Record") ?>&nbsp;<?php echo $assets_list->Pager->FromIndex ?>&nbsp;<?php echo $Language->Phrase("To") ?>&nbsp;<?php echo $assets_list->Pager->ToIndex ?>&nbsp;<?php echo $Language->Phrase("Of") ?>&nbsp;<?php echo $assets_list->Pager->RecordCount ?></span>
</div>
<?php } ?>
<?php if ($assets_list->TotalRecs > 0 && (!$assets_list->AutoHidePageSizeSelector || $assets_list->Pager->Visible)) { ?>
<div class="ew-pager">
<input type="hidden" name="t" value="assets">
<select name="<?php echo TABLE_REC_PER_PAGE ?>" class="form-control form-control-sm ew-tooltip" title="<?php echo $Language->phrase("RecordsPerPage") ?>" onchange="this.form.submit();">
<option value="5"<?php if ($assets_list->DisplayRecs == 5) { ?> selected<?php } ?>>5</option>
<option value="10"<?php if ($assets_list->DisplayRecs == 10) { ?> selected<?php } ?>>10</option>
<option value="20"<?php if ($assets_list->DisplayRecs == 20) { ?> selected<?php } ?>>20</option>
<option value="50"<?php if ($assets_list->DisplayRecs == 50) { ?> selected<?php } ?>>50</option>
<option value="100"<?php if ($assets_list->DisplayRecs == 100) { ?> selected<?php } ?>>100</option>
</select>
</div>
<?php } ?>
</form>
<?php } ?>
<div class="ew-list-other-options">
<?php $assets_list->OtherOptions->render("body") ?>
</div>
<div class="clearfix"></div>
</div>
<?php } ?>
<form name="fassetslist" id="fassetslist" class="form-inline ew-form ew-list-form" action="<?php echo CurrentPageName() ?>" method="post">
<?php if ($assets_list->CheckToken) { ?>
<input type="hidden" name="<?php echo TOKEN_NAME ?>" value="<?php echo $assets_list->Token ?>">
<?php } ?>
<input type="hidden" name="t" value="assets">
<div id="gmp_assets" class="<?php if (IsResponsiveLayout()) { ?>table-responsive <?php } ?>card-body ew-grid-middle-panel">
<?php if ($assets_list->TotalRecs > 0 || $assets->isGridEdit()) { ?>
<table id="tbl_assetslist" class="table ew-table"><!-- .ew-table ##-->
<thead>
	<tr class="ew-table-header">
<?php

// Header row
$assets_list->RowType = ROWTYPE_HEADER;

// Render list options
$assets_list->renderListOptions();

// Render list options (header, left)
$assets_list->ListOptions->render("header", "left");
?>
<?php if ($assets->kode_asset->Visible) { // kode_asset ?>
	<?php if ($assets->sortUrl($assets->kode_asset) == "") { ?>
		<th data-name="kode_asset" class="<?php echo $assets->kode_asset->headerCellClass() ?>"><div id="elh_assets_kode_asset" class="assets_kode_asset"><div class="ew-table-header-caption"><?php echo $assets->kode_asset->caption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="kode_asset" class="<?php echo $assets->kode_asset->headerCellClass() ?>"><div class="ew-pointer" onclick="ew.sort(event,'<?php echo $assets->SortUrl($assets->kode_asset) ?>',1);"><div id="elh_assets_kode_asset" class="assets_kode_asset">
			<div class="ew-table-header-btn"><span class="ew-table-header-caption"><?php echo $assets->kode_asset->caption() ?></span><span class="ew-table-header-sort"><?php if ($assets->kode_asset->getSort() == "ASC") { ?><i class="fa fa-sort-up"></i><?php } elseif ($assets->kode_asset->getSort() == "DESC") { ?><i class="fa fa-sort-down"></i><?php } ?></span></div>
		</div></div></th>
	<?php } ?>
<?php } ?>
<?php if ($assets->jenis_asset->Visible) { // jenis_asset ?>
	<?php if ($assets->sortUrl($assets->jenis_asset) == "") { ?>
		<th data-name="jenis_asset" class="<?php echo $assets->jenis_asset->headerCellClass() ?>"><div id="elh_assets_jenis_asset" class="assets_jenis_asset"><div class="ew-table-header-caption"><?php echo $assets->jenis_asset->caption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="jenis_asset" class="<?php echo $assets->jenis_asset->headerCellClass() ?>"><div class="ew-pointer" onclick="ew.sort(event,'<?php echo $assets->SortUrl($assets->jenis_asset) ?>',1);"><div id="elh_assets_jenis_asset" class="assets_jenis_asset">
			<div class="ew-table-header-btn"><span class="ew-table-header-caption"><?php echo $assets->jenis_asset->caption() ?></span><span class="ew-table-header-sort"><?php if ($assets->jenis_asset->getSort() == "ASC") { ?><i class="fa fa-sort-up"></i><?php } elseif ($assets->jenis_asset->getSort() == "DESC") { ?><i class="fa fa-sort-down"></i><?php } ?></span></div>
		</div></div></th>
	<?php } ?>
<?php } ?>
<?php if ($assets->merk->Visible) { // merk ?>
	<?php if ($assets->sortUrl($assets->merk) == "") { ?>
		<th data-name="merk" class="<?php echo $assets->merk->headerCellClass() ?>"><div id="elh_assets_merk" class="assets_merk"><div class="ew-table-header-caption"><?php echo $assets->merk->caption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="merk" class="<?php echo $assets->merk->headerCellClass() ?>"><div class="ew-pointer" onclick="ew.sort(event,'<?php echo $assets->SortUrl($assets->merk) ?>',1);"><div id="elh_assets_merk" class="assets_merk">
			<div class="ew-table-header-btn"><span class="ew-table-header-caption"><?php echo $assets->merk->caption() ?><?php echo $Language->phrase("SrchLegend") ?></span><span class="ew-table-header-sort"><?php if ($assets->merk->getSort() == "ASC") { ?><i class="fa fa-sort-up"></i><?php } elseif ($assets->merk->getSort() == "DESC") { ?><i class="fa fa-sort-down"></i><?php } ?></span></div>
		</div></div></th>
	<?php } ?>
<?php } ?>
<?php if ($assets->status->Visible) { // status ?>
	<?php if ($assets->sortUrl($assets->status) == "") { ?>
		<th data-name="status" class="<?php echo $assets->status->headerCellClass() ?>"><div id="elh_assets_status" class="assets_status"><div class="ew-table-header-caption"><?php echo $assets->status->caption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="status" class="<?php echo $assets->status->headerCellClass() ?>"><div class="ew-pointer" onclick="ew.sort(event,'<?php echo $assets->SortUrl($assets->status) ?>',1);"><div id="elh_assets_status" class="assets_status">
			<div class="ew-table-header-btn"><span class="ew-table-header-caption"><?php echo $assets->status->caption() ?></span><span class="ew-table-header-sort"><?php if ($assets->status->getSort() == "ASC") { ?><i class="fa fa-sort-up"></i><?php } elseif ($assets->status->getSort() == "DESC") { ?><i class="fa fa-sort-down"></i><?php } ?></span></div>
		</div></div></th>
	<?php } ?>
<?php } ?>
<?php

// Render list options (header, right)
$assets_list->ListOptions->render("header", "right");
?>
	</tr>
</thead>
<tbody>
<?php
if ($assets->ExportAll && $assets->isExport()) {
	$assets_list->StopRec = $assets_list->TotalRecs;
} else {

	// Set the last record to display
	if ($assets_list->TotalRecs > $assets_list->StartRec + $assets_list->DisplayRecs - 1)
		$assets_list->StopRec = $assets_list->StartRec + $assets_list->DisplayRecs - 1;
	else
		$assets_list->StopRec = $assets_list->TotalRecs;
}
$assets_list->RecCnt = $assets_list->StartRec - 1;
if ($assets_list->Recordset && !$assets_list->Recordset->EOF) {
	$assets_list->Recordset->moveFirst();
	$selectLimit = $assets_list->UseSelectLimit;
	if (!$selectLimit && $assets_list->StartRec > 1)
		$assets_list->Recordset->move($assets_list->StartRec - 1);
} elseif (!$assets->AllowAddDeleteRow && $assets_list->StopRec == 0) {
	$assets_list->StopRec = $assets->GridAddRowCount;
}

// Initialize aggregate
$assets->RowType = ROWTYPE_AGGREGATEINIT;
$assets->resetAttributes();
$assets_list->renderRow();
while ($assets_list->RecCnt < $assets_list->StopRec) {
	$assets_list->RecCnt++;
	if ($assets_list->RecCnt >= $assets_list->StartRec) {
		$assets_list->RowCnt++;

		// Set up key count
		$assets_list->KeyCount = $assets_list->RowIndex;

		// Init row class and style
		$assets->resetAttributes();
		$assets->CssClass = "";
		if ($assets->isGridAdd()) {
		} else {
			$assets_list->loadRowValues($assets_list->Recordset); // Load row values
		}
		$assets->RowType = ROWTYPE_VIEW; // Render view

		// Set up row id / data-rowindex
		$assets->RowAttrs = array_merge($assets->RowAttrs, array('data-rowindex'=>$assets_list->RowCnt, 'id'=>'r' . $assets_list->RowCnt . '_assets', 'data-rowtype'=>$assets->RowType));

		// Render row
		$assets_list->renderRow();

		// Render list options
		$assets_list->renderListOptions();
?>
	<tr<?php echo $assets->rowAttributes() ?>>
<?php

// Render list options (body, left)
$assets_list->ListOptions->render("body", "left", $assets_list->RowCnt);
?>
	<?php if ($assets->kode_asset->Visible) { // kode_asset ?>
		<td data-name="kode_asset"<?php echo $assets->kode_asset->cellAttributes() ?>>
<span id="el<?php echo $assets_list->RowCnt ?>_assets_kode_asset" class="assets_kode_asset">
<span<?php echo $assets->kode_asset->viewAttributes() ?>>
<?php echo $assets->kode_asset->getViewValue() ?></span>
</span>
</td>
	<?php } ?>
	<?php if ($assets->jenis_asset->Visible) { // jenis_asset ?>
		<td data-name="jenis_asset"<?php echo $assets->jenis_asset->cellAttributes() ?>>
<span id="el<?php echo $assets_list->RowCnt ?>_assets_jenis_asset" class="assets_jenis_asset">
<span<?php echo $assets->jenis_asset->viewAttributes() ?>>
<?php echo $assets->jenis_asset->getViewValue() ?></span>
</span>
</td>
	<?php } ?>
	<?php if ($assets->merk->Visible) { // merk ?>
		<td data-name="merk"<?php echo $assets->merk->cellAttributes() ?>>
<span id="el<?php echo $assets_list->RowCnt ?>_assets_merk" class="assets_merk">
<span<?php echo $assets->merk->viewAttributes() ?>>
<?php echo $assets->merk->getViewValue() ?></span>
</span>
</td>
	<?php } ?>
	<?php if ($assets->status->Visible) { // status ?>
		<td data-name="status"<?php echo $assets->status->cellAttributes() ?>>
<span id="el<?php echo $assets_list->RowCnt ?>_assets_status" class="assets_status">
<span<?php echo $assets->status->viewAttributes() ?>>
<?php echo $assets->status->getViewValue() ?></span>
</span>
</td>
	<?php } ?>
<?php

// Render list options (body, right)
$assets_list->ListOptions->render("body", "right", $assets_list->RowCnt);
?>
	</tr>
<?php
	}
	if (!$assets->isGridAdd())
		$assets_list->Recordset->moveNext();
}
?>
</tbody>
</table><!-- /.ew-table -->
<?php } ?>
<?php if (!$assets->CurrentAction) { ?>
<input type="hidden" name="action" id="action" value="">
<?php } ?>
</div><!-- /.ew-grid-middle-panel -->
</form><!-- /.ew-list-form -->
<?php

// Close recordset
if ($assets_list->Recordset)
	$assets_list->Recordset->Close();
?>
</div><!-- /.ew-grid -->
<?php } ?>
<?php if ($assets_list->TotalRecs == 0 && !$assets->CurrentAction) { // Show other options ?>
<div class="ew-list-other-options">
<?php $assets_list->OtherOptions->render("body") ?>
</div>
<div class="clearfix"></div>
<?php } ?>
<?php
$assets_list->showPageFooter();
if (DEBUG_ENABLED)
	echo GetDebugMessage();
?>
<?php if (!$assets->isExport()) { ?>
<script>

// Write your table-specific startup script here
// document.write("page loaded");

</script>
<?php } ?>
<?php include_once "footer.php" ?>
<?php
$assets_list->terminate();
?>