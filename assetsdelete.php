<?php
namespace PHPMaker2019\inventaris_assets;

// Session
if (session_status() !== PHP_SESSION_ACTIVE)
	session_start(); // Init session data

// Output buffering
ob_start(); 

// Autoload
include_once "autoload.php";
?>
<?php

// Write header
WriteHeader(FALSE);

// Create page object
$assets_delete = new assets_delete();

// Run the page
$assets_delete->run();

// Setup login status
SetupLoginStatus();
SetClientVar("login", LoginStatus());

// Global Page Rendering event (in userfn*.php)
Page_Rendering();

// Page Rendering event
$assets_delete->Page_Render();
?>
<?php include_once "header.php" ?>
<script>

// Form object
currentPageID = ew.PAGE_ID = "delete";
var fassetsdelete = currentForm = new ew.Form("fassetsdelete", "delete");

// Form_CustomValidate event
fassetsdelete.Form_CustomValidate = function(fobj) { // DO NOT CHANGE THIS LINE!

	// Your custom validation code here, return false if invalid.
	return true;
}

// Use JavaScript validation or not
fassetsdelete.validateRequired = <?php echo json_encode(CLIENT_VALIDATE) ?>;

// Dynamic selection lists
fassetsdelete.lists["x_jenis_asset"] = <?php echo $assets_delete->jenis_asset->Lookup->toClientList() ?>;
fassetsdelete.lists["x_jenis_asset"].options = <?php echo JsonEncode($assets_delete->jenis_asset->lookupOptions()) ?>;
fassetsdelete.lists["x_status"] = <?php echo $assets_delete->status->Lookup->toClientList() ?>;
fassetsdelete.lists["x_status"].options = <?php echo JsonEncode($assets_delete->status->options(FALSE, TRUE)) ?>;

// Form object for search
</script>
<script>

// Write your client script here, no need to add script tags.
</script>
<?php $assets_delete->showPageHeader(); ?>
<?php
$assets_delete->showMessage();
?>
<form name="fassetsdelete" id="fassetsdelete" class="form-inline ew-form ew-delete-form" action="<?php echo CurrentPageName() ?>" method="post">
<?php if ($assets_delete->CheckToken) { ?>
<input type="hidden" name="<?php echo TOKEN_NAME ?>" value="<?php echo $assets_delete->Token ?>">
<?php } ?>
<input type="hidden" name="t" value="assets">
<input type="hidden" name="action" id="action" value="delete">
<?php foreach ($assets_delete->RecKeys as $key) { ?>
<?php $keyvalue = is_array($key) ? implode($COMPOSITE_KEY_SEPARATOR, $key) : $key; ?>
<input type="hidden" name="key_m[]" value="<?php echo HtmlEncode($keyvalue) ?>">
<?php } ?>
<div class="card ew-card ew-grid">
<div class="<?php if (IsResponsiveLayout()) { ?>table-responsive <?php } ?>card-body ew-grid-middle-panel">
<table class="table ew-table">
	<thead>
	<tr class="ew-table-header">
<?php if ($assets->kode_asset->Visible) { // kode_asset ?>
		<th class="<?php echo $assets->kode_asset->headerCellClass() ?>"><span id="elh_assets_kode_asset" class="assets_kode_asset"><?php echo $assets->kode_asset->caption() ?></span></th>
<?php } ?>
<?php if ($assets->jenis_asset->Visible) { // jenis_asset ?>
		<th class="<?php echo $assets->jenis_asset->headerCellClass() ?>"><span id="elh_assets_jenis_asset" class="assets_jenis_asset"><?php echo $assets->jenis_asset->caption() ?></span></th>
<?php } ?>
<?php if ($assets->merk->Visible) { // merk ?>
		<th class="<?php echo $assets->merk->headerCellClass() ?>"><span id="elh_assets_merk" class="assets_merk"><?php echo $assets->merk->caption() ?></span></th>
<?php } ?>
<?php if ($assets->status->Visible) { // status ?>
		<th class="<?php echo $assets->status->headerCellClass() ?>"><span id="elh_assets_status" class="assets_status"><?php echo $assets->status->caption() ?></span></th>
<?php } ?>
	</tr>
	</thead>
	<tbody>
<?php
$assets_delete->RecCnt = 0;
$i = 0;
while (!$assets_delete->Recordset->EOF) {
	$assets_delete->RecCnt++;
	$assets_delete->RowCnt++;

	// Set row properties
	$assets->resetAttributes();
	$assets->RowType = ROWTYPE_VIEW; // View

	// Get the field contents
	$assets_delete->loadRowValues($assets_delete->Recordset);

	// Render row
	$assets_delete->renderRow();
?>
	<tr<?php echo $assets->rowAttributes() ?>>
<?php if ($assets->kode_asset->Visible) { // kode_asset ?>
		<td<?php echo $assets->kode_asset->cellAttributes() ?>>
<span id="el<?php echo $assets_delete->RowCnt ?>_assets_kode_asset" class="assets_kode_asset">
<span<?php echo $assets->kode_asset->viewAttributes() ?>>
<?php echo $assets->kode_asset->getViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($assets->jenis_asset->Visible) { // jenis_asset ?>
		<td<?php echo $assets->jenis_asset->cellAttributes() ?>>
<span id="el<?php echo $assets_delete->RowCnt ?>_assets_jenis_asset" class="assets_jenis_asset">
<span<?php echo $assets->jenis_asset->viewAttributes() ?>>
<?php echo $assets->jenis_asset->getViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($assets->merk->Visible) { // merk ?>
		<td<?php echo $assets->merk->cellAttributes() ?>>
<span id="el<?php echo $assets_delete->RowCnt ?>_assets_merk" class="assets_merk">
<span<?php echo $assets->merk->viewAttributes() ?>>
<?php echo $assets->merk->getViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($assets->status->Visible) { // status ?>
		<td<?php echo $assets->status->cellAttributes() ?>>
<span id="el<?php echo $assets_delete->RowCnt ?>_assets_status" class="assets_status">
<span<?php echo $assets->status->viewAttributes() ?>>
<?php echo $assets->status->getViewValue() ?></span>
</span>
</td>
<?php } ?>
	</tr>
<?php
	$assets_delete->Recordset->moveNext();
}
$assets_delete->Recordset->close();
?>
</tbody>
</table>
</div>
</div>
<div>
<button class="btn btn-primary ew-btn" name="btn-action" id="btn-action" type="submit"><?php echo $Language->phrase("DeleteBtn") ?></button>
<button class="btn btn-default ew-btn" name="btn-cancel" id="btn-cancel" type="button" data-href="<?php echo $assets_delete->getReturnUrl() ?>"><?php echo $Language->phrase("CancelBtn") ?></button>
</div>
</form>
<?php
$assets_delete->showPageFooter();
if (DEBUG_ENABLED)
	echo GetDebugMessage();
?>
<script>

// Write your table-specific startup script here
// document.write("page loaded");

</script>
<?php include_once "footer.php" ?>
<?php
$assets_delete->terminate();
?>