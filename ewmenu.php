<?php
namespace PHPMaker2019\inventaris_assets;

// Menu Language
if ($Language && $Language->LanguageFolder == $LANGUAGE_FOLDER)
	$MenuLanguage = &$Language;
else
	$MenuLanguage = new Language();

// Navbar menu
$topMenu = new Menu("navbar", TRUE, TRUE);
echo $topMenu->toScript();

// Sidebar menu
$sideMenu = new Menu("menu", TRUE, FALSE);
$sideMenu->addMenuItem(3, "mi_transaksi_peminjaman", $MenuLanguage->MenuPhrase("3", "MenuText"), "transaksi_peminjamanlist.php", -1, "", AllowListMenu('{1137E948-42AA-49E0-9701-11726AEB00AF}transaksi_peminjaman'), FALSE, FALSE, "fa-arrow-circle-up ", "", FALSE);
$sideMenu->addMenuItem(4, "mi_transaksi_pengembalian", $MenuLanguage->MenuPhrase("4", "MenuText"), "transaksi_pengembalianlist.php", -1, "", AllowListMenu('{1137E948-42AA-49E0-9701-11726AEB00AF}transaksi_pengembalian'), FALSE, FALSE, "fa-arrow-circle-down", "", FALSE);
$sideMenu->addMenuItem(18, "mci_Master", $MenuLanguage->MenuPhrase("18", "MenuText"), "", -1, "", IsLoggedIn(), FALSE, TRUE, "fa-tasks", "", FALSE);
$sideMenu->addMenuItem(1, "mi_assets", $MenuLanguage->MenuPhrase("1", "MenuText"), "assetslist.php", 18, "", AllowListMenu('{1137E948-42AA-49E0-9701-11726AEB00AF}assets'), FALSE, FALSE, "fa-laptop ", "", FALSE);
$sideMenu->addMenuItem(19, "mi_jenis_asset", $MenuLanguage->MenuPhrase("19", "MenuText"), "jenis_assetlist.php", 18, "", AllowListMenu('{1137E948-42AA-49E0-9701-11726AEB00AF}jenis_asset'), FALSE, FALSE, "fa-book", "", FALSE);
$sideMenu->addMenuItem(2, "mi_karyawan", $MenuLanguage->MenuPhrase("2", "MenuText"), "karyawanlist.php", 18, "", AllowListMenu('{1137E948-42AA-49E0-9701-11726AEB00AF}karyawan'), FALSE, FALSE, "fa-id-card-o", "", FALSE);
$sideMenu->addMenuItem(5, "mi_users", $MenuLanguage->MenuPhrase("5", "MenuText"), "userslist.php", 18, "", AllowListMenu('{1137E948-42AA-49E0-9701-11726AEB00AF}users'), FALSE, FALSE, "fa-users ", "", FALSE);
echo $sideMenu->toScript();
?>