<?php
namespace PHPMaker2019\inventaris_assets;

// Session
if (session_status() !== PHP_SESSION_ACTIVE)
	session_start(); // Init session data

// Output buffering
ob_start(); 

// Autoload
include_once "autoload.php";
?>
<?php

// Write header
WriteHeader(FALSE);

// Create page object
$jenis_asset_view = new jenis_asset_view();

// Run the page
$jenis_asset_view->run();

// Setup login status
SetupLoginStatus();
SetClientVar("login", LoginStatus());

// Global Page Rendering event (in userfn*.php)
Page_Rendering();

// Page Rendering event
$jenis_asset_view->Page_Render();
?>
<?php include_once "header.php" ?>
<?php if (!$jenis_asset->isExport()) { ?>
<script>

// Form object
currentPageID = ew.PAGE_ID = "view";
var fjenis_assetview = currentForm = new ew.Form("fjenis_assetview", "view");

// Form_CustomValidate event
fjenis_assetview.Form_CustomValidate = function(fobj) { // DO NOT CHANGE THIS LINE!

	// Your custom validation code here, return false if invalid.
	return true;
}

// Use JavaScript validation or not
fjenis_assetview.validateRequired = <?php echo json_encode(CLIENT_VALIDATE) ?>;

// Dynamic selection lists
// Form object for search

</script>
<script>

// Write your client script here, no need to add script tags.
</script>
<?php } ?>
<?php if (!$jenis_asset->isExport()) { ?>
<div class="btn-toolbar ew-toolbar">
<?php $jenis_asset_view->ExportOptions->render("body") ?>
<?php $jenis_asset_view->OtherOptions->render("body") ?>
<div class="clearfix"></div>
</div>
<?php } ?>
<?php $jenis_asset_view->showPageHeader(); ?>
<?php
$jenis_asset_view->showMessage();
?>
<form name="fjenis_assetview" id="fjenis_assetview" class="form-inline ew-form ew-view-form" action="<?php echo CurrentPageName() ?>" method="post">
<?php if ($jenis_asset_view->CheckToken) { ?>
<input type="hidden" name="<?php echo TOKEN_NAME ?>" value="<?php echo $jenis_asset_view->Token ?>">
<?php } ?>
<input type="hidden" name="t" value="jenis_asset">
<input type="hidden" name="modal" value="<?php echo (int)$jenis_asset_view->IsModal ?>">
<table class="table table-striped table-sm ew-view-table">
<?php if ($jenis_asset->nama_jenis->Visible) { // nama_jenis ?>
	<tr id="r_nama_jenis">
		<td class="<?php echo $jenis_asset_view->TableLeftColumnClass ?>"><span id="elh_jenis_asset_nama_jenis"><?php echo $jenis_asset->nama_jenis->caption() ?></span></td>
		<td data-name="nama_jenis"<?php echo $jenis_asset->nama_jenis->cellAttributes() ?>>
<span id="el_jenis_asset_nama_jenis">
<span<?php echo $jenis_asset->nama_jenis->viewAttributes() ?>>
<?php echo $jenis_asset->nama_jenis->getViewValue() ?></span>
</span>
</td>
	</tr>
<?php } ?>
</table>
</form>
<?php
$jenis_asset_view->showPageFooter();
if (DEBUG_ENABLED)
	echo GetDebugMessage();
?>
<?php if (!$jenis_asset->isExport()) { ?>
<script>

// Write your table-specific startup script here
// document.write("page loaded");

</script>
<?php } ?>
<?php include_once "footer.php" ?>
<?php
$jenis_asset_view->terminate();
?>