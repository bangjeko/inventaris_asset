<?php
namespace PHPMaker2019\inventaris_assets;

/**
 * Page class
 */
class assets_add extends assets
{

	// Page ID
	public $PageID = "add";

	// Project ID
	public $ProjectID = "{1137E948-42AA-49E0-9701-11726AEB00AF}";

	// Table name
	public $TableName = 'assets';

	// Page object name
	public $PageObjName = "assets_add";

	// Page headings
	public $Heading = "";
	public $Subheading = "";
	public $PageHeader;
	public $PageFooter;

	// Token
	public $Token = "";
	public $TokenTimeout = 0;
	public $CheckToken = CHECK_TOKEN;

	// Messages
	private $_message = "";
	private $_failureMessage = "";
	private $_successMessage = "";
	private $_warningMessage = "";

	// Page URL
	private $_pageUrl = "";

	// Page heading
	public function pageHeading()
	{
		global $Language;
		if ($this->Heading <> "")
			return $this->Heading;
		if (method_exists($this, "tableCaption"))
			return $this->tableCaption();
		return "";
	}

	// Page subheading
	public function pageSubheading()
	{
		global $Language;
		if ($this->Subheading <> "")
			return $this->Subheading;
		if ($this->TableName)
			return $Language->phrase($this->PageID);
		return "";
	}

	// Page name
	public function pageName()
	{
		return CurrentPageName();
	}

	// Page URL
	public function pageUrl()
	{
		if ($this->_pageUrl == "") {
			$this->_pageUrl = CurrentPageName() . "?";
			if ($this->UseTokenInUrl)
				$this->_pageUrl .= "t=" . $this->TableVar . "&"; // Add page token
		}
		return $this->_pageUrl;
	}

	// Get message
	public function getMessage()
	{
		return isset($_SESSION[SESSION_MESSAGE]) ? $_SESSION[SESSION_MESSAGE] : $this->_message;
	}

	// Set message
	public function setMessage($v)
	{
		AddMessage($this->_message, $v);
		$_SESSION[SESSION_MESSAGE] = $this->_message;
	}

	// Get failure message
	public function getFailureMessage()
	{
		return isset($_SESSION[SESSION_FAILURE_MESSAGE]) ? $_SESSION[SESSION_FAILURE_MESSAGE] : $this->_failureMessage;
	}

	// Set failure message
	public function setFailureMessage($v)
	{
		AddMessage($this->_failureMessage, $v);
		$_SESSION[SESSION_FAILURE_MESSAGE] = $this->_failureMessage;
	}

	// Get success message
	public function getSuccessMessage()
	{
		return isset($_SESSION[SESSION_SUCCESS_MESSAGE]) ? $_SESSION[SESSION_SUCCESS_MESSAGE] : $this->_successMessage;
	}

	// Set success message
	public function setSuccessMessage($v)
	{
		AddMessage($this->_successMessage, $v);
		$_SESSION[SESSION_SUCCESS_MESSAGE] = $this->_successMessage;
	}

	// Get warning message
	public function getWarningMessage()
	{
		return isset($_SESSION[SESSION_WARNING_MESSAGE]) ? $_SESSION[SESSION_WARNING_MESSAGE] : $this->_warningMessage;
	}

	// Set warning message
	public function setWarningMessage($v)
	{
		AddMessage($this->_warningMessage, $v);
		$_SESSION[SESSION_WARNING_MESSAGE] = $this->_warningMessage;
	}

	// Clear message
	public function clearMessage()
	{
		$this->_message = "";
		$_SESSION[SESSION_MESSAGE] = "";
	}

	// Clear failure message
	public function clearFailureMessage()
	{
		$this->_failureMessage = "";
		$_SESSION[SESSION_FAILURE_MESSAGE] = "";
	}

	// Clear success message
	public function clearSuccessMessage()
	{
		$this->_successMessage = "";
		$_SESSION[SESSION_SUCCESS_MESSAGE] = "";
	}

	// Clear warning message
	public function clearWarningMessage()
	{
		$this->_warningMessage = "";
		$_SESSION[SESSION_WARNING_MESSAGE] = "";
	}

	// Clear messages
	public function clearMessages()
	{
		$this->clearMessage();
		$this->clearFailureMessage();
		$this->clearSuccessMessage();
		$this->clearWarningMessage();
	}

	// Show message
	public function showMessage()
	{
		$hidden = FALSE;
		$html = "";

		// Message
		$message = $this->getMessage();
		if (method_exists($this, "Message_Showing"))
			$this->Message_Showing($message, "");
		if ($message <> "") { // Message in Session, display
			if (!$hidden)
				$message = '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' . $message;
			$html .= '<div class="alert alert-info alert-dismissible ew-info"><i class="icon fa fa-info"></i>' . $message . '</div>';
			$_SESSION[SESSION_MESSAGE] = ""; // Clear message in Session
		}

		// Warning message
		$warningMessage = $this->getWarningMessage();
		if (method_exists($this, "Message_Showing"))
			$this->Message_Showing($warningMessage, "warning");
		if ($warningMessage <> "") { // Message in Session, display
			if (!$hidden)
				$warningMessage = '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' . $warningMessage;
			$html .= '<div class="alert alert-warning alert-dismissible ew-warning"><i class="icon fa fa-warning"></i>' . $warningMessage . '</div>';
			$_SESSION[SESSION_WARNING_MESSAGE] = ""; // Clear message in Session
		}

		// Success message
		$successMessage = $this->getSuccessMessage();
		if (method_exists($this, "Message_Showing"))
			$this->Message_Showing($successMessage, "success");
		if ($successMessage <> "") { // Message in Session, display
			if (!$hidden)
				$successMessage = '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' . $successMessage;
			$html .= '<div class="alert alert-success alert-dismissible ew-success"><i class="icon fa fa-check"></i>' . $successMessage . '</div>';
			$_SESSION[SESSION_SUCCESS_MESSAGE] = ""; // Clear message in Session
		}

		// Failure message
		$errorMessage = $this->getFailureMessage();
		if (method_exists($this, "Message_Showing"))
			$this->Message_Showing($errorMessage, "failure");
		if ($errorMessage <> "") { // Message in Session, display
			if (!$hidden)
				$errorMessage = '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' . $errorMessage;
			$html .= '<div class="alert alert-danger alert-dismissible ew-error"><i class="icon fa fa-ban"></i>' . $errorMessage . '</div>';
			$_SESSION[SESSION_FAILURE_MESSAGE] = ""; // Clear message in Session
		}
		echo '<div class="ew-message-dialog' . (($hidden) ? ' d-none' : "") . '">' . $html . '</div>';
	}

	// Get message as array
	public function getMessages()
	{
		$ar = array();

		// Message
		$message = $this->getMessage();

		//if (method_exists($this, "Message_Showing"))
		//	$this->Message_Showing($message, "");

		if ($message <> "") { // Message in Session, display
			$ar["message"] = $message;
			$_SESSION[SESSION_MESSAGE] = ""; // Clear message in Session
		}

		// Warning message
		$warningMessage = $this->getWarningMessage();

		//if (method_exists($this, "Message_Showing"))
		//	$this->Message_Showing($warningMessage, "warning");

		if ($warningMessage <> "") { // Message in Session, display
			$ar["warningMessage"] = $warningMessage;
			$_SESSION[SESSION_WARNING_MESSAGE] = ""; // Clear message in Session
		}

		// Success message
		$successMessage = $this->getSuccessMessage();

		//if (method_exists($this, "Message_Showing"))
		//	$this->Message_Showing($successMessage, "success");

		if ($successMessage <> "") { // Message in Session, display
			$ar["successMessage"] = $successMessage;
			$_SESSION[SESSION_SUCCESS_MESSAGE] = ""; // Clear message in Session
		}

		// Failure message
		$failureMessage = $this->getFailureMessage();

		//if (method_exists($this, "Message_Showing"))
		//	$this->Message_Showing($failureMessage, "failure");

		if ($failureMessage <> "") { // Message in Session, display
			$ar["failureMessage"] = $failureMessage;
			$_SESSION[SESSION_FAILURE_MESSAGE] = ""; // Clear message in Session
		}
		return $ar;
	}

	// Show Page Header
	public function showPageHeader()
	{
		$header = $this->PageHeader;
		$this->Page_DataRendering($header);
		if ($header <> "") { // Header exists, display
			echo '<p id="ew-page-header">' . $header . '</p>';
		}
	}

	// Show Page Footer
	public function showPageFooter()
	{
		$footer = $this->PageFooter;
		$this->Page_DataRendered($footer);
		if ($footer <> "") { // Footer exists, display
			echo '<p id="ew-page-footer">' . $footer . '</p>';
		}
	}

	// Validate page request
	protected function isPageRequest()
	{
		global $CurrentForm;
		if ($this->UseTokenInUrl) {
			if ($CurrentForm)
				return ($this->TableVar == $CurrentForm->getValue("t"));
			if (Get("t") !== NULL)
				return ($this->TableVar == Get("t"));
		}
		return TRUE;
	}

	// Valid Post
	protected function validPost()
	{
		if (!$this->CheckToken || !IsPost() || IsApi())
			return TRUE;
		if (Post(TOKEN_NAME) === NULL)
			return FALSE;
		$fn = PROJECT_NAMESPACE . CHECK_TOKEN_FUNC;
		if (is_callable($fn))
			return $fn(Post(TOKEN_NAME), $this->TokenTimeout);
		return FALSE;
	}

	// Create Token
	public function createToken()
	{
		global $CurrentToken;
		$fn = PROJECT_NAMESPACE . CREATE_TOKEN_FUNC; // Always create token, required by API file/lookup request
		if ($this->Token == "" && is_callable($fn)) // Create token
			$this->Token = $fn();
		$CurrentToken = $this->Token; // Save to global variable
	}

	// Constructor
	public function __construct()
	{
		global $Language, $COMPOSITE_KEY_SEPARATOR;
		global $UserTable, $UserTableConn;

		// Initialize
		$GLOBALS["Page"] = &$this;
		$this->TokenTimeout = SessionTimeoutTime();

		// Language object
		if (!isset($Language))
			$Language = new Language();

		// Parent constuctor
		parent::__construct();

		// Table object (assets)
		if (!isset($GLOBALS["assets"]) || get_class($GLOBALS["assets"]) == PROJECT_NAMESPACE . "assets") {
			$GLOBALS["assets"] = &$this;
			$GLOBALS["Table"] = &$GLOBALS["assets"];
		}
		$this->CancelUrl = $this->pageUrl() . "action=cancel";

		// Table object (users)
		if (!isset($GLOBALS['users']))
			$GLOBALS['users'] = new users();

		// Page ID
		if (!defined(PROJECT_NAMESPACE . "PAGE_ID"))
			define(PROJECT_NAMESPACE . "PAGE_ID", 'add');

		// Table name (for backward compatibility)
		if (!defined(PROJECT_NAMESPACE . "TABLE_NAME"))
			define(PROJECT_NAMESPACE . "TABLE_NAME", 'assets');

		// Start timer
		if (!isset($GLOBALS["DebugTimer"]))
			$GLOBALS["DebugTimer"] = new Timer();

		// Debug message
		LoadDebugMessage();

		// Open connection
		if (!isset($GLOBALS["Conn"]))
			$GLOBALS["Conn"] = &$this->getConnection();

		// User table object (users)
		if (!isset($UserTable)) {
			$UserTable = new users();
			$UserTableConn = Conn($UserTable->Dbid);
		}
	}

	// Terminate page
	public function terminate($url = "")
	{
		global $ExportFileName, $TempImages;

		// Page Unload event
		$this->Page_Unload();

		// Global Page Unloaded event (in userfn*.php)
		Page_Unloaded();

		// Export
		global $EXPORT, $assets;
		if ($this->CustomExport && $this->CustomExport == $this->Export && array_key_exists($this->CustomExport, $EXPORT)) {
				$content = ob_get_contents();
			if ($ExportFileName == "")
				$ExportFileName = $this->TableVar;
			$class = PROJECT_NAMESPACE . $EXPORT[$this->CustomExport];
			if (class_exists($class)) {
				$doc = new $class($assets);
				$doc->Text = @$content;
				if ($this->isExport("email"))
					echo $this->exportEmail($doc->Text);
				else
					$doc->export();
				DeleteTempImages(); // Delete temp images
				exit();
			}
		}
		if (!IsApi())
			$this->Page_Redirecting($url);

		// Close connection
		CloseConnections();

		// Return for API
		if (IsApi()) {
			$res = $url === TRUE;
			if (!$res) // Show error
				WriteJson(array_merge(["success" => FALSE], $this->getMessages()));
			return;
		}

		// Go to URL if specified
		if ($url <> "") {
			if (!DEBUG_ENABLED && ob_get_length())
				ob_end_clean();

			// Handle modal response
			if ($this->IsModal) { // Show as modal
				$row = array("url" => $url, "modal" => "1");
				$pageName = GetPageName($url);
				if ($pageName != $this->getListUrl()) { // Not List page
					$row["caption"] = $this->getModalCaption($pageName);
					if ($pageName == "assetsview.php")
						$row["view"] = "1";
				} else { // List page should not be shown as modal => error
					$row["error"] = $this->getFailureMessage();
					$this->clearFailureMessage();
				}
				WriteJson($row);
			} else {
				SaveDebugMessage();
				AddHeader("Location", $url);
			}
		}
		exit();
	}

	// Get records from recordset
	protected function getRecordsFromRecordset($rs, $current = FALSE)
	{
		$rows = array();
		if (is_object($rs)) { // Recordset
			while ($rs && !$rs->EOF) {
				$this->loadRowValues($rs); // Set up DbValue/CurrentValue
				$row = $this->getRecordFromArray($rs->fields);
				if ($current)
					return $row;
				else
					$rows[] = $row;
				$rs->moveNext();
			}
		} elseif (is_array($rs)) {
			foreach ($rs as $ar) {
				$row = $this->getRecordFromArray($ar);
				if ($current)
					return $row;
				else
					$rows[] = $row;
			}
		}
		return $rows;
	}

	// Get record from array
	protected function getRecordFromArray($ar)
	{
		$row = array();
		if (is_array($ar)) {
			foreach ($ar as $fldname => $val) {
				if (array_key_exists($fldname, $this->fields) && ($this->fields[$fldname]->Visible || $this->fields[$fldname]->IsPrimaryKey)) { // Primary key or Visible
					$fld = &$this->fields[$fldname];
					if ($fld->HtmlTag == "FILE") { // Upload field
						if (EmptyValue($val)) {
							$row[$fldname] = NULL;
						} else {
							if ($fld->DataType == DATATYPE_BLOB) {

								//$url = FullUrl($fld->TableVar . "/" . API_FILE_ACTION . "/" . $fld->Param . "/" . rawurlencode($this->getRecordKeyValue($ar))); // URL rewrite format
								$url = FullUrl(GetPageName(API_URL) . "?" . API_OBJECT_NAME . "=" . $fld->TableVar . "&" . API_ACTION_NAME . "=" . API_FILE_ACTION . "&" . API_FIELD_NAME . "=" . $fld->Param . "&" . API_KEY_NAME . "=" . rawurlencode($this->getRecordKeyValue($ar))); // Query string format
								$row[$fldname] = ["mimeType" => ContentType($val), "url" => $url];
							} elseif (!$fld->UploadMultiple || !ContainsString($val, MULTIPLE_UPLOAD_SEPARATOR)) { // Single file
								$row[$fldname] = ["mimeType" => MimeContentType($val), "url" => FullUrl($fld->hrefPath() . $val)];
							} else { // Multiple files
								$files = explode(MULTIPLE_UPLOAD_SEPARATOR, $val);
								$ar = [];
								foreach ($files as $file) {
									if (!EmptyValue($file))
										$ar[] = ["type" => MimeContentType($file), "url" => FullUrl($fld->hrefPath() . $file)];
								}
								$row[$fldname] = $ar;
							}
						}
					} else {
						$row[$fldname] = $val;
					}
				}
			}
		}
		return $row;
	}

	// Get record key value from array
	protected function getRecordKeyValue($ar)
	{
		global $COMPOSITE_KEY_SEPARATOR;
		$key = "";
		if (is_array($ar)) {
			$key .= @$ar['id'];
		}
		return $key;
	}

	/**
	 * Hide fields for add/edit
	 *
	 * @return void
	 */
	protected function hideFieldsForAddEdit()
	{
		if ($this->isAdd() || $this->isCopy() || $this->isGridAdd())
			$this->id->Visible = FALSE;
	}
	public $FormClassName = "ew-horizontal ew-form ew-add-form";
	public $IsModal = FALSE;
	public $IsMobileOrModal = FALSE;
	public $DbMasterFilter = "";
	public $DbDetailFilter = "";
	public $StartRec;
	public $Priv = 0;
	public $OldRecordset;
	public $CopyRecord;

	//
	// Page run
	//

	public function run()
	{
		global $ExportType, $CustomExportType, $ExportFileName, $UserProfile, $Language, $Security, $RequestSecurity, $CurrentForm,
			$FormError, $SkipHeaderFooter;

		// Init Session data for API request if token found
		if (IsApi() && session_status() !== PHP_SESSION_ACTIVE) {
			$func = PROJECT_NAMESPACE . CHECK_TOKEN_FUNC;
			if (is_callable($func) && Param(TOKEN_NAME) !== NULL && $func(Param(TOKEN_NAME), SessionTimeoutTime()))
				session_start();
		}

		// Is modal
		$this->IsModal = (Param("modal") == "1");

		// User profile
		$UserProfile = new UserProfile();

		// Security
		$Security = new AdvancedSecurity();
		$validRequest = FALSE;

		// Check security for API request
		If (IsApi()) {

			// Check token first
			$func = PROJECT_NAMESPACE . CHECK_TOKEN_FUNC;
			if (is_callable($func) && Post(TOKEN_NAME) !== NULL)
				$validRequest = $func(Post(TOKEN_NAME), SessionTimeoutTime());
			elseif (is_array($RequestSecurity) && @$RequestSecurity["username"] <> "") // Login user for API request
				$Security->loginUser(@$RequestSecurity["username"], @$RequestSecurity["userid"], @$RequestSecurity["parentuserid"], @$RequestSecurity["userlevelid"]);
		}
		if (!$validRequest) {
			if (!$Security->isLoggedIn())
				$Security->autoLogin();
			if ($Security->isLoggedIn())
				$Security->TablePermission_Loading();
			$Security->loadCurrentUserLevel($this->ProjectID . $this->TableName);
			if ($Security->isLoggedIn())
				$Security->TablePermission_Loaded();
			if (!$Security->canAdd()) {
				$Security->saveLastUrl();
				$this->setFailureMessage(DeniedMessage()); // Set no permission
				if ($Security->canList())
					$this->terminate(GetUrl("assetslist.php"));
				else
					$this->terminate(GetUrl("login.php"));
				return;
			}
			if ($Security->isLoggedIn()) {
				$Security->UserID_Loading();
				$Security->loadUserID();
				$Security->UserID_Loaded();
			}
		}

		// Create form object
		$CurrentForm = new HttpForm();
		$this->CurrentAction = Param("action"); // Set up current action
		$this->id->Visible = FALSE;
		$this->kode_asset->setVisibility();
		$this->jenis_asset->setVisibility();
		$this->merk->setVisibility();
		$this->spesifikasi->setVisibility();
		$this->foto->setVisibility();
		$this->kelengkapan_awal->setVisibility();
		$this->keterangan_terbaru->setVisibility();
		$this->status->setVisibility();
		$this->created_by->setVisibility();
		$this->created_date->setVisibility();
		$this->current_asign->Visible = FALSE;
		$this->assign_date->Visible = FALSE;
		$this->back_date->Visible = FALSE;
		$this->hideFieldsForAddEdit();

		// Do not use lookup cache
		$this->setUseLookupCache(FALSE);

		// Global Page Loading event (in userfn*.php)
		Page_Loading();

		// Page Load event
		$this->Page_Load();

		// Check token
		if (!$this->validPost()) {
			Write($Language->phrase("InvalidPostRequest"));
			$this->terminate();
		}

		// Create Token
		$this->createToken();

		// Set up lookup cache
		$this->setupLookupOptions($this->jenis_asset);
		$this->setupLookupOptions($this->created_by);
		$this->setupLookupOptions($this->current_asign);

		// Check modal
		if ($this->IsModal)
			$SkipHeaderFooter = TRUE;
		$this->IsMobileOrModal = IsMobile() || $this->IsModal;
		$this->FormClassName = "ew-form ew-add-form ew-horizontal";
		$postBack = FALSE;

		// Set up current action
		if (IsApi()) {
			$this->CurrentAction = "insert"; // Add record directly
			$postBack = TRUE;
		} elseif (Post("action") !== NULL) {
			$this->CurrentAction = Post("action"); // Get form action
			$postBack = TRUE;
		} else { // Not post back

			// Load key values from QueryString
			$this->CopyRecord = TRUE;
			if (Get("id") !== NULL) {
				$this->id->setQueryStringValue(Get("id"));
				$this->setKey("id", $this->id->CurrentValue); // Set up key
			} else {
				$this->setKey("id", ""); // Clear key
				$this->CopyRecord = FALSE;
			}
			if ($this->CopyRecord) {
				$this->CurrentAction = "copy"; // Copy record
			} else {
				$this->CurrentAction = "show"; // Display blank record
			}
		}

		// Load old record / default values
		$loaded = $this->loadOldRecord();

		// Load form values
		if ($postBack) {
			$this->loadFormValues(); // Load form values
		}

		// Validate form if post back
		if ($postBack) {
			if (!$this->validateForm()) {
				$this->EventCancelled = TRUE; // Event cancelled
				$this->restoreFormValues(); // Restore form values
				$this->setFailureMessage($FormError);
				if (IsApi()) {
					$this->terminate();
					return;
				} else {
					$this->CurrentAction = "show"; // Form error, reset action
				}
			}
		}

		// Perform current action
		switch ($this->CurrentAction) {
			case "copy": // Copy an existing record
				if (!$loaded) { // Record not loaded
					if ($this->getFailureMessage() == "")
						$this->setFailureMessage($Language->phrase("NoRecord")); // No record found
					$this->terminate("assetslist.php"); // No matching record, return to list
				}
				break;
			case "insert": // Add new record
				$this->SendEmail = TRUE; // Send email on add success
				if ($this->addRow($this->OldRecordset)) { // Add successful
					if ($this->getSuccessMessage() == "")
						$this->setSuccessMessage($Language->phrase("AddSuccess")); // Set up success message
					$returnUrl = $this->getReturnUrl();
					if (GetPageName($returnUrl) == "assetslist.php")
						$returnUrl = $this->addMasterUrl($returnUrl); // List page, return to List page with correct master key if necessary
					elseif (GetPageName($returnUrl) == "assetsview.php")
						$returnUrl = $this->getViewUrl(); // View page, return to View page with keyurl directly
					if (IsApi()) { // Return to caller
						$this->terminate(TRUE);
						return;
					} else {
						$this->terminate($returnUrl);
					}
				} elseif (IsApi()) { // API request, return
					$this->terminate();
					return;
				} else {
					$this->EventCancelled = TRUE; // Event cancelled
					$this->restoreFormValues(); // Add failed, restore form values
				}
		}

		// Set up Breadcrumb
		$this->setupBreadcrumb();

		// Render row based on row type
		$this->RowType = ROWTYPE_ADD; // Render add type

		// Render row
		$this->resetAttributes();
		$this->renderRow();
	}

	// Get upload files
	protected function getUploadFiles()
	{
		global $CurrentForm, $Language;
		$this->foto->Upload->Index = $CurrentForm->Index;
		$this->foto->Upload->uploadFile();
		$this->foto->CurrentValue = $this->foto->Upload->FileName;
	}

	// Load default values
	protected function loadDefaultValues()
	{
		$this->id->CurrentValue = NULL;
		$this->id->OldValue = $this->id->CurrentValue;
		$this->kode_asset->CurrentValue = NULL;
		$this->kode_asset->OldValue = $this->kode_asset->CurrentValue;
		$this->jenis_asset->CurrentValue = NULL;
		$this->jenis_asset->OldValue = $this->jenis_asset->CurrentValue;
		$this->merk->CurrentValue = NULL;
		$this->merk->OldValue = $this->merk->CurrentValue;
		$this->spesifikasi->CurrentValue = NULL;
		$this->spesifikasi->OldValue = $this->spesifikasi->CurrentValue;
		$this->foto->Upload->DbValue = NULL;
		$this->foto->OldValue = $this->foto->Upload->DbValue;
		$this->foto->CurrentValue = NULL; // Clear file related field
		$this->kelengkapan_awal->CurrentValue = NULL;
		$this->kelengkapan_awal->OldValue = $this->kelengkapan_awal->CurrentValue;
		$this->keterangan_terbaru->CurrentValue = NULL;
		$this->keterangan_terbaru->OldValue = $this->keterangan_terbaru->CurrentValue;
		$this->status->CurrentValue = NULL;
		$this->status->OldValue = $this->status->CurrentValue;
		$this->created_by->CurrentValue = NULL;
		$this->created_by->OldValue = $this->created_by->CurrentValue;
		$this->created_date->CurrentValue = NULL;
		$this->created_date->OldValue = $this->created_date->CurrentValue;
		$this->current_asign->CurrentValue = NULL;
		$this->current_asign->OldValue = $this->current_asign->CurrentValue;
		$this->assign_date->CurrentValue = NULL;
		$this->assign_date->OldValue = $this->assign_date->CurrentValue;
		$this->back_date->CurrentValue = NULL;
		$this->back_date->OldValue = $this->back_date->CurrentValue;
	}

	// Load form values
	protected function loadFormValues()
	{

		// Load from form
		global $CurrentForm;
		$this->getUploadFiles(); // Get upload files

		// Check field name 'kode_asset' first before field var 'x_kode_asset'
		$val = $CurrentForm->hasValue("kode_asset") ? $CurrentForm->getValue("kode_asset") : $CurrentForm->getValue("x_kode_asset");
		if (!$this->kode_asset->IsDetailKey) {
			if (IsApi() && $val == NULL)
				$this->kode_asset->Visible = FALSE; // Disable update for API request
			else
				$this->kode_asset->setFormValue($val);
		}

		// Check field name 'jenis_asset' first before field var 'x_jenis_asset'
		$val = $CurrentForm->hasValue("jenis_asset") ? $CurrentForm->getValue("jenis_asset") : $CurrentForm->getValue("x_jenis_asset");
		if (!$this->jenis_asset->IsDetailKey) {
			if (IsApi() && $val == NULL)
				$this->jenis_asset->Visible = FALSE; // Disable update for API request
			else
				$this->jenis_asset->setFormValue($val);
		}

		// Check field name 'merk' first before field var 'x_merk'
		$val = $CurrentForm->hasValue("merk") ? $CurrentForm->getValue("merk") : $CurrentForm->getValue("x_merk");
		if (!$this->merk->IsDetailKey) {
			if (IsApi() && $val == NULL)
				$this->merk->Visible = FALSE; // Disable update for API request
			else
				$this->merk->setFormValue($val);
		}

		// Check field name 'spesifikasi' first before field var 'x_spesifikasi'
		$val = $CurrentForm->hasValue("spesifikasi") ? $CurrentForm->getValue("spesifikasi") : $CurrentForm->getValue("x_spesifikasi");
		if (!$this->spesifikasi->IsDetailKey) {
			if (IsApi() && $val == NULL)
				$this->spesifikasi->Visible = FALSE; // Disable update for API request
			else
				$this->spesifikasi->setFormValue($val);
		}

		// Check field name 'kelengkapan_awal' first before field var 'x_kelengkapan_awal'
		$val = $CurrentForm->hasValue("kelengkapan_awal") ? $CurrentForm->getValue("kelengkapan_awal") : $CurrentForm->getValue("x_kelengkapan_awal");
		if (!$this->kelengkapan_awal->IsDetailKey) {
			if (IsApi() && $val == NULL)
				$this->kelengkapan_awal->Visible = FALSE; // Disable update for API request
			else
				$this->kelengkapan_awal->setFormValue($val);
		}

		// Check field name 'keterangan_terbaru' first before field var 'x_keterangan_terbaru'
		$val = $CurrentForm->hasValue("keterangan_terbaru") ? $CurrentForm->getValue("keterangan_terbaru") : $CurrentForm->getValue("x_keterangan_terbaru");
		if (!$this->keterangan_terbaru->IsDetailKey) {
			if (IsApi() && $val == NULL)
				$this->keterangan_terbaru->Visible = FALSE; // Disable update for API request
			else
				$this->keterangan_terbaru->setFormValue($val);
		}

		// Check field name 'status' first before field var 'x_status'
		$val = $CurrentForm->hasValue("status") ? $CurrentForm->getValue("status") : $CurrentForm->getValue("x_status");
		if (!$this->status->IsDetailKey) {
			if (IsApi() && $val == NULL)
				$this->status->Visible = FALSE; // Disable update for API request
			else
				$this->status->setFormValue($val);
		}

		// Check field name 'created_by' first before field var 'x_created_by'
		$val = $CurrentForm->hasValue("created_by") ? $CurrentForm->getValue("created_by") : $CurrentForm->getValue("x_created_by");
		if (!$this->created_by->IsDetailKey) {
			if (IsApi() && $val == NULL)
				$this->created_by->Visible = FALSE; // Disable update for API request
			else
				$this->created_by->setFormValue($val);
		}

		// Check field name 'created_date' first before field var 'x_created_date'
		$val = $CurrentForm->hasValue("created_date") ? $CurrentForm->getValue("created_date") : $CurrentForm->getValue("x_created_date");
		if (!$this->created_date->IsDetailKey) {
			if (IsApi() && $val == NULL)
				$this->created_date->Visible = FALSE; // Disable update for API request
			else
				$this->created_date->setFormValue($val);
			$this->created_date->CurrentValue = UnFormatDateTime($this->created_date->CurrentValue, 0);
		}

		// Check field name 'id' first before field var 'x_id'
		$val = $CurrentForm->hasValue("id") ? $CurrentForm->getValue("id") : $CurrentForm->getValue("x_id");
	}

	// Restore form values
	public function restoreFormValues()
	{
		global $CurrentForm;
		$this->kode_asset->CurrentValue = $this->kode_asset->FormValue;
		$this->jenis_asset->CurrentValue = $this->jenis_asset->FormValue;
		$this->merk->CurrentValue = $this->merk->FormValue;
		$this->spesifikasi->CurrentValue = $this->spesifikasi->FormValue;
		$this->kelengkapan_awal->CurrentValue = $this->kelengkapan_awal->FormValue;
		$this->keterangan_terbaru->CurrentValue = $this->keterangan_terbaru->FormValue;
		$this->status->CurrentValue = $this->status->FormValue;
		$this->created_by->CurrentValue = $this->created_by->FormValue;
		$this->created_date->CurrentValue = $this->created_date->FormValue;
		$this->created_date->CurrentValue = UnFormatDateTime($this->created_date->CurrentValue, 0);
	}

	// Load row based on key values
	public function loadRow()
	{
		global $Security, $Language;
		$filter = $this->getRecordFilter();

		// Call Row Selecting event
		$this->Row_Selecting($filter);

		// Load SQL based on filter
		$this->CurrentFilter = $filter;
		$sql = $this->getCurrentSql();
		$conn = &$this->getConnection();
		$res = FALSE;
		$rs = LoadRecordset($sql, $conn);
		if ($rs && !$rs->EOF) {
			$res = TRUE;
			$this->loadRowValues($rs); // Load row values
			$rs->close();
		}
		return $res;
	}

	// Load row values from recordset
	public function loadRowValues($rs = NULL)
	{
		if ($rs && !$rs->EOF)
			$row = $rs->fields;
		else
			$row = $this->newRow();

		// Call Row Selected event
		$this->Row_Selected($row);
		if (!$rs || $rs->EOF)
			return;
		$this->id->setDbValue($row['id']);
		$this->kode_asset->setDbValue($row['kode_asset']);
		$this->jenis_asset->setDbValue($row['jenis_asset']);
		$this->merk->setDbValue($row['merk']);
		$this->spesifikasi->setDbValue($row['spesifikasi']);
		$this->foto->Upload->DbValue = $row['foto'];
		$this->foto->setDbValue($this->foto->Upload->DbValue);
		$this->kelengkapan_awal->setDbValue($row['kelengkapan_awal']);
		$this->keterangan_terbaru->setDbValue($row['keterangan_terbaru']);
		$this->status->setDbValue($row['status']);
		$this->created_by->setDbValue($row['created_by']);
		$this->created_date->setDbValue($row['created_date']);
		$this->current_asign->setDbValue($row['current_asign']);
		$this->assign_date->setDbValue($row['assign_date']);
		$this->back_date->setDbValue($row['back_date']);
	}

	// Return a row with default values
	protected function newRow()
	{
		$this->loadDefaultValues();
		$row = [];
		$row['id'] = $this->id->CurrentValue;
		$row['kode_asset'] = $this->kode_asset->CurrentValue;
		$row['jenis_asset'] = $this->jenis_asset->CurrentValue;
		$row['merk'] = $this->merk->CurrentValue;
		$row['spesifikasi'] = $this->spesifikasi->CurrentValue;
		$row['foto'] = $this->foto->Upload->DbValue;
		$row['kelengkapan_awal'] = $this->kelengkapan_awal->CurrentValue;
		$row['keterangan_terbaru'] = $this->keterangan_terbaru->CurrentValue;
		$row['status'] = $this->status->CurrentValue;
		$row['created_by'] = $this->created_by->CurrentValue;
		$row['created_date'] = $this->created_date->CurrentValue;
		$row['current_asign'] = $this->current_asign->CurrentValue;
		$row['assign_date'] = $this->assign_date->CurrentValue;
		$row['back_date'] = $this->back_date->CurrentValue;
		return $row;
	}

	// Load old record
	protected function loadOldRecord()
	{

		// Load key values from Session
		$validKey = TRUE;
		if (strval($this->getKey("id")) <> "")
			$this->id->CurrentValue = $this->getKey("id"); // id
		else
			$validKey = FALSE;

		// Load old record
		$this->OldRecordset = NULL;
		if ($validKey) {
			$this->CurrentFilter = $this->getRecordFilter();
			$sql = $this->getCurrentSql();
			$conn = &$this->getConnection();
			$this->OldRecordset = LoadRecordset($sql, $conn);
		}
		$this->loadRowValues($this->OldRecordset); // Load row values
		return $validKey;
	}

	// Render row values based on field settings
	public function renderRow()
	{
		global $Security, $Language, $CurrentLanguage;

		// Initialize URLs
		// Call Row_Rendering event

		$this->Row_Rendering();

		// Common render codes for all row types
		// id
		// kode_asset
		// jenis_asset
		// merk
		// spesifikasi
		// foto
		// kelengkapan_awal
		// keterangan_terbaru
		// status
		// created_by
		// created_date
		// current_asign
		// assign_date
		// back_date

		if ($this->RowType == ROWTYPE_VIEW) { // View row

			// kode_asset
			$this->kode_asset->ViewValue = $this->kode_asset->CurrentValue;
			$this->kode_asset->ViewCustomAttributes = "";

			// jenis_asset
			$curVal = strval($this->jenis_asset->CurrentValue);
			if ($curVal <> "") {
				$this->jenis_asset->ViewValue = $this->jenis_asset->lookupCacheOption($curVal);
				if ($this->jenis_asset->ViewValue === NULL) { // Lookup from database
					$filterWrk = "`id`" . SearchString("=", $curVal, DATATYPE_NUMBER, "");
					$sqlWrk = $this->jenis_asset->Lookup->getSql(FALSE, $filterWrk, '', $this);
					$rswrk = Conn()->execute($sqlWrk);
					if ($rswrk && !$rswrk->EOF) { // Lookup values found
						$arwrk = array();
						$arwrk[1] = $rswrk->fields('df');
						$this->jenis_asset->ViewValue = $this->jenis_asset->displayValue($arwrk);
						$rswrk->Close();
					} else {
						$this->jenis_asset->ViewValue = $this->jenis_asset->CurrentValue;
					}
				}
			} else {
				$this->jenis_asset->ViewValue = NULL;
			}
			$this->jenis_asset->ViewCustomAttributes = "";

			// merk
			$this->merk->ViewValue = $this->merk->CurrentValue;
			$this->merk->ViewCustomAttributes = "";

			// spesifikasi
			$this->spesifikasi->ViewValue = $this->spesifikasi->CurrentValue;
			$this->spesifikasi->ViewCustomAttributes = "";

			// foto
			$this->foto->UploadPath = "custom/uploads/foto_assets";
			if (!EmptyValue($this->foto->Upload->DbValue)) {
				$this->foto->ImageWidth = 50;
				$this->foto->ImageHeight = 50;
				$this->foto->ImageAlt = $this->foto->alt();
				$this->foto->ViewValue = $this->foto->Upload->DbValue;
			} else {
				$this->foto->ViewValue = "";
			}
			$this->foto->ViewCustomAttributes = "";

			// kelengkapan_awal
			$this->kelengkapan_awal->ViewValue = $this->kelengkapan_awal->CurrentValue;
			$this->kelengkapan_awal->ViewCustomAttributes = "";

			// keterangan_terbaru
			$this->keterangan_terbaru->ViewValue = $this->keterangan_terbaru->CurrentValue;
			$this->keterangan_terbaru->ViewCustomAttributes = "";

			// status
			if (strval($this->status->CurrentValue) <> "") {
				$this->status->ViewValue = $this->status->optionCaption($this->status->CurrentValue);
			} else {
				$this->status->ViewValue = NULL;
			}
			$this->status->ViewCustomAttributes = "";

			// created_by
			$curVal = strval($this->created_by->CurrentValue);
			if ($curVal <> "") {
				$this->created_by->ViewValue = $this->created_by->lookupCacheOption($curVal);
				if ($this->created_by->ViewValue === NULL) { // Lookup from database
					$filterWrk = "`id`" . SearchString("=", $curVal, DATATYPE_NUMBER, "");
					$sqlWrk = $this->created_by->Lookup->getSql(FALSE, $filterWrk, '', $this);
					$rswrk = Conn()->execute($sqlWrk);
					if ($rswrk && !$rswrk->EOF) { // Lookup values found
						$arwrk = array();
						$arwrk[1] = $rswrk->fields('df');
						$this->created_by->ViewValue = $this->created_by->displayValue($arwrk);
						$rswrk->Close();
					} else {
						$this->created_by->ViewValue = $this->created_by->CurrentValue;
					}
				}
			} else {
				$this->created_by->ViewValue = NULL;
			}
			$this->created_by->ViewCustomAttributes = "";

			// created_date
			$this->created_date->ViewValue = $this->created_date->CurrentValue;
			$this->created_date->ViewValue = FormatDateTime($this->created_date->ViewValue, 0);
			$this->created_date->ViewCustomAttributes = "";

			// current_asign
			$curVal = strval($this->current_asign->CurrentValue);
			if ($curVal <> "") {
				$this->current_asign->ViewValue = $this->current_asign->lookupCacheOption($curVal);
				if ($this->current_asign->ViewValue === NULL) { // Lookup from database
					$filterWrk = "`id`" . SearchString("=", $curVal, DATATYPE_NUMBER, "");
					$sqlWrk = $this->current_asign->Lookup->getSql(FALSE, $filterWrk, '', $this);
					$rswrk = Conn()->execute($sqlWrk);
					if ($rswrk && !$rswrk->EOF) { // Lookup values found
						$arwrk = array();
						$arwrk[1] = $rswrk->fields('df');
						$arwrk[2] = $rswrk->fields('df2');
						$this->current_asign->ViewValue = $this->current_asign->displayValue($arwrk);
						$rswrk->Close();
					} else {
						$this->current_asign->ViewValue = $this->current_asign->CurrentValue;
					}
				}
			} else {
				$this->current_asign->ViewValue = NULL;
			}
			$this->current_asign->ViewCustomAttributes = "";

			// assign_date
			$this->assign_date->ViewValue = $this->assign_date->CurrentValue;
			$this->assign_date->ViewValue = FormatDateTime($this->assign_date->ViewValue, 0);
			$this->assign_date->ViewCustomAttributes = "";

			// back_date
			$this->back_date->ViewValue = $this->back_date->CurrentValue;
			$this->back_date->ViewValue = FormatDateTime($this->back_date->ViewValue, 0);
			$this->back_date->ViewCustomAttributes = "";

			// kode_asset
			$this->kode_asset->LinkCustomAttributes = "";
			$this->kode_asset->HrefValue = "";
			$this->kode_asset->TooltipValue = "";

			// jenis_asset
			$this->jenis_asset->LinkCustomAttributes = "";
			$this->jenis_asset->HrefValue = "";
			$this->jenis_asset->TooltipValue = "";

			// merk
			$this->merk->LinkCustomAttributes = "";
			$this->merk->HrefValue = "";
			$this->merk->TooltipValue = "";

			// spesifikasi
			$this->spesifikasi->LinkCustomAttributes = "";
			$this->spesifikasi->HrefValue = "";
			$this->spesifikasi->TooltipValue = "";

			// foto
			$this->foto->LinkCustomAttributes = "";
			$this->foto->UploadPath = "custom/uploads/foto_assets";
			if (!EmptyValue($this->foto->Upload->DbValue)) {
				$this->foto->HrefValue = GetFileUploadUrl($this->foto, $this->foto->Upload->DbValue); // Add prefix/suffix
				$this->foto->LinkAttrs["target"] = ""; // Add target
				if ($this->isExport()) $this->foto->HrefValue = FullUrl($this->foto->HrefValue, "href");
			} else {
				$this->foto->HrefValue = "";
			}
			$this->foto->ExportHrefValue = $this->foto->UploadPath . $this->foto->Upload->DbValue;
			$this->foto->TooltipValue = "";
			if ($this->foto->UseColorbox) {
				if (EmptyValue($this->foto->TooltipValue))
					$this->foto->LinkAttrs["title"] = $Language->phrase("ViewImageGallery");
				$this->foto->LinkAttrs["data-rel"] = "assets_x_foto";
				AppendClass($this->foto->LinkAttrs["class"], "ew-lightbox");
			}

			// kelengkapan_awal
			$this->kelengkapan_awal->LinkCustomAttributes = "";
			$this->kelengkapan_awal->HrefValue = "";
			$this->kelengkapan_awal->TooltipValue = "";

			// keterangan_terbaru
			$this->keterangan_terbaru->LinkCustomAttributes = "";
			$this->keterangan_terbaru->HrefValue = "";
			$this->keterangan_terbaru->TooltipValue = "";

			// status
			$this->status->LinkCustomAttributes = "";
			$this->status->HrefValue = "";
			$this->status->TooltipValue = "";

			// created_by
			$this->created_by->LinkCustomAttributes = "";
			$this->created_by->HrefValue = "";
			$this->created_by->TooltipValue = "";

			// created_date
			$this->created_date->LinkCustomAttributes = "";
			$this->created_date->HrefValue = "";
			$this->created_date->TooltipValue = "";
		} elseif ($this->RowType == ROWTYPE_ADD) { // Add row

			// kode_asset
			$this->kode_asset->EditAttrs["class"] = "form-control";
			$this->kode_asset->EditCustomAttributes = "";
			if (REMOVE_XSS)
				$this->kode_asset->CurrentValue = HtmlDecode($this->kode_asset->CurrentValue);
			$this->kode_asset->EditValue = HtmlEncode($this->kode_asset->CurrentValue);
			$this->kode_asset->PlaceHolder = RemoveHtml($this->kode_asset->caption());

			// jenis_asset
			$this->jenis_asset->EditAttrs["class"] = "form-control";
			$this->jenis_asset->EditCustomAttributes = "";
			$curVal = trim(strval($this->jenis_asset->CurrentValue));
			if ($curVal <> "")
				$this->jenis_asset->ViewValue = $this->jenis_asset->lookupCacheOption($curVal);
			else
				$this->jenis_asset->ViewValue = $this->jenis_asset->Lookup !== NULL && is_array($this->jenis_asset->Lookup->Options) ? $curVal : NULL;
			if ($this->jenis_asset->ViewValue !== NULL) { // Load from cache
				$this->jenis_asset->EditValue = array_values($this->jenis_asset->Lookup->Options);
			} else { // Lookup from database
				if ($curVal == "") {
					$filterWrk = "0=1";
				} else {
					$filterWrk = "`id`" . SearchString("=", $this->jenis_asset->CurrentValue, DATATYPE_NUMBER, "");
				}
				$sqlWrk = $this->jenis_asset->Lookup->getSql(TRUE, $filterWrk, '', $this);
				$rswrk = Conn()->execute($sqlWrk);
				$arwrk = ($rswrk) ? $rswrk->GetRows() : array();
				if ($rswrk) $rswrk->Close();
				$this->jenis_asset->EditValue = $arwrk;
			}

			// merk
			$this->merk->EditAttrs["class"] = "form-control";
			$this->merk->EditCustomAttributes = "";
			if (REMOVE_XSS)
				$this->merk->CurrentValue = HtmlDecode($this->merk->CurrentValue);
			$this->merk->EditValue = HtmlEncode($this->merk->CurrentValue);
			$this->merk->PlaceHolder = RemoveHtml($this->merk->caption());

			// spesifikasi
			$this->spesifikasi->EditAttrs["class"] = "form-control";
			$this->spesifikasi->EditCustomAttributes = "";
			$this->spesifikasi->EditValue = HtmlEncode($this->spesifikasi->CurrentValue);
			$this->spesifikasi->PlaceHolder = RemoveHtml($this->spesifikasi->caption());

			// foto
			$this->foto->EditAttrs["class"] = "form-control";
			$this->foto->EditCustomAttributes = "";
			$this->foto->UploadPath = "custom/uploads/foto_assets";
			if (!EmptyValue($this->foto->Upload->DbValue)) {
				$this->foto->ImageWidth = 50;
				$this->foto->ImageHeight = 50;
				$this->foto->ImageAlt = $this->foto->alt();
				$this->foto->EditValue = $this->foto->Upload->DbValue;
			} else {
				$this->foto->EditValue = "";
			}
			if (!EmptyValue($this->foto->CurrentValue))
					$this->foto->Upload->FileName = $this->foto->CurrentValue;
			if (($this->isShow() || $this->isCopy()) && !$this->EventCancelled)
				RenderUploadField($this->foto);

			// kelengkapan_awal
			$this->kelengkapan_awal->EditAttrs["class"] = "form-control";
			$this->kelengkapan_awal->EditCustomAttributes = "";
			$this->kelengkapan_awal->EditValue = HtmlEncode($this->kelengkapan_awal->CurrentValue);
			$this->kelengkapan_awal->PlaceHolder = RemoveHtml($this->kelengkapan_awal->caption());

			// keterangan_terbaru
			$this->keterangan_terbaru->EditAttrs["class"] = "form-control";
			$this->keterangan_terbaru->EditCustomAttributes = "";
			$this->keterangan_terbaru->EditValue = HtmlEncode($this->keterangan_terbaru->CurrentValue);
			$this->keterangan_terbaru->PlaceHolder = RemoveHtml($this->keterangan_terbaru->caption());

			// status
			$this->status->EditAttrs["class"] = "form-control";
			$this->status->EditCustomAttributes = "";
			$this->status->EditValue = $this->status->options(TRUE);

			// created_by
			// created_date
			// Add refer script
			// kode_asset

			$this->kode_asset->LinkCustomAttributes = "";
			$this->kode_asset->HrefValue = "";

			// jenis_asset
			$this->jenis_asset->LinkCustomAttributes = "";
			$this->jenis_asset->HrefValue = "";

			// merk
			$this->merk->LinkCustomAttributes = "";
			$this->merk->HrefValue = "";

			// spesifikasi
			$this->spesifikasi->LinkCustomAttributes = "";
			$this->spesifikasi->HrefValue = "";

			// foto
			$this->foto->LinkCustomAttributes = "";
			$this->foto->UploadPath = "custom/uploads/foto_assets";
			if (!EmptyValue($this->foto->Upload->DbValue)) {
				$this->foto->HrefValue = GetFileUploadUrl($this->foto, $this->foto->Upload->DbValue); // Add prefix/suffix
				$this->foto->LinkAttrs["target"] = ""; // Add target
				if ($this->isExport()) $this->foto->HrefValue = FullUrl($this->foto->HrefValue, "href");
			} else {
				$this->foto->HrefValue = "";
			}
			$this->foto->ExportHrefValue = $this->foto->UploadPath . $this->foto->Upload->DbValue;

			// kelengkapan_awal
			$this->kelengkapan_awal->LinkCustomAttributes = "";
			$this->kelengkapan_awal->HrefValue = "";

			// keterangan_terbaru
			$this->keterangan_terbaru->LinkCustomAttributes = "";
			$this->keterangan_terbaru->HrefValue = "";

			// status
			$this->status->LinkCustomAttributes = "";
			$this->status->HrefValue = "";

			// created_by
			$this->created_by->LinkCustomAttributes = "";
			$this->created_by->HrefValue = "";

			// created_date
			$this->created_date->LinkCustomAttributes = "";
			$this->created_date->HrefValue = "";
		}
		if ($this->RowType == ROWTYPE_ADD || $this->RowType == ROWTYPE_EDIT || $this->RowType == ROWTYPE_SEARCH) // Add/Edit/Search row
			$this->setupFieldTitles();

		// Call Row Rendered event
		if ($this->RowType <> ROWTYPE_AGGREGATEINIT)
			$this->Row_Rendered();
	}

	// Validate form
	protected function validateForm()
	{
		global $Language, $FormError;

		// Initialize form error message
		$FormError = "";

		// Check if validation required
		if (!SERVER_VALIDATE)
			return ($FormError == "");
		if ($this->id->Required) {
			if (!$this->id->IsDetailKey && $this->id->FormValue != NULL && $this->id->FormValue == "") {
				AddMessage($FormError, str_replace("%s", $this->id->caption(), $this->id->RequiredErrorMessage));
			}
		}
		if ($this->kode_asset->Required) {
			if (!$this->kode_asset->IsDetailKey && $this->kode_asset->FormValue != NULL && $this->kode_asset->FormValue == "") {
				AddMessage($FormError, str_replace("%s", $this->kode_asset->caption(), $this->kode_asset->RequiredErrorMessage));
			}
		}
		if ($this->jenis_asset->Required) {
			if (!$this->jenis_asset->IsDetailKey && $this->jenis_asset->FormValue != NULL && $this->jenis_asset->FormValue == "") {
				AddMessage($FormError, str_replace("%s", $this->jenis_asset->caption(), $this->jenis_asset->RequiredErrorMessage));
			}
		}
		if ($this->merk->Required) {
			if (!$this->merk->IsDetailKey && $this->merk->FormValue != NULL && $this->merk->FormValue == "") {
				AddMessage($FormError, str_replace("%s", $this->merk->caption(), $this->merk->RequiredErrorMessage));
			}
		}
		if ($this->spesifikasi->Required) {
			if (!$this->spesifikasi->IsDetailKey && $this->spesifikasi->FormValue != NULL && $this->spesifikasi->FormValue == "") {
				AddMessage($FormError, str_replace("%s", $this->spesifikasi->caption(), $this->spesifikasi->RequiredErrorMessage));
			}
		}
		if ($this->foto->Required) {
			if ($this->foto->Upload->FileName == "" && !$this->foto->Upload->KeepFile) {
				AddMessage($FormError, str_replace("%s", $this->foto->caption(), $this->foto->RequiredErrorMessage));
			}
		}
		if ($this->kelengkapan_awal->Required) {
			if (!$this->kelengkapan_awal->IsDetailKey && $this->kelengkapan_awal->FormValue != NULL && $this->kelengkapan_awal->FormValue == "") {
				AddMessage($FormError, str_replace("%s", $this->kelengkapan_awal->caption(), $this->kelengkapan_awal->RequiredErrorMessage));
			}
		}
		if ($this->keterangan_terbaru->Required) {
			if (!$this->keterangan_terbaru->IsDetailKey && $this->keterangan_terbaru->FormValue != NULL && $this->keterangan_terbaru->FormValue == "") {
				AddMessage($FormError, str_replace("%s", $this->keterangan_terbaru->caption(), $this->keterangan_terbaru->RequiredErrorMessage));
			}
		}
		if ($this->status->Required) {
			if (!$this->status->IsDetailKey && $this->status->FormValue != NULL && $this->status->FormValue == "") {
				AddMessage($FormError, str_replace("%s", $this->status->caption(), $this->status->RequiredErrorMessage));
			}
		}
		if ($this->created_by->Required) {
			if (!$this->created_by->IsDetailKey && $this->created_by->FormValue != NULL && $this->created_by->FormValue == "") {
				AddMessage($FormError, str_replace("%s", $this->created_by->caption(), $this->created_by->RequiredErrorMessage));
			}
		}
		if ($this->created_date->Required) {
			if (!$this->created_date->IsDetailKey && $this->created_date->FormValue != NULL && $this->created_date->FormValue == "") {
				AddMessage($FormError, str_replace("%s", $this->created_date->caption(), $this->created_date->RequiredErrorMessage));
			}
		}
		if ($this->current_asign->Required) {
			if (!$this->current_asign->IsDetailKey && $this->current_asign->FormValue != NULL && $this->current_asign->FormValue == "") {
				AddMessage($FormError, str_replace("%s", $this->current_asign->caption(), $this->current_asign->RequiredErrorMessage));
			}
		}
		if ($this->assign_date->Required) {
			if (!$this->assign_date->IsDetailKey && $this->assign_date->FormValue != NULL && $this->assign_date->FormValue == "") {
				AddMessage($FormError, str_replace("%s", $this->assign_date->caption(), $this->assign_date->RequiredErrorMessage));
			}
		}
		if ($this->back_date->Required) {
			if (!$this->back_date->IsDetailKey && $this->back_date->FormValue != NULL && $this->back_date->FormValue == "") {
				AddMessage($FormError, str_replace("%s", $this->back_date->caption(), $this->back_date->RequiredErrorMessage));
			}
		}

		// Return validate result
		$validateForm = ($FormError == "");

		// Call Form_CustomValidate event
		$formCustomError = "";
		$validateForm = $validateForm && $this->Form_CustomValidate($formCustomError);
		if ($formCustomError <> "") {
			AddMessage($FormError, $formCustomError);
		}
		return $validateForm;
	}

	// Add record
	protected function addRow($rsold = NULL)
	{
		global $Language, $Security;
		if ($this->kode_asset->CurrentValue <> "") { // Check field with unique index
			$filter = "(kode_asset = '" . AdjustSql($this->kode_asset->CurrentValue, $this->Dbid) . "')";
			$rsChk = $this->loadRs($filter);
			if ($rsChk && !$rsChk->EOF) {
				$idxErrMsg = str_replace("%f", $this->kode_asset->caption(), $Language->phrase("DupIndex"));
				$idxErrMsg = str_replace("%v", $this->kode_asset->CurrentValue, $idxErrMsg);
				$this->setFailureMessage($idxErrMsg);
				$rsChk->close();
				return FALSE;
			}
		}
		$conn = &$this->getConnection();

		// Load db values from rsold
		$this->loadDbValues($rsold);
		if ($rsold) {
			$this->foto->OldUploadPath = "custom/uploads/foto_assets";
			$this->foto->UploadPath = $this->foto->OldUploadPath;
		}
		$rsnew = [];

		// kode_asset
		$this->kode_asset->setDbValueDef($rsnew, $this->kode_asset->CurrentValue, "", FALSE);

		// jenis_asset
		$this->jenis_asset->setDbValueDef($rsnew, $this->jenis_asset->CurrentValue, 0, FALSE);

		// merk
		$this->merk->setDbValueDef($rsnew, $this->merk->CurrentValue, "", FALSE);

		// spesifikasi
		$this->spesifikasi->setDbValueDef($rsnew, $this->spesifikasi->CurrentValue, "", FALSE);

		// foto
		if ($this->foto->Visible && !$this->foto->Upload->KeepFile) {
			$this->foto->Upload->DbValue = ""; // No need to delete old file
			if ($this->foto->Upload->FileName == "") {
				$rsnew['foto'] = NULL;
			} else {
				$rsnew['foto'] = $this->foto->Upload->FileName;
			}
		}

		// kelengkapan_awal
		$this->kelengkapan_awal->setDbValueDef($rsnew, $this->kelengkapan_awal->CurrentValue, "", FALSE);

		// keterangan_terbaru
		$this->keterangan_terbaru->setDbValueDef($rsnew, $this->keterangan_terbaru->CurrentValue, NULL, FALSE);

		// status
		$this->status->setDbValueDef($rsnew, $this->status->CurrentValue, 0, FALSE);

		// created_by
		$this->created_by->setDbValueDef($rsnew, CurrentUserID(), 0);
		$rsnew['created_by'] = &$this->created_by->DbValue;

		// created_date
		$this->created_date->setDbValueDef($rsnew, CurrentDate(), CurrentDate());
		$rsnew['created_date'] = &$this->created_date->DbValue;
		if ($this->foto->Visible && !$this->foto->Upload->KeepFile) {
			$this->foto->UploadPath = "custom/uploads/foto_assets";
			$oldFiles = EmptyValue($this->foto->Upload->DbValue) ? array() : array($this->foto->Upload->DbValue);
			if (!EmptyValue($this->foto->Upload->FileName)) {
				$newFiles = array($this->foto->Upload->FileName);
				$NewFileCount = count($newFiles);
				for ($i = 0; $i < $NewFileCount; $i++) {
					if ($newFiles[$i] <> "") {
						$file = $newFiles[$i];
						if (file_exists(UploadTempPath($this->foto, $this->foto->Upload->Index) . $file)) {
							if (DELETE_UPLOADED_FILES) {
								$oldFileFound = FALSE;
								$oldFileCount = count($oldFiles);
								for ($j = 0; $j < $oldFileCount; $j++) {
									$oldFile = $oldFiles[$j];
									if ($oldFile == $file) { // Old file found, no need to delete anymore
										unset($oldFiles[$j]);
										$oldFileFound = TRUE;
										break;
									}
								}
								if ($oldFileFound) // No need to check if file exists further
									continue;
							}
							$file1 = UniqueFilename($this->foto->physicalUploadPath(), $file); // Get new file name
							if ($file1 <> $file) { // Rename temp file
								while (file_exists(UploadTempPath($this->foto, $this->foto->Upload->Index) . $file1) || file_exists($this->foto->physicalUploadPath() . $file1)) // Make sure no file name clash
									$file1 = UniqueFilename($this->foto->physicalUploadPath(), $file1, TRUE); // Use indexed name
								rename(UploadTempPath($this->foto, $this->foto->Upload->Index) . $file, UploadTempPath($this->foto, $this->foto->Upload->Index) . $file1);
								$newFiles[$i] = $file1;
							}
						}
					}
				}
				$this->foto->Upload->DbValue = empty($oldFiles) ? "" : implode(MULTIPLE_UPLOAD_SEPARATOR, $oldFiles);
				$this->foto->Upload->FileName = implode(MULTIPLE_UPLOAD_SEPARATOR, $newFiles);
				$this->foto->setDbValueDef($rsnew, $this->foto->Upload->FileName, NULL, FALSE);
			}
		}

		// Call Row Inserting event
		$rs = ($rsold) ? $rsold->fields : NULL;
		$insertRow = $this->Row_Inserting($rs, $rsnew);
		if ($insertRow) {
			$conn->raiseErrorFn = $GLOBALS["ERROR_FUNC"];
			$addRow = $this->insert($rsnew);
			$conn->raiseErrorFn = '';
			if ($addRow) {
				if ($this->foto->Visible && !$this->foto->Upload->KeepFile) {
					$oldFiles = EmptyValue($this->foto->Upload->DbValue) ? array() : array($this->foto->Upload->DbValue);
					if (!EmptyValue($this->foto->Upload->FileName)) {
						$newFiles = array($this->foto->Upload->FileName);
						$newFiles2 = array($rsnew['foto']);
						$newFileCount = count($newFiles);
						for ($i = 0; $i < $newFileCount; $i++) {
							if ($newFiles[$i] <> "") {
								$file = UploadTempPath($this->foto, $this->foto->Upload->Index) . $newFiles[$i];
								if (file_exists($file)) {
									if (@$newFiles2[$i] <> "") // Use correct file name
										$newFiles[$i] = $newFiles2[$i];
									if (!$this->foto->Upload->saveToFile($newFiles[$i], TRUE, $i)) { // Just replace
										$this->setFailureMessage($Language->phrase("UploadErrMsg7"));
										return FALSE;
									}
								}
							}
						}
					} else {
						$newFiles = array();
					}
					if (DELETE_UPLOADED_FILES) {
						foreach ($oldFiles as $oldFile) {
							if ($oldFile <> "" && !in_array($oldFile, $newFiles))
								@unlink($this->foto->oldPhysicalUploadPath() . $oldFile);
						}
					}
				}
			}
		} else {
			if ($this->getSuccessMessage() <> "" || $this->getFailureMessage() <> "") {

				// Use the message, do nothing
			} elseif ($this->CancelMessage <> "") {
				$this->setFailureMessage($this->CancelMessage);
				$this->CancelMessage = "";
			} else {
				$this->setFailureMessage($Language->phrase("InsertCancelled"));
			}
			$addRow = FALSE;
		}
		if ($addRow) {

			// Call Row Inserted event
			$rs = ($rsold) ? $rsold->fields : NULL;
			$this->Row_Inserted($rs, $rsnew);
		}

		// foto
		if ($this->foto->Upload->FileToken <> "")
			CleanUploadTempPath($this->foto->Upload->FileToken, $this->foto->Upload->Index);
		else
			CleanUploadTempPath($this->foto, $this->foto->Upload->Index);

		// Write JSON for API request
		if (IsApi() && $addRow) {
			$row = $this->getRecordsFromRecordset([$rsnew], TRUE);
			WriteJson(["success" => TRUE, $this->TableVar => $row]);
		}
		return $addRow;
	}

	// Set up Breadcrumb
	protected function setupBreadcrumb()
	{
		global $Breadcrumb, $Language;
		$Breadcrumb = new Breadcrumb();
		$url = substr(CurrentUrl(), strrpos(CurrentUrl(), "/")+1);
		$Breadcrumb->add("list", $this->TableVar, $this->addMasterUrl("assetslist.php"), "", $this->TableVar, TRUE);
		$pageId = ($this->isCopy()) ? "Copy" : "Add";
		$Breadcrumb->add("add", $pageId, $url);
	}

	// Setup lookup options
	public function setupLookupOptions($fld)
	{
		if ($fld->Lookup !== NULL && $fld->Lookup->Options === NULL) {

			// No need to check any more
			$fld->Lookup->Options = [];

			// Set up lookup SQL
			switch ($fld->FieldVar) {
				default:
					$lookupFilter = "";
					break;
			}

			// Always call to Lookup->getSql so that user can setup Lookup->Options in Lookup_Selecting server event
			$sql = $fld->Lookup->getSql(FALSE, "", $lookupFilter, $this);

			// Set up lookup cache
			if ($fld->UseLookupCache && $sql <> "" && count($fld->Lookup->ParentFields) == 0 && count($fld->Lookup->Options) == 0) {
				$conn = &$this->getConnection();
				$totalCnt = $this->getRecordCount($sql);
				if ($totalCnt > $fld->LookupCacheCount) // Total count > cache count, do not cache
					return;
				$rs = $conn->execute($sql);
				$ar = [];
				while ($rs && !$rs->EOF) {
					$row = &$rs->fields;

					// Format the field values
					switch ($fld->FieldVar) {
						case "x_jenis_asset":
							break;
						case "x_created_by":
							break;
						case "x_current_asign":
							break;
					}
					$ar[strval($row[0])] = $row;
					$rs->moveNext();
				}
				if ($rs)
					$rs->close();
				$fld->Lookup->Options = $ar;
			}
		}
	}

	// Page Load event
	function Page_Load() {

		//echo "Page Load";
	}

	// Page Unload event
	function Page_Unload() {

		//echo "Page Unload";
	}

	// Page Redirecting event
	function Page_Redirecting(&$url) {

		// Example:
		//$url = "your URL";

	}

	// Message Showing event
	// $type = ''|'success'|'failure'|'warning'
	function Message_Showing(&$msg, $type) {
		if ($type == 'success') {

			//$msg = "your success message";
		} elseif ($type == 'failure') {

			//$msg = "your failure message";
		} elseif ($type == 'warning') {

			//$msg = "your warning message";
		} else {

			//$msg = "your message";
		}
	}

	// Page Render event
	function Page_Render() {

		//echo "Page Render";
	}

	// Page Data Rendering event
	function Page_DataRendering(&$header) {

		// Example:
		//$header = "your header";

	}

	// Page Data Rendered event
	function Page_DataRendered(&$footer) {

		// Example:
		//$footer = "your footer";

	}

	// Form Custom Validate event
	function Form_CustomValidate(&$customError) {

		// Return error message in CustomError
		return TRUE;
	}
}
?>