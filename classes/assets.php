<?php
namespace PHPMaker2019\inventaris_assets;

/**
 * Table class for assets
 */
class assets extends DbTable
{
	protected $SqlFrom = "";
	protected $SqlSelect = "";
	protected $SqlSelectList = "";
	protected $SqlWhere = "";
	protected $SqlGroupBy = "";
	protected $SqlHaving = "";
	protected $SqlOrderBy = "";
	public $UseSessionForListSql = TRUE;

	// Column CSS classes
	public $LeftColumnClass = "col-sm-2 col-form-label ew-label";
	public $RightColumnClass = "col-sm-10";
	public $OffsetColumnClass = "col-sm-10 offset-sm-2";
	public $TableLeftColumnClass = "w-col-2";

	// Export
	public $ExportDoc;

	// Fields
	public $id;
	public $kode_asset;
	public $jenis_asset;
	public $merk;
	public $spesifikasi;
	public $foto;
	public $kelengkapan_awal;
	public $keterangan_terbaru;
	public $status;
	public $created_by;
	public $created_date;
	public $current_asign;
	public $assign_date;
	public $back_date;

	// Constructor
	public function __construct()
	{
		global $Language, $CurrentLanguage;

		// Language object
		if (!isset($Language))
			$Language = new Language();
		$this->TableVar = 'assets';
		$this->TableName = 'assets';
		$this->TableType = 'TABLE';

		// Update Table
		$this->UpdateTable = "`assets`";
		$this->Dbid = 'DB';
		$this->ExportAll = TRUE;
		$this->ExportPageBreakCount = 0; // Page break per every n record (PDF only)
		$this->ExportPageOrientation = "portrait"; // Page orientation (PDF only)
		$this->ExportPageSize = "a4"; // Page size (PDF only)
		$this->ExportExcelPageOrientation = \PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::ORIENTATION_DEFAULT; // Page orientation (PhpSpreadsheet only)
		$this->ExportExcelPageSize = \PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::PAPERSIZE_A4; // Page size (PhpSpreadsheet only)
		$this->ExportWordPageOrientation = "portrait"; // Page orientation (PHPWord only)
		$this->ExportWordColumnWidth = NULL; // Cell width (PHPWord only)
		$this->DetailAdd = FALSE; // Allow detail add
		$this->DetailEdit = FALSE; // Allow detail edit
		$this->DetailView = FALSE; // Allow detail view
		$this->ShowMultipleDetails = FALSE; // Show multiple details
		$this->GridAddRowCount = 5;
		$this->AllowAddDeleteRow = TRUE; // Allow add/delete row
		$this->UserIDAllowSecurity = 0; // User ID Allow
		$this->BasicSearch = new BasicSearch($this->TableVar);

		// id
		$this->id = new DbField('assets', 'assets', 'x_id', 'id', '`id`', '`id`', 3, -1, FALSE, '`id`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'NO');
		$this->id->IsAutoIncrement = TRUE; // Autoincrement field
		$this->id->IsPrimaryKey = TRUE; // Primary key field
		$this->id->Sortable = TRUE; // Allow sort
		$this->id->DefaultErrorMessage = $Language->phrase("IncorrectInteger");
		$this->fields['id'] = &$this->id;

		// kode_asset
		$this->kode_asset = new DbField('assets', 'assets', 'x_kode_asset', 'kode_asset', '`kode_asset`', '`kode_asset`', 200, -1, FALSE, '`kode_asset`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->kode_asset->Nullable = FALSE; // NOT NULL field
		$this->kode_asset->Required = TRUE; // Required field
		$this->kode_asset->Sortable = TRUE; // Allow sort
		$this->fields['kode_asset'] = &$this->kode_asset;

		// jenis_asset
		$this->jenis_asset = new DbField('assets', 'assets', 'x_jenis_asset', 'jenis_asset', '`jenis_asset`', '`jenis_asset`', 3, -1, FALSE, '`jenis_asset`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'SELECT');
		$this->jenis_asset->Nullable = FALSE; // NOT NULL field
		$this->jenis_asset->Required = TRUE; // Required field
		$this->jenis_asset->Sortable = TRUE; // Allow sort
		$this->jenis_asset->UsePleaseSelect = TRUE; // Use PleaseSelect by default
		$this->jenis_asset->PleaseSelectText = $Language->phrase("PleaseSelect"); // PleaseSelect text
		$this->jenis_asset->Lookup = new Lookup('jenis_asset', 'jenis_asset', FALSE, 'id', ["nama_jenis","","",""], [], [], [], [], [], [], '', '');
		$this->jenis_asset->DefaultErrorMessage = $Language->phrase("IncorrectInteger");
		$this->fields['jenis_asset'] = &$this->jenis_asset;

		// merk
		$this->merk = new DbField('assets', 'assets', 'x_merk', 'merk', '`merk`', '`merk`', 200, -1, FALSE, '`merk`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->merk->Nullable = FALSE; // NOT NULL field
		$this->merk->Required = TRUE; // Required field
		$this->merk->Sortable = TRUE; // Allow sort
		$this->fields['merk'] = &$this->merk;

		// spesifikasi
		$this->spesifikasi = new DbField('assets', 'assets', 'x_spesifikasi', 'spesifikasi', '`spesifikasi`', '`spesifikasi`', 201, -1, FALSE, '`spesifikasi`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXTAREA');
		$this->spesifikasi->Nullable = FALSE; // NOT NULL field
		$this->spesifikasi->Required = TRUE; // Required field
		$this->spesifikasi->Sortable = TRUE; // Allow sort
		$this->fields['spesifikasi'] = &$this->spesifikasi;

		// foto
		$this->foto = new DbField('assets', 'assets', 'x_foto', 'foto', '`foto`', '`foto`', 201, -1, TRUE, '`foto`', FALSE, FALSE, FALSE, 'IMAGE', 'FILE');
		$this->foto->Sortable = TRUE; // Allow sort
		$this->fields['foto'] = &$this->foto;

		// kelengkapan_awal
		$this->kelengkapan_awal = new DbField('assets', 'assets', 'x_kelengkapan_awal', 'kelengkapan_awal', '`kelengkapan_awal`', '`kelengkapan_awal`', 201, -1, FALSE, '`kelengkapan_awal`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXTAREA');
		$this->kelengkapan_awal->Nullable = FALSE; // NOT NULL field
		$this->kelengkapan_awal->Required = TRUE; // Required field
		$this->kelengkapan_awal->Sortable = TRUE; // Allow sort
		$this->fields['kelengkapan_awal'] = &$this->kelengkapan_awal;

		// keterangan_terbaru
		$this->keterangan_terbaru = new DbField('assets', 'assets', 'x_keterangan_terbaru', 'keterangan_terbaru', '`keterangan_terbaru`', '`keterangan_terbaru`', 201, -1, FALSE, '`keterangan_terbaru`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXTAREA');
		$this->keterangan_terbaru->Sortable = TRUE; // Allow sort
		$this->fields['keterangan_terbaru'] = &$this->keterangan_terbaru;

		// status
		$this->status = new DbField('assets', 'assets', 'x_status', 'status', '`status`', '`status`', 3, -1, FALSE, '`status`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'SELECT');
		$this->status->Nullable = FALSE; // NOT NULL field
		$this->status->Required = TRUE; // Required field
		$this->status->Sortable = TRUE; // Allow sort
		$this->status->UsePleaseSelect = TRUE; // Use PleaseSelect by default
		$this->status->PleaseSelectText = $Language->phrase("PleaseSelect"); // PleaseSelect text
		$this->status->Lookup = new Lookup('status', 'assets', FALSE, '', ["","","",""], [], [], [], [], [], [], '', '');
		$this->status->OptionCount = 4;
		$this->status->DefaultErrorMessage = $Language->phrase("IncorrectInteger");
		$this->fields['status'] = &$this->status;

		// created_by
		$this->created_by = new DbField('assets', 'assets', 'x_created_by', 'created_by', '`created_by`', '`created_by`', 3, -1, FALSE, '`created_by`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'SELECT');
		$this->created_by->Nullable = FALSE; // NOT NULL field
		$this->created_by->Sortable = TRUE; // Allow sort
		$this->created_by->UsePleaseSelect = TRUE; // Use PleaseSelect by default
		$this->created_by->PleaseSelectText = $Language->phrase("PleaseSelect"); // PleaseSelect text
		$this->created_by->Lookup = new Lookup('created_by', 'v_user', FALSE, 'id', ["username","","",""], [], [], [], [], [], [], '', '');
		$this->created_by->DefaultErrorMessage = $Language->phrase("IncorrectInteger");
		$this->fields['created_by'] = &$this->created_by;

		// created_date
		$this->created_date = new DbField('assets', 'assets', 'x_created_date', 'created_date', '`created_date`', CastDateFieldForLike('`created_date`', 0, "DB"), 133, 0, FALSE, '`created_date`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->created_date->Nullable = FALSE; // NOT NULL field
		$this->created_date->Sortable = TRUE; // Allow sort
		$this->created_date->DefaultErrorMessage = str_replace("%s", $GLOBALS["DATE_FORMAT"], $Language->phrase("IncorrectDate"));
		$this->fields['created_date'] = &$this->created_date;

		// current_asign
		$this->current_asign = new DbField('assets', 'assets', 'x_current_asign', 'current_asign', '`current_asign`', '`current_asign`', 3, -1, FALSE, '`current_asign`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'SELECT');
		$this->current_asign->Nullable = FALSE; // NOT NULL field
		$this->current_asign->Required = TRUE; // Required field
		$this->current_asign->Sortable = TRUE; // Allow sort
		$this->current_asign->UsePleaseSelect = TRUE; // Use PleaseSelect by default
		$this->current_asign->PleaseSelectText = $Language->phrase("PleaseSelect"); // PleaseSelect text
		$this->current_asign->Lookup = new Lookup('current_asign', 'karyawan', FALSE, 'id', ["nik","nama_karyawan","",""], [], [], [], [], [], [], '', '');
		$this->current_asign->DefaultErrorMessage = $Language->phrase("IncorrectInteger");
		$this->fields['current_asign'] = &$this->current_asign;

		// assign_date
		$this->assign_date = new DbField('assets', 'assets', 'x_assign_date', 'assign_date', '`assign_date`', CastDateFieldForLike('`assign_date`', 0, "DB"), 133, 0, FALSE, '`assign_date`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->assign_date->Nullable = FALSE; // NOT NULL field
		$this->assign_date->Required = TRUE; // Required field
		$this->assign_date->Sortable = TRUE; // Allow sort
		$this->assign_date->DefaultErrorMessage = str_replace("%s", $GLOBALS["DATE_FORMAT"], $Language->phrase("IncorrectDate"));
		$this->fields['assign_date'] = &$this->assign_date;

		// back_date
		$this->back_date = new DbField('assets', 'assets', 'x_back_date', 'back_date', '`back_date`', CastDateFieldForLike('`back_date`', 0, "DB"), 133, 0, FALSE, '`back_date`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->back_date->Nullable = FALSE; // NOT NULL field
		$this->back_date->Required = TRUE; // Required field
		$this->back_date->Sortable = TRUE; // Allow sort
		$this->back_date->DefaultErrorMessage = str_replace("%s", $GLOBALS["DATE_FORMAT"], $Language->phrase("IncorrectDate"));
		$this->fields['back_date'] = &$this->back_date;
	}

	// Field Visibility
	public function getFieldVisibility($fldParm)
	{
		global $Security;
		return $this->$fldParm->Visible; // Returns original value
	}

	// Set left column class (must be predefined col-*-* classes of Bootstrap grid system)
	function setLeftColumnClass($class)
	{
		if (preg_match('/^col\-(\w+)\-(\d+)$/', $class, $match)) {
			$this->LeftColumnClass = $class . " col-form-label ew-label";
			$this->RightColumnClass = "col-" . $match[1] . "-" . strval(12 - (int)$match[2]);
			$this->OffsetColumnClass = $this->RightColumnClass . " " . str_replace("col-", "offset-", $class);
			$this->TableLeftColumnClass = preg_replace('/^col-\w+-(\d+)$/', "w-col-$1", $class); // Change to w-col-*
		}
	}

	// Single column sort
	public function updateSort(&$fld)
	{
		if ($this->CurrentOrder == $fld->Name) {
			$sortField = $fld->Expression;
			$lastSort = $fld->getSort();
			if ($this->CurrentOrderType == "ASC" || $this->CurrentOrderType == "DESC") {
				$thisSort = $this->CurrentOrderType;
			} else {
				$thisSort = ($lastSort == "ASC") ? "DESC" : "ASC";
			}
			$fld->setSort($thisSort);
			$this->setSessionOrderBy($sortField . " " . $thisSort); // Save to Session
		} else {
			$fld->setSort("");
		}
	}

	// Table level SQL
	public function getSqlFrom() // From
	{
		return ($this->SqlFrom <> "") ? $this->SqlFrom : "`assets`";
	}
	public function sqlFrom() // For backward compatibility
	{
		return $this->getSqlFrom();
	}
	public function setSqlFrom($v)
	{
		$this->SqlFrom = $v;
	}
	public function getSqlSelect() // Select
	{
		return ($this->SqlSelect <> "") ? $this->SqlSelect : "SELECT * FROM " . $this->getSqlFrom();
	}
	public function sqlSelect() // For backward compatibility
	{
		return $this->getSqlSelect();
	}
	public function setSqlSelect($v)
	{
		$this->SqlSelect = $v;
	}
	public function getSqlWhere() // Where
	{
		$where = ($this->SqlWhere <> "") ? $this->SqlWhere : "";
		$this->TableFilter = "";
		AddFilter($where, $this->TableFilter);
		return $where;
	}
	public function sqlWhere() // For backward compatibility
	{
		return $this->getSqlWhere();
	}
	public function setSqlWhere($v)
	{
		$this->SqlWhere = $v;
	}
	public function getSqlGroupBy() // Group By
	{
		return ($this->SqlGroupBy <> "") ? $this->SqlGroupBy : "";
	}
	public function sqlGroupBy() // For backward compatibility
	{
		return $this->getSqlGroupBy();
	}
	public function setSqlGroupBy($v)
	{
		$this->SqlGroupBy = $v;
	}
	public function getSqlHaving() // Having
	{
		return ($this->SqlHaving <> "") ? $this->SqlHaving : "";
	}
	public function sqlHaving() // For backward compatibility
	{
		return $this->getSqlHaving();
	}
	public function setSqlHaving($v)
	{
		$this->SqlHaving = $v;
	}
	public function getSqlOrderBy() // Order By
	{
		return ($this->SqlOrderBy <> "") ? $this->SqlOrderBy : "";
	}
	public function sqlOrderBy() // For backward compatibility
	{
		return $this->getSqlOrderBy();
	}
	public function setSqlOrderBy($v)
	{
		$this->SqlOrderBy = $v;
	}

	// Apply User ID filters
	public function applyUserIDFilters($filter)
	{
		return $filter;
	}

	// Check if User ID security allows view all
	public function userIDAllow($id = "")
	{
		$allow = USER_ID_ALLOW;
		switch ($id) {
			case "add":
			case "copy":
			case "gridadd":
			case "register":
			case "addopt":
				return (($allow & 1) == 1);
			case "edit":
			case "gridedit":
			case "update":
			case "changepwd":
			case "forgotpwd":
				return (($allow & 4) == 4);
			case "delete":
				return (($allow & 2) == 2);
			case "view":
				return (($allow & 32) == 32);
			case "search":
				return (($allow & 64) == 64);
			default:
				return (($allow & 8) == 8);
		}
	}

	// Get SQL
	public function getSql($where, $orderBy = "")
	{
		return BuildSelectSql($this->getSqlSelect(), $this->getSqlWhere(),
			$this->getSqlGroupBy(), $this->getSqlHaving(), $this->getSqlOrderBy(),
			$where, $orderBy);
	}

	// Table SQL
	public function getCurrentSql()
	{
		$filter = $this->CurrentFilter;
		$filter = $this->applyUserIDFilters($filter);
		$sort = $this->getSessionOrderBy();
		return $this->getSql($filter, $sort);
	}

	// Table SQL with List page filter
	public function getListSql()
	{
		$filter = $this->UseSessionForListSql ? $this->getSessionWhere() : "";
		AddFilter($filter, $this->CurrentFilter);
		$filter = $this->applyUserIDFilters($filter);
		$this->Recordset_Selecting($filter);
		$select = $this->getSqlSelect();
		$sort = $this->UseSessionForListSql ? $this->getSessionOrderBy() : "";
		return BuildSelectSql($select, $this->getSqlWhere(), $this->getSqlGroupBy(),
			$this->getSqlHaving(), $this->getSqlOrderBy(), $filter, $sort);
	}

	// Get ORDER BY clause
	public function getOrderBy()
	{
		$sort = $this->getSessionOrderBy();
		return BuildSelectSql("", "", "", "", $this->getSqlOrderBy(), "", $sort);
	}

	// Get record count
	public function getRecordCount($sql)
	{
		$cnt = -1;
		$rs = NULL;
		$sql = preg_replace('/\/\*BeginOrderBy\*\/[\s\S]+\/\*EndOrderBy\*\//', "", $sql); // Remove ORDER BY clause (MSSQL)
		$pattern = '/^SELECT\s([\s\S]+)\sFROM\s/i';

		// Skip Custom View / SubQuery and SELECT DISTINCT
		if (($this->TableType == 'TABLE' || $this->TableType == 'VIEW' || $this->TableType == 'LINKTABLE') &&
			preg_match($pattern, $sql) && !preg_match('/\(\s*(SELECT[^)]+)\)/i', $sql) && !preg_match('/^\s*select\s+distinct\s+/i', $sql)) {
			$sqlwrk = "SELECT COUNT(*) FROM " . preg_replace($pattern, "", $sql);
		} else {
			$sqlwrk = "SELECT COUNT(*) FROM (" . $sql . ") COUNT_TABLE";
		}
		$conn = &$this->getConnection();
		if ($rs = $conn->execute($sqlwrk)) {
			if (!$rs->EOF && $rs->FieldCount() > 0) {
				$cnt = $rs->fields[0];
				$rs->close();
			}
			return (int)$cnt;
		}

		// Unable to get count, get record count directly
		if ($rs = $conn->execute($sql)) {
			$cnt = $rs->RecordCount();
			$rs->close();
			return (int)$cnt;
		}
		return $cnt;
	}

	// Get record count based on filter (for detail record count in master table pages)
	public function loadRecordCount($filter)
	{
		$origFilter = $this->CurrentFilter;
		$this->CurrentFilter = $filter;
		$this->Recordset_Selecting($this->CurrentFilter);
		$select = $this->TableType == 'CUSTOMVIEW' ? $this->getSqlSelect() : "SELECT * FROM " . $this->getSqlFrom();
		$groupBy = $this->TableType == 'CUSTOMVIEW' ? $this->getSqlGroupBy() : "";
		$having = $this->TableType == 'CUSTOMVIEW' ? $this->getSqlHaving() : "";
		$sql = BuildSelectSql($select, $this->getSqlWhere(), $groupBy, $having, "", $this->CurrentFilter, "");
		$cnt = $this->getRecordCount($sql);
		$this->CurrentFilter = $origFilter;
		return $cnt;
	}

	// Get record count (for current List page)
	public function listRecordCount()
	{
		$filter = $this->getSessionWhere();
		AddFilter($filter, $this->CurrentFilter);
		$filter = $this->applyUserIDFilters($filter);
		$this->Recordset_Selecting($filter);
		$select = $this->TableType == 'CUSTOMVIEW' ? $this->getSqlSelect() : "SELECT * FROM " . $this->getSqlFrom();
		$groupBy = $this->TableType == 'CUSTOMVIEW' ? $this->getSqlGroupBy() : "";
		$having = $this->TableType == 'CUSTOMVIEW' ? $this->getSqlHaving() : "";
		$sql = BuildSelectSql($select, $this->getSqlWhere(), $groupBy, $having, "", $filter, "");
		$cnt = $this->getRecordCount($sql);
		return $cnt;
	}

	// INSERT statement
	protected function insertSql(&$rs)
	{
		$names = "";
		$values = "";
		foreach ($rs as $name => $value) {
			if (!isset($this->fields[$name]) || $this->fields[$name]->IsCustom)
				continue;
			$names .= $this->fields[$name]->Expression . ",";
			$values .= QuotedValue($value, $this->fields[$name]->DataType, $this->Dbid) . ",";
		}
		$names = preg_replace('/,+$/', "", $names);
		$values = preg_replace('/,+$/', "", $values);
		return "INSERT INTO " . $this->UpdateTable . " ($names) VALUES ($values)";
	}

	// Insert
	public function insert(&$rs)
	{
		$conn = &$this->getConnection();
		$success = $conn->execute($this->insertSql($rs));
		if ($success) {

			// Get insert id if necessary
			$this->id->setDbValue($conn->insert_ID());
			$rs['id'] = $this->id->DbValue;
		}
		return $success;
	}

	// UPDATE statement
	protected function updateSql(&$rs, $where = "", $curfilter = TRUE)
	{
		$sql = "UPDATE " . $this->UpdateTable . " SET ";
		foreach ($rs as $name => $value) {
			if (!isset($this->fields[$name]) || $this->fields[$name]->IsCustom || $this->fields[$name]->IsPrimaryKey)
				continue;
			$sql .= $this->fields[$name]->Expression . "=";
			$sql .= QuotedValue($value, $this->fields[$name]->DataType, $this->Dbid) . ",";
		}
		$sql = preg_replace('/,+$/', "", $sql);
		$filter = ($curfilter) ? $this->CurrentFilter : "";
		if (is_array($where))
			$where = $this->arrayToFilter($where);
		AddFilter($filter, $where);
		if ($filter <> "")
			$sql .= " WHERE " . $filter;
		return $sql;
	}

	// Update
	public function update(&$rs, $where = "", $rsold = NULL, $curfilter = TRUE)
	{
		$conn = &$this->getConnection();
		$success = $conn->execute($this->updateSql($rs, $where, $curfilter));
		return $success;
	}

	// DELETE statement
	protected function deleteSql(&$rs, $where = "", $curfilter = TRUE)
	{
		$sql = "DELETE FROM " . $this->UpdateTable . " WHERE ";
		if (is_array($where))
			$where = $this->arrayToFilter($where);
		if ($rs) {
			if (array_key_exists('id', $rs))
				AddFilter($where, QuotedName('id', $this->Dbid) . '=' . QuotedValue($rs['id'], $this->id->DataType, $this->Dbid));
		}
		$filter = ($curfilter) ? $this->CurrentFilter : "";
		AddFilter($filter, $where);
		if ($filter <> "")
			$sql .= $filter;
		else
			$sql .= "0=1"; // Avoid delete
		return $sql;
	}

	// Delete
	public function delete(&$rs, $where = "", $curfilter = FALSE)
	{
		$success = TRUE;
		$conn = &$this->getConnection();
		if ($success)
			$success = $conn->execute($this->deleteSql($rs, $where, $curfilter));
		return $success;
	}

	// Load DbValue from recordset or array
	protected function loadDbValues(&$rs)
	{
		if (!$rs || !is_array($rs) && $rs->EOF)
			return;
		$row = is_array($rs) ? $rs : $rs->fields;
		$this->id->DbValue = $row['id'];
		$this->kode_asset->DbValue = $row['kode_asset'];
		$this->jenis_asset->DbValue = $row['jenis_asset'];
		$this->merk->DbValue = $row['merk'];
		$this->spesifikasi->DbValue = $row['spesifikasi'];
		$this->foto->Upload->DbValue = $row['foto'];
		$this->kelengkapan_awal->DbValue = $row['kelengkapan_awal'];
		$this->keterangan_terbaru->DbValue = $row['keterangan_terbaru'];
		$this->status->DbValue = $row['status'];
		$this->created_by->DbValue = $row['created_by'];
		$this->created_date->DbValue = $row['created_date'];
		$this->current_asign->DbValue = $row['current_asign'];
		$this->assign_date->DbValue = $row['assign_date'];
		$this->back_date->DbValue = $row['back_date'];
	}

	// Delete uploaded files
	public function deleteUploadedFiles($row)
	{
		$this->loadDbValues($row);
		$this->foto->OldUploadPath = "custom/uploads/foto_assets";
		$oldFiles = EmptyValue($row['foto']) ? [] : [$row['foto']];
		foreach ($oldFiles as $oldFile) {
			if (file_exists($this->foto->oldPhysicalUploadPath() . $oldFile))
				@unlink($this->foto->oldPhysicalUploadPath() . $oldFile);
		}
	}

	// Record filter WHERE clause
	protected function sqlKeyFilter()
	{
		return "`id` = @id@";
	}

	// Get record filter
	public function getRecordFilter($row = NULL)
	{
		$keyFilter = $this->sqlKeyFilter();
		$val = is_array($row) ? (array_key_exists('id', $row) ? $row['id'] : NULL) : $this->id->CurrentValue;
		if (!is_numeric($val))
			return "0=1"; // Invalid key
		if ($val == NULL)
			return "0=1"; // Invalid key
		else
			$keyFilter = str_replace("@id@", AdjustSql($val, $this->Dbid), $keyFilter); // Replace key value
		return $keyFilter;
	}

	// Return page URL
	public function getReturnUrl()
	{
		$name = PROJECT_NAME . "_" . $this->TableVar . "_" . TABLE_RETURN_URL;

		// Get referer URL automatically
		if (ServerVar("HTTP_REFERER") <> "" && ReferPageName() <> CurrentPageName() && ReferPageName() <> "login.php") // Referer not same page or login page
			$_SESSION[$name] = ServerVar("HTTP_REFERER"); // Save to Session
		if (@$_SESSION[$name] <> "") {
			return $_SESSION[$name];
		} else {
			return "assetslist.php";
		}
	}
	public function setReturnUrl($v)
	{
		$_SESSION[PROJECT_NAME . "_" . $this->TableVar . "_" . TABLE_RETURN_URL] = $v;
	}

	// Get modal caption
	public function getModalCaption($pageName)
	{
		global $Language;
		if ($pageName == "assetsview.php")
			return $Language->phrase("View");
		elseif ($pageName == "assetsedit.php")
			return $Language->phrase("Edit");
		elseif ($pageName == "assetsadd.php")
			return $Language->phrase("Add");
		else
			return "";
	}

	// List URL
	public function getListUrl()
	{
		return "assetslist.php";
	}

	// View URL
	public function getViewUrl($parm = "")
	{
		if ($parm <> "")
			$url = $this->keyUrl("assetsview.php", $this->getUrlParm($parm));
		else
			$url = $this->keyUrl("assetsview.php", $this->getUrlParm(TABLE_SHOW_DETAIL . "="));
		return $this->addMasterUrl($url);
	}

	// Add URL
	public function getAddUrl($parm = "")
	{
		if ($parm <> "")
			$url = "assetsadd.php?" . $this->getUrlParm($parm);
		else
			$url = "assetsadd.php";
		return $this->addMasterUrl($url);
	}

	// Edit URL
	public function getEditUrl($parm = "")
	{
		$url = $this->keyUrl("assetsedit.php", $this->getUrlParm($parm));
		return $this->addMasterUrl($url);
	}

	// Inline edit URL
	public function getInlineEditUrl()
	{
		$url = $this->keyUrl(CurrentPageName(), $this->getUrlParm("action=edit"));
		return $this->addMasterUrl($url);
	}

	// Copy URL
	public function getCopyUrl($parm = "")
	{
		$url = $this->keyUrl("assetsadd.php", $this->getUrlParm($parm));
		return $this->addMasterUrl($url);
	}

	// Inline copy URL
	public function getInlineCopyUrl()
	{
		$url = $this->keyUrl(CurrentPageName(), $this->getUrlParm("action=copy"));
		return $this->addMasterUrl($url);
	}

	// Delete URL
	public function getDeleteUrl()
	{
		return $this->keyUrl("assetsdelete.php", $this->getUrlParm());
	}

	// Add master url
	public function addMasterUrl($url)
	{
		return $url;
	}
	public function keyToJson($htmlEncode = FALSE)
	{
		$json = "";
		$json .= "id:" . JsonEncode($this->id->CurrentValue, "number");
		$json = "{" . $json . "}";
		if ($htmlEncode)
			$json = HtmlEncode($json);
		return $json;
	}

	// Add key value to URL
	public function keyUrl($url, $parm = "")
	{
		$url = $url . "?";
		if ($parm <> "")
			$url .= $parm . "&";
		if ($this->id->CurrentValue != NULL) {
			$url .= "id=" . urlencode($this->id->CurrentValue);
		} else {
			return "javascript:ew.alert(ew.language.phrase('InvalidRecord'));";
		}
		return $url;
	}

	// Sort URL
	public function sortUrl(&$fld)
	{
		if ($this->CurrentAction || $this->isExport() ||
			in_array($fld->Type, array(128, 204, 205))) { // Unsortable data type
				return "";
		} elseif ($fld->Sortable) {
			$urlParm = $this->getUrlParm("order=" . urlencode($fld->Name) . "&amp;ordertype=" . $fld->reverseSort());
			return $this->addMasterUrl(CurrentPageName() . "?" . $urlParm);
		} else {
			return "";
		}
	}

	// Get record keys from Post/Get/Session
	public function getRecordKeys()
	{
		global $COMPOSITE_KEY_SEPARATOR;
		$arKeys = array();
		$arKey = array();
		if (Param("key_m") !== NULL) {
			$arKeys = Param("key_m");
			$cnt = count($arKeys);
		} else {
			if (Param("id") !== NULL)
				$arKeys[] = Param("id");
			elseif (IsApi() && Key(0) !== NULL)
				$arKeys[] = Key(0);
			elseif (IsApi() && Route(2) !== NULL)
				$arKeys[] = Route(2);
			else
				$arKeys = NULL; // Do not setup

			//return $arKeys; // Do not return yet, so the values will also be checked by the following code
		}

		// Check keys
		$ar = array();
		if (is_array($arKeys)) {
			foreach ($arKeys as $key) {
				if (!is_numeric($key))
					continue;
				$ar[] = $key;
			}
		}
		return $ar;
	}

	// Get filter from record keys
	public function getFilterFromRecordKeys()
	{
		$arKeys = $this->getRecordKeys();
		$keyFilter = "";
		foreach ($arKeys as $key) {
			if ($keyFilter <> "") $keyFilter .= " OR ";
			$this->id->CurrentValue = $key;
			$keyFilter .= "(" . $this->getRecordFilter() . ")";
		}
		return $keyFilter;
	}

	// Load rows based on filter
	public function &loadRs($filter)
	{

		// Set up filter (WHERE Clause)
		$sql = $this->getSql($filter);
		$conn = &$this->getConnection();
		$rs = $conn->execute($sql);
		return $rs;
	}

	// Load row values from recordset
	public function loadListRowValues(&$rs)
	{
		$this->id->setDbValue($rs->fields('id'));
		$this->kode_asset->setDbValue($rs->fields('kode_asset'));
		$this->jenis_asset->setDbValue($rs->fields('jenis_asset'));
		$this->merk->setDbValue($rs->fields('merk'));
		$this->spesifikasi->setDbValue($rs->fields('spesifikasi'));
		$this->foto->Upload->DbValue = $rs->fields('foto');
		$this->kelengkapan_awal->setDbValue($rs->fields('kelengkapan_awal'));
		$this->keterangan_terbaru->setDbValue($rs->fields('keterangan_terbaru'));
		$this->status->setDbValue($rs->fields('status'));
		$this->created_by->setDbValue($rs->fields('created_by'));
		$this->created_date->setDbValue($rs->fields('created_date'));
		$this->current_asign->setDbValue($rs->fields('current_asign'));
		$this->assign_date->setDbValue($rs->fields('assign_date'));
		$this->back_date->setDbValue($rs->fields('back_date'));
	}

	// Render list row values
	public function renderListRow()
	{
		global $Security, $CurrentLanguage, $Language;

		// Call Row Rendering event
		$this->Row_Rendering();

		// Common render codes
		// id
		// kode_asset
		// jenis_asset
		// merk
		// spesifikasi
		// foto
		// kelengkapan_awal
		// keterangan_terbaru
		// status
		// created_by
		// created_date
		// current_asign
		// assign_date
		// back_date
		// id

		$this->id->ViewValue = $this->id->CurrentValue;
		$this->id->ViewCustomAttributes = "";

		// kode_asset
		$this->kode_asset->ViewValue = $this->kode_asset->CurrentValue;
		$this->kode_asset->ViewCustomAttributes = "";

		// jenis_asset
		$curVal = strval($this->jenis_asset->CurrentValue);
		if ($curVal <> "") {
			$this->jenis_asset->ViewValue = $this->jenis_asset->lookupCacheOption($curVal);
			if ($this->jenis_asset->ViewValue === NULL) { // Lookup from database
				$filterWrk = "`id`" . SearchString("=", $curVal, DATATYPE_NUMBER, "");
				$sqlWrk = $this->jenis_asset->Lookup->getSql(FALSE, $filterWrk, '', $this);
				$rswrk = Conn()->execute($sqlWrk);
				if ($rswrk && !$rswrk->EOF) { // Lookup values found
					$arwrk = array();
					$arwrk[1] = $rswrk->fields('df');
					$this->jenis_asset->ViewValue = $this->jenis_asset->displayValue($arwrk);
					$rswrk->Close();
				} else {
					$this->jenis_asset->ViewValue = $this->jenis_asset->CurrentValue;
				}
			}
		} else {
			$this->jenis_asset->ViewValue = NULL;
		}
		$this->jenis_asset->ViewCustomAttributes = "";

		// merk
		$this->merk->ViewValue = $this->merk->CurrentValue;
		$this->merk->ViewCustomAttributes = "";

		// spesifikasi
		$this->spesifikasi->ViewValue = $this->spesifikasi->CurrentValue;
		$this->spesifikasi->ViewCustomAttributes = "";

		// foto
		$this->foto->UploadPath = "custom/uploads/foto_assets";
		if (!EmptyValue($this->foto->Upload->DbValue)) {
			$this->foto->ImageWidth = 50;
			$this->foto->ImageHeight = 50;
			$this->foto->ImageAlt = $this->foto->alt();
			$this->foto->ViewValue = $this->foto->Upload->DbValue;
		} else {
			$this->foto->ViewValue = "";
		}
		$this->foto->ViewCustomAttributes = "";

		// kelengkapan_awal
		$this->kelengkapan_awal->ViewValue = $this->kelengkapan_awal->CurrentValue;
		$this->kelengkapan_awal->ViewCustomAttributes = "";

		// keterangan_terbaru
		$this->keterangan_terbaru->ViewValue = $this->keterangan_terbaru->CurrentValue;
		$this->keterangan_terbaru->ViewCustomAttributes = "";

		// status
		if (strval($this->status->CurrentValue) <> "") {
			$this->status->ViewValue = $this->status->optionCaption($this->status->CurrentValue);
		} else {
			$this->status->ViewValue = NULL;
		}
		$this->status->ViewCustomAttributes = "";

		// created_by
		$curVal = strval($this->created_by->CurrentValue);
		if ($curVal <> "") {
			$this->created_by->ViewValue = $this->created_by->lookupCacheOption($curVal);
			if ($this->created_by->ViewValue === NULL) { // Lookup from database
				$filterWrk = "`id`" . SearchString("=", $curVal, DATATYPE_NUMBER, "");
				$sqlWrk = $this->created_by->Lookup->getSql(FALSE, $filterWrk, '', $this);
				$rswrk = Conn()->execute($sqlWrk);
				if ($rswrk && !$rswrk->EOF) { // Lookup values found
					$arwrk = array();
					$arwrk[1] = $rswrk->fields('df');
					$this->created_by->ViewValue = $this->created_by->displayValue($arwrk);
					$rswrk->Close();
				} else {
					$this->created_by->ViewValue = $this->created_by->CurrentValue;
				}
			}
		} else {
			$this->created_by->ViewValue = NULL;
		}
		$this->created_by->ViewCustomAttributes = "";

		// created_date
		$this->created_date->ViewValue = $this->created_date->CurrentValue;
		$this->created_date->ViewValue = FormatDateTime($this->created_date->ViewValue, 0);
		$this->created_date->ViewCustomAttributes = "";

		// current_asign
		$curVal = strval($this->current_asign->CurrentValue);
		if ($curVal <> "") {
			$this->current_asign->ViewValue = $this->current_asign->lookupCacheOption($curVal);
			if ($this->current_asign->ViewValue === NULL) { // Lookup from database
				$filterWrk = "`id`" . SearchString("=", $curVal, DATATYPE_NUMBER, "");
				$sqlWrk = $this->current_asign->Lookup->getSql(FALSE, $filterWrk, '', $this);
				$rswrk = Conn()->execute($sqlWrk);
				if ($rswrk && !$rswrk->EOF) { // Lookup values found
					$arwrk = array();
					$arwrk[1] = $rswrk->fields('df');
					$arwrk[2] = $rswrk->fields('df2');
					$this->current_asign->ViewValue = $this->current_asign->displayValue($arwrk);
					$rswrk->Close();
				} else {
					$this->current_asign->ViewValue = $this->current_asign->CurrentValue;
				}
			}
		} else {
			$this->current_asign->ViewValue = NULL;
		}
		$this->current_asign->ViewCustomAttributes = "";

		// assign_date
		$this->assign_date->ViewValue = $this->assign_date->CurrentValue;
		$this->assign_date->ViewValue = FormatDateTime($this->assign_date->ViewValue, 0);
		$this->assign_date->ViewCustomAttributes = "";

		// back_date
		$this->back_date->ViewValue = $this->back_date->CurrentValue;
		$this->back_date->ViewValue = FormatDateTime($this->back_date->ViewValue, 0);
		$this->back_date->ViewCustomAttributes = "";

		// id
		$this->id->LinkCustomAttributes = "";
		$this->id->HrefValue = "";
		$this->id->TooltipValue = "";

		// kode_asset
		$this->kode_asset->LinkCustomAttributes = "";
		$this->kode_asset->HrefValue = "";
		$this->kode_asset->TooltipValue = "";

		// jenis_asset
		$this->jenis_asset->LinkCustomAttributes = "";
		$this->jenis_asset->HrefValue = "";
		$this->jenis_asset->TooltipValue = "";

		// merk
		$this->merk->LinkCustomAttributes = "";
		$this->merk->HrefValue = "";
		$this->merk->TooltipValue = "";

		// spesifikasi
		$this->spesifikasi->LinkCustomAttributes = "";
		$this->spesifikasi->HrefValue = "";
		$this->spesifikasi->TooltipValue = "";

		// foto
		$this->foto->LinkCustomAttributes = "";
		$this->foto->UploadPath = "custom/uploads/foto_assets";
		if (!EmptyValue($this->foto->Upload->DbValue)) {
			$this->foto->HrefValue = GetFileUploadUrl($this->foto, $this->foto->Upload->DbValue); // Add prefix/suffix
			$this->foto->LinkAttrs["target"] = ""; // Add target
			if ($this->isExport()) $this->foto->HrefValue = FullUrl($this->foto->HrefValue, "href");
		} else {
			$this->foto->HrefValue = "";
		}
		$this->foto->ExportHrefValue = $this->foto->UploadPath . $this->foto->Upload->DbValue;
		$this->foto->TooltipValue = "";
		if ($this->foto->UseColorbox) {
			if (EmptyValue($this->foto->TooltipValue))
				$this->foto->LinkAttrs["title"] = $Language->phrase("ViewImageGallery");
			$this->foto->LinkAttrs["data-rel"] = "assets_x_foto";
			AppendClass($this->foto->LinkAttrs["class"], "ew-lightbox");
		}

		// kelengkapan_awal
		$this->kelengkapan_awal->LinkCustomAttributes = "";
		$this->kelengkapan_awal->HrefValue = "";
		$this->kelengkapan_awal->TooltipValue = "";

		// keterangan_terbaru
		$this->keterangan_terbaru->LinkCustomAttributes = "";
		$this->keterangan_terbaru->HrefValue = "";
		$this->keterangan_terbaru->TooltipValue = "";

		// status
		$this->status->LinkCustomAttributes = "";
		$this->status->HrefValue = "";
		$this->status->TooltipValue = "";

		// created_by
		$this->created_by->LinkCustomAttributes = "";
		$this->created_by->HrefValue = "";
		$this->created_by->TooltipValue = "";

		// created_date
		$this->created_date->LinkCustomAttributes = "";
		$this->created_date->HrefValue = "";
		$this->created_date->TooltipValue = "";

		// current_asign
		$this->current_asign->LinkCustomAttributes = "";
		$this->current_asign->HrefValue = "";
		$this->current_asign->TooltipValue = "";

		// assign_date
		$this->assign_date->LinkCustomAttributes = "";
		$this->assign_date->HrefValue = "";
		$this->assign_date->TooltipValue = "";

		// back_date
		$this->back_date->LinkCustomAttributes = "";
		$this->back_date->HrefValue = "";
		$this->back_date->TooltipValue = "";

		// Call Row Rendered event
		$this->Row_Rendered();

		// Save data for Custom Template
		$this->Rows[] = $this->customTemplateFieldValues();
	}

	// Render edit row values
	public function renderEditRow()
	{
		global $Security, $CurrentLanguage, $Language;

		// Call Row Rendering event
		$this->Row_Rendering();

		// id
		$this->id->EditAttrs["class"] = "form-control";
		$this->id->EditCustomAttributes = "";
		$this->id->EditValue = $this->id->CurrentValue;
		$this->id->ViewCustomAttributes = "";

		// kode_asset
		$this->kode_asset->EditAttrs["class"] = "form-control";
		$this->kode_asset->EditCustomAttributes = "";
		if (REMOVE_XSS)
			$this->kode_asset->CurrentValue = HtmlDecode($this->kode_asset->CurrentValue);
		$this->kode_asset->EditValue = $this->kode_asset->CurrentValue;
		$this->kode_asset->PlaceHolder = RemoveHtml($this->kode_asset->caption());

		// jenis_asset
		$this->jenis_asset->EditAttrs["class"] = "form-control";
		$this->jenis_asset->EditCustomAttributes = "";

		// merk
		$this->merk->EditAttrs["class"] = "form-control";
		$this->merk->EditCustomAttributes = "";
		if (REMOVE_XSS)
			$this->merk->CurrentValue = HtmlDecode($this->merk->CurrentValue);
		$this->merk->EditValue = $this->merk->CurrentValue;
		$this->merk->PlaceHolder = RemoveHtml($this->merk->caption());

		// spesifikasi
		$this->spesifikasi->EditAttrs["class"] = "form-control";
		$this->spesifikasi->EditCustomAttributes = "";
		$this->spesifikasi->EditValue = $this->spesifikasi->CurrentValue;
		$this->spesifikasi->PlaceHolder = RemoveHtml($this->spesifikasi->caption());

		// foto
		$this->foto->EditAttrs["class"] = "form-control";
		$this->foto->EditCustomAttributes = "";
		$this->foto->UploadPath = "custom/uploads/foto_assets";
		if (!EmptyValue($this->foto->Upload->DbValue)) {
			$this->foto->ImageWidth = 50;
			$this->foto->ImageHeight = 50;
			$this->foto->ImageAlt = $this->foto->alt();
			$this->foto->EditValue = $this->foto->Upload->DbValue;
		} else {
			$this->foto->EditValue = "";
		}
		if (!EmptyValue($this->foto->CurrentValue))
				$this->foto->Upload->FileName = $this->foto->CurrentValue;

		// kelengkapan_awal
		$this->kelengkapan_awal->EditAttrs["class"] = "form-control";
		$this->kelengkapan_awal->EditCustomAttributes = "";
		$this->kelengkapan_awal->EditValue = $this->kelengkapan_awal->CurrentValue;
		$this->kelengkapan_awal->PlaceHolder = RemoveHtml($this->kelengkapan_awal->caption());

		// keterangan_terbaru
		$this->keterangan_terbaru->EditAttrs["class"] = "form-control";
		$this->keterangan_terbaru->EditCustomAttributes = "";
		$this->keterangan_terbaru->EditValue = $this->keterangan_terbaru->CurrentValue;
		$this->keterangan_terbaru->PlaceHolder = RemoveHtml($this->keterangan_terbaru->caption());

		// status
		$this->status->EditAttrs["class"] = "form-control";
		$this->status->EditCustomAttributes = "";
		$this->status->EditValue = $this->status->options(TRUE);

		// created_by
		// created_date
		// current_asign

		$this->current_asign->EditAttrs["class"] = "form-control";
		$this->current_asign->EditCustomAttributes = "";

		// assign_date
		$this->assign_date->EditAttrs["class"] = "form-control";
		$this->assign_date->EditCustomAttributes = "";
		$this->assign_date->EditValue = FormatDateTime($this->assign_date->CurrentValue, 8);
		$this->assign_date->PlaceHolder = RemoveHtml($this->assign_date->caption());

		// back_date
		$this->back_date->EditAttrs["class"] = "form-control";
		$this->back_date->EditCustomAttributes = "";
		$this->back_date->EditValue = FormatDateTime($this->back_date->CurrentValue, 8);
		$this->back_date->PlaceHolder = RemoveHtml($this->back_date->caption());

		// Call Row Rendered event
		$this->Row_Rendered();
	}

	// Aggregate list row values
	public function aggregateListRowValues()
	{
	}

	// Aggregate list row (for rendering)
	public function aggregateListRow()
	{

		// Call Row Rendered event
		$this->Row_Rendered();
	}

	// Export data in HTML/CSV/Word/Excel/Email/PDF format
	public function exportDocument($doc, $recordset, $startRec = 1, $stopRec = 1, $exportPageType = "")
	{
		if (!$recordset || !$doc)
			return;
		if (!$doc->ExportCustom) {

			// Write header
			$doc->exportTableHeader();
			if ($doc->Horizontal) { // Horizontal format, write header
				$doc->beginExportRow();
				if ($exportPageType == "view") {
					$doc->exportCaption($this->kode_asset);
					$doc->exportCaption($this->jenis_asset);
					$doc->exportCaption($this->merk);
					$doc->exportCaption($this->spesifikasi);
					$doc->exportCaption($this->foto);
					$doc->exportCaption($this->kelengkapan_awal);
					$doc->exportCaption($this->keterangan_terbaru);
					$doc->exportCaption($this->status);
					$doc->exportCaption($this->created_by);
					$doc->exportCaption($this->created_date);
					$doc->exportCaption($this->current_asign);
					$doc->exportCaption($this->assign_date);
					$doc->exportCaption($this->back_date);
				} else {
					$doc->exportCaption($this->kode_asset);
					$doc->exportCaption($this->jenis_asset);
					$doc->exportCaption($this->merk);
					$doc->exportCaption($this->spesifikasi);
					$doc->exportCaption($this->foto);
					$doc->exportCaption($this->kelengkapan_awal);
					$doc->exportCaption($this->keterangan_terbaru);
					$doc->exportCaption($this->status);
					$doc->exportCaption($this->created_by);
					$doc->exportCaption($this->created_date);
					$doc->exportCaption($this->current_asign);
					$doc->exportCaption($this->assign_date);
					$doc->exportCaption($this->back_date);
				}
				$doc->endExportRow();
			}
		}

		// Move to first record
		$recCnt = $startRec - 1;
		if (!$recordset->EOF) {
			$recordset->moveFirst();
			if ($startRec > 1)
				$recordset->move($startRec - 1);
		}
		while (!$recordset->EOF && $recCnt < $stopRec) {
			$recCnt++;
			if ($recCnt >= $startRec) {
				$rowCnt = $recCnt - $startRec + 1;

				// Page break
				if ($this->ExportPageBreakCount > 0) {
					if ($rowCnt > 1 && ($rowCnt - 1) % $this->ExportPageBreakCount == 0)
						$doc->exportPageBreak();
				}
				$this->loadListRowValues($recordset);

				// Render row
				$this->RowType = ROWTYPE_VIEW; // Render view
				$this->resetAttributes();
				$this->renderListRow();
				if (!$doc->ExportCustom) {
					$doc->beginExportRow($rowCnt); // Allow CSS styles if enabled
					if ($exportPageType == "view") {
						$doc->exportField($this->kode_asset);
						$doc->exportField($this->jenis_asset);
						$doc->exportField($this->merk);
						$doc->exportField($this->spesifikasi);
						$doc->exportField($this->foto);
						$doc->exportField($this->kelengkapan_awal);
						$doc->exportField($this->keterangan_terbaru);
						$doc->exportField($this->status);
						$doc->exportField($this->created_by);
						$doc->exportField($this->created_date);
						$doc->exportField($this->current_asign);
						$doc->exportField($this->assign_date);
						$doc->exportField($this->back_date);
					} else {
						$doc->exportField($this->kode_asset);
						$doc->exportField($this->jenis_asset);
						$doc->exportField($this->merk);
						$doc->exportField($this->spesifikasi);
						$doc->exportField($this->foto);
						$doc->exportField($this->kelengkapan_awal);
						$doc->exportField($this->keterangan_terbaru);
						$doc->exportField($this->status);
						$doc->exportField($this->created_by);
						$doc->exportField($this->created_date);
						$doc->exportField($this->current_asign);
						$doc->exportField($this->assign_date);
						$doc->exportField($this->back_date);
					}
					$doc->endExportRow($rowCnt);
				}
			}

			// Call Row Export server event
			if ($doc->ExportCustom)
				$this->Row_Export($recordset->fields);
			$recordset->moveNext();
		}
		if (!$doc->ExportCustom) {
			$doc->exportTableFooter();
		}
	}

	// Lookup data from table
	public function lookup()
	{
		global $Language, $LANGUAGE_FOLDER, $PROJECT_ID;
		if (!isset($Language))
			$Language = new Language($LANGUAGE_FOLDER, Post("language", ""));
		global $Security, $RequestSecurity;

		// Check token first
		$func = PROJECT_NAMESPACE . "CheckToken";
		$validRequest = FALSE;
		if (is_callable($func) && Post(TOKEN_NAME) !== NULL) {
			$validRequest = $func(Post(TOKEN_NAME), SessionTimeoutTime());
			if ($validRequest) {
				if (!isset($Security)) {
					if (session_status() !== PHP_SESSION_ACTIVE)
						session_start(); // Init session data
					$Security = new AdvancedSecurity();
					if ($Security->isLoggedIn()) $Security->TablePermission_Loading();
					$Security->loadCurrentUserLevel($PROJECT_ID . $this->TableName);
					if ($Security->isLoggedIn()) $Security->TablePermission_Loaded();
					$validRequest = $Security->canList(); // List permission
					if ($validRequest) {
						$Security->UserID_Loading();
						$Security->loadUserID();
						$Security->UserID_Loaded();
					}
				}
			}
		} else {

			// User profile
			$UserProfile = new UserProfile();

			// Security
			$Security = new AdvancedSecurity();
			if (is_array($RequestSecurity)) // Login user for API request
				$Security->loginUser(@$RequestSecurity["username"], @$RequestSecurity["userid"], @$RequestSecurity["parentuserid"], @$RequestSecurity["userlevelid"]);
			$Security->TablePermission_Loading();
			$Security->loadCurrentUserLevel(CurrentProjectID() . $this->TableName);
			$Security->TablePermission_Loaded();
			$validRequest = $Security->canList(); // List permission
		}

		// Reject invalid request
		if (!$validRequest)
			return FALSE;

		// Load lookup parameters
		$distinct = ConvertToBool(Post("distinct"));
		$linkField = Post("linkField");
		$displayFields = Post("displayFields");
		$parentFields = Post("parentFields");
		if (!is_array($parentFields))
			$parentFields = [];
		$childFields = Post("childFields");
		if (!is_array($childFields))
			$childFields = [];
		$filterFields = Post("filterFields");
		if (!is_array($filterFields))
			$filterFields = [];
		$filterFieldVars = Post("filterFieldVars");
		if (!is_array($filterFieldVars))
			$filterFieldVars = [];
		$filterOperators = Post("filterOperators");
		if (!is_array($filterOperators))
			$filterOperators = [];
		$autoFillSourceFields = Post("autoFillSourceFields");
		if (!is_array($autoFillSourceFields))
			$autoFillSourceFields = [];
		$formatAutoFill = FALSE;
		$lookupType = Post("ajax", "unknown");
		$pageSize = -1;
		$offset = -1;
		$searchValue = "";
		if (SameText($lookupType, "modal")) {
			$searchValue = Post("sv", "");
			$pageSize = Post("recperpage", 10);
			$offset = Post("start", 0);
		} elseif (SameText($lookupType, "autosuggest")) {
			$searchValue = Get("q", "");
			$pageSize = Param("n", -1);
			$pageSize = is_numeric($pageSize) ? (int)$pageSize : -1;
			if ($pageSize <= 0)
				$pageSize = AUTO_SUGGEST_MAX_ENTRIES;
			$start = Param("start", -1);
			$start = is_numeric($start) ? (int)$start : -1;
			$page = Param("page", -1);
			$page = is_numeric($page) ? (int)$page : -1;
			$offset = $start >= 0 ? $start : ($page > 0 && $pageSize > 0 ? ($page - 1) * $pageSize : 0);
		}
		$userSelect = Decrypt(Post("s", ""));
		$userFilter = Decrypt(Post("f", ""));
		$userOrderBy = Decrypt(Post("o", ""));
		$keys = Post("keys");

		// Selected records from modal, skip parent/filter fields and show all records
		if ($keys !== NULL) {
			$parentFields = [];
			$filterFields = [];
			$filterFieldVars = [];
			$offset = 0;
			$pageSize = 0;
		}

		// Create lookup object and output JSON
		$lookup = new Lookup($linkField, $this->TableVar, $distinct, $linkField, $displayFields, $parentFields, $childFields, $filterFields, $filterFieldVars, $autoFillSourceFields);
		foreach ($filterFields as $i => $filterField) { // Set up filter operators
			if (@$filterOperators[$i] <> "")
				$lookup->setFilterOperator($filterField, $filterOperators[$i]);
		}
		$lookup->LookupType = $lookupType; // Lookup type
		if ($keys !== NULL) { // Selected records from modal
			if (is_array($keys))
				$keys = implode(LOOKUP_FILTER_VALUE_SEPARATOR, $keys);
			$lookup->FilterValues[] = $keys; // Lookup values
		} else { // Lookup values
			$lookup->FilterValues[] = Post("v0", Post("lookupValue", ""));
		}
		$cnt = is_array($filterFields) ? count($filterFields) : 0;
		for ($i = 1; $i <= $cnt; $i++)
			$lookup->FilterValues[] = Post("v" . $i, "");
		$lookup->SearchValue = $searchValue;
		$lookup->PageSize = $pageSize;
		$lookup->Offset = $offset;
		if ($userSelect <> "")
			$lookup->UserSelect = $userSelect;
		if ($userFilter <> "")
			$lookup->UserFilter = $userFilter;
		if ($userOrderBy <> "")
			$lookup->UserOrderBy = $userOrderBy;
		$lookup->toJson();
	}

	// Get file data
	public function getFileData($fldparm, $key, $resize, $width = THUMBNAIL_DEFAULT_WIDTH, $height = THUMBNAIL_DEFAULT_HEIGHT)
	{
		global $COMPOSITE_KEY_SEPARATOR;

		// Set up field name / file name field / file type field
		$fldName = "";
		$fileNameFld = "";
		$fileTypeFld = "";
		if ($fldparm == 'foto') {
			$fldName = "foto";
			$fileNameFld = "foto";
		} else {
			return FALSE; // Incorrect field
		}

		// Set up key values
		$ar = explode($COMPOSITE_KEY_SEPARATOR, $key);
		if (count($ar) == 1) {
			$this->id->CurrentValue = $ar[0];
		} else {
			return FALSE; // Incorrect key
		}

		// Set up filter (WHERE Clause)
		$filter = $this->getRecordFilter();
		$this->CurrentFilter = $filter;
		$sql = $this->getCurrentSql();
		$conn = &$this->getConnection();
		$dbtype = GetConnectionType($this->Dbid);
		if (($rs = $conn->execute($sql)) && !$rs->EOF) {
			$val = $rs->fields($fldName);
			if (!EmptyValue($val)) {
				$fld = $this->fields[$fldName];

				// Binary data
				if ($fld->DataType == DATATYPE_BLOB) {
					if ($dbtype <> "MYSQL") {
						if (is_array($val) || is_object($val)) // Byte array
							$val = BytesToString($val);
					}
					if ($resize)
						ResizeBinary($val, $width, $height);

					// Write file type
					if ($fileTypeFld <> "" && !EmptyValue($rs->fields($fileTypeFld))) {
						AddHeader("Content-type", $rs->fields($fileTypeFld));
					} else {
						AddHeader("Content-type", ContentType($val));
					}

					// Write file name
					if ($fileNameFld <> "" && !EmptyValue($rs->fields($fileNameFld)))
						AddHeader("Content-Disposition", "attachment; filename=\"" . $rs->fields($fileNameFld) . "\"");

					// Write file data
					if (StartsString("PK", $val) && ContainsString($val, "[Content_Types].xml") &&
						ContainsString($val, "_rels") && ContainsString($val, "docProps")) { // Fix Office 2007 documents
						if (!EndsString("\0\0\0", $val)) // Not ends with 3 or 4 \0
							$val .= "\0\0\0\0";
					}

					// Clear output buffer
					if (ob_get_length())
						ob_end_clean();

					// Write binary data
					Write($val);

				// Upload to folder
				} else {
					if ($fld->UploadMultiple)
						$files = explode(MULTIPLE_UPLOAD_SEPARATOR, $val);
					else
						$files = [$val];
					$data = [];
					$ar = [];
					foreach ($files as $file) {
						if (!EmptyValue($file))
							$ar[$file] = FullUrl($fld->hrefPath() . $file);
					}
					$data[$fld->Param] = $ar;
					WriteJson($data);
				}
			}
			$rs->close();
			return TRUE;
		}
		return FALSE;
	}

	// Table level events
	// Recordset Selecting event
	function Recordset_Selecting(&$filter) {

		// Enter your code here
	}

	// Recordset Selected event
	function Recordset_Selected(&$rs) {

		//echo "Recordset Selected";
	}

	// Recordset Search Validated event
	function Recordset_SearchValidated() {

		// Example:
		//$this->MyField1->AdvancedSearch->SearchValue = "your search criteria"; // Search value

	}

	// Recordset Searching event
	function Recordset_Searching(&$filter) {

		// Enter your code here
	}

	// Row_Selecting event
	function Row_Selecting(&$filter) {

		// Enter your code here
	}

	// Row Selected event
	function Row_Selected(&$rs) {

		//echo "Row Selected";
	}

	// Row Inserting event
	function Row_Inserting($rsold, &$rsnew) {

		// Enter your code here
		// To cancel, set return value to FALSE

		return TRUE;
	}

	// Row Inserted event
	function Row_Inserted($rsold, &$rsnew) {

		//echo "Row Inserted"
	}

	// Row Updating event
	function Row_Updating($rsold, &$rsnew) {

		// Enter your code here
		// To cancel, set return value to FALSE

		return TRUE;
	}

	// Row Updated event
	function Row_Updated($rsold, &$rsnew) {

		//echo "Row Updated";
	}

	// Row Update Conflict event
	function Row_UpdateConflict($rsold, &$rsnew) {

		// Enter your code here
		// To ignore conflict, set return value to FALSE

		return TRUE;
	}

	// Grid Inserting event
	function Grid_Inserting() {

		// Enter your code here
		// To reject grid insert, set return value to FALSE

		return TRUE;
	}

	// Grid Inserted event
	function Grid_Inserted($rsnew) {

		//echo "Grid Inserted";
	}

	// Grid Updating event
	function Grid_Updating($rsold) {

		// Enter your code here
		// To reject grid update, set return value to FALSE

		return TRUE;
	}

	// Grid Updated event
	function Grid_Updated($rsold, $rsnew) {

		//echo "Grid Updated";
	}

	// Row Deleting event
	function Row_Deleting(&$rs) {

		// Enter your code here
		// To cancel, set return value to False

		return TRUE;
	}

	// Row Deleted event
	function Row_Deleted(&$rs) {

		//echo "Row Deleted";
	}

	// Email Sending event
	function Email_Sending($email, &$args) {

		//var_dump($email); var_dump($args); exit();
		return TRUE;
	}

	// Lookup Selecting event
	function Lookup_Selecting($fld, &$filter) {

		//var_dump($fld->Name, $fld->Lookup, $filter); // Uncomment to view the filter
		// Enter your code here

	}

	// Row Rendering event
	function Row_Rendering() {

		// Enter your code here
	}

	// Row Rendered event
	function Row_Rendered() {

		// To view properties of field class, use:
		//var_dump($this-><FieldName>);

	}

	// User ID Filtering event
	function UserID_Filtering(&$filter) {

		// Enter your code here
	}
}
?>