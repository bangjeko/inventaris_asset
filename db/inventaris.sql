-- phpMyAdmin SQL Dump
-- version 4.4.15.9
-- https://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jan 24, 2020 at 12:54 PM
-- Server version: 5.6.37
-- PHP Version: 7.1.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `inventaris`
--

-- --------------------------------------------------------

--
-- Table structure for table `assets`
--

CREATE TABLE IF NOT EXISTS `assets` (
  `id` int(11) NOT NULL,
  `kode_asset` varchar(255) NOT NULL,
  `jenis_asset` int(11) NOT NULL,
  `merk` varchar(255) NOT NULL,
  `spesifikasi` text NOT NULL,
  `foto` text,
  `kelengkapan_awal` text NOT NULL,
  `keterangan_terbaru` text,
  `status` int(11) NOT NULL,
  `current_asign` int(11) NOT NULL,
  `assign_date` date NOT NULL,
  `back_date` date NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` date NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `assets`
--

INSERT INTO `assets` (`id`, `kode_asset`, `jenis_asset`, `merk`, `spesifikasi`, `foto`, `kelengkapan_awal`, `keterangan_terbaru`, `status`, `current_asign`, `assign_date`, `back_date`, `created_by`, `created_date`) VALUES
(2, 'A01', 1, 'Asus', 'SPek Apik', 'asus.jpeg', 'Komplit', 'ok', 1, 0, '2020-01-20', '2020-01-20', -1, '2020-01-22'),
(3, 'A02', 1, 'HP', 'Spek Paling jos', NULL, 'Komplit Njerit', NULL, 1, 0, '0000-00-00', '0000-00-00', -1, '2020-01-22');

-- --------------------------------------------------------

--
-- Table structure for table `jenis_asset`
--

CREATE TABLE IF NOT EXISTS `jenis_asset` (
  `id` int(11) NOT NULL,
  `nama_jenis` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jenis_asset`
--

INSERT INTO `jenis_asset` (`id`, `nama_jenis`) VALUES
(1, 'Notebook');

-- --------------------------------------------------------

--
-- Table structure for table `karyawan`
--

CREATE TABLE IF NOT EXISTS `karyawan` (
  `id` int(11) NOT NULL,
  `nama_karyawan` varchar(255) NOT NULL,
  `nik` varchar(20) NOT NULL,
  `divisi` varchar(255) NOT NULL,
  `jabatan` varchar(255) NOT NULL,
  `atasan_langsung` varchar(255) DEFAULT NULL,
  `nomor_telepon` varchar(20) NOT NULL,
  `email` varchar(50) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` date NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `karyawan`
--

INSERT INTO `karyawan` (`id`, `nama_karyawan`, `nik`, `divisi`, `jabatan`, `atasan_langsung`, `nomor_telepon`, `email`, `created_by`, `created_date`) VALUES
(1, 'Dono', '12345', 'Networking', 'Staff Networking', 'Darno', '08781719191', 'dono@gmail.com', -1, '2020-01-22');

-- --------------------------------------------------------

--
-- Table structure for table `transaksi_peminjaman`
--

CREATE TABLE IF NOT EXISTS `transaksi_peminjaman` (
  `id` int(11) NOT NULL,
  `id_karyawan` int(11) NOT NULL,
  `id_asset` int(11) NOT NULL,
  `tanggal_pinjam` date NOT NULL,
  `tanggal_kembali` date NOT NULL,
  `kelengkapan_asset` text,
  `keterangan` text,
  `created_by` int(11) NOT NULL,
  `created_date` date NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `transaksi_peminjaman`
--

INSERT INTO `transaksi_peminjaman` (`id`, `id_karyawan`, `id_asset`, `tanggal_pinjam`, `tanggal_kembali`, `kelengkapan_asset`, `keterangan`, `created_by`, `created_date`) VALUES
(6, 1, 3, '2020-01-19', '2020-01-24', 'Komplit', 'aa', 2, '2020-01-19');

-- --------------------------------------------------------

--
-- Table structure for table `transaksi_pengembalian`
--

CREATE TABLE IF NOT EXISTS `transaksi_pengembalian` (
  `id` int(11) NOT NULL,
  `id_karyawan` int(11) NOT NULL,
  `id_asset` int(11) NOT NULL,
  `tanggal_kembali` date NOT NULL,
  `keterangan` text,
  `created_by` int(11) NOT NULL,
  `created_date` date NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `transaksi_pengembalian`
--

INSERT INTO `transaksi_pengembalian` (`id`, `id_karyawan`, `id_asset`, `tanggal_kembali`, `keterangan`, `created_by`, `created_date`) VALUES
(1, 1, 2, '2020-01-19', 'ok', 2, '2020-01-19'),
(2, 1, 3, '2020-01-19', NULL, 2, '2020-01-19');

-- --------------------------------------------------------

--
-- Table structure for table `userlevelpermissions`
--

CREATE TABLE IF NOT EXISTS `userlevelpermissions` (
  `userlevelid` int(11) NOT NULL,
  `tablename` varchar(255) NOT NULL DEFAULT '',
  `permission` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `userlevelpermissions`
--

INSERT INTO `userlevelpermissions` (`userlevelid`, `tablename`, `permission`) VALUES
(-2, '{4CC22A5F-F7CC-47B8-B12F-E4E9477EA048}assets', 0),
(-2, '{4CC22A5F-F7CC-47B8-B12F-E4E9477EA048}karyawan', 0),
(-2, '{4CC22A5F-F7CC-47B8-B12F-E4E9477EA048}transaksi_peminjaman', 0),
(-2, '{4CC22A5F-F7CC-47B8-B12F-E4E9477EA048}transaksi_pengembalian', 0),
(-2, '{4CC22A5F-F7CC-47B8-B12F-E4E9477EA048}userlevelpermissions', 0),
(-2, '{4CC22A5F-F7CC-47B8-B12F-E4E9477EA048}userlevels', 0),
(-2, '{4CC22A5F-F7CC-47B8-B12F-E4E9477EA048}users', 0),
(10, '{1137E948-42AA-49E0-9701-11726AEB00AF}assets', 104),
(10, '{1137E948-42AA-49E0-9701-11726AEB00AF}cetak.php', 104),
(10, '{1137E948-42AA-49E0-9701-11726AEB00AF}karyawan', 104),
(10, '{1137E948-42AA-49E0-9701-11726AEB00AF}transaksi_peminjaman', 105),
(10, '{1137E948-42AA-49E0-9701-11726AEB00AF}transaksi_pengembalian', 105),
(10, '{1137E948-42AA-49E0-9701-11726AEB00AF}userlevelpermissions', 0),
(10, '{1137E948-42AA-49E0-9701-11726AEB00AF}userlevels', 0),
(10, '{1137E948-42AA-49E0-9701-11726AEB00AF}users', 108),
(10, '{1137E948-42AA-49E0-9701-11726AEB00AF}v_user', 104),
(10, '{4CC22A5F-F7CC-47B8-B12F-E4E9477EA048}assets', 104),
(10, '{4CC22A5F-F7CC-47B8-B12F-E4E9477EA048}karyawan', 104),
(10, '{4CC22A5F-F7CC-47B8-B12F-E4E9477EA048}transaksi_peminjaman', 111),
(10, '{4CC22A5F-F7CC-47B8-B12F-E4E9477EA048}transaksi_pengembalian', 111),
(10, '{4CC22A5F-F7CC-47B8-B12F-E4E9477EA048}userlevelpermissions', 0),
(10, '{4CC22A5F-F7CC-47B8-B12F-E4E9477EA048}userlevels', 0),
(10, '{4CC22A5F-F7CC-47B8-B12F-E4E9477EA048}users', 0),
(11, '{1137E948-42AA-49E0-9701-11726AEB00AF}assets', 127),
(11, '{1137E948-42AA-49E0-9701-11726AEB00AF}cetak.php', 104),
(11, '{1137E948-42AA-49E0-9701-11726AEB00AF}karyawan', 127),
(11, '{1137E948-42AA-49E0-9701-11726AEB00AF}transaksi_peminjaman', 127),
(11, '{1137E948-42AA-49E0-9701-11726AEB00AF}transaksi_pengembalian', 127),
(11, '{1137E948-42AA-49E0-9701-11726AEB00AF}userlevelpermissions', 0),
(11, '{1137E948-42AA-49E0-9701-11726AEB00AF}userlevels', 0),
(11, '{1137E948-42AA-49E0-9701-11726AEB00AF}users', 108),
(11, '{1137E948-42AA-49E0-9701-11726AEB00AF}v_user', 104),
(11, '{4CC22A5F-F7CC-47B8-B12F-E4E9477EA048}assets', 111),
(11, '{4CC22A5F-F7CC-47B8-B12F-E4E9477EA048}karyawan', 111),
(11, '{4CC22A5F-F7CC-47B8-B12F-E4E9477EA048}transaksi_peminjaman', 111),
(11, '{4CC22A5F-F7CC-47B8-B12F-E4E9477EA048}transaksi_pengembalian', 111),
(11, '{4CC22A5F-F7CC-47B8-B12F-E4E9477EA048}userlevelpermissions', 111),
(11, '{4CC22A5F-F7CC-47B8-B12F-E4E9477EA048}userlevels', 111),
(11, '{4CC22A5F-F7CC-47B8-B12F-E4E9477EA048}users', 111);

-- --------------------------------------------------------

--
-- Table structure for table `userlevels`
--

CREATE TABLE IF NOT EXISTS `userlevels` (
  `userlevelid` int(11) NOT NULL,
  `userlevelname` varchar(80) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `userlevels`
--

INSERT INTO `userlevels` (`userlevelid`, `userlevelname`) VALUES
(-2, 'Anonymous'),
(-1, 'Administrator'),
(0, 'Default'),
(10, 'Petugas Inventaris'),
(11, 'Manager Inventaris');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL,
  `nama_lengkap` varchar(255) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` text NOT NULL,
  `userlevel_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `nama_lengkap`, `username`, `password`, `userlevel_id`) VALUES
(1, 'Aldy Hernawan', 'aldy', '$2y$10$7kdXQJrsMSEmcHRzfsw/4elJm37os.Ad4udqJnhHySjDbtQib.7RG', 10),
(2, 'Aldo', 'aldo', '$2y$10$W59BTfU60Er8a9yI6kYSk.BpuaeZXunvYEr5Ocxk1PJVogaItcFO6', 11);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_user`
--
CREATE TABLE IF NOT EXISTS `v_user` (
`id` int(11)
,`username` varchar(50)
,`password` text
,`userlevel_id` int(11)
);

-- --------------------------------------------------------

--
-- Structure for view `v_user`
--
DROP TABLE IF EXISTS `v_user`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_user` AS select `users`.`id` AS `id`,`users`.`username` AS `username`,`users`.`password` AS `password`,`users`.`userlevel_id` AS `userlevel_id` from `users`;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `assets`
--
ALTER TABLE `assets`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `kode_asset` (`kode_asset`);

--
-- Indexes for table `jenis_asset`
--
ALTER TABLE `jenis_asset`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `karyawan`
--
ALTER TABLE `karyawan`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `nik` (`nik`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `nomor_telepon` (`nomor_telepon`);

--
-- Indexes for table `transaksi_peminjaman`
--
ALTER TABLE `transaksi_peminjaman`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transaksi_pengembalian`
--
ALTER TABLE `transaksi_pengembalian`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `userlevelpermissions`
--
ALTER TABLE `userlevelpermissions`
  ADD PRIMARY KEY (`userlevelid`,`tablename`);

--
-- Indexes for table `userlevels`
--
ALTER TABLE `userlevels`
  ADD PRIMARY KEY (`userlevelid`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `assets`
--
ALTER TABLE `assets`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `jenis_asset`
--
ALTER TABLE `jenis_asset`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `karyawan`
--
ALTER TABLE `karyawan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `transaksi_peminjaman`
--
ALTER TABLE `transaksi_peminjaman`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `transaksi_pengembalian`
--
ALTER TABLE `transaksi_pengembalian`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
