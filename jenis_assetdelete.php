<?php
namespace PHPMaker2019\inventaris_assets;

// Session
if (session_status() !== PHP_SESSION_ACTIVE)
	session_start(); // Init session data

// Output buffering
ob_start(); 

// Autoload
include_once "autoload.php";
?>
<?php

// Write header
WriteHeader(FALSE);

// Create page object
$jenis_asset_delete = new jenis_asset_delete();

// Run the page
$jenis_asset_delete->run();

// Setup login status
SetupLoginStatus();
SetClientVar("login", LoginStatus());

// Global Page Rendering event (in userfn*.php)
Page_Rendering();

// Page Rendering event
$jenis_asset_delete->Page_Render();
?>
<?php include_once "header.php" ?>
<script>

// Form object
currentPageID = ew.PAGE_ID = "delete";
var fjenis_assetdelete = currentForm = new ew.Form("fjenis_assetdelete", "delete");

// Form_CustomValidate event
fjenis_assetdelete.Form_CustomValidate = function(fobj) { // DO NOT CHANGE THIS LINE!

	// Your custom validation code here, return false if invalid.
	return true;
}

// Use JavaScript validation or not
fjenis_assetdelete.validateRequired = <?php echo json_encode(CLIENT_VALIDATE) ?>;

// Dynamic selection lists
// Form object for search

</script>
<script>

// Write your client script here, no need to add script tags.
</script>
<?php $jenis_asset_delete->showPageHeader(); ?>
<?php
$jenis_asset_delete->showMessage();
?>
<form name="fjenis_assetdelete" id="fjenis_assetdelete" class="form-inline ew-form ew-delete-form" action="<?php echo CurrentPageName() ?>" method="post">
<?php if ($jenis_asset_delete->CheckToken) { ?>
<input type="hidden" name="<?php echo TOKEN_NAME ?>" value="<?php echo $jenis_asset_delete->Token ?>">
<?php } ?>
<input type="hidden" name="t" value="jenis_asset">
<input type="hidden" name="action" id="action" value="delete">
<?php foreach ($jenis_asset_delete->RecKeys as $key) { ?>
<?php $keyvalue = is_array($key) ? implode($COMPOSITE_KEY_SEPARATOR, $key) : $key; ?>
<input type="hidden" name="key_m[]" value="<?php echo HtmlEncode($keyvalue) ?>">
<?php } ?>
<div class="card ew-card ew-grid">
<div class="<?php if (IsResponsiveLayout()) { ?>table-responsive <?php } ?>card-body ew-grid-middle-panel">
<table class="table ew-table">
	<thead>
	<tr class="ew-table-header">
<?php if ($jenis_asset->nama_jenis->Visible) { // nama_jenis ?>
		<th class="<?php echo $jenis_asset->nama_jenis->headerCellClass() ?>"><span id="elh_jenis_asset_nama_jenis" class="jenis_asset_nama_jenis"><?php echo $jenis_asset->nama_jenis->caption() ?></span></th>
<?php } ?>
	</tr>
	</thead>
	<tbody>
<?php
$jenis_asset_delete->RecCnt = 0;
$i = 0;
while (!$jenis_asset_delete->Recordset->EOF) {
	$jenis_asset_delete->RecCnt++;
	$jenis_asset_delete->RowCnt++;

	// Set row properties
	$jenis_asset->resetAttributes();
	$jenis_asset->RowType = ROWTYPE_VIEW; // View

	// Get the field contents
	$jenis_asset_delete->loadRowValues($jenis_asset_delete->Recordset);

	// Render row
	$jenis_asset_delete->renderRow();
?>
	<tr<?php echo $jenis_asset->rowAttributes() ?>>
<?php if ($jenis_asset->nama_jenis->Visible) { // nama_jenis ?>
		<td<?php echo $jenis_asset->nama_jenis->cellAttributes() ?>>
<span id="el<?php echo $jenis_asset_delete->RowCnt ?>_jenis_asset_nama_jenis" class="jenis_asset_nama_jenis">
<span<?php echo $jenis_asset->nama_jenis->viewAttributes() ?>>
<?php echo $jenis_asset->nama_jenis->getViewValue() ?></span>
</span>
</td>
<?php } ?>
	</tr>
<?php
	$jenis_asset_delete->Recordset->moveNext();
}
$jenis_asset_delete->Recordset->close();
?>
</tbody>
</table>
</div>
</div>
<div>
<button class="btn btn-primary ew-btn" name="btn-action" id="btn-action" type="submit"><?php echo $Language->phrase("DeleteBtn") ?></button>
<button class="btn btn-default ew-btn" name="btn-cancel" id="btn-cancel" type="button" data-href="<?php echo $jenis_asset_delete->getReturnUrl() ?>"><?php echo $Language->phrase("CancelBtn") ?></button>
</div>
</form>
<?php
$jenis_asset_delete->showPageFooter();
if (DEBUG_ENABLED)
	echo GetDebugMessage();
?>
<script>

// Write your table-specific startup script here
// document.write("page loaded");

</script>
<?php include_once "footer.php" ?>
<?php
$jenis_asset_delete->terminate();
?>