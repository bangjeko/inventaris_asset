<?php
namespace PHPMaker2019\inventaris_assets;

// Session
if (session_status() !== PHP_SESSION_ACTIVE)
	session_start(); // Init session data

// Output buffering
ob_start(); 

// Autoload
include_once "autoload.php";
?>
<?php

// Write header
WriteHeader(FALSE);

// Create page object
$karyawan_list = new karyawan_list();

// Run the page
$karyawan_list->run();

// Setup login status
SetupLoginStatus();
SetClientVar("login", LoginStatus());

// Global Page Rendering event (in userfn*.php)
Page_Rendering();

// Page Rendering event
$karyawan_list->Page_Render();
?>
<?php include_once "header.php" ?>
<?php if (!$karyawan->isExport()) { ?>
<script>

// Form object
currentPageID = ew.PAGE_ID = "list";
var fkaryawanlist = currentForm = new ew.Form("fkaryawanlist", "list");
fkaryawanlist.formKeyCountName = '<?php echo $karyawan_list->FormKeyCountName ?>';

// Form_CustomValidate event
fkaryawanlist.Form_CustomValidate = function(fobj) { // DO NOT CHANGE THIS LINE!

	// Your custom validation code here, return false if invalid.
	return true;
}

// Use JavaScript validation or not
fkaryawanlist.validateRequired = <?php echo json_encode(CLIENT_VALIDATE) ?>;

// Dynamic selection lists
// Form object for search

var fkaryawanlistsrch = currentSearchForm = new ew.Form("fkaryawanlistsrch");

// Filters
fkaryawanlistsrch.filterList = <?php echo $karyawan_list->getFilterList() ?>;

// Init search panel as collapsed
fkaryawanlistsrch.initSearchPanel = true;
</script>
<script>

// Write your client script here, no need to add script tags.
</script>
<?php } ?>
<?php if (!$karyawan->isExport()) { ?>
<div class="btn-toolbar ew-toolbar">
<?php if ($karyawan_list->TotalRecs > 0 && $karyawan_list->ExportOptions->visible()) { ?>
<?php $karyawan_list->ExportOptions->render("body") ?>
<?php } ?>
<?php if ($karyawan_list->ImportOptions->visible()) { ?>
<?php $karyawan_list->ImportOptions->render("body") ?>
<?php } ?>
<?php if ($karyawan_list->SearchOptions->visible()) { ?>
<?php $karyawan_list->SearchOptions->render("body") ?>
<?php } ?>
<?php if ($karyawan_list->FilterOptions->visible()) { ?>
<?php $karyawan_list->FilterOptions->render("body") ?>
<?php } ?>
<div class="clearfix"></div>
</div>
<?php } ?>
<?php
$karyawan_list->renderOtherOptions();
?>
<?php if ($Security->CanSearch()) { ?>
<?php if (!$karyawan->isExport() && !$karyawan->CurrentAction) { ?>
<form name="fkaryawanlistsrch" id="fkaryawanlistsrch" class="form-inline ew-form ew-ext-search-form" action="<?php echo CurrentPageName() ?>">
<?php $searchPanelClass = ($karyawan_list->SearchWhere <> "") ? " show" : ""; ?>
<div id="fkaryawanlistsrch-search-panel" class="ew-search-panel collapse<?php echo $searchPanelClass ?>">
<input type="hidden" name="cmd" value="search">
<input type="hidden" name="t" value="karyawan">
	<div class="ew-basic-search">
<div id="xsr_1" class="ew-row d-sm-flex">
	<div class="ew-quick-search input-group">
		<input type="text" name="<?php echo TABLE_BASIC_SEARCH ?>" id="<?php echo TABLE_BASIC_SEARCH ?>" class="form-control" value="<?php echo HtmlEncode($karyawan_list->BasicSearch->getKeyword()) ?>" placeholder="<?php echo HtmlEncode($Language->phrase("Search")) ?>">
		<input type="hidden" name="<?php echo TABLE_BASIC_SEARCH_TYPE ?>" id="<?php echo TABLE_BASIC_SEARCH_TYPE ?>" value="<?php echo HtmlEncode($karyawan_list->BasicSearch->getType()) ?>">
		<div class="input-group-append">
			<button class="btn btn-primary" name="btn-submit" id="btn-submit" type="submit"><?php echo $Language->phrase("SearchBtn") ?></button>
			<button type="button" data-toggle="dropdown" class="btn btn-primary dropdown-toggle dropdown-toggle-split" aria-haspopup="true" aria-expanded="false"><span id="searchtype"><?php echo $karyawan_list->BasicSearch->getTypeNameShort() ?></span></button>
			<div class="dropdown-menu dropdown-menu-right">
				<a class="dropdown-item<?php if ($karyawan_list->BasicSearch->getType() == "") echo " active"; ?>" href="javascript:void(0);" onclick="ew.setSearchType(this)"><?php echo $Language->phrase("QuickSearchAuto") ?></a>
				<a class="dropdown-item<?php if ($karyawan_list->BasicSearch->getType() == "=") echo " active"; ?>" href="javascript:void(0);" onclick="ew.setSearchType(this,'=')"><?php echo $Language->phrase("QuickSearchExact") ?></a>
				<a class="dropdown-item<?php if ($karyawan_list->BasicSearch->getType() == "AND") echo " active"; ?>" href="javascript:void(0);" onclick="ew.setSearchType(this,'AND')"><?php echo $Language->phrase("QuickSearchAll") ?></a>
				<a class="dropdown-item<?php if ($karyawan_list->BasicSearch->getType() == "OR") echo " active"; ?>" href="javascript:void(0);" onclick="ew.setSearchType(this,'OR')"><?php echo $Language->phrase("QuickSearchAny") ?></a>
			</div>
		</div>
	</div>
</div>
	</div>
</div>
</form>
<?php } ?>
<?php } ?>
<?php $karyawan_list->showPageHeader(); ?>
<?php
$karyawan_list->showMessage();
?>
<?php if ($karyawan_list->TotalRecs > 0 || $karyawan->CurrentAction) { ?>
<div class="card ew-card ew-grid<?php if ($karyawan_list->isAddOrEdit()) { ?> ew-grid-add-edit<?php } ?> karyawan">
<?php if (!$karyawan->isExport()) { ?>
<div class="card-header ew-grid-upper-panel">
<?php if (!$karyawan->isGridAdd()) { ?>
<form name="ew-pager-form" class="form-inline ew-form ew-pager-form" action="<?php echo CurrentPageName() ?>">
<?php if (!isset($karyawan_list->Pager)) $karyawan_list->Pager = new PrevNextPager($karyawan_list->StartRec, $karyawan_list->DisplayRecs, $karyawan_list->TotalRecs, $karyawan_list->AutoHidePager) ?>
<?php if ($karyawan_list->Pager->RecordCount > 0 && $karyawan_list->Pager->Visible) { ?>
<div class="ew-pager">
<span><?php echo $Language->Phrase("Page") ?>&nbsp;</span>
<div class="ew-prev-next"><div class="input-group input-group-sm">
<div class="input-group-prepend">
<!-- first page button -->
	<?php if ($karyawan_list->Pager->FirstButton->Enabled) { ?>
	<a class="btn btn-default" title="<?php echo $Language->phrase("PagerFirst") ?>" href="<?php echo $karyawan_list->pageUrl() ?>start=<?php echo $karyawan_list->Pager->FirstButton->Start ?>"><i class="icon-first ew-icon"></i></a>
	<?php } else { ?>
	<a class="btn btn-default disabled" title="<?php echo $Language->phrase("PagerFirst") ?>"><i class="icon-first ew-icon"></i></a>
	<?php } ?>
<!-- previous page button -->
	<?php if ($karyawan_list->Pager->PrevButton->Enabled) { ?>
	<a class="btn btn-default" title="<?php echo $Language->phrase("PagerPrevious") ?>" href="<?php echo $karyawan_list->pageUrl() ?>start=<?php echo $karyawan_list->Pager->PrevButton->Start ?>"><i class="icon-prev ew-icon"></i></a>
	<?php } else { ?>
	<a class="btn btn-default disabled" title="<?php echo $Language->phrase("PagerPrevious") ?>"><i class="icon-prev ew-icon"></i></a>
	<?php } ?>
</div>
<!-- current page number -->
	<input class="form-control" type="text" name="<?php echo TABLE_PAGE_NO ?>" value="<?php echo $karyawan_list->Pager->CurrentPage ?>">
<div class="input-group-append">
<!-- next page button -->
	<?php if ($karyawan_list->Pager->NextButton->Enabled) { ?>
	<a class="btn btn-default" title="<?php echo $Language->phrase("PagerNext") ?>" href="<?php echo $karyawan_list->pageUrl() ?>start=<?php echo $karyawan_list->Pager->NextButton->Start ?>"><i class="icon-next ew-icon"></i></a>
	<?php } else { ?>
	<a class="btn btn-default disabled" title="<?php echo $Language->phrase("PagerNext") ?>"><i class="icon-next ew-icon"></i></a>
	<?php } ?>
<!-- last page button -->
	<?php if ($karyawan_list->Pager->LastButton->Enabled) { ?>
	<a class="btn btn-default" title="<?php echo $Language->phrase("PagerLast") ?>" href="<?php echo $karyawan_list->pageUrl() ?>start=<?php echo $karyawan_list->Pager->LastButton->Start ?>"><i class="icon-last ew-icon"></i></a>
	<?php } else { ?>
	<a class="btn btn-default disabled" title="<?php echo $Language->phrase("PagerLast") ?>"><i class="icon-last ew-icon"></i></a>
	<?php } ?>
</div>
</div>
</div>
<span>&nbsp;<?php echo $Language->Phrase("of") ?>&nbsp;<?php echo $karyawan_list->Pager->PageCount ?></span>
<div class="clearfix"></div>
</div>
<?php } ?>
<?php if ($karyawan_list->Pager->RecordCount > 0) { ?>
<div class="ew-pager ew-rec">
	<span><?php echo $Language->Phrase("Record") ?>&nbsp;<?php echo $karyawan_list->Pager->FromIndex ?>&nbsp;<?php echo $Language->Phrase("To") ?>&nbsp;<?php echo $karyawan_list->Pager->ToIndex ?>&nbsp;<?php echo $Language->Phrase("Of") ?>&nbsp;<?php echo $karyawan_list->Pager->RecordCount ?></span>
</div>
<?php } ?>
<?php if ($karyawan_list->TotalRecs > 0 && (!$karyawan_list->AutoHidePageSizeSelector || $karyawan_list->Pager->Visible)) { ?>
<div class="ew-pager">
<input type="hidden" name="t" value="karyawan">
<select name="<?php echo TABLE_REC_PER_PAGE ?>" class="form-control form-control-sm ew-tooltip" title="<?php echo $Language->phrase("RecordsPerPage") ?>" onchange="this.form.submit();">
<option value="5"<?php if ($karyawan_list->DisplayRecs == 5) { ?> selected<?php } ?>>5</option>
<option value="10"<?php if ($karyawan_list->DisplayRecs == 10) { ?> selected<?php } ?>>10</option>
<option value="20"<?php if ($karyawan_list->DisplayRecs == 20) { ?> selected<?php } ?>>20</option>
<option value="50"<?php if ($karyawan_list->DisplayRecs == 50) { ?> selected<?php } ?>>50</option>
<option value="100"<?php if ($karyawan_list->DisplayRecs == 100) { ?> selected<?php } ?>>100</option>
</select>
</div>
<?php } ?>
</form>
<?php } ?>
<div class="ew-list-other-options">
<?php $karyawan_list->OtherOptions->render("body") ?>
</div>
<div class="clearfix"></div>
</div>
<?php } ?>
<form name="fkaryawanlist" id="fkaryawanlist" class="form-inline ew-form ew-list-form" action="<?php echo CurrentPageName() ?>" method="post">
<?php if ($karyawan_list->CheckToken) { ?>
<input type="hidden" name="<?php echo TOKEN_NAME ?>" value="<?php echo $karyawan_list->Token ?>">
<?php } ?>
<input type="hidden" name="t" value="karyawan">
<div id="gmp_karyawan" class="<?php if (IsResponsiveLayout()) { ?>table-responsive <?php } ?>card-body ew-grid-middle-panel">
<?php if ($karyawan_list->TotalRecs > 0 || $karyawan->isGridEdit()) { ?>
<table id="tbl_karyawanlist" class="table ew-table"><!-- .ew-table ##-->
<thead>
	<tr class="ew-table-header">
<?php

// Header row
$karyawan_list->RowType = ROWTYPE_HEADER;

// Render list options
$karyawan_list->renderListOptions();

// Render list options (header, left)
$karyawan_list->ListOptions->render("header", "left");
?>
<?php if ($karyawan->nama_karyawan->Visible) { // nama_karyawan ?>
	<?php if ($karyawan->sortUrl($karyawan->nama_karyawan) == "") { ?>
		<th data-name="nama_karyawan" class="<?php echo $karyawan->nama_karyawan->headerCellClass() ?>"><div id="elh_karyawan_nama_karyawan" class="karyawan_nama_karyawan"><div class="ew-table-header-caption"><?php echo $karyawan->nama_karyawan->caption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="nama_karyawan" class="<?php echo $karyawan->nama_karyawan->headerCellClass() ?>"><div class="ew-pointer" onclick="ew.sort(event,'<?php echo $karyawan->SortUrl($karyawan->nama_karyawan) ?>',1);"><div id="elh_karyawan_nama_karyawan" class="karyawan_nama_karyawan">
			<div class="ew-table-header-btn"><span class="ew-table-header-caption"><?php echo $karyawan->nama_karyawan->caption() ?><?php echo $Language->phrase("SrchLegend") ?></span><span class="ew-table-header-sort"><?php if ($karyawan->nama_karyawan->getSort() == "ASC") { ?><i class="fa fa-sort-up"></i><?php } elseif ($karyawan->nama_karyawan->getSort() == "DESC") { ?><i class="fa fa-sort-down"></i><?php } ?></span></div>
		</div></div></th>
	<?php } ?>
<?php } ?>
<?php if ($karyawan->nik->Visible) { // nik ?>
	<?php if ($karyawan->sortUrl($karyawan->nik) == "") { ?>
		<th data-name="nik" class="<?php echo $karyawan->nik->headerCellClass() ?>"><div id="elh_karyawan_nik" class="karyawan_nik"><div class="ew-table-header-caption"><?php echo $karyawan->nik->caption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="nik" class="<?php echo $karyawan->nik->headerCellClass() ?>"><div class="ew-pointer" onclick="ew.sort(event,'<?php echo $karyawan->SortUrl($karyawan->nik) ?>',1);"><div id="elh_karyawan_nik" class="karyawan_nik">
			<div class="ew-table-header-btn"><span class="ew-table-header-caption"><?php echo $karyawan->nik->caption() ?><?php echo $Language->phrase("SrchLegend") ?></span><span class="ew-table-header-sort"><?php if ($karyawan->nik->getSort() == "ASC") { ?><i class="fa fa-sort-up"></i><?php } elseif ($karyawan->nik->getSort() == "DESC") { ?><i class="fa fa-sort-down"></i><?php } ?></span></div>
		</div></div></th>
	<?php } ?>
<?php } ?>
<?php if ($karyawan->divisi->Visible) { // divisi ?>
	<?php if ($karyawan->sortUrl($karyawan->divisi) == "") { ?>
		<th data-name="divisi" class="<?php echo $karyawan->divisi->headerCellClass() ?>"><div id="elh_karyawan_divisi" class="karyawan_divisi"><div class="ew-table-header-caption"><?php echo $karyawan->divisi->caption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="divisi" class="<?php echo $karyawan->divisi->headerCellClass() ?>"><div class="ew-pointer" onclick="ew.sort(event,'<?php echo $karyawan->SortUrl($karyawan->divisi) ?>',1);"><div id="elh_karyawan_divisi" class="karyawan_divisi">
			<div class="ew-table-header-btn"><span class="ew-table-header-caption"><?php echo $karyawan->divisi->caption() ?><?php echo $Language->phrase("SrchLegend") ?></span><span class="ew-table-header-sort"><?php if ($karyawan->divisi->getSort() == "ASC") { ?><i class="fa fa-sort-up"></i><?php } elseif ($karyawan->divisi->getSort() == "DESC") { ?><i class="fa fa-sort-down"></i><?php } ?></span></div>
		</div></div></th>
	<?php } ?>
<?php } ?>
<?php if ($karyawan->jabatan->Visible) { // jabatan ?>
	<?php if ($karyawan->sortUrl($karyawan->jabatan) == "") { ?>
		<th data-name="jabatan" class="<?php echo $karyawan->jabatan->headerCellClass() ?>"><div id="elh_karyawan_jabatan" class="karyawan_jabatan"><div class="ew-table-header-caption"><?php echo $karyawan->jabatan->caption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="jabatan" class="<?php echo $karyawan->jabatan->headerCellClass() ?>"><div class="ew-pointer" onclick="ew.sort(event,'<?php echo $karyawan->SortUrl($karyawan->jabatan) ?>',1);"><div id="elh_karyawan_jabatan" class="karyawan_jabatan">
			<div class="ew-table-header-btn"><span class="ew-table-header-caption"><?php echo $karyawan->jabatan->caption() ?><?php echo $Language->phrase("SrchLegend") ?></span><span class="ew-table-header-sort"><?php if ($karyawan->jabatan->getSort() == "ASC") { ?><i class="fa fa-sort-up"></i><?php } elseif ($karyawan->jabatan->getSort() == "DESC") { ?><i class="fa fa-sort-down"></i><?php } ?></span></div>
		</div></div></th>
	<?php } ?>
<?php } ?>
<?php if ($karyawan->atasan_langsung->Visible) { // atasan_langsung ?>
	<?php if ($karyawan->sortUrl($karyawan->atasan_langsung) == "") { ?>
		<th data-name="atasan_langsung" class="<?php echo $karyawan->atasan_langsung->headerCellClass() ?>"><div id="elh_karyawan_atasan_langsung" class="karyawan_atasan_langsung"><div class="ew-table-header-caption"><?php echo $karyawan->atasan_langsung->caption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="atasan_langsung" class="<?php echo $karyawan->atasan_langsung->headerCellClass() ?>"><div class="ew-pointer" onclick="ew.sort(event,'<?php echo $karyawan->SortUrl($karyawan->atasan_langsung) ?>',1);"><div id="elh_karyawan_atasan_langsung" class="karyawan_atasan_langsung">
			<div class="ew-table-header-btn"><span class="ew-table-header-caption"><?php echo $karyawan->atasan_langsung->caption() ?><?php echo $Language->phrase("SrchLegend") ?></span><span class="ew-table-header-sort"><?php if ($karyawan->atasan_langsung->getSort() == "ASC") { ?><i class="fa fa-sort-up"></i><?php } elseif ($karyawan->atasan_langsung->getSort() == "DESC") { ?><i class="fa fa-sort-down"></i><?php } ?></span></div>
		</div></div></th>
	<?php } ?>
<?php } ?>
<?php if ($karyawan->nomor_telepon->Visible) { // nomor_telepon ?>
	<?php if ($karyawan->sortUrl($karyawan->nomor_telepon) == "") { ?>
		<th data-name="nomor_telepon" class="<?php echo $karyawan->nomor_telepon->headerCellClass() ?>"><div id="elh_karyawan_nomor_telepon" class="karyawan_nomor_telepon"><div class="ew-table-header-caption"><?php echo $karyawan->nomor_telepon->caption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="nomor_telepon" class="<?php echo $karyawan->nomor_telepon->headerCellClass() ?>"><div class="ew-pointer" onclick="ew.sort(event,'<?php echo $karyawan->SortUrl($karyawan->nomor_telepon) ?>',1);"><div id="elh_karyawan_nomor_telepon" class="karyawan_nomor_telepon">
			<div class="ew-table-header-btn"><span class="ew-table-header-caption"><?php echo $karyawan->nomor_telepon->caption() ?><?php echo $Language->phrase("SrchLegend") ?></span><span class="ew-table-header-sort"><?php if ($karyawan->nomor_telepon->getSort() == "ASC") { ?><i class="fa fa-sort-up"></i><?php } elseif ($karyawan->nomor_telepon->getSort() == "DESC") { ?><i class="fa fa-sort-down"></i><?php } ?></span></div>
		</div></div></th>
	<?php } ?>
<?php } ?>
<?php if ($karyawan->_email->Visible) { // email ?>
	<?php if ($karyawan->sortUrl($karyawan->_email) == "") { ?>
		<th data-name="_email" class="<?php echo $karyawan->_email->headerCellClass() ?>"><div id="elh_karyawan__email" class="karyawan__email"><div class="ew-table-header-caption"><?php echo $karyawan->_email->caption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="_email" class="<?php echo $karyawan->_email->headerCellClass() ?>"><div class="ew-pointer" onclick="ew.sort(event,'<?php echo $karyawan->SortUrl($karyawan->_email) ?>',1);"><div id="elh_karyawan__email" class="karyawan__email">
			<div class="ew-table-header-btn"><span class="ew-table-header-caption"><?php echo $karyawan->_email->caption() ?><?php echo $Language->phrase("SrchLegend") ?></span><span class="ew-table-header-sort"><?php if ($karyawan->_email->getSort() == "ASC") { ?><i class="fa fa-sort-up"></i><?php } elseif ($karyawan->_email->getSort() == "DESC") { ?><i class="fa fa-sort-down"></i><?php } ?></span></div>
		</div></div></th>
	<?php } ?>
<?php } ?>
<?php

// Render list options (header, right)
$karyawan_list->ListOptions->render("header", "right");
?>
	</tr>
</thead>
<tbody>
<?php
if ($karyawan->ExportAll && $karyawan->isExport()) {
	$karyawan_list->StopRec = $karyawan_list->TotalRecs;
} else {

	// Set the last record to display
	if ($karyawan_list->TotalRecs > $karyawan_list->StartRec + $karyawan_list->DisplayRecs - 1)
		$karyawan_list->StopRec = $karyawan_list->StartRec + $karyawan_list->DisplayRecs - 1;
	else
		$karyawan_list->StopRec = $karyawan_list->TotalRecs;
}
$karyawan_list->RecCnt = $karyawan_list->StartRec - 1;
if ($karyawan_list->Recordset && !$karyawan_list->Recordset->EOF) {
	$karyawan_list->Recordset->moveFirst();
	$selectLimit = $karyawan_list->UseSelectLimit;
	if (!$selectLimit && $karyawan_list->StartRec > 1)
		$karyawan_list->Recordset->move($karyawan_list->StartRec - 1);
} elseif (!$karyawan->AllowAddDeleteRow && $karyawan_list->StopRec == 0) {
	$karyawan_list->StopRec = $karyawan->GridAddRowCount;
}

// Initialize aggregate
$karyawan->RowType = ROWTYPE_AGGREGATEINIT;
$karyawan->resetAttributes();
$karyawan_list->renderRow();
while ($karyawan_list->RecCnt < $karyawan_list->StopRec) {
	$karyawan_list->RecCnt++;
	if ($karyawan_list->RecCnt >= $karyawan_list->StartRec) {
		$karyawan_list->RowCnt++;

		// Set up key count
		$karyawan_list->KeyCount = $karyawan_list->RowIndex;

		// Init row class and style
		$karyawan->resetAttributes();
		$karyawan->CssClass = "";
		if ($karyawan->isGridAdd()) {
		} else {
			$karyawan_list->loadRowValues($karyawan_list->Recordset); // Load row values
		}
		$karyawan->RowType = ROWTYPE_VIEW; // Render view

		// Set up row id / data-rowindex
		$karyawan->RowAttrs = array_merge($karyawan->RowAttrs, array('data-rowindex'=>$karyawan_list->RowCnt, 'id'=>'r' . $karyawan_list->RowCnt . '_karyawan', 'data-rowtype'=>$karyawan->RowType));

		// Render row
		$karyawan_list->renderRow();

		// Render list options
		$karyawan_list->renderListOptions();
?>
	<tr<?php echo $karyawan->rowAttributes() ?>>
<?php

// Render list options (body, left)
$karyawan_list->ListOptions->render("body", "left", $karyawan_list->RowCnt);
?>
	<?php if ($karyawan->nama_karyawan->Visible) { // nama_karyawan ?>
		<td data-name="nama_karyawan"<?php echo $karyawan->nama_karyawan->cellAttributes() ?>>
<span id="el<?php echo $karyawan_list->RowCnt ?>_karyawan_nama_karyawan" class="karyawan_nama_karyawan">
<span<?php echo $karyawan->nama_karyawan->viewAttributes() ?>>
<?php echo $karyawan->nama_karyawan->getViewValue() ?></span>
</span>
</td>
	<?php } ?>
	<?php if ($karyawan->nik->Visible) { // nik ?>
		<td data-name="nik"<?php echo $karyawan->nik->cellAttributes() ?>>
<span id="el<?php echo $karyawan_list->RowCnt ?>_karyawan_nik" class="karyawan_nik">
<span<?php echo $karyawan->nik->viewAttributes() ?>>
<?php echo $karyawan->nik->getViewValue() ?></span>
</span>
</td>
	<?php } ?>
	<?php if ($karyawan->divisi->Visible) { // divisi ?>
		<td data-name="divisi"<?php echo $karyawan->divisi->cellAttributes() ?>>
<span id="el<?php echo $karyawan_list->RowCnt ?>_karyawan_divisi" class="karyawan_divisi">
<span<?php echo $karyawan->divisi->viewAttributes() ?>>
<?php echo $karyawan->divisi->getViewValue() ?></span>
</span>
</td>
	<?php } ?>
	<?php if ($karyawan->jabatan->Visible) { // jabatan ?>
		<td data-name="jabatan"<?php echo $karyawan->jabatan->cellAttributes() ?>>
<span id="el<?php echo $karyawan_list->RowCnt ?>_karyawan_jabatan" class="karyawan_jabatan">
<span<?php echo $karyawan->jabatan->viewAttributes() ?>>
<?php echo $karyawan->jabatan->getViewValue() ?></span>
</span>
</td>
	<?php } ?>
	<?php if ($karyawan->atasan_langsung->Visible) { // atasan_langsung ?>
		<td data-name="atasan_langsung"<?php echo $karyawan->atasan_langsung->cellAttributes() ?>>
<span id="el<?php echo $karyawan_list->RowCnt ?>_karyawan_atasan_langsung" class="karyawan_atasan_langsung">
<span<?php echo $karyawan->atasan_langsung->viewAttributes() ?>>
<?php echo $karyawan->atasan_langsung->getViewValue() ?></span>
</span>
</td>
	<?php } ?>
	<?php if ($karyawan->nomor_telepon->Visible) { // nomor_telepon ?>
		<td data-name="nomor_telepon"<?php echo $karyawan->nomor_telepon->cellAttributes() ?>>
<span id="el<?php echo $karyawan_list->RowCnt ?>_karyawan_nomor_telepon" class="karyawan_nomor_telepon">
<span<?php echo $karyawan->nomor_telepon->viewAttributes() ?>>
<?php echo $karyawan->nomor_telepon->getViewValue() ?></span>
</span>
</td>
	<?php } ?>
	<?php if ($karyawan->_email->Visible) { // email ?>
		<td data-name="_email"<?php echo $karyawan->_email->cellAttributes() ?>>
<span id="el<?php echo $karyawan_list->RowCnt ?>_karyawan__email" class="karyawan__email">
<span<?php echo $karyawan->_email->viewAttributes() ?>>
<?php echo $karyawan->_email->getViewValue() ?></span>
</span>
</td>
	<?php } ?>
<?php

// Render list options (body, right)
$karyawan_list->ListOptions->render("body", "right", $karyawan_list->RowCnt);
?>
	</tr>
<?php
	}
	if (!$karyawan->isGridAdd())
		$karyawan_list->Recordset->moveNext();
}
?>
</tbody>
</table><!-- /.ew-table -->
<?php } ?>
<?php if (!$karyawan->CurrentAction) { ?>
<input type="hidden" name="action" id="action" value="">
<?php } ?>
</div><!-- /.ew-grid-middle-panel -->
</form><!-- /.ew-list-form -->
<?php

// Close recordset
if ($karyawan_list->Recordset)
	$karyawan_list->Recordset->Close();
?>
</div><!-- /.ew-grid -->
<?php } ?>
<?php if ($karyawan_list->TotalRecs == 0 && !$karyawan->CurrentAction) { // Show other options ?>
<div class="ew-list-other-options">
<?php $karyawan_list->OtherOptions->render("body") ?>
</div>
<div class="clearfix"></div>
<?php } ?>
<?php
$karyawan_list->showPageFooter();
if (DEBUG_ENABLED)
	echo GetDebugMessage();
?>
<?php if (!$karyawan->isExport()) { ?>
<script>

// Write your table-specific startup script here
// document.write("page loaded");

</script>
<?php } ?>
<?php include_once "footer.php" ?>
<?php
$karyawan_list->terminate();
?>