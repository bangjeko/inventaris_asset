<?php
namespace PHPMaker2019\inventaris_assets;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

// Global user functions
// Page Loading event
function Page_Loading() {

	//echo "Page Loading";
}

// Page Rendering event
function Page_Rendering() {

	//echo "Page Rendering";
}

// Page Unloaded event
function Page_Unloaded() {

	//echo "Page Unloaded";
}

// Personal Data Downloading event
function PersonalData_Downloading(&$row) {

	//echo "PersonalData Downloading";
}

// Personal Data Deleted event
function PersonalData_Deleted($row) {

	//echo "PersonalData Deleted";
}

function tgl_indo($tanggal) {
	$bulan = array(
		1 => 'Januari',
		'Februari',
		'Maret',
		'April',
		'Mei',
		'Juni',
		'Juli',
		'Agustus',
		'September',
		'Oktober',
		'November',
		'Desember',
	);
	$pecahkan = explode('-', $tanggal);
	return $pecahkan[2] . ' ' . $bulan[(int) $pecahkan[1]] . ' ' . $pecahkan[0];
}
if(!function_exists("get1row")){

	function get1row($s){
		return ExecuteRow($s);
	}
}
if(!function_exists("_query")){

	function _query($sql){
		return ExecuteRows($sql);
	}	
}

function pinjam_asset($id_asset,$id_karyawan,$status,$tanggal_pinjam,$tanggal_kembali){
	_query("UPDATE assets SET status = '".$status."', current_asign  = '".$id_karyawan."', assign_date = '".$tanggal_kembali."', back_date = '".$tanggal_kembali."' WHERE id = '".$id_asset."'");
}

function update_assets($id_asset,$id_karyawan,$status,$keterangan){
	_query("UPDATE assets SET status = '".$status."', current_asign  = '".$id_karyawan."', keterangan_terbaru = '".$keterangan."', assign_date = '', back_date = ''  WHERE id = '".$id_asset."'");
}

function cetak($tablename,$id){
	$out.="onclick='cetak_bukti(\"$tablename\",\"$id\")' ";
	$out.="class='btn btn-block btn-info' >CETAK</";
	return $out;
}
?>